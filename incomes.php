<?php
require "config.php";
if( isset($_POST['newName']) ){
    $_POST['newName'] = $_POST['newName'] == "" ? "Dude" : $_POST['newName'];
    $LS->updateUser(array(
        "name" => $_POST['newName']
    ));
}
//ROLES
$UserGeting = $LS->getUser();
$userID = $UserGeting['id'];
require_once "php/user_roles.php";
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <title>INCOMES</title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/foundation.min.css"><!---->
    <link href="css/foundation-icons/foundation-icons.css" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/flat-icon/flaticon.css" rel="stylesheet" />
        <!-- data tables -->
        <link rel="stylesheet" type="text/css" href="css/csstables/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/responsive.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/buttons.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/select.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/editor.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="examples/resources/syntax/shCore.css">
        <link rel="stylesheet" type="text/css" href="examples/resources/demo.css">
    <!-- datepicker -->
    <link rel="stylesheet" type="text/css" href="css/foundation-datepicker.min.css"><!---->
    <!-- languaje 
     FOR ENGLISH OF GB, and change script down to en-GB//   <script src="js/locales/foundation-datepicker.en-GB.js"></script>-->
    <script src="js/locales/foundation-datepicker.es.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 

    <style type="text/css" class="init">
        .results {
            font-weight: bold;
            color: #33cc66;
        }
        .results_bad {
            font-weight: bold;
            color: #ff3333;
        }
        .fact-validada {
            background-color: #a3d7f7 !important;
        }

        .fact-validadanok {
            background-color: #ff9696 !important;
        }

        .fact-provisionado {
            background-color: #FFFFCE !important;
        }

        .fact-brasil {
            background-color: #f9ed93 !important;
        }

        .fact-facturado {
            background-color: #99ff99 !important;
        }

        td.highlight {
            background-color: whitesmoke !important;
        }

        .RS {
            font-size: 10px;
        }

    </style>
    <!-- JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/vendor/modernizr.js"></script>
        <!-- date picker Foundation -->
        <script type="text/javascript" language="javascript" src="js/foundation-datepicker.js"></script><!---->
        <!-- dta tables -->
        <script type="text/javascript" language="javascript" src="js/jstables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.responsive.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.buttons.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.select.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.editor.min.js"></script>
        <script type="text/javascript" language="javascript" src="examples/resources/syntax/shCore.js"></script>
        <script type="text/javascript" language="javascript" src="examples/resources/demo.js"></script>
        
        <!-- EXPORT BUTTON -->
        <script type="text/javascript" language="javascript" src="js/jstables/jszip.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/pdfmake.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/vfs_fonts.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/buttons.html5.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/buttons.print.min.js"></script>
        <script type="text/javascript" language="javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js"></script>
        

        <!-- END EXPORT BUTTON -->

        <script type="text/javascript" language="javascript" class="init">  
    var editor; // use a global for the submit and return data rendering in the examples
$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        // get value
        var searchDate = $('#min').val();
        // remove "-"
        var stringDate = searchDate.split("-");
        // concatenate array of values
        var year = stringDate[0];
        var month = stringDate[1];
        var day = stringDate[2];
        var dateSplited = year+month+day;
        // parse to float
        var min = parseFloat(dateSplited);
        // console.log(min);
        //var max = parseInt( $('#max').val(), 10 );
         // get value
        var currentDate = data[22];
        // remove "-"
        var splitedCurrentDate = currentDate.split("-");
        // concatenate array of values
        var yearCurrent = splitedCurrentDate[0];
        var monthCurrent = splitedCurrentDate[1];
        var dayCurrent = splitedCurrentDate[2];
        var currentDsplited = yearCurrent+monthCurrent+dayCurrent;
        // parse to float
        var date = parseFloat(currentDsplited);
        // var date = parseFloat( data[13] ) || 0; // use data for the date column
        // console.log(date);
        if ( ( isNaN( min ) ) ||( min == date  ) )
        {
            return true;    
        }
        return false;
    }
);
    $(document).ready(function() {
        var euroRenderer = $.fn.dataTable.render.number( ',', '.', 2, '€' ).display;
        var dollarRenderer = $.fn.dataTable.render.number( ',', '.', 2, '$' ).display;
        $.fn.dataTable.ext.errMode = 'none';
       
        editor = new $.fn.dataTable.Editor({
            ajax: "examples/php/staff.php",
            table: "#example",
            fields: [ {
                    label: "Advertiser:",
                    name: "incomes.advertiser",
                    type: "select"
                }, {
                    label: "Advertiser Social:",
                    name: "incomes.social",
                    type: "select"
                }, {
                    label: "AM:",
                    name: "incomes.am",
                    type: "select"
                },{
                    label: "Clicks:",
                    name: "incomes.clicks"
                }, {
                    label: "Conv:",
                    name: "incomes.conversion"
                }, {
                    label: "Revenue:",
                    name: "incomes.revenue"
                }, {
                    label: "Start date:",
                    name: "incomes.date",
                    type: "datetime"
                }, {
                    label: "Platform Adv:",
                    name: "incomes.platform_revenue"
                }, {
                    label: "Scrubs:",
                    name: "incomes.scrubs"
                }, {
                    label: "Hold:",
                    name: "incomes.hold"
                }, {
                    label: "Difference:",
                    name: "incomes.difference"
                }, {
                    label: "Validated:",
                    name: "incomes.validated"
                }, {
                    label: "Accumulated:",
                    name: "incomes.accumulated"
                }, {
                    label: "Status:",
                    name: "incomes.status",
                    type: "select"
                }, {
                    label: "Notas AM:",
                    name: "incomes.notas_am"
                }, {
                    label: "Notas Finance:",
                    name: "incomes.notas_finance"
                }
            ]
        } ),

    // Activate an inline edit on click of a table cell
    // or a DataTables Responsive data cell
    $('#example').on( 'click', 'tbody td:not(.child), tbody span.dtr-data', function (e) {
        // Ignore the Responsive control and checkbox columns
        // console.log("me ejecuto " + counter);
        if ( $(this).hasClass('select-checkbox') ) {
            return;
        }
        <?PHP
        if (loadRoles($userID) == 2)
        {
        ?>
        if ( $(this).hasClass('CONTFACT') ) {
            return;
        }
        if ( $(this).hasClass('AM') ) {
            editor.inline( this );
        }
        <?PHP
        }
        ?>

        <?PHP
        if (loadRoles($userID) == 1)
        {
        ?>
        if ($(this).hasClass('initial') ) {
            return;
        }
        if ($(this).hasClass('CONT') ) {
            editor.inline( this );
        }
        <?PHP
        }
        ?>

        <?PHP
        if (loadRoles($userID) == 1 || loadRoles($userID) == 2)
        {
        ?>
        if ($(this).hasClass('END') ) {
            return;
        }
        <?PHP
        }
        ?>
        <?PHP
        if (loadRoles($userID) == 5)
        {
        ?>
            editor.inline( this );
         <?PHP
        }
        ?>   
        
        
        // editor.inline( this );

    }),

//when editor is submited, check if data incomes status is one of the options we need.
    editor.on('postEdit', function(e, json, data) 
    {  
        var modifier = editor.modifier();
        var currentRow = table.row(modifier).node();
        var validated = Math.round(data.incomes.validated);

        if ((data.incomes.status == 2 || data.incomes.status == 3 || data.incomes.status == 4 || data.incomes.status == 5 || data.incomes.status == 6)&& (validated >= 100))
        {
                //get the object value of the row ID
                var myobj = table.row( editor.modifier() ).data();
                var keysArray = Object.keys(myobj);
                var valuesArray = Object.keys(myobj).map(function(k) {
                   return String(myobj[k]);
                });
                var mydata = valuesArray[keysArray.indexOf("DT_RowId")]; 
                var resId = mydata.substring(4);
                // var resultado = $('td', currentRow).eq(11).text();
                    
                $.ajax({
                    type: "POST",
                    url: "php/update_docname.php",
                    data: {
                            advid: resId
                        },
                    success: function() {
                        $('#example').DataTable().ajax.reload();
                    }
                });  
        }
    }),

 $('#example').DataTable({


    initComplete: function () {
            this.api().columns([2,11]).every( function () {
                var column = this;
                var select = $('<select><option value="">All</option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
                column.data().unique().sort().each( function ( d, j ) {
                    if(column.search() === '^'+d+'$'){
                        select.append( '<option value="'+d+'" selected="selected">'+d+'</option>' )
                    } else {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    }
                } );
            } );
        },
        "rowCallback": function( row, data, index ) {

            if ( data.incomes.status == "2" || data.incomes.status == "4" || data.incomes.status == "0" ) {  
                $("td:eq(7)", row).addClass( 'AM CONT' );  
            }
            if (data.incomes.status == "1") {  
                $("td:eq(7)", row).addClass( 'AM CONT' );  
            }
            if (data.incomes.status == "9" || data.incomes.status == "10") {  
                $("td:eq(7)", row).addClass( 'CONTFACT' );  
            }
            if (data.incomes.status == "7") {  
                $("td:eq(7)", row).addClass( 'END' );  
            }
            
            $("td:eq(2)", row).addClass( 'AM' ); 
            $("td:eq(3)", row).addClass( 'AM' );  
            $("td:eq(4)", row).addClass( 'AM' );  
            $("td:eq(5)", row).addClass( 'AM' );  
            $("td:eq(6)", row).addClass( 'AM' );  
            $("td:eq(9)", row).addClass( 'AM' );  
            $("td:eq(10)", row).addClass( 'CONT' ); 


            //switch statement background (td) color
           switch (data.incomes.status) 
           {
            case "1":
                $(row).removeClass();
                $(row).addClass( 'even' );
                break;
            case "2":
                $(row).removeClass();
                $(row).addClass( 'fact-validada' );
                break;
             case "4":
                $(row).removeClass();
                $(row).addClass( 'fact-validadanok' );
                break;
            case "7":
                $(row).removeClass();
                $(row).addClass( 'fact-facturado' );
                break;
            case "9":
                $(row).removeClass();
                $(row).addClass( 'fact-provisionado' );
                break;
            case "10":
                $(row).removeClass();
                $(row).addClass( 'fact-validadanok' );
                break;

                }
            },
    // colors depending on status
     "createdRow": function( row, data, dataIndex ) {  
        //autoset difference
        // if ( ((data.incomes.revenue.replace(/[\$,]/g, '') * 1 >= 0)&&(data.incomes.platform_revenue.replace(/[\$,]/g, '') * 1 >= 0)) ) 
        // {
            ///////////////////////////
            //                       //
            //                       //
            //     OLD Difference    //
            //                       //
            //                       //
            ///////////////////////////
        // if ( (data.incomes.platform_revenue.replace(/[\$,]/g, '') * 1 > 0) ) 
        // {
        //          var revenueValue = data.incomes.revenue.replace(/[\$,]/g, '') * 1;
        //          var platformValue = data.incomes.platform_revenue.replace(/[\$,]/g, '') * 1;
        //          var datetime = data.incomes.date;
        //          var advertiser = data.incomes.advertiser;
        //          var result = platformValue - revenueValue;
        //          // $('td', row).eq(8).html(result);

        //          $.ajax({
        //             type: "POST",
        //             url: "php/diff_update.php",
        //             data: {
        //                     advid: advertiser,
        //                     rev: revenueValue,
        //                     date: datetime,
        //                     diff: result
        //                 },
        //             success: function(data) {
        //                 var table = $('#example').DataTable();
        //                 table.draw();
        //                 // $('#example').DataTable().ajax.reload();
        //             }
        //         });

        //     }

        //      // difference negative
        //  if ( data.incomes.difference.replace(/[\$,]/g, '') * 1 < 0 ) {
        //          $('td', row).eq(5).addClass('fa fa-exclamation');
        //     }
        
        },

         "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            },
            // Total over all pages
            revenue = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            platforms = api
                .column( 6, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            differences = api
                .column( 7, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

             scrubs = api
                .column( 8, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

             holds = api
                .column( 9, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

             validates = api
                .column( 10, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            // Update footer
            $( api.column( 5 ).footer() ).html(
                '<span class="results">$'+(revenue).toFixed(2)+'</span>'
            );
            $( api.column( 6 ).footer() ).html(
                '<span class="results">$'+(platforms).toFixed(2)+'</span>'
            );
            $( api.column( 7 ).footer() ).html(
                '<span class="results_bad">$'+(differences).toFixed(2)+'</span>'
            );
            $( api.column( 8 ).footer() ).html(
                '<span class="results_bad">$'+(scrubs).toFixed(2)+'</span>'
            );
            $( api.column( 9 ).footer() ).html(
                '<span class="results_bad">$'+(holds).toFixed(2)+'</span>'
            );
            $( api.column( 10 ).footer() ).html(
                '<span class="results">$'+(validates).toFixed(2)+'</span>'
            );
        },
        
        responsive: true,
        scrollY: 550,
        bPaginate: false, 
        dom: "Bfrtip",
        columnDefs: [
        { "visible": false, "targets": 3 },
        { "visible": false, "targets": 4 },
        { "visible": false, "targets": 7 },
        { "visible": false, "targets": 9 },
        { "visible": false, "targets": 15 },// For HEADERS required
        { "visible": false, "targets": 16 },
        { "visible": false, "targets": 17 },
        { "visible": false, "targets": 18 },
        { "visible": false, "targets": 19 },
        { "visible": false, "targets": 20 },
        { "visible": false, "targets": 21 },
        { "visible": false, "targets": 22 },
        { "visible": false, "targets": 23 },
        { "visible": false, "targets": 24 },
        { "visible": false, "targets": 25 },
        { "visible": false, "targets": 26 },//For LINEAS required
        { "visible": false, "targets": 27 },
        { "visible": false, "targets": 28 },
        { "visible": false, "targets": 29 },
        { "visible": false, "targets": 30 },
        { "visible": false, "targets": 31 },// ID
        ],
        deferRender: true,
        ajax: "examples/php/staff.php",
        columns: [
            {   // Responsive control column
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            { data: null, render: function ( data, type, row ) {
                // Combine the first and last names into a single table field
                return data.advertisers.nombre+' <br><span class="RS">RS: '+data.advertisers.social+'</span>';
            } },
            // { data: "advertisers.nombre" },
            { data: "accountmanager.nombre", editField: "incomes.am"},
            { data: "incomes.clicks" },
            { data: "incomes.conversion" },
            { data: "incomes.revenue" , render: function ( data, type, row ) {
            if ( row.advertisers.money === '€' ) {
                return euroRenderer(data); } return dollarRenderer( data );} },
            { data: "incomes.platform_revenue" , render: function ( data, type, row ) {
            if ( row.advertisers.money === '€' ) {
                return euroRenderer(data); } return dollarRenderer( data );} },
            { data: "incomes.difference" , render: function ( data, type, row ) {
            if ( row.advertisers.money === '€' ) {
                return euroRenderer(data); } return dollarRenderer( data );} },
            { data: "incomes.scrubs" , render: function ( data, type, row ) {
            if ( row.advertisers.money === '€' ) {
                return euroRenderer(data); } return dollarRenderer( data );} },
            { data: "incomes.hold" , render: function ( data, type, row ) {
            if ( row.advertisers.money === '€' ) {
                return euroRenderer(data); } return dollarRenderer( data );} },
            { data: "incomes.validated", render: function ( data, type, row ) {
            if ( row.advertisers.money === '€' ) {
                return euroRenderer(data); } return dollarRenderer( data );} },
            { data: "status.name", editField: "incomes.status"},
            {   // TOTAL FACT.
                data: null, render: function ( data, type, row ) {
                 var total_fact = +data.incomes.validated + +data.incomes.accumulated;
                 if ( row.advertisers.money === '€' ) {
                return euroRenderer(total_fact); } return dollarRenderer( total_fact );
                }
            },
            { data: "incomes.notas_am" },
            { data: "incomes.notas_finance" },
            {   // SAGE TIPO EMPRESA
                data: null,
                defaultContent: 'ALC',
            },
            {   // SAGE COMPANY CODE
                data: null,
                defaultContent: '11',
            },
            {   // SAGE DATE CURRENT
                data: null, render: function ( data, type, row ) {
                    var date_exercise = data.incomes.date.substr(0, 4);
                    return date_exercise;
                }
            },
            {   // SAGE DOC. SERIE
                data: null,
                defaultContent: 'AVT',
            },
            {   // SAGE DOC NUMBER
                data: "incomes.increment"
            },
            {   // SAGE CLIENT CODE
                data: "advertisers.cod_client"
            },
            {   // SAGE DOC DATE
                data: null, render: function ( data, type, row ) {
                    //Get current day/month/year date
                    var today = new Date();
                    var dd = today.getDate();
                    var mm = today.getMonth()+1; //January is 0!
                    var yyyy = today.getFullYear();

                    if(dd<10) {
                        dd = '0'+dd
                    } 

                    if(mm<10) {
                        mm = '0'+mm
                    } 

                    today = dd + '/' + mm + '/' + yyyy;
                    return today;
                }
            },
            {   // SAGE DOC DATE START
                data: "incomes.date"
            },
            {   // SAGE DOC DATE FINISH
                data: "incomes.date"
            },
            {   // SAGE IVA INDICATOR, DEPENDING ON CURRENT NATION ES = I, OTHER = E
                data: null, render: function ( data, type, row ) {
                    if(data.advertisers.nation === 'ES'){
                        return 'I';
                    } else {return 'E';}
                }
            },
            {   // SAGE Nation
                data: "advertisers.nation"
            },
            {   // SAGE Lines Cod Arti
                data: null, render: function ( data, type, row ) {
                    var eu_countries = [ "AL", "AD", "AM", "AT", "BY", "BE", "BA", "BG", "CH", "CY", "CZ", "DE",
                        "DK", "EE", "ES", "FO", "FI", "FR", "GB", "GE", "GI", "GR", "HU", "HR",
                        "IE", "IS", "IT", "LT", "LU", "LV", "MC", "MK", "MT", "NO", "NL", "PL",
                        "PT", "RO", "RU", "SE", "SI", "SK", "SM", "TR", "UA", "VA",
                    ];
                    switch (true) {
                        case data.advertisers.nation === 'ES' && data.advertisers.money === '€':
                            return 'CPI ING PÑ';
                        break;
                        case data.advertisers.nation === 'ES' && data.advertisers.money === '$':
                            return 'CPI PÑ USD';
                        break;
                        case data.advertisers.nation !== 'ES' && (eu_countries.includes(data.advertisers.nation))==true && data.advertisers.money === '€':
                            return "CPI ING CE";
                        break;
                        case data.advertisers.nation !== 'ES' && (eu_countries.includes(data.advertisers.nation))==true && data.advertisers.money === '$':
                            return "CPI CEE USD";
                        break;
                        case (eu_countries.includes(data.advertisers.nation)) == false && data.advertisers.money=== '€':
                            return "CPI ING EX";
                        break;
                        case (eu_countries.includes(data.advertisers.nation)) == false && data.advertisers.money=== '$':
                            return "CPI EX USD";
                        break;
                    }
                }
            },
            {   // SAGE Lines Unidad
                data: null,
                defaultContent: '1',
            },
            {   // SAGE precio
                data: "incomes.validated"
            },
            {   // SAGE descripción articulo
                data: null, render: function ( data, type, row ) {
                // Combine the first and last names into a single table field
                return 'advertising campaign '+data.incomes.date;
                }
            },
            {   // SAGE precio
                data: null, render: function ( data, type, row ) {
                // Combine the first and last names into a single table field
                return 'PED(ventas '+data.incomes.date+')';
                }
            },
            { data: "incomes.id" },
            { data: null , render: function ( data, type, row ) {
            if ( row.advertisers.money === '€' ) {
                return euroRenderer(data.incomes.accumulated); } return dollarRenderer( data.incomes.accumulated);} 
            },
        ],
        order: [ 1, 'asc' ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },// Export Buttons
        buttons: [
         <?PHP
            if (loadRoles($userID) == 5 || loadRoles($userID) == 2)
            {
            ?>
            { extend: "create", editor: editor },
            { extend: "edit",   editor: editor },
            { extend: "remove", editor: editor },
            <?PHP
            }
            ?>
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copy',
                   {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    'csv',
                    'pdf',
                    'print'
                ]
            },
            <?PHP
            if (loadRoles($userID) == 5)
            {
            ?>
                'colvis',
            
                {
                
                    extend: 'excelHtml5',
                    text: 'SAGE CABECERAS',
                    exportOptions: {
                        rows: function ( idx, data, node ) {
                            var validated = Math.round(data.incomes.validated);
                            if((data.incomes.status == '2' || data.incomes.status == '3' || data.incomes.status == '4' || data.incomes.status == '5' || data.incomes.status == '6') && (validated >= 100)) {
                                return true;}
                            else {return false;}
                        },
                         columns: [ 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 ]
                    }
               
                },
                {
                    extend: 'csvHtml5',
                    text: 'SAGE LINEAS',
                    exportOptions: {
                        rows: function ( idx, data, node ) {
                            var validated = Math.round(data.incomes.validated);
                            if((data.incomes.status == '2' || data.incomes.status == '3' || data.incomes.status == '4' || data.incomes.status == '5' || data.incomes.status == '6') && (validated >= 100)) {
                                return true;}
                            else {return false;}
                        },
                         columns: [ 15, 16, 17, 18, 19, 26, 27, 28, 29, 30 ]
                    }
                },
            <?PHP
                }
            ?>
        ],
    });

 $('#min').change( function() {
        table.draw();
    } );

// highlights table
  var table = $('#example').DataTable();
    $('#example tbody')
        .on( 'mouseenter', 'td', function () {
    //old functionality
        } );
    $('#example tbody').on( 'click', 'td', function () {
    //old functionality
    } );
});
    </script>

</head>
<body>
    <div class="row full-width wrapper">
        <div class="large-12 columns">
            <div id="top-menu">
                <div class="row">
                    <div class="large-2 medium-4 small-12 columns top-part-no-padding">
                        <div class="logo-bg">
                            <img src="img/logo.png" alt="logo"/>
                            <i class="fi-list toggles" data-toggle="hide"></i>
                        </div>
                    </div>
                    <div class="large-10 medium-8 small-12 columns top-menu">
                        <div class="row">
                            <div class="large-6 medium-6 small-12 columns">
                                <div class="row">
                                    <div class="large-8 columns">
                                        <input id="Text1" type="text" class="search-text" placeholder="Search" />
                                    </div>
                                </div>
                            </div>
                            <div class="large-4 medium-6 small-12 columns text-center">
                                <div class="row">
                                    <div class="medium-3 small-3 columns">
                                        <div class="notification">
                                            <i class="fi-mail"></i>
                                            <span class="mail">4</span>
                                        </div>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                        <div class="notification">
                                            <i class="fi-megaphone"></i>
                                            <span class="megaphone">5</span>
                                        </div>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                        <!-- <img src="assets/img/user-medium.png" class="top-bar-picture"  alt="picture"> -->
                                        <span style="color:white;">
                                        <?php
                                            $details = $LS->getUser();
                                            echo $details['username'];
                                        ?>
                                        </span> 
                                       <!--  <button class="button" type="button" data-toggle="example-dropdown-bottom-right">Toggle Dropdown</button>
                                        <div class="dropdown-pane" data-position="bottom" data-alignment="right" id="example-dropdown-bottom-right" data-dropdown data-auto-focus="true">
                                          asdasdasd
                                        </div> -->
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                       <a href="logout.php"> <i class="fi-power power-off"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="no-padding">
                    <div class="large-2 medium-12 small-12 columns">
                        <!-- menu -->
                        <?PHP include'menu.php';?>
                    </div>
                </div>
                <div class="large-10 medium-12 small-12 columns light-grey-bg-pattern">
                    <div class="row">
                        <div class="large-5 columns">
                            <div class="page-name">
                                <h3 class="left">INCOMES</h3>

                            </div>
                        </div>
                        <div class="large-7 columns" style="margin-top: 10px;">
                                <!-- datepicker -->
                                <div class="row collapse date" id="dpMonths" data-date-format="yyyy-mm-dd" data-start-view="year" data-min-view="year" style="max-width:200px;">
                                <div class="small-2 columns">   
                                    <span class="prefix"><i class="fa fa-calendar"></i></span>
                                </div>
                                <div class="small-10 columns">
                                    <input type="text" size="16" value="<?=date('Y-m-d', strtotime("first day of -1 month")); ?>" id="min" name="min" >  
                                </div>
                            </div>
                            <!-- end datepicker -->
                        </div>
                             
                            

                    </div>
                    <!-- tables -->
                    <div id="staff">
                         
                        <table id="example" class="display responsive" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <!--1--><th></th>
                                    <!--2--><th>Advertiser & Comp. Name</th>
                                    <!--3--><th>Account Manager</th>
                                    <!--4--><th>clicks</th>
                                    <!--5--><th>Conv</th>
                                    <!--6--><th>Revenue</th>
                                    <!--7--><th>Platf.Adv</th>
                                    <!--8--><th>Difference</th>
                                    <!--9--><th>Scrubs</th>
                                    <!--10--><th>Hold</th>
                                    <!--11--><th>Validated</th>
                                    <!--12--><th>Status</th>
                                    <!--13--><th>Total Fact.</th>
                                    <!--14--><th>Notas(AM)</th>
                                    <!--15--><th>Notas(Finance)</th>
                                    <!--16--><th>TipoDocumentoLc</th>
                                    <!--17--><th>CodigoEmpresa</th>
                                    <!--18--><th>EjercicioDocumento</th>
                                    <!--19--><th>SerieDocumento</th>
                                    <!--20--><th>NumeroDocumento</th>
                                    <!--21--><th>CodigoCliente</th>
                                    <!--22--><th>FechaDocumento</th>
                                    <!--23--><th>FechaInicio</th>
                                    <!--24--><th>FechaFin</th>
                                    <!--25--><th>IndicadorIva</th>
                                    <!--26--><th>SiglaNacion</th>
                                    <!--27--><th>CodigoArticulo</th>
                                    <!--28--><th>Unidades</th>
                                    <!--29--><th>Precio</th>
                                    <!--30--><th>DescripcionArticulo</th>
                                    <!--31--><th>Descripción Línea</th>
                                    <!--32--><th>ID</th>
                                    <!--33--><th>Acc.</th>
                            
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <!--1--><th></th>
                                    <!--2--><th style="text-align:left">Total:</th>
                                    <!--3--><th></th>
                                    <!--4--><th></th>
                                    <!--5--><th></th>
                                    <!--6--><th></th> 
                                    <!--7--><th></th>
                                    <!--8--><th></th>
                                    <!--9--><th></th>
                                    <!--10--><th></th>
                                    <!--11--><th></th>
                                    <!--12--><th></th>
                                    <!--13--><th></th>
                                    <!--14--><th></th>
                                    <!--15--><th></th>
                                    <!--16--><th></th>
                                    <!--17--><th></th>
                                    <!--18--><th></th>
                                    <!--19--><th></th>
                                    <!--20--><th></th>
                                    <!--21--><th></th>
                                    <!--22--><th></th>
                                    <!--23--><th></th>
                                    <!--24--><th></th>
                                    <!--25--><th></th>
                                    <!--26--><th></th>
                                    <!--27--><th></th>
                                    <!--28--><th></th>
                                    <!--29--><th></th>
                                    <!--30--><th></th>
                                    <!--31--><th></th>
                                    <!--32--><th></th>
                                    <!--33--><th></th>

                                </tr>
                                <tr>
                                    <!--1--> <th></th>
                                    <!--2--><th>Advertiser & Comp. Name</th>
                                    <!--3--><th>Account Manager</th>
                                    <!--4--><th>clicks</th>
                                    <!--5--><th>Conv</th>
                                    <!--6--><th>Revenue</th>
                                    <!--7--><th>Platf.Adv</th>
                                    <!--8--><th>Difference</th>
                                    <!--9--><th>Scrubs</th>
                                    <!--10--><th>Hold</th>
                                    <!--11--><th>Validated</th>
                                    <!--12--><th>Status</th>
                                    <!--13--><th>Total Fact.</th>
                                    <!--14--><th>Notas(AM)</th>
                                    <!--15--><th>Notas(Finance)</th>
                                    <!--16--><th>TipoDocumentoLc</th>
                                    <!--17--><th>CodigoEmpresa</th>
                                    <!--18--><th>EjercicioDocumento</th>
                                    <!--19--><th>SerieDocumento</th>
                                    <!--20--><th>NumeroDocumento</th>
                                    <!--21--><th>CodigoCliente</th>
                                    <!--22--><th>FechaDocumento</th>
                                    <!--23--><th>FechaInicio</th>
                                    <!--24--><th>FechaFin</th>
                                    <!--25--><th>IndicadorIva</th>
                                    <!--26--><th>SiglaNacion</th>
                                    <!--27--><th>CodigoArticulo</th>
                                    <!--28--><th>Unidades</th>
                                    <!--29--><th>Precio</th>
                                    <!--30--><th>DescripcionArticulo</th>
                                    <!--31--><th>Descripción Línea</th>
                                    <!--32--><th>ID</th>
                                    <!--33--><th>Acc.</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- end tables -->
                </div>
            </div>
        </div>
    </div>

    
    <script src="js/menu.js"></script>
    <script>
        $(document).foundation();     
    </script>
    <script>
   $(function () {
        // datepicker limited to months
        $('#dpMonths').fdatepicker();

    });
    </script>

</body>
</html>

<!-- Localized -->