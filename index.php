<?php
require "config.php";
if( isset($_POST['newName']) ){
    $_POST['newName'] = $_POST['newName'] == "" ? "Dude" : $_POST['newName'];
    $LS->updateUser(array(
        "name" => $_POST['newName']
    ));
}
include('conect.php');
?>
<!doctype html>
<html class="no-js" lang="en">
<head> 
    <title>WELCOME TO BILLYING ADMIN</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/foundation.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/foundation-icons/foundation-icons.css" rel="stylesheet" />
    <script src="js/vendor/modernizr.js"></script>
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/flat-icon/flaticon.css" rel="stylesheet" />
    <link href="css/tree.min.css" rel="stylesheet" />
    <link href="css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="css/todos.css" rel="stylesheet" />
    
</head>
<body>
    <div class="row full-width wrapper">
        <div class="large-12 columns content-bg">
            <div id="top-menu">
                <div class="row">
                    <div class="large-2 medium-4 small-12 columns top-part-no-padding">
                        <div class="logo-bg">
                            <img src="img/logo.png" alt="logo"/>
                            <i class="fi-list toggles" data-toggle="hide"></i>
                        </div>
                    </div>
                    <div class="large-10 medium-8 small-12 columns top-menu">
                        <div class="row">
                            <div class="large-6 medium-6 small-12 columns">
                                <div class="row">
                                    <div class="large-8 columns">
                                        <input id="Text1" type="text" class="search-text" placeholder="Search" />
                                    </div>
                                </div>
                            </div>
                            <div class="large-4 medium-6 small-12 columns text-center">
                                <div class="row">
                                    <div class="medium-3 small-3 columns">
                                        <div class="notification">
                                            <!-- <i class="fi-mail"></i>
                                            <span class="mail">4</span> -->
                                        </div>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                        <div class="notification">
                                            <!-- <i class="fi-megaphone"></i>
                                            <span class="megaphone">5</span> -->
                                        </div>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                        <!--<img src="img/32.jpg" alt="picture" class="top-bar-picture" />-->
                                        Hi 
                                            <?php
                                            $details = $LS->getUser();
                                            echo $details['username'] ,"!";
                                            ?>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                       <a href="logout.php"> <i class="fi-power power-off"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="no-padding">
                    <div class="large-2 medium-12 small-12 columns">
                        <!-- menu -->
                        <?PHP include'menu.php';?>
                    </div>
                </div>
                <div class="large-10 medium-12 small-12 columns light-grey-bg-pattern">
                    <div class="row">
                        <div class="large-10 columns">
                            <div class="page-name">
                                <h3 class="left">Dashboard</h3>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div id="dashboard">
                        <div class="row">
                            <div class="large-3 medium-6 small-12 columns">
                                <div class="stats border">
                                    <div class="left">
                                        Validated  on<?PHP $currentMonth=date('Y-m-d', strtotime("first day of -1 month")); 
                                        echo date('F Y', mktime(0, 0, 0, date('m')-1, 1, date('Y')));?>
                                        <?PHP 
                                            $sqlSum = "Select SUM(validated) AS value_val FROM incomes WHERE date='$currentMonth'";
                                            $sqlValue = $con->query($sqlSum);
                                            $row = $sqlValue->fetch_assoc();
                                            $validated = $row['value_val'];
                                        ?>
                                        <h3>$<?=$validated;?></h3>
                                    </div>
                                    <i class="fi-price-tag right sales"></i>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="large-3 medium-6 small-12 columns">
                                <div class="stats border">
                                    <div class="left">
                                        Revenue on<?PHP $currentMonth=date('Y-m-d', strtotime("first day of -1 month")); 
                                        echo date('F Y', mktime(0, 0, 0, date('m')-1, 1, date('Y')));?>
                                        <?PHP 
                                            $sqlSum = "Select SUM(revenue) AS value_rev FROM incomes WHERE date='$currentMonth'";
                                            $sqlValue = $con->query($sqlSum);
                                            $row = $sqlValue->fetch_assoc();
                                            $revenue = $row['value_rev'];
                                        ?>
                                        <h3>$<?=$revenue;?></h3>
                                    </div>
                                    <i class="fa fa-dollar right orders"></i>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="large-3 medium-6 small-12 columns">
                                <div class="stats border">
                                    <div class="left">
                                        Costs validated on <?PHP $currentMonth=date('Y-m-d', strtotime("first day of -1 month")); echo date('F Y', mktime(0, 0, 0, date('m')-1, 1, date('Y')));?>
                                        <?PHP 
                                            $sqlSum = "Select SUM(validacion) AS value_valida FROM costes WHERE date='$currentMonth'";
                                            $sqlValue = $con->query($sqlSum);
                                            $row = $sqlValue->fetch_assoc();
                                            $costs = $row['value_valida'];
                                        ?>
                                        <h3>$<?=$costs;?></h3>
                                    </div>
                                    <i class="fa fa-money right profit"></i>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="large-3 medium-6 small-12 columns">
                                <div class="stats border">
                                    <div class="left">
                                        You win on <?PHP $currentMonth=date('Y-m-d', strtotime("first day of -1 month")); echo date('F Y', mktime(0, 0, 0, date('m')-1, 1, date('Y')));?>
                                        <?PHP 
                                            $math = $validated - $costs;
                                        ?>
                                        <h3>$<?=$math;?></h3>
                                    </div>
                                    <i class="fa fa-dollar right profit"></i>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="large-12 columns">
                                <div class="custom-panel">
                                    <div class="custom-panel-heading">
                                        <h4>Year progression of <?=date("Y");?></h4>
                                    </div>
                                    <!-- <div class="custom-panel-body">
                                        <div id="morris-area-chart"></div>
                                    </div> -->
                                    <div class="col-sm-6 text-center">
                                      <div id="area-chart"></div>
                                    </div>
                                </div>
                            </div>
                           <!--  <div class="large-4 columns">
                                <div class="custom-panel">
                                    <div class="custom-panel-heading">
                                        <h4>Charts</h4>
                                    </div>
                                    <div class="custom-panel-body">
                                        <div id="morris-donut-chart"></div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <br />
                       
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="js/plugins/morris/morris.js"></script>
    <script src="js/todos.js"></script>
    <script src="js/menu.js"></script>
    <script>
        $(document).foundation(); 
        var date = new Date();
        var current_year = date.getFullYear();
        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        

        function foo() {
            $.ajax({
                type: "POST",
                url: "php/get_incomes_months.php",
                data: {year:current_year},
                dataType: "json",
                success: function(data) {
                  var data_array = data.map(function(obj) {
                    return {
                      y: obj['date'],
                      a: obj['validated'],
                      b: obj['revenue'],
                      c: obj['costes']
                    };
                  });
                  fooDone(data_array);
                },
                error: function(data){
                    
                }
            });
        };

        function fooDone(data_array) {
           // alert(data_array.toSource());
            // Y= DATE YEAR + MONTH. A:TOTAL EARNED. B:TOTAL INCOME. C:TOTAL COST.
            var data = data_array;
            config = {
              data: data,
              xkey: 'y',
              ykeys: ['c', 'a', 'b'],
              labels: ['Total Cost', 'Total Validated','Total Revenue'],
              xLabelFormat: function(x) { // <--- x.getMonth() returns valid index
                var month = months[x.getMonth()];
                return month;
              },
              dateFormat: function(x) {
                var month = months[new Date(x).getMonth()];
                return month;
              },
              fillOpacity: 0,
              hideHover: 'auto',
              behaveLikeLine: true,
              resize: true,
              pointFillColors:['#ffffff'],
              pointStrokeColors: ['black'],
              lineColors:['#ee0c0c','#60f562', '#07a5bb']
              };
            config.element = 'area-chart';
            Morris.Area(config);
           };

        foo(); //call happens here
    </script>
    <script src="js/morris-demo.js"></script>
</body>
</html>

<!-- Localized -->