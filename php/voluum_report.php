<?PHP
$key=$_POST['key'];
$date_end=$_POST['date_end'];
$date_start=$_POST['date_start'];
$country=$_POST['country'];
$offer=$_POST['offer'];
$campaign_id=$_POST['campaign_id'];

get_tokken($key,$date_end, $date_start,$country,$offer,$campaign_id);


 
/********************
*
* Get Auth Tokken required each time an import is done
* @param: 
*
*********************/

function get_tokken($option,$date_end, $date_start,$country,$offer,$campaign_id)
{
	$data_string = '{"email":"platforms@advancemobile.net","password":"Megazord123"}';   

	$ch = curl_init('https://api.voluum.com/auth/session');                                                                      
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
	    'Content-Type: application/json; charset=utf-8',
	    'Accept: application/json'
	));                                                                                                                 

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
	   echo 'Error:' . curl_error($ch);
	  exit;
	}

	curl_close ($ch);
	$decodejson = array();

    // create a beauty json ^^
    $decodejson = json_decode($result,true);
    $token = $decodejson['token'];

    //Get JSon Value
    switch ($option) {
    case 'initial':
        execute_country($token,$date_end,$date_start);
        break;
    case 'country':
        execute_offer($token,$date_end,$date_start,$country);
        break;
    case 'offer':
        execute_campaign($token,$date_end,$date_start,$offer);
        break;
    case 'table':
        execute_table($token,$date_end,$date_start,$campaign_id);
        break;

}
     
     return NULL;

}

function execute_country($token,$end,$start)
{ 
	$array_country = array();

	$ch = curl_init('https://panel-api.voluum.com/report?from='.$start.'T00:00:00Z&to='.$end.'T23:00:00Z&tz=Europe%2FParis&sort=visits&direction=desc&columns=countryName&groupBy=country-code&offset=0&limit=300&include=ACTIVE&conversionTimeMode=VISIT');

	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                                                                     
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
	    'cache-control: no-cache',
	    'content-type: application/json',
	    'cwauth-token: '.$token.''
	));                                                                                                                 

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
	   echo 'Error:' . curl_error($ch);
	  exit;
	}

	curl_close ($ch);
	$decodejson = array();

    // create a beauty json ^^
    $decodejson = json_decode($result,true);
    // print_r($decodejson['rows']);

    $max = sizeof($decodejson['rows']);
	for($i = 0; $i < $max;$i++)
    {
    	if($decodejson['rows'][$i]['countryName'] != null)
    	{
	        $array_country[] = array( 
	        	'country' => $decodejson['rows'][$i]['countryName'],
	        	// 'countryCode' => $decodejson['rows'][$i]['country-code']
	    	);
	    }
    }

	echo json_encode($array_country);
	return null;
}

function execute_offer($token,$end,$start,$country)
{ 
	$array_offer = array();
	$arr = array();

	$ch = curl_init('https://panel-api.voluum.com/report?from='.$start.'T00:00:00Z&to='.$end.'T23:00:00Z&tz=Europe%2FParis&filter='.$country.'&direction=desc&columns=offerName&columns=visits&groupBy=offer&offset=0&limit=200&include=ALL&conversionTimeMode=VISIT');

	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                                                                     
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
	    'cache-control: no-cache',
	    'content-type: application/json',
	    'cwauth-token: '.$token.''
	));                                                                                                                 

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
	   echo 'Error:' . curl_error($ch);
	  exit;
	}

	curl_close ($ch);
	$decodejson = array();

    // create a beauty json ^^
    $decodejson = json_decode($result,true);
    // print_r($decodejson['rows']);

    $max = sizeof($decodejson['rows']);
	for($i = 0; $i < $max;$i++)
    {
    	if($decodejson['rows'][$i]['visits'] > 0 && !empty($decodejson['rows'][$i]['offerName']))
    	{
    		$needle = $decodejson['rows'][$i]['offerName'];
			$whatIWant = substr($needle, strpos($needle, " - ") + 1);    
			$whatINeed = substr($whatIWant, strpos($whatIWant, " - ") + 1);   
			$whatReallyIs = substr($whatINeed, strpos($whatINeed, " - ") + 1); 
	        $array_offer[] = array( 
	        	'offer' => $whatReallyIs,
	        	 // 'visits' => $decodejson['rows'][$i]['visits']
	    	);
	    }

    }

	echo json_encode($array_offer);
	return null;
}

function execute_campaign($token,$end,$start,$offer)
{ 
	$array_campaign = array();

	$offer_spaces = str_replace(' ', '+', strtolower($offer));

	$ch = curl_init('https://panel-api.voluum.com/report?from='.$start.'T00:00:00Z&to='.$end.'T23:00:00Z&tz=Europe%2FParis&filter='.$offer_spaces.'&direction=desc&columns=campaignName&columns=campaignId&groupBy=campaign&offset=0&limit=100&include=ALL&conversionTimeMode=VISIT');
	

	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                                                                     
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
	    'cache-control: no-cache',
	    'content-type: application/json',
	    'cwauth-token: '.$token.''
	));                                                                                                                 

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
	   echo 'Error:' . curl_error($ch);
	  exit;
	}

	curl_close ($ch);
	$decodejson = array();

    // create a beauty json ^^
    $decodejson = json_decode($result,true);
    // print_r($decodejson['rows']);

    $max = sizeof($decodejson['rows']);
	for($i = 0; $i < $max;$i++)
    {
    	// if($decodejson['rows'][$i]['visits'] > 0)
    	// {
	        $array_campaign[] = array( 
	        	'campaign' => $decodejson['rows'][$i]['campaignName'],
	        	'campaignId' => $decodejson['rows'][$i]['campaignId']
	    	);
	    // }
    }

	echo json_encode($array_campaign);
	return null;
}

function execute_table($token,$end,$start,$campaign_id)
{ 
	$array_table = array();

	$ch = curl_init('https://panel-api.voluum.com/report?from='.$start.'T00:00:00Z&to='.$end.'T23:00:00Z&tz=Europe%2FParis&sort=visits&direction=desc&columns=day&columns=visits&columns=conversions&columns=revenue&columns=cost&columns=profit&columns=cv&columns=roi&columns=epc&columns=ap&groupBy=day&offset=0&limit=100&include=ACTIVE&conversionTimeMode=VISIT&filter1=campaign&filter1Value='.$campaign_id.'');

	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                                                                     
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
	    'cache-control: no-cache',
	    'content-type: application/json',
	    'cwauth-token: '.$token.''
	));                                                                                                                 

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
	   echo 'Error:' . curl_error($ch);
	  exit;
	}

	curl_close ($ch);
	$decodejson = array();

    // create a beauty json ^^
    $decodejson = json_decode($result,true);
    // print_r($decodejson['rows']);

    $max = sizeof($decodejson['rows']);
	for($i = 0; $i < $max;$i++)
    {
    	// if($decodejson['rows'][$i]['visits'] > 0)
    	// {
	        $array_table[] = array( 
	        	'date' => $decodejson['rows'][$i]['day'],
	        	'visits' => $decodejson['rows'][$i]['visits'],
	        	'conversions' => $decodejson['rows'][$i]['conversions'],
	        	'revenue' => $decodejson['rows'][$i]['revenue'],
	        	'cost' => $decodejson['rows'][$i]['cost'],
	        	'profit' => $decodejson['rows'][$i]['profit'],
	        	'cv' => $decodejson['rows'][$i]['cv'],
	        	'roi' => $decodejson['rows'][$i]['roi'],
	        	'epc' => $decodejson['rows'][$i]['epc'],
	        	'ap' => $decodejson['rows'][$i]['ap']

	    	);
	    // }
    }

	echo json_encode($array_table);
	return null;
}



?>