<?php
include('../conect.php');
$year = $_POST['year'];

$query ="SELECT 
	SUM(union_tb.validated) AS validated, 
	SUM(union_tb.revenue) AS revenue, 
	SUM(union_tb.costes) AS costes, 
	union_tb.date
FROM (
        SELECT 
        	SUM(validated) AS 'validated', 
        	'0' AS 'costes',  
        	SUM(revenue) AS 'revenue', 
        	DATE_FORMAT(DATE, '%Y-%m') AS 'date'
        FROM incomes
            WHERE YEAR(DATE) = $year
        GROUP BY(DATE_FORMAT(DATE, '%Y-%m'))
    UNION
        SELECT 
	        '0' AS 'validated',  
	        SUM(validacion) AS 'costes', 
	        '0' AS 'revenue',  
	        DATE_FORMAT(DATE, '%Y-%m') AS 'date'
        FROM costes
        	WHERE YEAR(DATE) = $year
        GROUP BY(DATE_FORMAT(DATE, '%Y-%m'))
) 
AS union_tb 
WHERE 1 
GROUP BY union_tb.date;";

$result = mysqli_query($con, $query);
$sum_validated = array();
while ($row = mysqli_fetch_assoc($result)) {
  array_push($sum_validated, $row);
}

echo json_encode($sum_validated);
mysqli_close($con);

?>