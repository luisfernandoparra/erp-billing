<?php
require "config.php";
if( isset($_POST['newName']) ){
    $_POST['newName'] = $_POST['newName'] == "" ? "Dude" : $_POST['newName'];
    $LS->updateUser(array(
        "name" => $_POST['newName']
    ));
}
//ROLES
$UserGeting = $LS->getUser();
$userID = $UserGeting['id'];
require_once "php/user_roles.php";
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <title>CAMPAIGN MONTH</title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/foundation.min.css"><!---->
    <link href="css/foundation-icons/foundation-icons.css" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/flat-icon/flaticon.css" rel="stylesheet" />

        <!-- data tables -->
        <link rel="stylesheet" type="text/css" href="css/csstables/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/responsive.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/buttons.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/select.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/editor.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="examples/resources/syntax/shCore.css">
        <link rel="stylesheet" type="text/css" href="examples/resources/demo.css">
    <!-- datepicker -->
    <link rel="stylesheet" type="text/css" href="css/foundation-datepicker.min.css"><!---->
    <!-- languaje 
     FOR ENGLISH OF GB, and change script down to en-GB//   <script src="js/locales/foundation-datepicker.en-GB.js"></script>-->
    <script src="js/locales/foundation-datepicker.es.js"></script>
 

    <style type="text/css" class="init">
        .results {
            font-weight: bold;
            color: #33cc66;
        }
        .results_bad {
            font-weight: bold;
            color: #ff3333;
        }
        .fact-validada {
            background-color: #a3d7f7 !important;
        }
        .fact-validadanok {
            background-color: #f98b8b !important;
        }
        .fact-provisionado {
            background-color: #FFFFCE !important;
        }
        .fact-brasil {
            background-color: #f9ed93 !important;
        }
        .fact-facturado {
            background-color: #99ff99 !important;
        }
        td.highlight {
            background-color: whitesmoke !important;
        }
        .RS {
            font-size: 10px;
        }
        
    </style>
    <!-- JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/vendor/modernizr.js"></script>
        <!-- date picker Foundation -->
        <script type="text/javascript" language="javascript" src="js/foundation-datepicker.js"></script><!---->
        <!-- dta tables -->
        <script type="text/javascript" language="javascript" src="js/jstables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.responsive.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.buttons.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.select.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.editor.min.js"></script>
        <script type="text/javascript" language="javascript" src="examples/resources/syntax/shCore.js"></script>
        <script type="text/javascript" language="javascript" src="examples/resources/demo.js"></script>
        
        <!-- EXPORT BUTTON -->
        <script type="text/javascript" language="javascript" src="js/jstables/jszip.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/pdfmake.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/vfs_fonts.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/buttons.html5.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/buttons.print.min.js"></script>
        <!-- END EXPORT BUTTON -->

        <script type="text/javascript" language="javascript" class="init">  
    var editor; // use a global for the submit and return data rendering in the examples
$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        // get value
        var searchDate = $('#min').val();
        //remove "-"
        var stringDate = searchDate.split("-");
        // concatenate array of values
        var year = stringDate[0];
        var month = stringDate[1];
        //var day = stringDate[2];
        var dateSplited = year+month;
        // parse to float
        var min = parseFloat(dateSplited);
        // console.log(min);
        //var max = parseInt( $('#max').val(), 10 );
         // get value
        var currentDate = data[1] || 0;
        // console.log(currentDate);
        // remove "-"
        var splitedCurrentDate = currentDate.split("-");
        // concatenate array of values
        var yearCurrent = splitedCurrentDate[0];
        var monthCurrent = splitedCurrentDate[1];
        // var dayCurrent = splitedCurrentDate[2];
        var currentDsplited = yearCurrent+monthCurrent;
        // parse to float
        var date = parseFloat(currentDsplited);
        // var date = parseFloat( data[13] ) || 0; // use data for the date column
        // console.log(date);
        if ( ( isNaN( min ) ) ||( min == date  ) )
        {
            return true;    
        }
        return false;
     }
);
    $(document).ready(function() {
        var euroRenderer = $.fn.dataTable.render.number( ',', '.', 2, '€' ).display;
        var dollarRenderer = $.fn.dataTable.render.number( ',', '.', 2, '$' ).display;
        //data edition
        editor = new $.fn.dataTable.Editor({
            ajax: "examples/php/campaign_month_db.php",
            table: "#example",
            fields: [ 
                    {
                    label: "Current Month:",
                    name: "campaigns_month_close.date",
                    type: "datetime"
                }, {
                    label: "Client:",
                    name: "campaigns_month_close.client"
                }, {
                    label: "Int. Mail:",
                    name: "campaigns_month_close.internal_mail",
                    def: "0.00"
                }, {
                    label: "Ext. Mail:",
                    name: "campaigns_month_close.external_mail",
                    def: "0.00"
                },{
                    label: "Inv. Fb:",
                    name: "campaigns_month_close.invest_fb",
                    def: "0.00"
                }, {
                    label: "Fee Fb:",
                    name: "campaigns_month_close.fee_fb",
                    def: "0.00"
                }, {
                    label: "Setup Fb:",
                    name: "campaigns_month_close.setup_fb",
                    def: "0.00"
                }, {
                    label: "Inv. AdWords:",
                    name: "campaigns_month_close.invest_adwords",
                    def: "0.00"
                }, {
                    label: "Fee AdWords:",
                    name: "campaigns_month_close.fee_adwords",
                    def: "0.00"
                }, {
                    label: "Setup AdWords:",
                    name: "campaigns_month_close.setup_adwords",
                    def: "0.00"
                }, {
                    label: "Inv. RTB:",
                    name: "campaigns_month_close.invest_rtb",
                    def: "0.00"
                }, {
                    label: "Fee RTB:",
                    name: "campaigns_month_close.fee_rtb",
                    def: "0.00"
                }, {
                    label: "Setup RTB:",
                    name: "campaigns_month_close.setup_rtb",
                    def: "0.00"
                }, {
                    label: "Inv. Amazon:",
                    name: "campaigns_month_close.invest_amazon",
                    def: "0.00"
                }, {
                    label: "Fee Amazon:",
                    name: "campaigns_month_close.fee_amazon",
                    def: "0.00"
                }, {
                    label: "Setup Amazon:",
                    name: "campaigns_month_close.setup_amazon",
                    def: "0.00"
                }, {
                    label: "Tracking:",
                    name: "campaigns_month_close.tracking",
                    def: "0.00"
                }, {
                    label: "Design:",
                    name: "campaigns_month_close.design",
                    def: "0.00"
                }, {
                    label: "Contents:",
                    name: "campaigns_month_close.contents",
                    def: "0.00"
                }
            ]
        } ),

    // Activate an inline edit on click of a table cell
    // or a DataTables Responsive data cell
    $('#example').on( 'click', 'tbody td:not(.child), tbody span.dtr-data', function (e) {
        // Ignore the Responsive control and checkbox columns
        if ( $(this).hasClass( 'control' ) || $(this).hasClass('select-checkbox') ) {
            return;
        }

         <?PHP
        if (loadRoles($userID) == 5)
        {
        ?>
            editor.inline( this );
        <?PHP
        }
        ?>

        
    } ),



 $('#example').DataTable({
    //update td color depending on valor
    "rowCallback": function( row, data, index ) {
            // STATUS ADVERTISER
            // if ( data.costes.status == "1" ) {
            //     $("td:eq(11)", row).removeClass( 'fact-validadanok' ); 
            //     $("td:eq(11)", row).addClass( 'fact-facturado' ); 
            // } 
            // if ( data.costes.status == "2" ) {  
            //     $("td:eq(11)", row).removeClass( 'fact-facturado' ); 
            //     $("td:eq(11)", row).addClass( 'fact-validadanok' );  
            // }

            

            
        },

         "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 : 
                    typeof i === 'number' ?
                        i : 0;
            },
            // Total over all pages
            IntMail = api
                .column( 3, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            ExtMail = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            InvFb = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
             FeeFb = api
                .column( 6, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

             SetupFb = api
                .column( 7, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
                
             InvAdW = api
                .column( 8, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

             FeeAdW = api
                .column( 9, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

             SetupAdW = api
                .column( 10, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );    
            InvRTB = api
                .column( 11, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 ); 
            FeeRTB = api
                .column( 12, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );  
            SetupRTB = api
                .column( 13, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );   
            InvAmazon = api
                .column( 14, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 ); 
            FeeAmazon = api
                .column( 15, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 ); 
            SetupAmazon = api
                .column( 16, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );  
            tracking = api
                .column( 17, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );   
            design = api
                .column( 18, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );   
            content = api
                .column( 19, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );            


            // Update footer
            $( api.column( 3 ).footer() ).html(
                '<span class="results">'+(IntMail).toFixed(2)+'</span>'
            );
            $( api.column( 4 ).footer() ).html(
                '<span class="results">'+(ExtMail).toFixed(2)+'</span>'
            );
            $( api.column( 5 ).footer() ).html(
                '<span class="results">'+(InvFb).toFixed(2)+'</span>'
            );
            $( api.column( 6 ).footer() ).html(
                '<span class="results">'+(FeeFb).toFixed(2)+'</span>'
            );
            $( api.column( 7 ).footer() ).html(
                '<span class="results">'+(SetupFb).toFixed(2)+'</span>'
            );
            $( api.column( 8 ).footer() ).html(
                '<span class="results">'+(InvAdW).toFixed(2)+'</span>'
            );
            $( api.column( 9 ).footer() ).html(
                '<span class="results">'+(FeeAdW).toFixed(2)+'</span>'
            );
            $( api.column( 10 ).footer() ).html(
                '<span class="results">'+(SetupAdW).toFixed(2)+'</span>'
            );
            $( api.column( 11 ).footer() ).html(
                '<span class="results">'+(InvRTB).toFixed(2)+'</span>'
            );
            $( api.column( 12 ).footer() ).html(
                '<span class="results">'+(FeeRTB).toFixed(2)+'</span>'
            );
            $( api.column( 13 ).footer() ).html(
                '<span class="results">'+(SetupRTB).toFixed(2)+'</span>'
            );
            $( api.column( 14 ).footer() ).html(
                '<span class="results">'+(InvAmazon).toFixed(2)+'</span>'
            );
            $( api.column( 15 ).footer() ).html(
                '<span class="results">'+(FeeAmazon).toFixed(2)+'</span>'
            );
            $( api.column( 16 ).footer() ).html(
                '<span class="results">'+(SetupAmazon).toFixed(2)+'</span>'
            );
            $( api.column( 17 ).footer() ).html(
                '<span class="results">'+(tracking).toFixed(2)+'</span>'
            );
            $( api.column( 18 ).footer() ).html(
                '<span class="results">'+(design).toFixed(2)+'</span>'
            );
            $( api.column( 19 ).footer() ).html(
                '<span class="results">'+(content).toFixed(2)+'</span>'
            );
            
        },
         
        responsive: true,
        scrollY: 550,
        bPaginate: false, 
        dom: "Bfrtip",
        deferRender: true,
        ajax: "examples/php/campaign_month_db.php",
        columns: [

            {   // Responsive control column
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
        // {   // costs discrepancy.
            //     data: function ( data, type, row ) {
            //         if(data.campaigns_month_close.plataff == 0)
            //         {return data.campaigns_month_close.plataff; }
            //         else {
            //             var discrepancy_total = +data.campaigns_month_close.plataff- + data.campaigns_month_close.totaldollar ;
            //             return discrepancy_total;
            //         }
            //     }
            // },
            // { data: "campaigns_month_close.validacion", render: function ( data, type, row ) {
            // if ( row.campaigns_month_close.validacion === '€' ) {
            //     return euroRenderer(data); } return dollarRenderer( data );} },
            { data: "campaigns_month_close.date" },
            { data: "campaigns_month_close.client" },
            { data: "campaigns_month_close.internal_mail" , render: $.fn.dataTable.render.number( ',', '.', 2, '€' ) },
            { data: "campaigns_month_close.external_mail" , render: $.fn.dataTable.render.number( ',', '.', 2, '€' ) },
            { data: "campaigns_month_close.invest_fb" , render: $.fn.dataTable.render.number( ',', '.', 2, '€' ) },
            { data: "campaigns_month_close.fee_fb" , render: $.fn.dataTable.render.number( ',', '.', 2, '€' ) },
            { data: "campaigns_month_close.setup_fb" , render: $.fn.dataTable.render.number( ',', '.', 2, '€' ) },
            { data: "campaigns_month_close.invest_adwords" , render: $.fn.dataTable.render.number( ',', '.', 2, '€' ) },
            { data: "campaigns_month_close.fee_adwords" , render: $.fn.dataTable.render.number( ',', '.', 2, '€' ) },
            { data: "campaigns_month_close.setup_adwords" , render: $.fn.dataTable.render.number( ',', '.', 2, '€' ) },
            { data: "campaigns_month_close.invest_rtb" , render: $.fn.dataTable.render.number( ',', '.', 2, '€' ) },
            { data: "campaigns_month_close.fee_rtb" , render: $.fn.dataTable.render.number( ',', '.', 2, '€' ) },
            { data: "campaigns_month_close.setup_rtb" , render: $.fn.dataTable.render.number( ',', '.', 2, '€' ) },
            { data: "campaigns_month_close.invest_amazon" , render: $.fn.dataTable.render.number( ',', '.', 2, '€' ) },
            { data: "campaigns_month_close.fee_amazon" , render: $.fn.dataTable.render.number( ',', '.', 2, '€' ) },
            { data: "campaigns_month_close.setup_amazon" , render: $.fn.dataTable.render.number( ',', '.', 2, '€' ) },
            { data: "campaigns_month_close.tracking" , render: $.fn.dataTable.render.number( ',', '.', 2, '€' ) },
            { data: "campaigns_month_close.design" , render: $.fn.dataTable.render.number( ',', '.', 2, '€' ) },
            { data: "campaigns_month_close.contents" , render: $.fn.dataTable.render.number( ',', '.', 2, '€' ) },

        ],
        order: [ 2, 'asc' ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        buttons: [
        <?PHP
            if (loadRoles($userID) == 5)
            {
            ?>
            { extend: "create", editor: editor },
            { extend: "edit",   editor: editor },
            { extend: "remove", editor: editor },
            <?PHP
            }
            ?>
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ]
            }
        ],
        
         
    });
            //call draw table when new date is selected
         $('#min').change( function() {
                table.draw();
            } );
        // highlights table
          var table = $('#example').DataTable();
        $('#example tbody')
            .on( 'mouseenter', 'td', function () {
                //deprecated
            } );

        $('#example tbody').on( 'click', 'td', function () {
        // deprecated
        } )
});
    </script>

</head>
<body>
    <div class="row full-width wrapper">
        <div class="large-12 columns">
            <div id="top-menu">
                <div class="row">
                    <div class="large-2 medium-4 small-12 columns top-part-no-padding">
                        <div class="logo-bg">
                            <img src="img/logo.png" alt="logo"/>
                            <i class="fi-list toggles" data-toggle="hide"></i>
                        </div>
                    </div>
                    <div class="large-10 medium-8 small-12 columns top-menu">
                        <div class="row">
                            <div class="large-6 medium-6 small-12 columns">
                                <div class="row">
                                    <div class="large-8 columns">
                                        <input id="Text1" type="text" class="search-text" placeholder="Search" />
                                    </div>
                                </div>
                            </div>
                            <div class="large-4 medium-6 small-12 columns text-center">
                                <div class="row">
                                    <div class="medium-3 small-3 columns">
                                        <div class="notification">
                                            <i class="fi-mail"></i>
                                            <span class="mail">4</span>
                                        </div>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                        <div class="notification">
                                            <i class="fi-megaphone"></i>
                                            <span class="megaphone">5</span>
                                        </div>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                        <!--<img src="img/32.jpg" alt="picture" class="top-bar-picture" />-->
                                        Hi 
                                            <?php
                                            $details = $LS->getUser();
                                            echo $details['username'] ,"!";
                                            ?>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                       <a href="logout.php"> <i class="fi-power power-off"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="no-padding">
                    <div class="large-2 medium-12 small-12 columns">
                        <!-- menu -->
                        <?PHP include'menu.php';?>
                    </div>
                </div>
                <div class="large-10 medium-12 small-12 columns light-grey-bg-pattern">
                    <div class="row">
                        <div class="large-5 columns">
                            <div class="page-name">
                                <h3 class="left">CAMPAIGN MONTH</h3>
                            </div>
                        </div>
                        <div class="large-7 columns" style="margin-top: 10px;">
                                <!-- datepicker -->
                                        <div class="row collapse date" id="dpMonths" data-date-format="yyyy-mm-dd" data-start-view="year" data-min-view="year" style="max-width:200px;">
                                <div class="small-2 columns">   
                                    <span class="prefix"><i class="fa fa-calendar"></i></span>
                                </div>
                                <div class="small-10 columns">
                                    <input type="text" size="16" value="<?=date('Y-m-d', strtotime("first day of -1 month")); ?>" id="min" name="min" >  
                                </div>
                            </div>
                            <!-- end datepicker -->
                        </div>
                             
                            

                    </div>
                    <!-- tables -->
                    <div id="staff">
                         
                        <table id="example" class="display responsive" cellspacing="0" width="100%">
                            <thead>
                            <tr>
				                <th colspan="3"></th>
				                <th colspan="2">Mail</th>
				                <th colspan="3">Facebook</th>
				                <th colspan="3">AdWords</th>
				                <th colspan="3">RTB</th>
				                <th colspan="3">Amazon</th>
				                <th colspan="3">Others</th>
				            </tr>
                                <tr>
                                    <!-- 0 --><th></th>
                                    <!-- 1 --><th></th>
                                    <!-- 2 --><th>Client</th>
                                    <!-- 3 --><th>Int.</th>
                                    <!-- 4 --><th>Ext.</th>
                                    <!-- 5 --><th>Inv.</th>
                                    <!-- 6 --><th>Fee</th>
                                    <!-- 7 --><th>Setup</th>
                                    <!-- 8 --><th>Inv.</th>
                                    <!-- 9 --><th>Fee</th>
                                    <!-- 10 --><th>Setup</th>
                                    <!-- 11 --><th>Inv.</th>
                                    <!-- 12 --><th>Fee</th>
                                    <!-- 13 --><th>Setup</th>
                                    <!-- 14 --><th>Inv.</th>
                                    <!-- 15 --><th>Fee</th>
                                    <!-- 16 --><th>Setup</th>
                                    <!-- 17 --><th>Tracking</th>
                                    <!-- 18 --><th>Design</th>
                                    <!-- 19 --><th>Content</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <!-- 0 --><th></th>
                                    <!-- 1 --><th></th>
                                    <!-- 2 --><th style="text-align:left">Total:</th>
                                    <!-- 3 --><th></th>
                                    <!-- 4 --><th></th>
                                    <!-- 5 --><th></th>
                                    <!-- 6 --><th></th>
                                    <!-- 7 --><th></th>
                                    <!-- 8 --><th></th>
                                    <!-- 9 --><th></th>
                                    <!-- 10 --><th></th>
                                    <!-- 11 --><th></th>
                                    <!-- 12 --><th></th>
                                    <!-- 13 --><th></th>
                                    <!-- 14 --><th></th>
                                    <!-- 15 --><th></th>
                                    <!-- 16 --><th></th>
                                    <!-- 17 --><th></th>
                                    <!-- 18 --><th></th>
                                    <!-- 19 --><th></th>
                                </tr>
                                <tr>
                                    <!-- 0 --><th></th>
                                    <!-- 1 --><th></th>
                                    <!-- 2 --><th>Client</th>
                                    <!-- 3 --><th>Int. Mail</th>
                                    <!-- 4 --><th>Ext. Mail</th>
                                    <!-- 5 --><th>Inv. Fb</th>
                                    <!-- 6 --><th>Fee Fb</th>
                                    <!-- 7 --><th>Setup Fb</th>
                                    <!-- 8 --><th>Inv. AdW</th>
                                    <!-- 9 --><th>Fee AdW</th>
                                    <!-- 10 --><th>Setup AdW</th>
                                    <!-- 11 --><th>Inv. RTB</th>
                                    <!-- 12 --><th>Fee RTB</th>
                                    <!-- 13 --><th>Setup RTB</th>
                                    <!-- 14 --><th>Inv.Amaz</th>
                                    <!-- 15 --><th>Fee Amaz</th>
                                    <!-- 16 --><th>Setup Amaz</th>
                                    <!-- 17 --><th>Tracking</th>
                                    <!-- 18 --><th>Design</th>
                                    <!-- 19 --><th>Content</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- end tables -->
                </div>
            </div>
        </div>
    </div>

    
    <script src="js/menu.js"></script>
    <script>
        $(document).foundation();     
    </script>
    <script>
   $(function () {
        // datepicker limited to months
        $('#dpMonths').fdatepicker();

    });
    </script>

</body>
</html>

<!-- Localized -->