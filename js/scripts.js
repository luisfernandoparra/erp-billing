$(document).ready(function() {

 $("#dpd2").change(function(){
        var url = "php/voluum_report.php"; 
            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: {key:'initial', date_end: $('#dpd2').val(),date_start: $('#dpd1').val()}, 
                beforeSend: function(){
                    $('#loader').html("<img with='200px' height='200px' src='img/ajax-loader.gif' /> Loading...");
                },
                success: function(data) 
                {
                    $('#loader').html("");
                    // $('#country').find('option').remove();
                    $('#country')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="0" selected>SELECT...</option>')
                        .val('0');
                    $('#offer')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="0" selected>SELECT...</option>')
                        .val('0');
                    $('#campaign')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="0" selected>SELECT...</option>')
                        .val('0');
                    $.each(data, function(k, v) {
                        $('<option>').val(v.country).text(v.country).appendTo('#country');
                    });
                }
        });
    });
     $("#country").change(function(){
        var url = "php/voluum_report.php"; 
            $.ajax({
               type: "POST",
               dataType: "json",
               url: url,
               data: {key:'country', date_end: $('#dpd2').val(),date_start: $('#dpd1').val(),country:$('#country option:selected').val()}, 
               beforeSend: function(){
                    $('#loader').html("<img with='200px' height='200px' src='img/ajax-loader.gif' /> Loading...");
                },
               success: function(data) 
               {
                    $('#loader').html("");
                     $('#offer')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="0" selected>SELECT...</option>')
                        .val('0');
                    $('#campaign')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="0" selected>SELECT...</option>')
                        .val('0');
                    $.each(data, function(k, v) {
                        $('<option>').val(v.offer).text(v.offer).appendTo('#offer');
                    });
               }
        });
    });
    $("#offer").change(function(){
        var url = "php/voluum_report.php"; 
            $.ajax({
               type: "POST",
               dataType: "json",
               url: url,
               data: {key:'offer', date_end: $('#dpd2').val(),date_start: $('#dpd1').val(),offer:$('#offer option:selected').val()}, 
               beforeSend: function(){
                    $('#loader').html("<img with='200px' height='200px' src='img/ajax-loader.gif' /> Loading...");
                },
               success: function(data) 
               {
                    $('#loader').html("");
                     $('#campaign')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="0" selected>SELECT...</option>')
                        .val('0');
                    $.each(data, function(k, v) {
                        $('<option>').val(v.campaignId).text(v.campaign).appendTo('#campaign');
                    });
               }
        });
    });
    $(".sending").click(function(){
        var url = "php/voluum_report.php"; 
        var html = "";
            $.ajax({
               type: "POST",
               dataType: "json",
               url: url,
               data: {key:'table', date_end: $('#dpd2').val(),date_start: $('#dpd1').val(),campaign_id:$('#campaign option:selected').val()}, 
               beforeSend: function(){
                    $('#loader').html("<img with='200px' height='200px' src='img/ajax-loader.gif' /> Loading...");
                },
               success: function(data) 
               {
                    $('#loader').html("");
                    $('#table_information').html("");
                       
                    html+="<tr><td></td><td>DATE</td><td>VISITS</td><td>CONV.</td><td>REV</td><td>COST</td><td>PROFIT</td><td>CV</td><td>ROI</td><td>EPC</td><td>AP</td></tr>";
                    var sum_visits = 0;
                    var sum_conv = 0;
                    var sum_rev = 0;
                    var sum_cost = 0;
                    var sum_profit = 0;
                    var sum_cv = 0;
                    var sum_roi = 0;
                    var sum_epc = 0;
                    var sum_ap = 0;
                    for(var i =0;i < data.length;i++)
                        {
                            var item = data[i];

                            sum_visits+=item.visits;
                            sum_conv+=item.conversions;
                            sum_rev+=item.revenue;
                            sum_cost+=item.cost;
                            sum_profit+=item.profit;
                            sum_cv+=item.cv;
                            sum_roi+=item.roi;
                            sum_epc+=item.epc;
                            sum_ap+=item.ap;

                            if(item.roi > 0.1)
                            {
                              html +="<tr>";
                              html +="<td><i class='fa fa-long-arrow-up' aria-hidden='true' style='color:green;'></i></td><td>"+item.date+"</td><td>"+item.visits+"</td><td>"+item.conversions+"</td><td>"+item.revenue+"</td><td>"+item.cost+"</td><td>"+item.profit+"</td><td>"+item.cv+"</td><td>"+item.roi+"</td><td>"+item.epc+"</td><td>"+item.ap+"</td>";
                              html +="</tr>";
                            }
                            else if(item.roi < 0)
                            {
                              html +="<tr>";
                              html +="<td><i class='fa fa-long-arrow-down' aria-hidden='true' style='color:red;'></i></td><td>"+item.date+"</td><td>"+item.visits+"</td><td>"+item.conversions+"</td><td>"+item.revenue+"</td><td>"+item.cost+"</td><td>"+item.profit+"</td><td>"+item.cv+"</td><td>"+item.roi+"</td><td>"+item.epc+"</td><td>"+item.ap+"</td>";
                              html +="</tr>";
                            }
                            else
                            {
                              html +="<tr>";
                              html +="<td><i class='fa fa-minus' aria-hidden='true'></i></td><td>"+item.date+"</td><td>"+item.visits+"</td><td>"+item.conversions+"</td><td>"+item.revenue+"</td><td>"+item.cost+"</td><td>"+item.profit+"</td><td>"+item.cv+"</td><td>"+item.roi+"</td><td>"+item.epc+"</td><td>"+item.ap+"</td>";
                              html +="</tr>";
                            }
                            
                        }
                        html+="<tr><td></td><td>Total</td><td>"+sum_visits+"</td><td>"+sum_conv+"</td><td>"+sum_rev+" $</td><td>"+sum_cost+" $</td><td>"+sum_profit+" $</td><td>"+sum_cv+" %</td><td>"+sum_roi+" %</td><td>"+sum_epc+" $</td><td>"+sum_ap+" $</td></tr>";
                         html+="<tr><td></td><td>DATE</td><td>VISITS</td><td>CONV.</td><td>REV</td><td>COST</td><td>PROFIT</td><td>CV</td><td>ROI</td><td>EPC</td><td>AP</td></tr>";
                    
                    $("#table_information").html(html);
                  
               }
        });
    });

 //end of document
    });