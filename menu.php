<?PHP $current_page = $_SERVER['PHP_SELF'];
//ROLES
$UserGeting = $LS->getUser();
$userID = $UserGeting['id'];
require_once "php/user_roles.php";
?>

<ul class="side-nav">
    <li <?php echo $current_page=='/index.php' ? ' class="active"' : ''; ?> >
    	<a href="index.php"><i style="padding-right: 8px;" class="fa fa-tachometer fa-2x"></i>Dashboard</a>
    </li>
    <?php
        if (loadRoles($userID) == 5)
        {
        ?>
    <li <?php echo $current_page=='/campaigns_month.php' ? ' class="active"' : ''; ?>>
        <a href="campaigns_month.php"><i style="padding-right: 8px;" class="fa fa-bar-chart fa-2x"></i>Campaigns month</a>
    </li>
    <li <?php echo $current_page=='/campaigns.php' ? ' class="active"' : ''; ?>>
        <a href="campaigns.php"><i style="padding-right: 8px;" class="fa fa-area-chart fa-2x"></i>Campaigns</a>
    </li>
    <?PHP } ?>
    <li>
        <ul style="margin-left: 14px;">
            <li>
                <dl class="accordion" data-accordion="myAccordionGroup">
                    <dd>
                        <a href="#panel3c"><i style="padding-right: 8px;" class="fa fa-address-card-o fa-2x"></i>Address Book <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                        <div id="panel3c" class="content">
                            <ul>
                                <li <?php echo $current_page=='/billing_admin/adv-vs-am.php' ? ' class="active"' : ''; ?>>
                                    <a href="adv-vs-am.php"><i style="padding-right: 8px;" class="fa fa-user-secret"></i>ADVERTISERS</a>
                                </li>
                               <!--  <li <?php echo $current_page=='/billing_admin/affiliates.php' ? ' class="active"' : ''; ?>>
                                    <a href="affiliates.php"><i style="padding-right: 8px;" class="fa fa-handshake-o"></i>AFFILIATES</a>
                                </li> -->
                            </ul>
                        </div>
                    </dd>
                </dl>
            </li>
        </ul>
    </li>
    
</ul>