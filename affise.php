<?php
require "config.php";
if( isset($_POST['newName']) ){
    $_POST['newName'] = $_POST['newName'] == "" ? "Dude" : $_POST['newName'];
    $LS->updateUser(array(
        "name" => $_POST['newName']
    ));
}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>CSV upload</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/foundation.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/foundation-icons/foundation-icons.css" rel="stylesheet" />
    <script src="js/vendor/modernizr.js"></script>
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/flat-icon/flaticon.css" rel="stylesheet" />
    <link href="css/tree.min.css" rel="stylesheet" />
    <!-- datepicker -->
        <script src="js/foundation.min.js"></script>
    <script src="js/vendor/modernizr.js"></script>
    <link rel="stylesheet" type="text/css" href="css/foundation-datepicker.min.css"><!---->
    <!-- languaje 
     FOR ENGLISH OF GB, and change script down to en-GB//   <script src="js/locales/foundation-datepicker.en-GB.js"></script>-->
    <script src="js/locales/foundation-datepicker.es.js"></script>
    <!-- date picker Foundation -->
    <script type="text/javascript" language="javascript" src="js/foundation-datepicker.js"></script><!---->
</head>
<body>
    <div class="row full-width wrapper">
        <div class="large-12 columns content-bg">
            <div id="top-menu">
                <div class="row">
                    <div class="large-2 medium-4 small-12 columns top-part-no-padding">
                        <div class="logo-bg">
                            <img src="img/logo.png" alt="logo"/>
                            <i class="fi-list toggles" data-toggle="hide"></i>
                        </div>
                    </div>
                    <div class="large-10 medium-8 small-12 columns top-menu">
                        <div class="row">
                            <div class="large-6 medium-6 small-12 columns">
                                <div class="row">
                                    <div class="large-8 columns">
                                        <input id="Text1" type="text" class="search-text" placeholder="Search" />
                                    </div>
                                </div>
                            </div>
                            <div class="large-4 medium-6 small-12 columns text-center">
                                <div class="row">
                                    <div class="medium-3 small-3 columns">
                                        <div class="notification">
                                            <i class="fi-mail"></i>
                                            <span class="mail">4</span>
                                        </div>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                        <div class="notification">
                                            <i class="fi-megaphone"></i>
                                            <span class="megaphone">5</span>
                                        </div>
                                    </div>
                                     <div class="medium-3 small-3 columns">
                                        <!--<img src="img/32.jpg" alt="picture" class="top-bar-picture" />-->
                                        Hi 
                                            <?php
                                            $details = $LS->getUser();
                                            echo $details['username'] ,"!";
                                            ?>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                       <a href="logout.php"> <i class="fi-power power-off"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="no-padding">
                    <div class="large-2 medium-12 small-12 columns">
                        <!-- menu -->
                        <?PHP include'menu.php';?>
                    </div>
                </div>
                <div class="large-10 medium-12 small-12 columns light-grey-bg-pattern">
                    <div class="row">
                        <div class="large-10 columns">
                            <div class="page-name">
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div id="form">
                        <div class="row">
                            <div class="large-12 columns">
                                <div class="custom-panel">
                                    <div class="custom-panel-heading">
                                        <h4>TEST AFFISE</h4>
                                    </div>
                                    <div class="custom-panel-body">
                                        <form action="https://advancemobile.go2affise.com/click?pid=28&offer_id=2379" id="form1" method="POST">
									  		First name: <input type="text" name="fname" id="fname"><br>
									  		Phone number: <input type="text" name="pnumber" id="pnumber"><br>
									  		City: <input type="text" name="city" id="city"><br>
									  		<input type="submit" id="submit" class="button tiny radius right coral-bg" name="submit" value="submit">
										</form> 
                                        	
                                    <?PHP
                                        //conversion is done
                                        if(isset($_GET['clickid']))
                                        {
                                            echo"<iframe src='http://advancemobile.go2affise.com/success.php?afstatus=1' height='1' width='1'  style='visibility: hidden;'/>'";
                                        }
                                    ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="js/menu.js"></script>
    <script>
        $(document).foundation();
        // tracking information 
        $('#submit').click(function(event) {
            event.preventDefault();
                var url = $('#form1').attr('action')+'&sub1='+$('#fname').val()+'&sub2='+$('#pnumber').val()+'&sub3='+$('#city').val();
                console.log(url);
                $(location).attr('href', url);
        });
    </script>  
    <script>
   $(function () {
        // datepicker limited to months
        $('#dpMonths').fdatepicker();

    });
    </script>  
</body>
</html>

<!-- Localized -->