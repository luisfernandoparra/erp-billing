<?php
require "config.php";
if( isset($_POST['newName']) ){
    $_POST['newName'] = $_POST['newName'] == "" ? "Dude" : $_POST['newName'];
    $LS->updateUser(array(
        "name" => $_POST['newName']
    ));
}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <title>ADVERTISERS</title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/foundation.min.css"><!---->
    <link href="css/foundation-icons/foundation-icons.css" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/flat-icon/flaticon.css" rel="stylesheet" />
        <!-- data tables -->
        <link rel="stylesheet" type="text/css" href="css/csstables/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/responsive.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/buttons.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/select.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/editor.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="examples/resources/syntax/shCore.css">
        <link rel="stylesheet" type="text/css" href="examples/resources/demo.css">
    <!-- datepicker -->
    <link rel="stylesheet" type="text/css" href="css/foundation-datepicker.min.css"><!---->
    <!-- languaje 
     FOR ENGLISH OF GB, and change script down to en-GB//   <script src="js/locales/foundation-datepicker.en-GB.js"></script>-->
    <script src="js/locales/foundation-datepicker.es.js"></script>
 

    <style type="text/css" class="init">
        .results {
            font-weight: bold;
            color: #33cc66;
        }
        .results_bad {
            font-weight: bold;
            color: #ff3333;
        }
        .fact-validada {
            background-color: #a3d7f7 !important;
        }
        .fact-validadanok {
            background-color: #f98b8b !important;
        }
        .fact-faltandatos {
            background-color: #ffe6b3 !important;
        }
        .fact-brasil {
            background-color: #f9ed93 !important;
        }
        .fact-facturado {
            background-color: #99ff99 !important;
        }
        td.highlight {
            background-color: whitesmoke !important;
        }
    </style>
    <!-- JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/vendor/modernizr.js"></script>
        <!-- date picker Foundation -->
        <script type="text/javascript" language="javascript" src="js/foundation-datepicker.js"></script><!---->
        <!-- dta tables -->
        <script type="text/javascript" language="javascript" src="js/jstables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.responsive.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.buttons.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.select.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.editor.min.js"></script>
        <script type="text/javascript" language="javascript" src="examples/resources/syntax/shCore.js"></script>
        <script type="text/javascript" language="javascript" src="examples/resources/demo.js"></script>
        
        <!-- EXPORT BUTTON -->
        <script type="text/javascript" language="javascript" src="js/jstables/jszip.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/pdfmake.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/vfs_fonts.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/buttons.html5.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/buttons.print.min.js"></script>
        <!-- END EXPORT BUTTON -->

        <script type="text/javascript" language="javascript" class="init">  
   
var editor; // use a global for the submit and return data rendering in the examples
 

$(document).ready(function() {
    editor = new $.fn.dataTable.Editor( {
        ajax: "examples/php/adver-vs-am.php",
        table: "#example",
        fields: [ {
                label: "Advertiser:",
                name: "advertisers.nombre"
            }, {
                label: "Company Name:",
                name: "advertisers.social"
            }, {
                label: "AM:",
                name: "advertisers.id_am",
                type: "select"
            },  {
                label: "Transaction Money:",
                name: "advertisers.money"
            },  {
                label: "Client code:",
                name: "advertisers.cod_client"
            },  {
                label: "Nationality:",
                name: "advertisers.nation"
            },  {
                label: "Contact mail:",
                name: "advertisers.contact_mail"
            },  {
                label: "CifDni:",
                name: "advertisers.CifDni"
            },  {
                label: "Phone:",
                name: "advertisers.phone"
            },  {
                label: "State:",
                name: "advertisers.state"
            },  {
                label: "Notes:",
                name: "advertisers.notes"
            },  {
                label: "Address:",
                name: "advertisers.address"
            }
        ]
    } );

    // Activate an inline edit on click of a table cell
    // $('#example').on( 'click', 'tbody td:not(:first-child)', function (e) {
    //     editor.inline( this );
    // } );

    // Activate an inline edit on click of a table cell
    // or a DataTables Responsive data cell
    $('#example').on( 'click', 'tbody td:not(.child), tbody span.dtr-data', function (e) {
        // Ignore the Responsive control and checkbox columns
        // console.log("me ejecuto " + counter);
        if ( $(this).hasClass('select-checkbox') ) {
            return;
        }
        editor.inline( this );

    }),

    $('#example').DataTable( {
        dom: "Bfrtip",
        "pageLength": 30,
        ajax: "examples/php/adver-vs-am.php",
        columns: [
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            { data: "advertisers.nombre" },
            { data: "advertisers.social" },
            { data: "accountmanager.nombre", editField: "advertisers.id_am"},
            { data: "advertisers.money"},
            { data: "advertisers.cod_client"},
            { data: "advertisers.nation"},
            { data: "advertisers.contact_mail"},
            { data: "advertisers.CifDni"},
            { data: "advertisers.phone"},
            { data: "advertisers.state"},
            { data: "advertisers.notes"},
            { data: "advertisers.address"}
        ],
          order: [ 1, 'asc' ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        buttons: [
            { extend: "create", editor: editor },
            { extend: "edit",   editor: editor },
            { extend: "remove", editor: editor },
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ]
            }
        ],
    } );
} );



    </script>

</head>
<body>
    <div class="row full-width wrapper">
        <div class="large-12 columns">
            <div id="top-menu">
                <div class="row">
                    <div class="large-2 medium-4 small-12 columns top-part-no-padding">
                        <div class="logo-bg">
                            <img src="img/logo.png" alt="logo"/>
                            <i class="fi-list toggles" data-toggle="hide"></i>
                        </div>
                    </div>
                    <div class="large-10 medium-8 small-12 columns top-menu">
                        <div class="row">
                            <div class="large-6 medium-6 small-12 columns">
                                <div class="row">
                                    <div class="large-8 columns">
                                        <input id="Text1" type="text" class="search-text" placeholder="Search" />
                                    </div>
                                </div>
                            </div>
                            <div class="large-4 medium-6 small-12 columns text-center">
                                <div class="row">
                                    <div class="medium-3 small-3 columns">
                                        <div class="notification">
                                            <i class="fi-mail"></i>
                                            <span class="mail">4</span>
                                        </div>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                        <div class="notification">
                                            <i class="fi-megaphone"></i>
                                            <span class="megaphone">5</span>
                                        </div>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                        <!--<img src="img/32.jpg" alt="picture" class="top-bar-picture" />-->
                                        Hi 
                                            <?php
                                            $details = $LS->getUser();
                                            echo $details['username'] ,"!";
                                            ?>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                       <a href="logout.php"> <i class="fi-power power-off"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="no-padding">
                    <div class="large-2 medium-12 small-12 columns">
                        <!-- menu -->
                        <?PHP include'menu.php';?>
                    </div>
                </div>
                <div class="large-10 medium-12 small-12 columns light-grey-bg-pattern">
                    <div class="row">
                        <div class="large-10 columns">
                            <div class="page-name">
                                <h3 class="left">ADVERTISERS</h3>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                    </div>
                    <!-- tables -->
                    <div id="staff">
                       
                        <table id="example" class="display responsive" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                <!-- 1 --><th></th>
                                <!-- 2 --><th>Advertiser</th>
                                <!-- 3 --><th>Company Name</th>
                                <!-- 4 --><th>AM</th>
                                <!-- 5 --><th>Transaction</th>
                                <!-- 6 --><th>Client code</th>
                                <!-- 7 --><th>Nationality</th>
                                <!-- 8 --><th>Contact mail</th>
                                <!-- 9 --><th>CifDni</th>
                                <!-- 10 --><th>Phone</th>
                                <!-- 11 --><th>State</th>
                                <!-- 12 --><th>Notes</th>
                                <!-- 13 --><th>Address</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                <!-- 1 --><th></th>
                                <!-- 2 --><th></th>
                                <!-- 3 --><th></th>
                                <!-- 4 --><th></th>
                                <!-- 5 --><th></th>
                                <!-- 6 --><th></th>
                                <!-- 7 --><th></th>
                                <!-- 8 --><th></th>
                                <!-- 9 --><th></th>
                                <!-- 10 --><th></th>
                                <!-- 11 --><th></th>
                                <!-- 12 --><th></th>
                                <!-- 13 --><th></th>
                                </tr>
                                <tr>
                                <!-- 1 --><th></th>
                                <!-- 2 --><th>Advertiser</th>
                                <!-- 3 --><th>Company Name</th>
                                <!-- 4 --><th>AM</th>
                                <!-- 5 --><th>Transaction</th>
                                <!-- 6 --><th>Client code</th>
                                <!-- 7 --><th>Nationality</th>
                                <!-- 8 --><th>Contact mail</th>
                                <!-- 9 --><th>CifDni</th>
                                <!-- 10 --><th>Phone</th>
                                <!-- 11 --><th>State</th>
                                <!-- 12 --><th>Notes</th>
                                <!-- 13 --><th>Address</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- end tables -->
                </div>
            </div>
        </div>
    </div>

    
    <script src="js/menu.js"></script>
    <script>
        $(document).foundation();     
    </script>

</body>
</html>

<!-- Localized -->