<?php
// DataTables PHP library
include( "../../php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'campaigns_month_close', 'id' )
	->fields(
        Field::inst( 'campaigns_month_close.date' )
            ->validator( 'Validate::notEmpty'),
        Field::inst( 'campaigns_month_close.client' )
            ->validator( 'Validate::notEmpty' ),
        Field::inst( 'campaigns_month_close.internal_mail' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'campaigns_month_close.external_mail' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'campaigns_month_close.invest_fb' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'campaigns_month_close.fee_fb' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'campaigns_month_close.setup_fb' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'campaigns_month_close.invest_adwords' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'campaigns_month_close.fee_adwords' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'campaigns_month_close.setup_adwords' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'campaigns_month_close.invest_rtb' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'campaigns_month_close.fee_rtb' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'campaigns_month_close.setup_rtb' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'campaigns_month_close.invest_amazon' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'campaigns_month_close.fee_amazon' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'campaigns_month_close.setup_amazon' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'campaigns_month_close.tracking' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'campaigns_month_close.design' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'campaigns_month_close.contents' )
            ->validator( 'Validate::numeric' )
	)
	->process( $_POST )
	->json();
