<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "../../php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'scrubs', 'id' )
	->fields(
		Field::inst( 'scrubs.status_adv' )
		 ->options( Options::inst()
                ->table( 'status_adv' )
                ->value( 'id' )
                ->label( 'value' )
            )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'status_adv.value' ),
		Field::inst( 'scrubs.advertiser' )
			 ->options( Options::inst()
                ->table( 'advertisers' )
                ->value( 'id' )
                ->label( 'nombre' )
            )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'advertisers.nombre' ),
		Field::inst( 'scrubs.offer' ),
		Field::inst( 'scrubs.affiliate' )
			 ->options( Options::inst()
                ->table( 'affiliates' )
                ->value( 'id' )
                ->label( 'nombre' )
            )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'affiliates.nombre' ),
		Field::inst( 'scrubs.conversions' )
			->validator( 'Validate::numeric' ),
		Field::inst( 'scrubs.revenue_disc' )
			->validator( 'Validate::numeric' ),
		Field::inst( 'scrubs.traffic_team' )
			 ->options( Options::inst()
                ->table( 'traffic_team' )
                ->value( 'id' )
                ->label( 'value' )
            )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'traffic_team.value' ),
        Field::inst( 'scrubs.status_aff' )
		 ->options( Options::inst()
                ->table( 'status_aff' )
                ->value( 'id' )
                ->label( 'value' )
            )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'status_aff.value' ),

        Field::inst( 'scrubs.reason' )
		 ->options( Options::inst()
                ->table( 'reason_scrubs' )
                ->value( 'id' )
                ->label( 'value' )
            )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'reason_scrubs.value' ),
		Field::inst( 'scrubs.cost_disc' ),
        Field::inst( 'scrubs.notas_am' ),
		Field::inst( 'scrubs.notas_aff' ),
		Field::inst( 'scrubs.date' )->validator( 'Validate::notEmpty'),
		Field::inst( 'scrubs.date_recived' )->validator( 'Validate::notEmpty')
	)
	//->leftJoin( 'advertisers', 'advertisers.id', '=', 'scrubs.advertisers' )
	->leftJoin( 'status_adv', 'status_adv.id', '=', 'scrubs.status_adv' )
	->leftJoin( 'advertisers', 'advertisers.id', '=', 'scrubs.advertiser' )
	->leftJoin( 'affiliates', 'affiliates.id', '=', 'scrubs.affiliate' )
	->leftJoin( 'traffic_team', 'traffic_team.id', '=', 'scrubs.traffic_team' )
	->leftJoin( 'status_aff', 'status_aff.id', '=', 'scrubs.status_aff' )
	->leftJoin( 'reason_scrubs', 'reason_scrubs.id', '=', 'scrubs.reason' )
	->process( $_POST )
	->json();


