<?php
 
/*
 * Example PHP implementation used for the index.html example
 */
 
// DataTables PHP library
include( "../../php/DataTables.php" );
 
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Options,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'affiliates' )
    ->fields(
        Field::inst( 'affiliates.nombre' )->validator( 'Validate::notEmpty' ),
        Field::inst( 'affiliates.social' ),
        Field::inst( 'affiliates.affm' )
             ->options( Options::inst()
                ->table( 'traffic_team' )
                ->value( 'id' )
                ->label( 'value' )
            )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'traffic_team.value' ),
        Field::inst( 'affiliates.money' ),
        Field::inst( 'affiliates.cod_client' ),
        Field::inst( 'affiliates.nation' ),
        Field::inst( 'affiliates.contact_mail' ),
        Field::inst( 'affiliates.CifDni' ),
        Field::inst( 'affiliates.phone' ),
        Field::inst( 'affiliates.state' ),
        Field::inst( 'affiliates.notes' ),
        Field::inst( 'affiliates.address' )
    )
    ->leftJoin( 'traffic_team', 'traffic_team.id', '=', 'affiliates.affm' )
    ->process( $_POST )
    ->json();

