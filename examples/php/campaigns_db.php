<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "../../php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'campaigns', 'id' )
	->fields(
        Field::inst( 'campaigns.entity_tax_office' )
                         ->options( Options::inst()
                ->table( 'advertisers' )
                ->value( 'cod_client' )
                ->label( 'nombre' )
            )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'advertisers.nombre' ),
        Field::inst( 'campaigns.social' )
                 ->options( Options::inst()
                ->table( 'advertisers' )
                ->value( 'id' )
                ->label( 'social' )
            )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'advertisers.social' ),
	Field::inst( 'campaigns.url'),
	Field::inst( 'campaigns.name_native'),
	Field::inst( 'campaigns.status'),
	Field::inst( 'campaigns.name_english'),
        Field::inst( 'campaigns.date'),
        Field::inst( 'campaigns.date_created'),
        Field::inst( 'campaigns.date_started'),
        Field::inst( 'campaigns.currency')
        	->setFormatter( 'Format::ifEmpty', '€'),
        Field::inst( 'campaigns.country' )
             ->options( Options::inst()
                ->table( 'country' )
                ->value( 'id' )
                ->label( 'name' )
            )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'country.name' ),
        Field::inst( 'campaigns.key_account_manager')
                         ->options( Options::inst()
                ->table( 'accountmanager' )
                ->value( 'id' )
                ->label( 'nombre' )
            )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'accountmanager.nombre' ),
        Field::inst( 'campaigns.domain_list')
        	->setFormatter( 'Format::ifEmpty', null),
        Field::inst( 'campaigns.alias')
        	->setFormatter( 'Format::ifEmpty', null),
        Field::inst( 'campaigns.category_id')
        	->setFormatter( 'Format::ifEmpty', null),
        Field::inst( 'campaigns.category_label')
        	->setFormatter( 'Format::ifEmpty', null),
        Field::inst( 'campaigns.category_type')
        	->setFormatter( 'Format::ifEmpty', null),
        Field::inst( 'campaigns.subcategory_type')
	)
	->leftJoin( 'advertisers', 'advertisers.cod_client', '=', 'campaigns.entity_tax_office' )
        ->leftJoin( 'accountmanager', 'accountmanager.id', '=', 'campaigns.key_account_manager' )
        ->leftJoin( 'country', 'country.id', '=', 'campaigns.country' )
	->process( $_POST )
	->json();


