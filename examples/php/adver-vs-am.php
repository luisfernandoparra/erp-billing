<?php 
// DataTables PHP library
include( "../../php/DataTables.php" );
 
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Options,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'advertisers' )
    ->fields(
        Field::inst( 'advertisers.nombre' )->validator( 'Validate::notEmpty' ),
        Field::inst( 'advertisers.social' ),
        Field::inst( 'advertisers.id_am' )
             ->options( Options::inst()
                ->table( 'accountmanager' )
                ->value( 'id' )
                ->label( 'nombre' )
            )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'accountmanager.nombre' ),
        Field::inst( 'advertisers.money' ),
        Field::inst( 'advertisers.cod_client' ),
        Field::inst( 'advertisers.nation' ),
        Field::inst( 'advertisers.contact_mail' ),
        Field::inst( 'advertisers.CifDni' ),
        Field::inst( 'advertisers.phone' ),
        Field::inst( 'advertisers.state' ),
        Field::inst( 'advertisers.notes' ),
        Field::inst( 'advertisers.address' )
    )
    ->leftJoin( 'accountmanager', 'accountmanager.id', '=', 'advertisers.id_am' )
    ->process( $_POST )
    ->json();
