<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "../../php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'incomes', 'id' )
	->fields(
		Field::inst( 'incomes.social' )
		 ->options( Options::inst()
                ->table( 'advertisers' )
                ->value( 'id' )
                ->label( 'social' )
            )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'advertisers.social' ),
		Field::inst( 'incomes.advertiser' )
			 ->options( Options::inst()
                ->table( 'advertisers' )
                ->value( 'id' )
                ->label( 'nombre' )
            )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'advertisers.nombre' ),
       	Field::inst( 'incomes.am' )
			 ->options( Options::inst()
                ->table( 'accountmanager' )
                ->value( 'id' )
                ->label( 'nombre' )
            )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'accountmanager.nombre' ),
		Field::inst( 'incomes.clicks' )
			->validator( 'Validate::numeric' ),
		Field::inst( 'incomes.conversion' )
			->validator( 'Validate::numeric' ),
		Field::inst( 'incomes.revenue' )
			->validator( 'Validate::numeric' ),
		Field::inst( 'incomes.platform_revenue' )
			->validator( 'Validate::numeric' ),
		Field::inst( 'incomes.difference' )
			->validator( 'Validate::numeric' ),
		Field::inst( 'incomes.scrubs' )
			->validator( 'Validate::numeric' ),
		Field::inst( 'incomes.hold' )
			->validator( 'Validate::numeric' ),
		Field::inst( 'incomes.validated' )
			->validator( 'Validate::numeric' ),
		Field::inst( 'advertisers.money' )
        	->setFormatter( 'Format::ifEmpty', null ),
		Field::inst( 'incomes.status' )
			 ->options( Options::inst()
                ->table( 'status' )
                ->value( 'id' )
                ->label( 'name' )
            )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'status.name' ),
        Field::inst( 'incomes.id' ),
        Field::inst( 'incomes.increment' )
        	->setFormatter( 'Format::ifEmpty', null ),
        Field::inst( 'advertisers.cod_client' )
        	->setFormatter( 'Format::ifEmpty', null ),
        Field::inst( 'incomes.accumulated' )
        	->setFormatter( 'Format::ifEmpty', null ),
        Field::inst( 'advertisers.nation' )
        	->setFormatter( 'Format::ifEmpty', null ),
		Field::inst( 'incomes.notas_am' ),
		Field::inst( 'incomes.notas_finance' ),
		Field::inst( 'incomes.date' )->validator( 'Validate::notEmpty' )
			  //->getFormatter( 'Format::date_sql_to_format', 'Ymd' )
			  //->setFormatter( 'Format::date_format_to_sql', 'Ymd' )
	)
	->leftJoin( 'advertisers', 'advertisers.id', '=', 'incomes.advertiser' )
	->leftJoin( 'accountmanager', 'accountmanager.id', '=', 'incomes.am' )
	->leftJoin( 'status', 'status.id', '=', 'incomes.status' )
	->process( $_POST )
	->json();


