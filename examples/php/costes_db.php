<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "../../php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'costes', 'id' )
	->fields(
		Field::inst( 'costes.affiliate' ) //Affiliate
			 ->options( Options::inst()
                ->table( 'affiliates' )
                ->value( 'id' )
                ->label( 'nombre' )
            )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'affiliates.nombre' ),
        Field::inst( 'costes.affm' )
             ->options( Options::inst()
                ->table( 'traffic_team' )
                ->value( 'id' )
                ->label( 'value' )
            )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'traffic_team.value' ),
        Field::inst( 'costes.totaldollar' )
            ->validator( 'Validate::numeric'),
        Field::inst( 'costes.totaleuro' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'costes.plataff' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'costes.discrepancia' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'costes.scrubs' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'costes.onhold' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'costes.validacion' )
            ->validator( 'Validate::numeric' ),
        Field::inst( 'costes.comments' ),
        Field::inst( 'costes.status' )
            ->options( Options::inst()
                ->table( 'status_aff' )
                ->value( 'id' )
                ->label( 'value' )
            )
            ->validator( 'Validate::dbValues' ),
        Field::inst( 'status_aff.value' ),
		Field::inst( 'costes.date' )->validator( 'Validate::notEmpty'),
        Field::inst( 'costes.fact_number' )
	)
	->leftJoin( 'affiliates', 'affiliates.id', '=', 'costes.affiliate' )
    ->leftJoin( 'traffic_team', 'traffic_team.id', '=', 'costes.affm' )
	->leftJoin( 'status_aff', 'status_aff.id', '=', 'costes.status' )
	->process( $_POST )
	->json();
