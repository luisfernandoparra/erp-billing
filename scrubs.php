<?php
require "config.php";
if( isset($_POST['newName']) ){
    $_POST['newName'] = $_POST['newName'] == "" ? "Dude" : $_POST['newName'];
    $LS->updateUser(array(
        "name" => $_POST['newName']
    ));
}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <title>SCRUBS</title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/foundation.min.css"><!---->
    <link href="css/foundation-icons/foundation-icons.css" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/flat-icon/flaticon.css" rel="stylesheet" />

        <!-- data tables -->
        <link rel="stylesheet" type="text/css" href="css/csstables/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/responsive.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/buttons.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/select.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/editor.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="examples/resources/syntax/shCore.css">
        <link rel="stylesheet" type="text/css" href="examples/resources/demo.css">
    <!-- datepicker -->
    <link rel="stylesheet" type="text/css" href="css/foundation-datepicker.min.css"><!---->
    <!-- languaje 
     FOR ENGLISH OF GB, and change script down to en-GB//   <script src="js/locales/foundation-datepicker.en-GB.js"></script>-->
    <script src="js/locales/foundation-datepicker.es.js"></script>
 

    <style type="text/css" class="init">
        .results {
            font-weight: bold;
            color: #33cc66;
        }
        .results_bad {
            font-weight: bold;
            color: #ff3333;
        }
        .fact-validada {
            background-color: #a3d7f7 !important;
        }
        .fact-validadanok {
            background-color: #f98b8b !important;
        }
        .fact-provisionado {
            background-color: #FFFFCE !important;
        }
        .fact-brasil {
            background-color: #f9ed93 !important;
        }
        .fact-facturado {
            background-color: #99ff99 !important;
        }
        td.highlight {
            background-color: whitesmoke !important;
        }
        .RS {
            font-size: 10px;
        }
       
    </style>
    <!-- JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/vendor/modernizr.js"></script>
        <!-- date picker Foundation -->
        <script type="text/javascript" language="javascript" src="js/foundation-datepicker.js"></script><!---->
        <!-- dta tables -->
        <script type="text/javascript" language="javascript" src="js/jstables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.responsive.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.buttons.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.select.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.editor.min.js"></script>
        <script type="text/javascript" language="javascript" src="examples/resources/syntax/shCore.js"></script>
        <script type="text/javascript" language="javascript" src="examples/resources/demo.js"></script>
        
        <!-- EXPORT BUTTON -->
        <script type="text/javascript" language="javascript" src="js/jstables/jszip.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/pdfmake.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/vfs_fonts.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/buttons.html5.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/buttons.print.min.js"></script>
        <!-- END EXPORT BUTTON -->

        <script type="text/javascript" language="javascript" class="init">  
    var editor; // use a global for the submit and return data rendering in the examples
$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        // get value
        var searchDate = $('#min').val();
        //remove "-"
        var stringDate = searchDate.split("-");
        // concatenate array of values
        var year = stringDate[0];
        var month = stringDate[1];
        //var day = stringDate[2];
        var dateSplited = year+month;
        // parse to float
        var min = parseFloat(dateSplited);
        // console.log(min);
        //var max = parseInt( $('#max').val(), 10 );
         // get value
        var currentDate = data[1] || 0;
        // console.log(currentDate);
        // remove "-"
        var splitedCurrentDate = currentDate.split("-");
        // concatenate array of values
        var yearCurrent = splitedCurrentDate[0];
        var monthCurrent = splitedCurrentDate[1];
        // var dayCurrent = splitedCurrentDate[2];
        var currentDsplited = yearCurrent+monthCurrent;
        // parse to float
        var date = parseFloat(currentDsplited);
        // var date = parseFloat( data[13] ) || 0; // use data for the date column
        // console.log(date);
        if ( ( isNaN( min ) ) ||( min == date  ) )
        {
            return true;    
        }
        return false;
     }
);
    $(document).ready(function() {

        editor = new $.fn.dataTable.Editor({
            ajax: "examples/php/scrubs.php",
            table: "#example",
            fields: [ 
                    {
                    label: "Month Scrub:",
                    name: "scrubs.date",
                    type: "datetime"
                }, {
                    label: "Scrub received:",
                    name: "scrubs.date_recived",
                    type: "datetime"
                }, {
                    label: "Status Adv:",
                    name: "scrubs.status_adv",
                    type: "select"
                }, {
                    label: "Advertiser:",
                    name: "scrubs.advertiser",
                    type: "select"
                },{
                    label: "offer:",
                    name: "scrubs.offer"
                }, {
                    label: "Affiliate:",
                    name: "scrubs.affiliate",
                    type: "select"
                }, {
                    label: "Conversions:",
                    name: "scrubs.conversions"
                }, {
                    label: "Revenue Disc:",
                    name: "scrubs.revenue_disc"
                }, {
                    label: "Cost Disc:",
                    name: "scrubs.cost_disc"
                }, {
                    label: "Traffic Team:",
                    name: "scrubs.traffic_team",
                    type: "select"
                }, {
                    label: "Status Aff:",
                    name: "scrubs.status_aff",
                    type: "select"
                }, {
                    label: "Reason:",
                    name: "scrubs.reason",
                    type: "select"
                }, {
                    label: "Notas AM:",
                    name: "scrubs.notas_am"
                }, {
                    label: "Notas Aff:",
                    name: "scrubs.notas_aff"
                }
            ]
        } ),

    // Activate an inline edit on click of a table cell
    // or a DataTables Responsive data cell
    $('#example').on( 'click', 'tbody td:not(.child), tbody span.dtr-data', function (e) {
        // Ignore the Responsive control and checkbox columns
        if ( $(this).hasClass( 'control' ) || $(this).hasClass('select-checkbox') ) {
            return;
        }

        editor.inline( this );
    } ),



 $('#example').DataTable({

    // initComplete: function () {
    //         this.api().columns([3,12]).every( function () {
    //             var column = this;
    //             var select = $('<select><option value="">All</option></select>')
    //                 .appendTo( $(column.footer()).empty() )
    //                 .on( 'change', function () {
    //                     var val = $.fn.dataTable.util.escapeRegex(
    //                         $(this).val()
    //                     );
 
    //                     column
    //                         .search( val ? '^'+val+'$' : '', true, false )
    //                         .draw();
    //                 } );
 
    //             column.data().unique().sort().each( function ( d, j ) {
    //                 if(column.search() === '^'+d+'$'){
    //                     select.append( '<option value="'+d+'" selected="selected">'+d+'</option>' )
    //                 } else {
    //                     select.append( '<option value="'+d+'">'+d+'</option>' )
    //                 }
    //             } );
    //         } );
    //     },
    // colors depending on status
     // "createdRow": function( row, data, dataIndex ) {

     //        // if(data.scrubs.status_adv == '1' && data.scrubs.status_aff == '1')
     //        // {
     //        //      $(row).addClass( 'fact-facturado' );
     //        // }
     //    },
        "rowCallback": function( row, data, index ) {
            // STATUS ADVERTISER
            if ( data.scrubs.status_adv == "1" ) {
                $("td:eq(3)", row).removeClass( 'fact-validadanok' ); 
                $("td:eq(3)", row).addClass( 'fact-facturado' ); 
            } 
            if ( data.scrubs.status_adv == "2" ) {  
                $("td:eq(3)", row).removeClass( 'fact-facturado' ); 
                $("td:eq(3)", row).addClass( 'fact-validadanok' );  
            }

             // STATUS AFFILIATE
            if ( data.scrubs.status_aff == "1" ) {
                 $("td:eq(11)", row).removeClass( 'fact-validadanok' );
                $("td:eq(11)", row).addClass( 'fact-facturado' );
            } 
            if ( data.scrubs.status_aff == "2" ){  
                $("td:eq(11)", row).removeClass( 'fact-facturado' ); 
                $("td:eq(11)", row).addClass( 'fact-validadanok' ); 
            }
    
        },
        // footer calculation

         "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            },
            // Total over all pages
            conversions = api
                .column( 7, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            revenue = api
                .column( 8, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            cost_disc = api
                .column( 9, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
            $( api.column( 7 ).footer() ).html(
                '<span class="results">'+(conversions)+'</span>'
            );
            $( api.column( 8 ).footer() ).html(
                '<span class="results">$'+(revenue).toFixed(2)+'</span>'
            );
            $( api.column( 9 ).footer() ).html(
                '<span class="results">$'+(cost_disc).toFixed(2)+'</span>'
            );
            
        },
         
        responsive: true,
        scrollY: 550,
        scrollX: true,
        bPaginate: false, 
        dom: "Bfrtip",
       
        deferRender: true,
        ajax: "examples/php/scrubs.php",
        columns: [

            {   // Responsive control column
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            { data: "scrubs.date" },
            { data: "scrubs.date_recived" },
            { data: "status_adv.value", editField: "scrubs.status_adv"},
            { data: "advertisers.nombre", editField: "scrubs.advertiser"},
            { data: "scrubs.offer" },
            { data: "affiliates.nombre", editField: "scrubs.affiliate"},
            { data: "scrubs.conversions" },
            { data: "scrubs.revenue_disc" , render: $.fn.dataTable.render.number( ',', '.', 2, '$' ) },
            { data: "scrubs.cost_disc" , render: $.fn.dataTable.render.number( ',', '.', 2, '$' ) },
            { data: "traffic_team.value", editField: "scrubs.traffic_team"},
            { data: "status_aff.value", editField: "scrubs.status_aff"},
            { data: "reason_scrubs.value", editField: "scrubs.reason"},
            { data: "scrubs.notas_am" },
            { data: "scrubs.notas_aff" }
        ],
        order: [ 1, 'asc' ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        buttons: [
            { extend: "create", editor: editor },
            { extend: "edit",   editor: editor },
            { extend: "remove", editor: editor },
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ]
            }
        ],
        
         
    });

 $('#min').change( function() {
        table.draw();
    } );
// highlights table
  var table = $('#example').DataTable();
    $('#example tbody')
        .on( 'mouseenter', 'td', function () {

        } );
        $('#example tbody').on( 'click', 'td', function () {
        } )

});
    </script>

</head>
<body>
    <div class="row full-width wrapper">
        <div class="large-12 columns">
            <div id="top-menu">
                <div class="row">
                    <div class="large-2 medium-4 small-12 columns top-part-no-padding">
                        <div class="logo-bg">
                            <img src="img/logo.png" alt="logo"/>
                            <i class="fi-list toggles" data-toggle="hide"></i>
                        </div>
                    </div>
                    <div class="large-10 medium-8 small-12 columns top-menu">
                        <div class="row">
                            <div class="large-6 medium-6 small-12 columns">
                                <div class="row">
                                    <div class="large-8 columns">
                                        <input id="Text1" type="text" class="search-text" placeholder="Search" />
                                    </div>
                                </div>
                            </div>
                            <div class="large-4 medium-6 small-12 columns text-center">
                                <div class="row">
                                    <div class="medium-3 small-3 columns">
                                        <div class="notification">
                                            <i class="fi-mail"></i>
                                            <span class="mail">4</span>
                                        </div>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                        <div class="notification">
                                            <i class="fi-megaphone"></i>
                                            <span class="megaphone">5</span>
                                        </div>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                        <!--<img src="img/32.jpg" alt="picture" class="top-bar-picture" />-->
                                        Hi 
                                            <?php
                                            $details = $LS->getUser();
                                            echo $details['username'] ,"!";
                                            ?>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                       <a href="logout.php"> <i class="fi-power power-off"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="no-padding">
                    <div class="large-2 medium-12 small-12 columns">
                        <!-- menu -->
                        <?PHP include'menu.php';?>
                    </div>
                </div>
                <div class="large-10 medium-12 small-12 columns light-grey-bg-pattern">
                    <div class="row">
                        <div class="large-5 columns">
                            <div class="page-name">
                                <h3 class="left">SCRUBS</h3>
                            </div>
                        </div>
                        <div class="large-7 columns" style="margin-top: 10px;">
                                <!-- datepicker -->
                                        <div class="row collapse date" id="dpMonths" data-date-format="yyyy-mm-dd" data-start-view="year" data-min-view="year" style="max-width:200px;">
                                <div class="small-2 columns">   
                                    <span class="prefix"><i class="fa fa-calendar"></i></span>
                                </div>
                                <div class="small-10 columns">
                                    <input type="text" size="16" value="<?=date('Y-m-d', strtotime("first day of -1 month")); ?>" id="min" name="min" >  
                                </div>
                            </div>
                            <!-- end datepicker -->
                        </div>
                             
                            

                    </div>
                    <!-- tables -->
                    <div id="staff">
                         
                        <table id="example" class="display responsive" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    
                                    <!-- 1 --><th></th>
                                    <!-- 2 --><th>Current month</th>
                                    <!-- 3 --><th>Received</th>
                                    <!-- 4 --><th>Status Adv</th>
                                    <!-- 5 --><th>Advertiser</th>
                                    <!-- 6 --><th>Offer</th>
                                    <!-- 7 --><th>Affiliate</th>
                                    <!-- 8 --><th>Conv.</th>
                                    <!-- 9 --><th>Revenue Disc</th>
                                    <!-- 9/10 --><th>Cost Disc</th>
                                    <!-- 10 --><th>Traffic Team</th>
                                    <!-- 11 --><th>Status Aff</th>
                                    <!-- 12 --><th>Reason</th>
                                    <!-- 13 --><th>Notas(AM)</th>
                                    <!-- 14 --><th>Notas(Affi)</th>
                                    
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    
                                    <!-- 1 --><th></th>
                                    <!-- 2 --><th></th>
                                    <!-- 3 --><th></th>
                                    <!-- 4 --><th></th>
                                    <!-- 5 --><th></th>
                                    <!-- 6 --><th></th>
                                    <!-- 7 --><th style="text-align:left">Total:</th>
                                    <!-- 8 --><th></th>
                                    <!-- 9 --><th></th>
                                    <!-- 9/10 --><th></th>
                                    <!-- 10 --><th></th>
                                    <!-- 11 --><th></th>
                                    <!-- 12 --><th></th>
                                    <!-- 13 --><th></th>
                                    <!-- 14 --><th></th>
                                   
                                </tr>
                                <tr>
                                    
                                    <!-- 1 --><th></th>
                                    <!-- 2 --><th>Current month</th>
                                    <!-- 3 --><th>Received</th>
                                    <!-- 4 --><th>Status Adv</th>
                                    <!-- 5 --><th>Advertiser</th>
                                    <!-- 6 --><th>Offer</th>
                                    <!-- 7 --><th>Affiliate</th>
                                    <!-- 8 --><th>Conv.</th>
                                    <!-- 9 --><th>Revenue Disc</th>
                                    <!-- 9/10 --><th>Cost Disc</th>
                                    <!-- 10 --><th>Traffic Team</th>
                                    <!-- 11 --><th>Status Aff</th>
                                    <!-- 12 --><th>Reason</th>
                                    <!-- 13 --><th>Notas(AM)</th>
                                    <!-- 14 --><th>Notas(Affi)</th>
                          
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- end tables -->
                </div>
            </div>
        </div>
    </div>

    
    <script src="js/menu.js"></script>
    <script>
        $(document).foundation();     
    </script>
    <script>
   $(function () {
        // datepicker limited to months
        $('#dpMonths').fdatepicker();

    });
    </script>

</body>
</html>

<!-- Localized -->