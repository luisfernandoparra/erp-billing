<?php
require "config.php";
if( isset($_POST['newName']) ){
    $_POST['newName'] = $_POST['newName'] == "" ? "Dude" : $_POST['newName'];
    $LS->updateUser(array(
        "name" => $_POST['newName']
    ));
}
//ROLES
$UserGeting = $LS->getUser();
$userID = $UserGeting['id'];
require_once "php/user_roles.php";
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <title>CAMPAIGNS</title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/foundation.min.css"><!---->
    <link href="css/foundation-icons/foundation-icons.css" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/flat-icon/flaticon.css" rel="stylesheet" />

        <!-- data tables -->
        <link rel="stylesheet" type="text/css" href="css/csstables/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/responsive.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/buttons.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/select.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/csstables/editor.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="examples/resources/syntax/shCore.css">
        <link rel="stylesheet" type="text/css" href="examples/resources/demo.css">
    <!-- datepicker -->
    <link rel="stylesheet" type="text/css" href="css/foundation-datepicker.min.css"><!---->
    <!-- languaje 
     FOR ENGLISH OF GB, and change script down to en-GB//   <script src="js/locales/foundation-datepicker.en-GB.js"></script>-->
    <script src="js/locales/foundation-datepicker.es.js"></script>
 

    <style type="text/css" class="init">
        .results {
            font-weight: bold;
            color: #33cc66;
        }
        .results_bad {
            font-weight: bold;
            color: #ff3333;
        }
        .fact-validada {
            background-color: #a3d7f7 !important;
        }
        .fact-validadanok {
            background-color: #f98b8b !important;
        }
        .fact-provisionado {
            background-color: #FFFFCE !important;
        }
        .fact-brasil {
            background-color: #f9ed93 !important;
        }
        .fact-facturado {
            background-color: #99ff99 !important;
        }
        td.highlight {
            background-color: whitesmoke !important;
        }
        .RS {
            font-size: 10px;
        }
        
    </style>
    <!-- JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/vendor/modernizr.js"></script>
        <!-- date picker Foundation -->
        <script type="text/javascript" language="javascript" src="js/foundation-datepicker.js"></script><!---->
        <!-- dta tables -->
        <script type="text/javascript" language="javascript" src="js/jstables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.responsive.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.buttons.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.select.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/dataTables.editor.min.js"></script>
        <script type="text/javascript" language="javascript" src="examples/resources/syntax/shCore.js"></script>
        <script type="text/javascript" language="javascript" src="examples/resources/demo.js"></script>
        
        <!-- EXPORT BUTTON -->
        <script type="text/javascript" language="javascript" src="js/jstables/jszip.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/pdfmake.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/vfs_fonts.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/buttons.html5.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jstables/buttons.print.min.js"></script>
        <!-- END EXPORT BUTTON -->

        <script type="text/javascript" language="javascript" class="init">  
    var editor; // use a global for the submit and return data rendering in the examples
$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        // get value
        var searchDate = $('#min').val();
        //remove "-"
        var stringDate = searchDate.split("-");
        // concatenate array of values
        var year = stringDate[0];
        var month = stringDate[1];
        //var day = stringDate[2];
        var dateSplited = year+month;
        // parse to float
        var min = parseFloat(dateSplited);
        // console.log(min);
        //var max = parseInt( $('#max').val(), 10 );
         // get value
        var currentDate = data[1] || 0;
        // console.log(currentDate);
        // remove "-"
        var splitedCurrentDate = currentDate.split("-");
        // concatenate array of values
        var yearCurrent = splitedCurrentDate[0];
        var monthCurrent = splitedCurrentDate[1];
        // var dayCurrent = splitedCurrentDate[2];
        var currentDsplited = yearCurrent+monthCurrent;
        // parse to float
        var date = parseFloat(currentDsplited);
        // var date = parseFloat( data[13] ) || 0; // use data for the date column
        // console.log(date);
        if ( ( isNaN( min ) ) ||( min == date  ) )
        {
            return true;    
        }
        return false;
     }
);
    $(document).ready(function() {
        var euroRenderer = $.fn.dataTable.render.number( ',', '.', 2, '€' ).display;
        var dollarRenderer = $.fn.dataTable.render.number( ',', '.', 2, '$' ).display;
        //data edition
        editor = new $.fn.dataTable.Editor({
            ajax: "examples/php/campaigns_db.php",
            table: "#example",
            fields: [ 
                {
                    label: "Month",
                    name: "campaigns.date",
                    type: "datetime"
                },{
                    label: "Date Created",
                    name: "campaigns.date_created",
                    type: "datetime"
                }, {
                    label: "Date Started:",
                    name: "campaigns.date_started",
                    type: "datetime"
                }, {
                    label: "Entity:",
                    name: "campaigns.entity_tax_office",
                    type: "select"
                }, {
                    label: "Url:",
                    name: "campaigns.url"
                },{
                    label: "Name Native:",
                    name: "campaigns.name_native"
                }, {
                    label: "Name English:",
                    name: "campaigns.name_english"
                }, {
                    label: "Status:",
                    name: "campaigns.status"
                }, {
                    label: "Currency:",
                    name: "campaigns.currency",
                    def: "€"
                }, {
                    label: "Country:",
                    name: "campaigns.country",
                    type: "select"
                }, {
                    label: "AM:",
                    name: "campaigns.key_account_manager",
                    type:"select"
                }, {
                    label: "Domain list:",
                    name: "campaigns.domain_list"
                }, {
                    label: "Alias:",
                    name: "campaigns.alias"
                }, {
                    label: "Category id:",
                    name: "campaigns.setup_rtb"
                }, {
                    label: "Categ. label:",
                    name: "campaigns.invest_amazon"
                }, {
                    label: "Categ. type:",
                    name: "campaigns.fee_amazon"
                }, {
                    label: "Subcat. type:",
                    name: "campaigns.setup_amazon"
                }
            ]
        } ),

    // Activate an inline edit on click of a table cell
    // or a DataTables Responsive data cell
    $('#example').on( 'click', 'tbody td:not(.child), tbody span.dtr-data', function (e) {
        // Ignore the Responsive control and checkbox columns
        if ( $(this).hasClass( 'control' ) || $(this).hasClass('select-checkbox') ) {
            return;
        }

         <?PHP
        if (loadRoles($userID) == 5)
        {
        ?>
            editor.inline( this );
        <?PHP
        }
        ?>

        
    } ),



 $('#example').DataTable({
    //update td color depending on valor
    "rowCallback": function( row, data, index ) {
            // STATUS ADVERTISER
            // if ( data.costes.status == "1" ) {
            //     $("td:eq(11)", row).removeClass( 'fact-validadanok' ); 
            //     $("td:eq(11)", row).addClass( 'fact-facturado' ); 
            // } 
            // if ( data.costes.status == "2" ) {  
            //     $("td:eq(11)", row).removeClass( 'fact-facturado' ); 
            //     $("td:eq(11)", row).addClass( 'fact-validadanok' );  
            // }

            

            
        },

         "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 : 
                    typeof i === 'number' ?
                        i : 0;
            },
            // Total over all pages
            IntMail = api
                .column( 3, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

          

            // Update footer
            // $( api.column( 3 ).footer() ).html(
            //     '<span class="results">'+(IntMail).toFixed(2)+'</span>'
            // );
           
            
        },
         
        responsive: true,
        scrollY: 550,
        bPaginate: false, 
        dom: "Bfrtip",
          columnDefs: [
        { "visible": false, "targets": 1 },
        ],
        deferRender: true,
        ajax: "examples/php/campaigns_db.php",
        columns: [

            {   // Responsive control column
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
        // {   // costs discrepancy.
            //     data: function ( data, type, row ) {
            //         if(data.campaigns_month_close.plataff == 0)
            //         {return data.campaigns_month_close.plataff; }
            //         else {
            //             var discrepancy_total = +data.campaigns_month_close.plataff- + data.campaigns_month_close.totaldollar ;
            //             return discrepancy_total;
            //         }
            //     }
            // },
            // { data: "campaigns_month_close.validacion", render: function ( data, type, row ) {
            // if ( row.campaigns_month_close.validacion === '€' ) {
            //     return euroRenderer(data); } return dollarRenderer( data );} },
            { data: "campaigns.date" },
             { data: null, render: function ( data, type, row ) {
                // Combine the first and last names into a single table field
                return data.advertisers.nombre+' <br><span class="RS">RS: '+data.advertisers.social+'</span>';
            } },
            { data: "campaigns.url" },
            { data: "campaigns.name_native" },
            { data: "campaigns.name_english" },
            { data: "campaigns.status" },
            { data: "campaigns.date_created" },
            { data: "campaigns.date_started"}, 
            { data: "campaigns.currency"},
            { data: "country.name", editField: "campaigns.country"},
            { data: "accountmanager.nombre", editField: "campaigns.key_account_manager"},
            { data: "campaigns.domain_list" },
            { data: "campaigns.alias" },
            { data: "campaigns.category_id" },
            { data: "campaigns.category_label" },
            { data: "campaigns.category_type" },
            { data: "campaigns.subcategory_type" }
        ],
        order: [ 2, 'asc' ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        buttons: [
        <?PHP
            if (loadRoles($userID) == 5)
            {
            ?>
            { extend: "create", editor: editor },
            { extend: "edit",   editor: editor },
            { extend: "remove", editor: editor },
            <?PHP
            }
            ?>
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ]
            }
        ],
        
         
    });
            //call draw table when new date is selected
         $('#min').change( function() {
                table.draw();
            } );
        // highlights table
          var table = $('#example').DataTable();
        $('#example tbody')
            .on( 'mouseenter', 'td', function () {
                //deprecated
            } );

        $('#example tbody').on( 'click', 'td', function () {
        // deprecated
        } )
});
    </script>

</head>
<body>
    <div class="row full-width wrapper">
        <div class="large-12 columns">
            <div id="top-menu">
                <div class="row">
                    <div class="large-2 medium-4 small-12 columns top-part-no-padding">
                        <div class="logo-bg">
                            <img src="img/logo.png" alt="logo"/>
                            <i class="fi-list toggles" data-toggle="hide"></i>
                        </div>
                    </div>
                    <div class="large-10 medium-8 small-12 columns top-menu">
                        <div class="row">
                            <div class="large-6 medium-6 small-12 columns">
                                <div class="row">
                                    <div class="large-8 columns">
                                        <input id="Text1" type="text" class="search-text" placeholder="Search" />
                                    </div>
                                </div>
                            </div>
                            <div class="large-4 medium-6 small-12 columns text-center">
                                <div class="row">
                                    <div class="medium-3 small-3 columns">
                                        <div class="notification">
                                            <i class="fi-mail"></i>
                                            <span class="mail">4</span>
                                        </div>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                        <div class="notification">
                                            <i class="fi-megaphone"></i>
                                            <span class="megaphone">5</span>
                                        </div>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                        <!--<img src="img/32.jpg" alt="picture" class="top-bar-picture" />-->
                                        Hi 
                                            <?php
                                            $details = $LS->getUser();
                                            echo $details['username'] ,"!";
                                            ?>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                       <a href="logout.php"> <i class="fi-power power-off"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="no-padding">
                    <div class="large-2 medium-12 small-12 columns">
                        <!-- menu -->
                        <?PHP include'menu.php';?>
                    </div>
                </div>
                <div class="large-10 medium-12 small-12 columns light-grey-bg-pattern">
                    <div class="row">
                        <div class="large-5 columns">
                            <div class="page-name">
                                <h3 class="left">CAMPAIGNS</h3>
                            </div>
                        </div>
                        <div class="large-7 columns" style="margin-top: 10px;">
                                <!-- datepicker -->
                                        <div class="row collapse date" id="dpMonths" data-date-format="yyyy-mm-dd" data-start-view="year" data-min-view="year" style="max-width:200px;">
                                <div class="small-2 columns">   
                                    <span class="prefix"><i class="fa fa-calendar"></i></span>
                                </div>
                                <div class="small-10 columns">
                                    <input type="text" size="16" value="<?=date('Y-m-d', strtotime("first day of -1 month")); ?>" id="min" name="min" >  
                                </div>
                            </div>
                            <!-- end datepicker -->
                        </div>
                             
                            

                    </div>
                    <!-- tables -->
                    <div id="staff">
                         
                        <table id="example" class="display responsive" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <!-- 0 --><th></th>
                                    <!-- 1 --><th></th>
                                    <!-- 2 --><th>Client</th>
                                    <!-- 3 --><th>URL</th>
                                    <!-- 4 --><th>Name Native</th>
                                    <!-- 5 --><th>Name En.</th>
                                    <!-- 6 --><th>Status</th>
                                    <!-- 7 --><th>Date Created</th>
                                    <!-- 8 --><th>Date Started</th>
                                    <!-- 9 --><th>Currency</th>
                                    <!-- 10 --><th>Country</th>
                                    <!-- 11 --><th>AM.</th>
                                    <!-- 12 --><th>Domain List</th>
                                    <!-- 13 --><th>Alias</th>
                                    <!-- 14 --><th>Category</th>
                                    <!-- 15 --><th>Cat. label</th>
                                    <!-- 16 --><th>Cat. Type</th>
                                    <!-- 17 --><th>Subcat. Type</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <!-- 0 --><th></th>
                                    <!-- 1 --><th></th>
                                    <!-- 2 --><th></th>
                                    <!-- 3 --><th></th>
                                    <!-- 4 --><th></th>
                                    <!-- 5 --><th></th>
                                    <!-- 6 --><th></th>
                                    <!-- 7 --><th></th>
                                    <!-- 8 --><th></th>
                                    <!-- 9 --><th></th>
                                    <!-- 10 --><th></th>
                                    <!-- 11 --><th></th>
                                    <!-- 12 --><th></th>
                                    <!-- 13 --><th></th>
                                    <!-- 14 --><th></th>
                                    <!-- 15 --><th></th>
                                    <!-- 16 --><th></th>
                                    <!-- 17 --><th></th>
                                </tr>
                                <tr>
                                    <!-- 0 --><th></th>
                                    <!-- 1 --><th></th>
                                    <!-- 2 --><th>Client</th>
                                    <!-- 3 --><th>URL</th>
                                    <!-- 4 --><th>Name Native</th>
                                    <!-- 5 --><th>Name En.</th>
                                    <!-- 6 --><th>Status</th>
                                    <!-- 7 --><th>Date Created</th>
                                    <!-- 8 --><th>Date Started</th>
                                    <!-- 9 --><th>Currency</th>
                                    <!-- 10 --><th>Country</th>
                                    <!-- 11 --><th>AM.</th>
                                    <!-- 12 --><th>Domain List</th>
                                    <!-- 13 --><th>Alias</th>
                                    <!-- 14 --><th>Category</th>
                                    <!-- 15 --><th>Cat. label</th>
                                    <!-- 16 --><th>Cat. Type</th>
                                    <!-- 17 --><th>Subcat. Type</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- end tables -->
                </div>
            </div>
        </div>
    </div>

    
    <script src="js/menu.js"></script>
    <script>
        $(document).foundation();     
    </script>
    <script>
   $(function () {
        // datepicker limited to months
        $('#dpMonths').fdatepicker();

    });
    </script>

</body>
</html>

<!-- Localized -->