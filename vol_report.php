<?php
require "config.php";
if( isset($_POST['newName']) ){
    $_POST['newName'] = $_POST['newName'] == "" ? "Dude" : $_POST['newName'];
    $LS->updateUser(array(
        "name" => $_POST['newName']
    ));
}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>VOLUUM REPORT</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/foundation.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/foundation-icons/foundation-icons.css" rel="stylesheet" />
    <script src="js/vendor/modernizr.js"></script>
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/flat-icon/flaticon.css" rel="stylesheet" />
    <link href="css/tree.min.css" rel="stylesheet" />
    <!-- datepicker -->
        <script src="js/foundation.min.js"></script>
    <script src="js/vendor/modernizr.js"></script>
    <link rel="stylesheet" type="text/css" href="css/foundation-datepicker.min.css"><!---->
    <!-- languaje 
     FOR ENGLISH OF GB, and change script down to en-GB//   <script src="js/locales/foundation-datepicker.en-GB.js"></script>-->
    <script src="js/locales/foundation-datepicker.es.js"></script>
    <!-- date picker Foundation -->
    <script type="text/javascript" language="javascript" src="js/foundation-datepicker.js"></script><!---->
</head>
<body>
    <div class="row full-width wrapper">
        <div class="large-12 columns content-bg">
            <div id="top-menu">
                <div class="row">
                    <div class="large-2 medium-4 small-12 columns top-part-no-padding">
                        <div class="logo-bg">
                            <img src="img/logo.png" alt="logo"/>
                            <i class="fi-list toggles" data-toggle="hide"></i>
                        </div>
                    </div>
                    <div class="large-10 medium-8 small-12 columns top-menu">
                        <div class="row">
                            <div class="large-6 medium-6 small-12 columns">
                                <div class="row">
                                    <div class="large-8 columns">
                                        <input id="Text1" type="text" class="search-text" placeholder="Search" />
                                    </div>
                                </div>
                            </div>
                            <div class="large-4 medium-6 small-12 columns text-center">
                                <div class="row">
                                    <div class="medium-3 small-3 columns">
                                        <div class="notification">
                                            <i class="fi-mail"></i>
                                            <span class="mail">4</span>
                                        </div>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                        <div class="notification">
                                            <i class="fi-megaphone"></i>
                                            <span class="megaphone">5</span>
                                        </div>
                                    </div>
                                     <div class="medium-3 small-3 columns">
                                        <!--<img src="img/32.jpg" alt="picture" class="top-bar-picture" />-->
                                        Hi 
                                            <?php
                                            $details = $LS->getUser();
                                            echo $details['username'] ,"!";
                                            ?>
                                    </div>
                                    <div class="medium-3 small-3 columns">
                                       <a href="logout.php"> <i class="fi-power power-off"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="no-padding">
                    <div class="large-2 medium-12 small-12 columns">
                        <!-- menu -->
                        <?PHP include'menu.php';?>
                    </div>
                </div>
                <div class="large-10 medium-12 small-12 columns light-grey-bg-pattern">
                    <div class="row">
                        <div class="large-10 columns">
                            <div class="page-name">
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- <div id="form">
                        <div class="row">
                            <div class="large-12 columns">
                                <div class="custom-panel">
                                    <div class="custom-panel-heading">
                                        <h4>VOLUUM REPORT</h4>
                                    </div>
                                    <div class="custom-panel-body">
                                           
                                    
                                            <div>
                                                 <?PHP
                                                 if(isset($_GET['status']))
                                                 {
                                                    if($_GET['status'] == 'ok')
                                                    {
                                                        echo "<span style='color:green; font-size:20px;'>Voluum costs successfully imported.</span>";
                                                    }
                                                    else echo "<span style='color:red; font-size:20px;'>Something went wrong, please contact technical service.</span>";
                                                 }
                                                  ?>
                                            </div>
                                        <div class="clearfix">
                                            <?PHP
                                            if(isset($_POST["submit"]))
                                            {
                                                $fecha = $_POST['min'];
                                                // Connect to the MySQL server and select the corporate database
                                                include('conect.php');
                                                //document name and extension.
                                                $filename=$_FILES["file"]["name"];
                                                //save on temporal
                                                $filename2=$_FILES['file']['tmp_name'];
                                                //get extension
                                                $ext=substr($filename,strrpos($filename,"."),(strlen($filename)-strrpos($filename,".")));

                                                //we check,file must have .csv extention
                                                if($ext==".csv" && $fecha!="")
                                                {
                                                    // Open and parse the sales.csv file
                                                    $file = fopen($filename2, "r");
                                                    while ($emapData = fgetcsv($file, 10000, ";")) 
                                                    {
                                                        //check if advertiser exists
                                                        $sqlInfo = "SELECT id FROM affiliates WHERE nombre LIKE '%$emapData[0]%';";
                                                          // echo '<u>Select advertiser ID:</u> '.$sqlInfo .'<br>';
                                                          $result = $con->query($sqlInfo);
                                                        //IF YES
                                                        if(mysqli_num_rows($result) > 0 )
                                                        { 
                                                            //OPTION ADVERTISER EXIST 
                                                            while($row = $result->fetch_assoc()) 
                                                            {
                                                                //insert on Incomes
                                                                $sqlIncomes = "INSERT INTO costes(`date`,`affiliate`,`totaldollar`) VALUES ('$fecha','".$row['id']."','$emapData[1]');";
                                                                  // echo "Si existe: ".$sqlIncomes."<br>";
                                                                 $insertIncome = $con->query($sqlIncomes);
                                                            }
                                                        }
                                                        else //if not exists first create the advertiser and insert on incomes with the new ID
                                                        {
                                                            $queryNewAdv = "INSERT INTO affiliates(nombre,social,affm) VALUES ('$emapData[0]','', '4');";
                                                              // echo 'insert advertiser: '. $queryNewAdv .'<br>';
                                                             $insertAdv = $con->query($queryNewAdv);
                                                            $NewID = $con->insert_id;
                                                             // echo 'NUEVO ID: '. $NewID .'<br>';
                                                            $sqlIncomes = "INSERT INTO costes(date,affiliate,totaldollar) VALUES ('$fecha','$NewID','$emapData[1]');";
                                                              // echo 'insert income: '.$sqlIncomes.'<br>';
                                                             $insertIncome = $con->query($sqlIncomes);
                                                        }
                                                        
                                                    }
                                                    fclose($file);
                                                         echo "<span style='color:green; font-size:20px;'>CSV File has been successfully Imported.</span>";
                                                }
                                                else if($ext!=".csv"){
                                                    echo "<span style='color:red; font-size:20px;'>Error: Please Upload only CSV File</span>";
                                                }
                                                else if($fecha==""){
                                                    echo "<span style='color:red; font-size:20px;'>Error: Please select a date</span>";
                                                }

                                                $con->close();
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->

                <div class="row">
                        <div class="large-12 columns">
                            <div class="page-name">
                                <h3 class="left">VOLUUM REPORT</h3>
                            </div>
                        </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-3 col-md-3 large-2 columns">
                        <label for="exampleFormControlSelect1">DATE START</label>
                         <!-- datepicker -->
                        <input type="text" class="span2" value="" data-date-format="yyyy-mm-dd" id="dpd1">
                        <!-- end datepicker -->
                    </div>
                    <div class="col-sm-3 col-md-3 large-2 columns">
                        <label for="exampleFormControlSelect1">DATE END</label>
                        <input type="text" class="span2" value="" data-date-format="yyyy-mm-dd" id="dpd2">
                    </div>
                    <div class="col-sm-2 col-md-3 large-2 columns">
                        <label for="exampleFormControlSelect1">COUNTRY</label>
                        <select class="form-control" id="country">
                         <option value="0" selected>SELECT...</option>
                        </select>
                    </div>
                    <div class="col-sm-3 col-md-3 large-2 columns">
                        <label for="exampleFormControlSelect1">OFFERS</label>
                        <select class="form-control" id="offer">
                         <option value="0" selected>SELECT...</option>
                        </select>
                    </div>
                    <div class="col-sm-3 col-md-3 large-2 columns">
                        <label for="exampleFormControlSelect1">CAMPAIGNS</label>
                        <select class="form-control" id="campaign">
                         <option value="0" selected>SELECT...</option>
                        </select>
                    </div>
                    <div class="col-sm-3 col-md-3 large-2 columns" style="padding-top: 17px;padding-right: 5%;">
                        <input type="submit" class="button tiny radius right coral-bg sending" value="SEND" />
                    </div>
                </div>
                <div class="row">
                 <div class="col-sm-12 col-md-12 large-12 columns" id="loader" style="text-align: center;">

                 </div>
                </div>
                <hr>
                <div class="row" >
                 <div class="col-sm-12 col-md-12 large-12 columns" style="text-align: center;">
                      <table style="width: 100%;">
                         <tbody id="table_information">

                        </tbody>
                      </table>
                 </div>
                </div>

            </div>
        </div>
    </div>

    <script src="js/menu.js"></script>
    <script>
        $(document).foundation();
    </script>  

    <script>
   $(function () {
        // implementation of disabled form fields
                var nowTemp = new Date();
                var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
                var checkin = $('#dpd1').fdatepicker({
                    onRender: function (date) {
                        return date.valueOf() < now.valueOf() ? '' : '';
                    }
                }).on('changeDate', function (ev) {
                    if (ev.date.valueOf() > checkout.date.valueOf()) {
                        var newDate = new Date(ev.date)
                        newDate.setDate(newDate.getDate() + 1);
                        checkout.update(newDate);
                    }
                    checkin.hide();
                    $('#dpd2')[0].focus();
                }).data('datepicker');
                var checkout = $('#dpd2').fdatepicker({
                    onRender: function (date) {
                        return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
                    }
                }).on('changeDate', function (ev) {
                    checkout.hide();
                }).data('datepicker');

    });
    </script>  

<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>