-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:8889
-- Tiempo de generación: 11-10-2017 a las 18:21:09
-- Versión del servidor: 5.6.35
-- Versión de PHP: 7.0.15


SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `netsaleserp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accountmanager`
--

CREATE TABLE `accountmanager` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `accountmanager`
--

INSERT INTO `accountmanager` (`id`, `nombre`) VALUES
(2, 'Kamila Krajnik'),
(3, 'Camilla Rolando');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `advertisers`
--

CREATE TABLE `advertisers` (
  `id` int(2) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `social` varchar(200) DEFAULT NULL,
  `id_am` varchar(30) NOT NULL DEFAULT 'Camilla',
  `money` varchar(2) NOT NULL DEFAULT '$',
  `cod_client` int(12) DEFAULT NULL,
  `nation` varchar(50) DEFAULT NULL,
  `contact_mail` varchar(200) DEFAULT NULL,
  `CifDni` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `notes` varchar(1000) DEFAULT 'Notes...',
  `address` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `advertisers`
--

INSERT INTO `advertisers` (`id`, `nombre`, `social`, `id_am`, `money`, `cod_client`, `nation`, `contact_mail`, `CifDni`, `phone`, `state`, `notes`, `address`) VALUES
(1, '', 'FENIX DEALS C.C.', '1', '$', 1, '', '', 'B86305513', '', 'Madrid', '', ''),
(2, '', 'RAPPELS PENDIENTES DE EMITIR', '1', '$', 2, '', '', '', '', '', '', ''),
(3, 'IBRANDS MEDIOS INTERACTIVOS S.L.', 'IBRANDS MEDIOS INTERACTIVOS, S.L.', '1', '$', 430000001, '', 'ana.escuris@ibrands.es', 'B85718880', '', 'Madrid', '', ''),
(4, '', 'VISTA PRINT ESPA—A, S.L.', '1', '$', 430000002, '', '', 'B83734319', '93.545.9851', 'Barcelona', '', ''),
(5, '', 'ULISES INTERACTIVE, S.L.', '1', '$', 430000003, '', '', 'B64133424', '934926960', 'Barcelona', '', ''),
(6, '', 'DIGITAL DISTRIBUTION MANAGEMENT SL', '1', '$', 430000004, '', 'facturacion@digidis.net', 'B84276773', '911108765', 'Madrid', '', ''),
(7, 'T2O Admedia Services, S.L.', 'T20 ADMEDIA SERVICES S.L.', '1', '$', 430000006, '', 'Finanzas@t2o.es', 'B83859850', '915358066', 'Madrid', '', ''),
(8, 'NIVORIA SOLUTIONS S.L.', 'NIVORIA SOLUTIONS, S.L.', '1', '$', 430000007, '', 'invoice@nivoria.com', 'B85166932', '935305975', 'Barcelona', '', ''),
(9, 'AFFILINET ESPA—A', 'AFFILINET GMBH', '1', '$', 430000008, '', 'affilinet_es1@mailgb.custhelp.com', '192148952', '', 'M¸nchen', '', ''),
(10, '', 'EGENTIC GMBH', '1', '$', 430000009, '', 'joana.pizarro@egentic.com', '215956112', '', 'Taunus', '', ''),
(11, 'Digital Media Publicidad Marketing y DistribuciÛn, S.L.', 'DIGITAL MEDIA PUBLICIDAD Y MARKETIN', '1', '$', 430000010, '', 'rgarcia@digitalmedia-comunicacion.es', 'B85178788', '915631164', 'Madrid', '', ''),
(12, '', 'MAMVO PERFORMANCE, S.L.U.', '1', '$', 430000011, '', 'aalvarez@antevenio.com', 'B81440810', '914149192', 'Madrid', '', ''),
(13, '', 'TELNET MARKETING AG', '1', '$', 430000012, '', 'sascha.badelt@gmail.com ', '308233236', '', 'Glarus', '', ''),
(14, 'Kwanko (Netaffiliation)', 'KWANKO S.A.', '1', '$', 430000013, '', 'ingrid.beaurin@kwanko.com', '50440546885', '910086102', 'Bourg-La-Reine', '', ''),
(15, 'KARPE DEAL S.L', 'KARPE DEAL S.L.', '1', '$', 430000014, '', '', 'B86305513', '', 'Madrid', '', ''),
(16, '', 'COME AND STAY SPAIN, S.L.', '1', '$', 430000015, '', '', 'B85106508', 'Tel: +34 91 534', 'Madrid', '', ''),
(17, 'E-RetailAdvertising', 'E-RETAIL ADVERTISING, S.L.', '1', '$', 430000016, '', 'amartin@e-retailadvertising.com', 'B85442283', '911890586', 'Madrid', '', ''),
(18, '', 'ZIZER COZY MARKETERS S.L.', '1', '$', 430000018, '', 'fabio@zizer.es', 'B86774924', '', 'Madrid', '', ''),
(19, 'Traffic Control OU', 'TRAFFIC CONTROL S.A', '1', '$', 430000019, '', 'joy@credy.eu', '132329', '', 'Geneva', '', ''),
(20, 'Meetic', 'MEETIC SAS', '1', '$', 430000020, '', 'comptabilite@meetic-corp.com', '88439780339', '', 'Paris', '', ''),
(21, 'La Torre Wallace S.L.', 'LA TORRE WALLACE, S.L.', '1', '$', 430000021, '', 'cgalloy@ltw.es', 'B85142859', '914686013', 'Madrid', '', ''),
(22, 'ARENA MEDIA COMUNICATIONS ESPA—A , S.A', 'ARENA MEDIA COMUNICATIONS ESPA—A, SA', '1', '$', 430000022, '', 'proveedores.havasmedia@havasmedia.com', 'A80537327', '914569007', 'Madrid', '', ''),
(23, '', 'EFFILIATION', '1', '$', 430000025, '', '', '88432831550', '', 'Paris', '', ''),
(24, '', 'PRISA DIGITAL', '1', '$', 430000026, '', '', 'B82649690', '', 'Madrid', '', ''),
(25, '', 'THE OPTION MACHINE', '1', '$', 430000027, '', '', '41514422179', '33182834093', 'Paris', '', ''),
(26, '', 'LATITUDE PUB, SARL', '1', '$', 430000028, '', '', '35493176655', '', 'Nice', '', ''),
(27, 'Demalia S.L.', 'DEMALIA S.L.', '1', '$', 430000029, '', 'garcia@demalia.com', 'B66450909', '', 'Valencia', '', ''),
(28, '', 'SPARTOO SAS', '1', '$', 430000030, '', '', '26489895821', '', 'Grenoble', '', ''),
(29, 'R Advertising', 'REACTIVEPUB (R-ADVERTISING)', '1', '$', 430000031, '', 'daf@r-advertising.com', '80502207079', '', 'Mougins', '', ''),
(30, '', 'SELECTA PERFUMES, S.L.', '1', '$', 430000032, '', '', 'B99276453', '', 'Zaragoza', '', ''),
(31, '', 'EDM EINFACH DIREKT MEDIA GMBH', '1', '$', 430000033, '', '', '281169270', '', 'Berlin', '', ''),
(32, '', 'WALMERIC SOLUCIONES S.L.', '1', '$', 430000034, '', 'alba.calvillo@walmeric.com', 'B85144350', '', 'Madrid', '', ''),
(33, '', 'HEADWAY DIGITAL', '1', '$', 430000035, '', '', '217208190012', '', 'Montevideo', '', ''),
(34, '', 'ORGANIC ADVANCED COMPANY', '1', '$', 430000036, '', 'francisco@dooplan.com', 'B64804644', '932776941', 'BARCELONA', '', ''),
(35, '', 'PROCENET VIRTUAL, S.L.', '1', '$', 430000037, '', '', 'B83654723', '', 'Madrid', '', ''),
(36, 'Public-idees sa', 'PUBLIC-IDEES, S.A.', '1', '$', 430000039, '', 'contabilidad@public-ideas.com', '33481117000', 'TÈl. : +34 91 4', 'Levallois Perret', '', ''),
(37, '', 'SECURITAS DIRECT S.A.U', '1', '$', 430000040, '', '', 'A26106013', '', 'Madrid', '', ''),
(38, 'SEAS', 'SEAS, ESTUDIOS SUPERIORES ABIERTOS,†SAU', '1', '$', 430000041, '', 'aitorbenede@estudiosabiertos.com', 'A50973098', '976 700 660', 'Zaragoza', '', ''),
(39, 'XTRADE LTD', 'YANGA MEDIA SARL', '1', '$', 430000042, '', 'admin@yangamedia.com', '29519025019', '', 'Boulogne Billancourt', '', ''),
(40, '', 'LEAD MEDIA PERFORMANCE MARKETING, SL', '1', '$', 430000043, '', '', 'B86211901', '+34 91 411 49 4', 'Madrid', '', ''),
(41, '', 'FENIX DATA SL', '1', '$', 430000044, '', '', 'B86185170', '', 'Madrid', '', ''),
(42, '', 'MAXIDAT SL', '1', '$', 430000045, '', '', 'B86185204', '', 'Madrid', '', ''),
(43, '', 'DUJOK ICONSULTING SL', '1', '$', 430000046, '', '', 'B86287794', '', 'Madrid', '', ''),
(44, 'Interneting MCO , S.L.', 'INTERNET, MARKETING Y CONTENIDOS ONLINE ', '1', '$', 430000047, '', 'sferrer@interneting-mco.com', 'B86127560', '610492595', 'Madrid', '', ''),
(45, 'Epsilon Built Holdings Ltd', 'EPSILON BUILT HOLDINES LIMITED SKA', '1', '$', 430000048, '', 'a.bartkowiak@intredo.com', '5252567826', '', 'VARSOVIA', '', ''),
(46, 'Kanlli Publicidad SA', 'KANLLI PUBLICIDAD, S.A.', '1', '$', 430000049, '', '', 'A84733070', '+34 917 259 220', 'Madrid', '', ''),
(47, '', 'VERTIGO MEDIA S.L. (BEVERLY HILLS)', '1', '$', 430000050, '', 'accountspayable@beverlyhills-editions.com', 'B65333346', '', 'Barcelona', '', ''),
(48, '', 'ELIXIS MEDIA, SL', '1', '$', 430000051, '', 'glouis-luoisy@elixis.com', 'B65392441', '931 65 11 01', 'Barcelona', '', ''),
(49, 'Adpepper Media Spain S.L.', 'AD PEPPER MEDIA SPAIN, S.A.', '1', '$', 430000052, '', 'fruiz@adpepper.com', 'A61927117', '', 'Madrid', '', ''),
(50, '', 'MARKETING ADJAL, S.L.', '1', '$', 430000053, '', '', 'B22348544', '974234490', 'Huesca', '', ''),
(51, 'PERMISSION LEAD S.L.', 'PERMISSIONLEAD', '1', '$', 430000054, '', 'contabilidad@permissionlead.com', 'B64921935', '933622222', 'Barcelona', '', ''),
(52, 'Natexo Spain', 'NATEXO SPAIN, S.L.', '1', '$', 430000056, '', 'Invoicing_Spain@natexo.com', 'B65729741', '932385408', 'Barcelona', '', ''),
(53, '', 'DANISPORT SIGLO XXI, S.L.', '1', '$', 430000058, '', '', 'B84464825', '', 'Madrid', '', ''),
(54, '', 'PRODUCCIONES LO NUNCA VISTO, SLU', '1', '$', 430000060, '', '', 'B84242858', '918506535', 'Madrid', '', ''),
(55, 'Linkemann S.L.', 'LINKEMANN, S.L.', '1', '$', 430000061, '', 'delivery@linkemann.com', 'B65512196', '93 611 54 66', 'Barcelona', '', ''),
(56, '', 'TRADEDOUBLER ESPA—A, S.L.', '1', '$', 430000062, '', '', 'B82666892', '', 'Madrid', '', ''),
(57, '', 'QUISMA SPAIN, SLU', '1', '$', 430000063, '', 'lucia.uriarte@xaxis.com', 'B86189560', '34 917361080', 'Madrid', '', ''),
(58, 'Zanox', 'ZANOX', '1', '$', 430000064, '', 'pagos@zanox.com', '209981705', '', 'Berlin', '', ''),
(59, '', 'VODAFONE ONO†SAU', '1', '$', 430000065, '', '', 'A62186556', '', 'Madrid', '', ''),
(60, '', 'OFFERUM, S.L.', '1', '$', 430000066, '', 'albert.sanclemente@offerum.com', 'B65296402', '', 'Barcelona', '', ''),
(61, '', 'GENESIS SEGUROS GENERALES, S.A.', '1', '$', 430000067, '', '', 'A79409058', '', 'Madrid', '', ''),
(62, 'IMF International Bussiness School SL', 'IMF INTERNATIONAL BUSINESS SCHOOL', '1', '$', 430000068, '', 'igarcia@imf-formacion.com', 'B83074146', '913645157 (Ext.', 'Madrid', '', ''),
(63, 'Inkamarketing', 'INKA MARKETING S.L.', '1', '$', 430000069, '', 'dmartin@inkamarketing.es', 'B85795847', '914153447', 'Madrid', '', ''),
(64, '', 'IGNASI SALVADOR JOU', '1', '$', 430000070, '', '', '77615799E', '', 'Barcelona', '', ''),
(65, '', 'ZIELTRAFFIC', '1', '$', 430000072, '', '', 'B84806629', '', 'Madrid', '', ''),
(66, '', 'EUROPALEADS GENERATION, S.L.', '1', '$', 430000073, '', '', 'B65442493', '', 'Barcelona', '', ''),
(67, '', 'PINK STONE ADVERTISEMENT GROUP', '1', '$', 430000074, '', 'comercial@pinkstone.es', 'B85631661', '916.027.264', 'Madrid', '', ''),
(68, '', 'SURVEY SAMPLING INTERNATIONAL, LLC', '1', '$', 430000075, '', '', '810468293B01', '', 'Rotterdam', '', ''),
(69, '', 'CALLWIN 900, S.L.', '1', '$', 430000077, '', '', 'B86237625', '', 'Madrid', '', ''),
(70, '', 'OPTIMUM MEDIA DIRECTION, SLU', '1', '$', 430000078, '', '', 'B83228783', '', 'Madrid', '', ''),
(71, '', 'VORTEX MEDIA LIMITED', '1', '$', 430000079, '', '', '59345227000', '', 'Hong Kong', '', ''),
(72, '', 'FAST TRACK NETWORK SL', '1', '$', 430000080, '', '', 'B86296712', '', 'Madrid', '', ''),
(73, '', 'FINTEL MARKETING,S.L.', '1', '$', 430000081, '', 'alexander@fintelmarketing.com', 'B86420213', '', 'Madrid', '', ''),
(74, '', 'SHERRYTEL SOLUCIONES, S.L.', '1', '$', 430000082, '', 'jpriofrio@sherrytel.com', 'B11885936', '', 'C·diz', '', ''),
(75, '', 'SANITAS NUEVOS NEGOCIOS, S.L.', '1', '$', 430000083, '', 'efabe@sanitas.es', 'B86331253', '', 'Madrid', '', ''),
(76, 'OMNICOM MEDIA GROUP, S.L.', 'OMNICOM MEDIA GROUP, S.L.', '1', '$', 430000084, '', 'almudena.garrido@omnicommediagroup.com', 'B64055429', '91 789 00 09', 'Madrid', '', ''),
(77, '', 'ARKEERO MEDIA S.L.', '1', '$', 430000085, '', '', 'B86582236', '915987836', 'Madrid', '', ''),
(78, '', 'GROUPALIA COMPRA COLECTIVA, S.L.', '1', '$', 430000086, '', 'carlos.garcia@groupalia.com', 'B65291593', '93 159 31 00 (E', 'Barcelona', '', ''),
(79, '', 'KIABI', '1', '$', 430000087, '', '', 'N0012788F', '', 'Hem', '', ''),
(80, '', 'SINERTEC', '1', '$', 430000088, '', '', 'B82700154', '', 'Madrid', '', ''),
(81, 'Antevenio - Mamvo Performance S.L.U', 'ANTEVENIO ESP S.L.U', '1', '$', 430000089, '', 'administracion@es.antevenioperformance.com', 'B85737708', '', 'Madrid', '', ''),
(82, '', 'CLICKPOINT PUBLICIDAD, S.L.U.', '1', '$', 430000090, '', '', '122053272', '', 'Londres', '', ''),
(83, '', 'POWERSPACE SPAIN S.L.', '1', '$', 430000091, '', 'finance@powerspace.com', 'B65023855', '', 'Barcelona', '', ''),
(84, 'GRUPOGATES, FORMACI”N EMPRESARIAL Y MASTERS, S.L.', 'GRUPO GATES', '1', '$', 430000092, '', 'grupogates@grupogates.com', 'B82892134', '', 'Madrid', '', ''),
(85, '', 'ATRAPALO, S.L.', '1', '$', 430000093, '', '', 'B62288568', '', 'Barcelona', '', ''),
(86, '', 'MEDIACONTACTS BARCELONA', '1', '$', 430000094, '', 'felip.bergada@es.mediacontacts.com', 'A81586893', '', 'Madrid', '', ''),
(87, '', 'SARENZA', '1', '$', 430000095, '', '', '6480188507', '', 'Paris', '', ''),
(88, '', 'ADMS (AEGON DIRECT MARKETING SERVICES EU', '1', '$', 430000096, '', '', 'W0066897J', '917035409', 'Madrid', '', ''),
(89, '', 'RANDOM APS', '1', '$', 430000097, '', '', '32356710', '', 'Aalborg', '', ''),
(90, 'Double 6', 'DOUBLE 6', '1', '$', 430000099, '', 'anayancy@digital-6.es', '490373776', '', 'Lausanne', '', ''),
(91, '', 'PICARISIMO, S.L.', '1', '$', 430000100, '', '', 'B86305596', '', 'Madrid', '', ''),
(92, '', 'ARGONAS (CLICKO D.O.O.)', '1', '$', 430000102, '', '', 'B64133424', '', 'Barcelona', '', ''),
(93, '', 'ATRAKTOR AD OPERATORS, S.L.', '1', '$', 430000103, '', '', 'B85065985', '', 'Hong-Kong', '', ''),
(94, '', 'MARTINEZ Y HERVAS MARKETING, S.L.', '1', '$', 430000104, '', '', 'B83529438', '', 'Madrid', '', ''),
(95, '', 'MAS MOVIL TELECOM 3.0, S.A.', '1', '$', 430000105, '', '', 'A84633643', '', 'Madrid', '', ''),
(96, '', 'IBERIS GLOBAL MEDIA', '1', '$', 430000106, '', '', 'B86014008', '', 'Madrid', '', ''),
(97, 'Aprendemas (Virtuatenea)', 'VIRTUATENEA, S.L.', '1', '$', 430000107, '', 'administracion@aprendemas.com', 'B84880467', '916364200', 'Madrid', '', ''),
(98, '', 'SUPER COMPARADOR DE CONSUMO, S.L.', '1', '$', 430000108, '', '', 'B65212839', '', 'Barcelona', '', ''),
(99, '', 'OFFER SERVICE BV', '1', '$', 430000109, '', '', '851891743B01', '+ 31 6 53 12 42', 'Amsterdam', '', ''),
(100, '', 'PERFORMACE ADVERTISING, S.L. (MATOMY)', '1', '$', 430000110, '', '', 'B85853810', '91 732 0673', 'Madrid', '', ''),
(101, 'BigBank AS Consumer Finance S.E.', 'BIGBANK AS CONSUMER FINANCE SUCURSAL EN ', '1', '$', 430000111, '', 'laura.suanez@bigbank.es', 'W0531072G', '91 140 19 38', 'Madrid', '', ''),
(102, '', 'MUNDOSABOR SIGLO XXI, S.L.', '1', '$', 430000112, '', '', 'B65481236', '', 'Barcelona', '', ''),
(103, 'Dataondemand', 'DATA ON DEMAND', '1', '$', 430000113, '', '', '30791086028', '', 'Paris', '', ''),
(104, '', 'ARKEERO (DULMIDA S.L.)', '1', '$', 430000114, '', 'mcerrada@arkeero.com', 'B86582236', '', 'Madrid', '', ''),
(105, '', 'WALVA MARKETING ONLINE', '1', '$', 430000115, '', 'facturacion@walva.com', 'B86133055', '', 'Madrid', '', ''),
(106, '', 'ZALANDO GMBH', '1', '$', 430000116, '', 'samantha.wnuk@zalando.de', '260543043', '', 'Berlin', '', ''),
(107, 'ACTIPLAY ITALIA SRL', 'CLICKADV SRL', '1', '$', 430000117, '', 'carmen.mattiello@payclick.it', '6116871218', '', '', '', ''),
(108, '', 'BELEADER INTERNET MARKETING, S.L.', '1', '$', 430000118, '', '', 'B85218154', '902540025', 'Madrid', '', ''),
(109, '', 'AXIATEL SAS', '1', '$', 430000119, '', '', '70523980795', '', 'Courbevoie Cedex', '', ''),
(110, '', 'PABLO OTERO RIVERA', '1', '$', 430000120, '', '', '72719051C', '', 'Alicante', '', ''),
(111, '', 'COMERCIO DIGITAL, S.A.', '1', '$', 430000121, '', '', 'A62233028', '', 'Barcelona', '', ''),
(112, '', 'ED AND ASSOCIES', '1', '$', 430000122, '', '', '92503678476', '', 'Lyon', '', ''),
(113, '', 'SMART HOLIDAY SAS', '1', '$', 430000123, '', 'oriane.mirza@weekendesk.fr', '64482380888', '', 'Paris', '', ''),
(114, '', 'EASYVOYAGE', '1', '$', 430000124, '', 'marieme.diatta@webedia-group.com', '76432123446', '00331 77 48 01 ', 'Paris', '', ''),
(115, 'Adgravity S.L.', 'ADGRAVITY GROUP SL', '1', '$', 430000125, '', 'invoice@adgravitygroup.com', 'B86959806', '', 'Madrid', '', ''),
(116, '', 'ADCLICK LDA', '1', '$', 430000126, '', '', '508113199', '', 'Porto', '', ''),
(117, '', 'MEDICOS SIN FRONTERAS', '1', '$', 430000127, '', '', 'G58277534', '', 'Barcelona', '', ''),
(118, '', 'TURISMO DE IRLANDA', '1', '$', 430000128, '', '', 'A83287110', '', 'Madrid', '', ''),
(119, 'Thyssen Krupp', 'THYSSENKRUPP ENCASA, S.L.U.', '1', '$', 430000129, '', 'belen.galan@tkec.es', 'B84452184', '91 492 22 89', 'Madrid', '', ''),
(120, 'TradeDoubler AB', 'TRADE DOUBLER, AB', '1', '$', 430000130, '', 'eva.manzanares@smgiberia.es', '556575742301', '', 'Stockholm', '', ''),
(121, 'INFOEMPLEO, S.L.', 'INFOEMPLEO, SL', '1', '$', 430000131, '', '', 'B28975704', '', 'Madrid', '', ''),
(122, '', 'PHOENIX SPRL', '1', '$', 430000132, '', '', '833999169', '', 'Watermael-Boitsfort', '', ''),
(123, '', 'ZIEL87KAPITAL, S.L.', '1', '$', 430000133, '', '', 'B84806629', '', 'Madrid', '', ''),
(124, 'iAhorro Business Solutions, S.L.', 'IAHORRO BUSINESS SOLUTIONS, S.L.', '1', '$', 430000134, '', 'finanzas@iahorro.com', 'B85989622', '915358066', 'Madrid', '', ''),
(125, '', 'BE REPUBLIC', '1', '$', 430000135, '', '', 'B64850910', '', 'Barcelona', '', ''),
(126, '', 'COFIDIS, S.A. SUC EN ESPA—A', '1', '$', 430000136, '', 'oriol.ramon@cofidis.es', 'W0017686G', '93 508 31 70', 'Barcelona', '', ''),
(127, 'IMKTG LTD', 'IMKTG†LTD', '1', '$', 430000137, '', '', '122053272', '933193001', 'London', '', ''),
(128, '3038 Agency S.L.U, (Webpilots)', '30/38 AGENCY, S.L.', '1', '$', 430000138, '', 'elena.navarro@webpilots.com', 'B65618241', '933 96 85 07', 'Barcelona', '', ''),
(129, 'MediaWeb Ltd', 'MEDIAWEB LTD', '1', '$', 430000139, '', 't.yin@b-fly.biz', '39868039-001-', '', 'Hong Kong', '', ''),
(130, 'ASTROWAY', 'ASTROWAY', '1', '$', 430000140, '', 't.yin@b-fly.biz', '37610307-012-', '', 'Hong-Kong', '', ''),
(131, '', 'MEDONIA MARKETING', '1', '$', 430000141, '', '', 'I1661003', '', 'British', '', ''),
(132, '', 'IBIZALINK, S.L.U', '1', '$', 430000142, '', 'info@ibizalink.com', 'B57276495', '', 'Islas Baleares', '', ''),
(133, '', 'WEBPILOTS ESPA—A S.L.', '1', '$', 430000143, '', 'elena.navarro@webpilots.com', 'B62268339', '933 96 85 04', 'Barcelona', '', ''),
(134, '', 'TELMEL CONTACT CENTER S.L.', '1', '$', 430000145, '', 'administracion@telmel.es', 'B52027034', '', 'M·laga', '', ''),
(135, 'Media Adgo, S.L.', 'MEDIA ADGO, S.L.', '1', '$', 430000146, '', 'accounting@advertising-go.es', 'B66137704', '933487412', 'Barcelona', '', ''),
(136, 'Clabere Negocios', 'CLABERE NEGOCIOS S.L.', '1', '$', 430000147, '', 'contacto@bankimia.com', 'B64687890', '931 990 930', 'Barcelona', '', ''),
(137, 'Cyberclick Agent S.L.', 'CYBERCLICK AGENT S.L.', '1', '$', 430000148, '', 'sllinas@cyberclick.es', 'B62084959', '935088234', 'Barcelona', '', ''),
(138, '', 'INNOVA MEDIALEAD S.L.', '1', '$', 430000149, '', 'drc@innovamedialead.com', 'B57771255', '', 'Madrid', '', ''),
(139, 'Gravity4 // EuroAds Spain S.L. ', 'EUROADS MARKETING SPAIN, S.L.', '1', '$', 430000150, '', 'gb@euroads.com', 'B86498920', '910177541', 'Madrid', '', ''),
(140, 'Ambar Medline SL', 'AMBAR MEDLINE S.L.', '1', '$', 430000151, '', 'anamaria.garcia@lineadirecta.es', 'B85658573', '660381707', 'Madrid', '', ''),
(141, '', 'PERFORMACHINE', '1', '$', 430000152, '', '', '7740120964', '', 'Lodi', '', ''),
(142, '', 'WHISBI TECHNOLOGIES S.L.', '1', '$', 430000153, '', '', 'B85508281', '', 'Barcelona', '', ''),
(143, '', 'MARKETING ADJAL S.L.', '1', '$', 430000154, '', '', 'B22348544', '', 'Huesca', '', ''),
(144, 'Smart Digital  Advertising, S.L. (Smart4ads)', 'SMART DIGITAL ADVERTISING, S.L.', '1', '$', 430000155, '', 'claudia.mihaita@smart4ads.es', 'B86258001', '', 'Madrid', '', ''),
(145, 'Internovus LTD', 'INTERNOVUS LTD', '1', '$', 430000156, '', 'monica.p@internovus.com', '1713361', '972722280980', '', '', ''),
(146, 'Groupe ConcoursMania SA', 'GROUPE CONCOURSMANIA', '1', '$', 430000157, '', ' compta@concoursmania.com', '84433234325', '', 'Bordeaux', '', ''),
(147, '', 'SVETLOZAR PLAMENOV PENKOVSKI', '1', '$', 430000158, '', '', 'X6318466K', '', 'Barcelona', '', ''),
(148, 'Afilead, S.L.U.', 'AFILEAD S.L.U.', '1', '$', 430000159, '', 'veronica@afilead.com', 'B86179389', '91 850 65 35', 'Madrid', '', ''),
(149, '', 'DOVECONVENIENE SRL', '1', '$', 430000160, '', 'm.mata@geniale', '3156531208', '0034 911 876876', 'Bologna', '', ''),
(150, '', 'ABSER TECHNOLOGIES S.L.', '1', '$', 430000161, '', 'alberto@abser-t.com', 'B84279249', '918281111', 'Vizcaya', '', ''),
(151, '', 'BORSENYA SLU', '1', '$', 430000162, '', '', 'B86810421', '', 'Madrid', '', ''),
(152, '', 'EXTEL CONTACT CENTER', '1', '$', 430000163, '', '', 'A80221781', '', 'Madrid', '', ''),
(153, '', 'CONTENT IGNITION LTD', '1', '$', 430000164, '', 'jose.parrado@contentignition.com', '509971709', '+351 211 571 01', 'Lisboa', '', ''),
(154, '', 'VOYAGE PRIVE SLU', '1', '$', 430000165, '', '', 'B64876980', '', 'Barcelona', '', ''),
(155, '', 'PESCADOS PEPA CB', '1', '$', 430000166, '', '', 'E27118462', '', 'Lugo', '', ''),
(156, '', 'LBI SPAIN DIGITAL AGENCY S.L.', '1', '$', 430000167, '', '', 'B85586105', '', 'Madrid', '', ''),
(157, '', 'ADSALSA PUBLICIDAD, S.L.', '1', '$', 430000170, '', 'victorchamizo@adsalsagroup.com', 'B53967170', '', 'Alicante', '', ''),
(158, '', 'BONOWEB S.L.', '1', '$', 430000171, '', '', 'B64827959', '', 'Barcelona', '', ''),
(159, '', 'CORREOMASTER, S.L.', '1', '$', 430000172, '', '', 'B54015730', '', 'Alicante', '', ''),
(160, 'Digital 6 EspaÒa S.L.', 'DIGITAL 6 ESPA—A S.L.', '1', '$', 430000173, '', 'anayancy@digital-6.es', 'B65912321', '', 'Barcelona', '', ''),
(161, '', 'EMAILING NETWORK SARL', '1', '$', 430000174, '', 'sdelblanco@emailingnetwork.com', '77494084395', '934673626', 'Paris', '', ''),
(162, 'Impacting', 'IMPACTING EMAIL MARKETING SOLUTIONS SL', '1', '$', 430000175, '', '', 'B85884773', '', 'Madrid', '', ''),
(163, '', 'COMERPOSDAT S.L.', '1', '$', 430000176, '', 'raul@cdirect.es', 'B63329452', '', 'Barcelona', '', ''),
(164, 'VIP Response BV', 'VIP RESPONSE', '1', '$', 430000177, '', '', '852627725B01', '', 'Ev Utrecht', '', ''),
(165, 'WEBBDONE S.r.l.', 'WEBBDONE, S.R.L.', '1', '$', 430000178, '', 'marco.cassinetti@webbdone.com', '6692450965', '', 'Lodi', '', ''),
(166, '', 'MOMOMOBILE', '1', '$', 430000179, '', 'jtajuelo@momomobile.es', 'B86111036', '', 'Madrid', '', ''),
(167, '', 'SATPHONE S.L.', '1', '$', 430000180, '', 'nnaranjo@agenciab12.com', 'B47504485', '', 'Palencia', '', ''),
(168, '', 'MLS DI MARCO MERLO', '1', '$', 430000181, '', '', '1561670090', '', 'Milan', '', ''),
(169, '', 'CLINICA BAVIERA', '1', '$', 430000182, '', 'aalbert@clinicabaviera.com', 'A80240427', '', 'Madrid', '', ''),
(170, '', 'BEDIGITALNOW S.R.L.', '1', '$', 430000183, '', 'rebecca.martinucci@bedigitalnow.it', '10671251006', '', 'Colleferro (Rm)', '', ''),
(171, 'AssurAgency SAS', 'ASSURAGENCY SAS', '1', '$', 430000184, '', 'comptabilite@assuragency.com', '60445009632', '0033.4.89.29.10', 'La Seyne Sur Mer', '', ''),
(172, 'Starcom Mediavest Group Iberia', 'STARCOM MEDIAVEST GROUP IBERIA', '1', '$', 430000185, '', 'laura.de-luque@smgiberia.es', 'B82465865', '', 'Madrid', '', ''),
(173, '', 'COLECTIVIA', '1', '$', 430000187, '', 'jorge.eizaguirre@colectivia.com', 'B95612958', '', 'Vizcaya', '', ''),
(174, '', 'VISUAL MERCHANDISER SL', '1', '$', 430000188, '', 'martin@amantis.net', 'B85093185', '', 'Madrid', '', ''),
(175, '', 'COREGISTROS, S.L.', '1', '$', 430000189, '', 'elozano@coregistros.com', 'B65146078', '936112173', 'Barcelona', '', ''),
(176, '', 'CUSTOMERTOP S.L.', '1', '$', 430000190, '', 'javier.perez@customertop.es', 'B84134196', '', 'Madrid', '', ''),
(177, '', 'NEXTEDIA CONTACT', '1', '$', 430000191, '', '', '10505282764', '', 'Boulognes-Billiancou', '', ''),
(178, '', 'TOLUNA SAS', '1', '$', 430000192, '', 'emily.mccorriston@toluna.com', '8432033181', '', 'Perret Cedex', '', ''),
(179, '', 'SERVICIOS UNIFICADOS MARKETING ARAG”N SL', '1', '$', 430000193, '', '', 'B50903020', '+34 976 399 155', 'Zaragoza', '', ''),
(180, 'FS PERFORMANCE GROUP SL (Acai)', 'FS PERFORMANCE GROUP S.L.', '1', '$', 430000195, '', 'invoicing@acai-performance.com ', 'B66240672', '932502808', 'Barcelona', '', ''),
(181, '', 'ENITI MEDIA, S.L.', '1', '$', 430000196, '', 'maria@eniti.es', 'B76544840', 'T. 822110904', 'Santa Cruz De Teneri', '', ''),
(182, '', 'ITEMA SARL', '1', '$', 430000197, '', 'claire.ramon@item-a.com', '20423827955', '+33 556 26 48 4', 'Andernos-Les-Bains', '', ''),
(183, '', 'OUTPOST BV', '1', '$', 430000198, '', '', '009889139B01', '', 'Amsterdam', '', ''),
(184, 'KREDITECH HOLDING SSL GMBH', 'KREDITECH HOLDING SSL GMBH', '1', '$', 430000199, '', 'xavier.romero@kreditech.com', '282010517', '', 'Hamburg', '', ''),
(185, '', 'PARNASO COMUNICACION SL', '1', '$', 430000200, '', 'mariadelbarco@parnasocomunicacion.com', 'B91620427', '955 502 673', 'Sevilla', '', ''),
(186, '', 'ESOTER SARL', '1', '$', 430000201, '', 'mila@infodata-media.com', '115712274', '', 'GenËve', '', ''),
(187, '', 'ALN TELEMARKETING SL', '1', '$', 430000202, '', 'compras@alngrupo.com', 'B24606139', '', 'Leon', '', ''),
(188, '', 'CANALMAIL S.L.', '1', '$', 430000203, '', '', 'B82604646', '', 'Madrid', '', ''),
(189, '', 'BEFEVER SPAIN S.L.', '1', '$', 430000204, '', '', 'B86779832', '', 'Madrid', '', ''),
(190, '', 'SHAREMYCLICK S.L.', '1', '$', 430000205, '', 'invoice@sharemyclick.com', 'B66154824', '', 'Barcelona', '', ''),
(191, '', 'PEDRO GUERRA SANCHEZ', '1', '$', 430000206, '', '', '07533261W', '91 173 86 33', 'Madrid', '', ''),
(192, 'LEAD TO LEAD PERFORMANCE BASED ADVERTISING, S.L.', 'LEAD TO LEAD PERFORMANCE BASED ADVERTISI', '1', '$', 430000207, '', 'facturacion.es@leadtolead.com', 'B85685808', '91 270 12 20', 'Madrid', '', ''),
(193, '', 'REEF INTERACTIVE S.A.', '1', '$', 430000208, '', '', '722935', '', 'Baar', '', ''),
(194, 'WAN Media Market S.I.', 'LEGMAN ESPA—A S.L.', '1', '$', 430000210, '', 'direccion.legman@gmail.com', 'B86213899', '', 'Madrid', '', ''),
(195, '', 'ADTZ', '1', '$', 430000211, '', 'dzurita@adtz.com', 'B85533560', '', 'Madrid', '', ''),
(196, '', 'IMAS COMUNICACION', '1', '$', 430000212, '', '', 'B86456910', '91 392 3712', 'Madrid', '', ''),
(197, 'Inversiones 2012 Inteligencia de Negocio S.L.', 'INVERSIONES 2012 INTELIGENCIA DE NEGOCIO', '1', '$', 430000213, '', 'facturacion@agenciab12.com', 'B34260604', '916 629 534', 'Madrid', '', ''),
(198, '', 'WHO-REMEMBERS-ME.COM LTD', '1', '$', 430000214, '', 'accounts@wrm-media.com', '867844663', '', 'Wakefield', '', ''),
(199, '', 'HELPMYCASH S.L.', '1', '$', 430000215, '', 'marketing@helpmycash.com', 'B64647050', '', 'Barcelona', '', ''),
(200, 'Efficiency Network', 'EFFICIENCY NETWORK', '1', '$', 430000216, '', 'compta-fournisseur@effi-net.com', '80517913513', '972335255', 'Paris', '', ''),
(201, '', 'MAPFRE FAMILIAR, COMPA—IA DE SEGURO Y RE', '1', '$', 430000217, '', 'abagonz@mapfre.com', 'A28141935', '', 'Madrid', '', ''),
(202, '', 'CRAZY4MEDIA MOBILE MARKETING & BILL', '1', '$', 430000218, '', 'elviraduque@crazy4media.com', 'B90125741', '', 'Sevilla', '', ''),
(203, '', 'NET REAL SOLUTIONS S.L.', '1', '$', 430000219, '', '', 'B12550877', '964523331', 'Castellon', '', ''),
(204, '', 'NIVORIA ANDORRA S.L.', '1', '$', 430000220, '', 'marita@nivoria.com', 'L709426W', '935305975', 'Andorra', '', ''),
(205, 'Publicis Media', 'PUBLICIS†MEDIA†SPAIN,†S.L.U.', '1', '$', 430000221, '', 'proveedores@publicisgroupe.net', 'B81875627', '', 'Madrid', '', ''),
(206, 'Mobile Dreams Factory', 'MOBILE DREAMS FACTORY S.L.', '1', '$', 430000222, '', 'diana@mobiledreams.mobi', 'B83859512', '', 'Madrid', '', ''),
(207, '', 'SAFECAP INVESTMENTS LTD', '1', '$', 430000223, '', 'yma.blanco@safecapltd.com', 'GX111AA', '', 'Gibraltar', '', ''),
(208, '', 'SERVICIOS INTEGRALES CONSULT DENTAL SL', '1', '$', 430000224, '', '', 'B86763612', '91 236 00 30', 'Madrid', '', ''),
(209, '', 'FROGGIE S.L.', '1', '$', 430000225, '', '', 'B91109454', '', 'Sevilla', '', ''),
(210, 'Rock Internet, S.L. (Arkeero)', 'ROCK INTERNET S.L. (ARKEERO)', '1', '$', 430000226, '', 'billing@arkeero.com', 'B86907953', '', 'Madrid', '', ''),
(211, 'Roiandco 2012 S.L.', 'ROIANDCO 2012 S.L.', '1', '$', 430000227, '', 'ssl@roiandco.com', 'B65947525', '932184621', 'Barcelona', '', ''),
(212, '', 'KELISTO IBERIA SL', '1', '$', 430000228, '', 'eduardo.mas@kelisto.es', 'B86746161', '', 'Madrid', '', ''),
(213, '', 'SANITALIA EXPLOTACION Y GESTION DE INFRA', '1', '$', 430000229, '', '', 'B51006914', '917443810', 'Madrid', '', ''),
(214, '', 'NET GROUP MEDIA (2014) LTD', '1', '$', 430000230, '', '', '515029676', '', 'Israel', '', ''),
(215, '', 'GALLINA CLUECA S.L', '1', '$', 430000231, '', '', 'B65698235', '931 600 124', 'Barcelona', '', ''),
(216, '', 'VENTURA 24', '1', '$', 430000232, '', 'silvia.peral@ventura24.es', 'B83139253', '', 'Madrid', '', ''),
(217, 'C.A.I. ICEBERG TELECOMUNICACIONES S.L.', 'C.A.I. ICEBERG TELECOMUNICACIONES, SL', '1', '$', 430000233, '', 'marketing@v3click.com', 'B47696927', '', 'Valladolid', '', ''),
(218, '', 'MONTAIGNE AGENCY SARL', '1', '$', 430000234, '', '', '66014460124', '', 'GenËve', '', ''),
(219, '', 'INBOXLABS LLC', '1', '$', 430000235, '', 'juan@inbox-labs.com', '32-0480982 ', '', 'Delaware', '', ''),
(220, 'Sahna. e- Servicios Integrales de Salud S.A.', 'SAHNA-E, SERVICIOS INTEGRALES DE SA', '1', '$', 430000236, '', 'damaris.ayala@nectar.es', 'A85372597', '913834700', 'Madrid', '', ''),
(221, '', 'CARIAL S.L.', '1', '$', 430000237, '', 'administracion@carialcomunicacion.es', 'B85320950', '', 'Madrid', '', ''),
(222, '', 'UNIGLOBAL GESTION, S.L. AG.SEG.VINC', '1', '$', 430000238, '', '', 'B86439825', '', 'Madrid', '', ''),
(223, 'Digital Group Branch S.L.', 'DIGITAL GROUP BRANCH, S.L.', '1', '$', 430000239, '', 'facturacion@digitalgroup.es', 'B85533321', '914855542', 'Madrid', '', ''),
(224, '', 'MOBUSI MOBILE ADVERTISING S.L.', '1', '$', 430000240, '', '', 'B86439577', '', 'Madrid', '', ''),
(225, '', 'APRENDE FORMACION, S.L.', '1', '$', 430000241, '', '', 'B18830547', '', 'Madrid', '', ''),
(226, '', 'UNI-IMPULS ESPA—A, S.L.', '1', '$', 430000242, '', '', 'B86898145', '', 'Madrid', '', ''),
(227, '', 'HAVAS MANAGEMENT ESPA—A SL', '1', '$', 430000243, '', 'proveedores.havasmedia@havasmedia.com', 'B84061654', '914569007', 'Madrid', '', ''),
(228, 'Sanitas Sociedad Anonima De Seguros SA', 'SANITAS SOCIEDAD ANONIMA DE SEGUROS SA', '1', '$', 430000244, '', 'lgonzaleza@sanitas.es', 'A28037042', '648417310', 'Madrid', '', ''),
(229, '', '11811 NUEVA INFORMACION TELEFONICA', '1', '$', 430000245, '', 'jose.parrado@11811.es', 'A82511361', '', 'Madrid', '', ''),
(230, '', 'VIA EVOLUTION S.L.', '1', '$', 430000246, '', 'alejandro.aguilera@viaevolution.com', 'B85226751', '', 'Madrid', '', ''),
(231, 'Inteligencia Ymedia SA', 'INTELIGENCIA YMEDIA S.A.', '1', '$', 430000247, '', 'maria.kerekes@ymedia.es', 'A84909498', '91-1330530', 'Madrid', '', ''),
(232, '', 'SERVICIOS GLOBALES DE GESTION ONLINE SL', '1', '$', 430000248, '', '', 'B81937138', '', 'Madrid', '', ''),
(233, 'Ampliffica', 'AMPLIFFICA S.L.', '1', '$', 430000249, '', 'j.delval@ampliffica.com', 'B86702255', '615108458', 'Madrid', '', ''),
(234, '', 'HUARIS CALL CENTER, SL', '1', '$', 430000250, '', 'administracion@huariscc.es ', 'B24588592', '987192001', 'Leon', '', ''),
(235, '', 'TUENTI TECHNOLOGIES S.L.U.', '1', '$', 430000251, '', 'proveedores@tuenti.com', 'B84675529', '', 'Madrid', '', ''),
(236, '', 'ZZ BEDS & DREAMS S.L.', '1', '$', 430000252, '', '', 'B26516724', '', 'LogroÒo', '', ''),
(237, '', 'PLACE DES LEADS', '1', '$', 430000253, '', 'facturacion@placedesleads.com', '64523662633', '', 'Paris', '', ''),
(238, 'MARKET 360 DEGREES, S.L.', 'MARKET 360 DEGREES S.L.', '1', '$', 430000254, '', 'facturacion@market360.es', 'B86508454', '', 'Madrid', '', ''),
(239, '', 'E-RETAILADVERTISING', '1', '$', 430000255, '', 'amartin@e-retailadvertising.com', 'B85442283', '', 'Madrid', '', ''),
(240, '', 'WEAD MEDIA', '1', '$', 430000256, '', '', 'B86973708', '', 'Madrid', '', ''),
(241, 'Emagister Servicios de FormaciÛn, S.L.', 'EMAGISTER SERVICIOS DE FORMACION SL', '1', '$', 430000257, '', 'proveedores@emagister.com', 'B62673108', '935519900', 'Barcelona', '', ''),
(242, '', 'PHOTOBOX SAS', '1', '$', 430000258, '', '', '7428703979', '', 'Paris', '', ''),
(243, '', 'ESMARTIA', '1', '$', 430000259, '', '', 'B86801107', '', 'Madrid', '', ''),
(244, '', 'BICLAMEDIA SL', '1', '$', 430000260, '', 'arantxa@biclamedia.com', 'B87022117', '810520167', 'Madrid', '', ''),
(245, 'Digital Circle 360', 'DIGITAL CIRCLE 360 SL', '1', '$', 430000261, '', 'adil@digitalcircle360.com', 'B66320730', '932528206', 'Barcelona', '', ''),
(246, 'Leads Global International S.L.', 'LEADS GLOBAL INTERNATIONAL', '1', '$', 430000262, '', 'facturacion@leads.global ', 'B54570809', '', 'ALICANTE/ALACANT', '', ''),
(247, 'Teradata', 'TERADATA LTD', '1', '$', 430000264, '', 'Yasmin.Peiris@Teradata.com', '905963895', '', 'Londres', '', ''),
(248, 'Vip Reformas', 'SERV. TELEMATICO DE  INFOMACION VIP', '1', '$', 430000265, '', 'rafael.jimenez@vipreformas.es', 'B86067774', '91-8273777', 'Guadalajara', '', ''),
(249, 'Media Planning Group, S.A,', 'HAVAS MEDIA GROUP SPAIN SAU', '1', '$', 430000266, '', '', 'A78809662', '', 'MADRID', '', ''),
(250, 'Base and Co SARL', 'BASE&CO', '1', '$', 430000267, '', 'fgramirez@baseandco.com', '80482981776', '', 'Paris', '', ''),
(251, '', 'ARVATO SERVICES IBERIA S.A.', '1', '$', 430000268, '', 'mireia.canicio@arvato.es', 'A17020157', '', 'Barcelona', '', ''),
(252, '', 'MOBILOGIA', '1', '$', 430000269, '', '', 'B19293968', '', 'Castilla La Mancha', '', ''),
(253, 'Global Sales Solution Line SL', 'GLOBAL SALES SOLUTIONS LINE S.L.', '1', '$', 430000270, '', 'support.digital.ecommerce@grupogss.com', 'B82311861', '', 'Madrid', '', ''),
(254, '', 'CRITEO FRANCE', '1', '$', 430000271, '', '', '62520843780', '', 'Paris', '', ''),
(255, 'VELORIA ONLINE SOLUTIONS S.L.', 'VELORIA ONLINE SOLUTIONS', '1', '$', 430000272, '', 'miguel@veloria.es', 'B27460625', '619152722', 'Lugo', '', ''),
(256, '', 'GESTION DE MEDIOS S.A.', '1', '$', 430000273, '', 'elena.cans@gestiondemedios.es', 'A20305736', '', 'San Sebastian', '', ''),
(257, 'TeleMark BPO SL', 'TELEMARK SPAIN SL', '1', '$', 430000274, '', 'administracionleon@telemark-spain.com.', 'B24470460', '', 'LeÛn', '', ''),
(258, 'Equmedia', 'EQUMEDIA', '1', '$', 430000275, '', 'info@equmedia.es', 'B81108334', '917450160', 'Madrid', '', ''),
(259, '', 'JUNGLE BOX ,S.L.', '1', '$', 430000276, '', 'bcarvalho@aprendemas.com', 'B82550054', '916364200', 'MADRID', '', ''),
(260, '', 'ASOCIACI”N ESPA—A CON ACNUR', '1', '$', 430000277, '', '', 'G80757560', '', 'MADRID', '', ''),
(261, '', 'KHING LTD', '1', '$', 430000278, '', 'invoicing@khingdom.com', '175221227', '', 'LONDON', '', ''),
(262, 'Reial AutomÚbil Club de Catalunya', 'RACC', '1', '$', 430000279, '', 'miriam.vinuesa@racc.es', 'G08307928', '934955000', 'BARCELONA', '', ''),
(263, '', 'INESDI, S.L.', '1', '$', 430000280, '', '', 'B65416083', '654637753', 'BARCELONA', '', ''),
(264, 'MetLife Europe Limited', 'METLIFE EUROPE LIMITED', '1', '$', 430000281, '', 'moira.rowell@metlife.es', 'W0072536F', '+34 917 243 888', 'MADRID', '', ''),
(265, '', 'CORAL MERCADOS SL', '1', '$', 430000282, '', '', 'B51014389', '', 'CEUTA', '', ''),
(266, '', 'EUROCIO FREENTIME SL', '1', '$', 430000283, '', 'victor.hernandez@eurocios.com', 'B65878555', '93 667 56 52', 'BARCELONA', '', ''),
(267, '', 'RBA REVISTAS, S.L.', '1', '$', 430000284, '', 'proveedores-admon@Rba.es', 'B64610389', '934157374', 'BARCELONA', '', ''),
(268, '', 'LEADGNOSIS, S.L.', '1', '$', 430000285, '', 'jmoya@leadgnosis.com', 'B86702172', '607 429 802', 'MADRID', '', ''),
(269, '', 'MOVILE INTERNET MOVEL S/A', '1', '$', 430000286, '', 'beatriz.mendes@movile.com', '08654191/0001', '551121630600', 'Sao Paolo', '', ''),
(270, '', 'CELA OPEN INSTITUTE S.L.', '1', '$', 430000287, '', '', 'B86344108', '', 'MADRID', '', ''),
(271, '', 'OCEAN TALENT SERVICE, S.L.', '1', '$', 430000288, '', '', 'B86862984', '932802020', 'MADRID', '', ''),
(272, '', 'UNIDAD EDITORIAL INFORMACI”N GENERAL SLU', '1', '$', 430000289, '', 'maria.velayos@unidadeditorial.es', 'B85157790', '', 'MADRID', '', ''),
(273, 'BUBBLEFISH SALES & MARKETING SOLUTIONS SL', 'BUBBLEFISH SALES & MARKETING SOLUTIONS,', '1', '$', 430000290, '', 'antoniogv@bubblefish.es', 'B86838497', '910314040', 'MADRID', '', ''),
(274, 'HOFMANN SLU', 'HOFMANN SLU', '1', '$', 430000291, '', 'admon@hofmann.es ', 'B46047502', '', 'VALENCIA/VAL…NCIA', '', ''),
(275, 'Forward Holding Spain SL', 'FORWARD HOLDING SPAIN SL', '1', '$', 430000292, '', '', 'B85001782', '', 'MADRID', '', ''),
(276, '', 'ETHINKING', '1', '$', 430000293, '', '', 'B86830403', '', 'MADRID', '', ''),
(277, '', 'TRIVAGO GMBH', '1', '$', 430000294, '', '', '814414038', '', '', '', ''),
(278, '', 'INDICIA GESTION ONLINE, S.L.', '1', '$', 430000295, '', 'luis@segurindicia.com', 'B63188999', '', 'BARCELONA', '', ''),
(279, '', 'SAY MEDIA GROUP LTD', '1', '$', 430000296, '', '', '514293620', '+972-3-6209614', 'TEL AVIV', '', ''),
(280, '', 'HAPPYTEL COMUNICACIONES S.L.', '1', '$', 430000297, '', 'jmzamora@happytelc.net', 'B83431221', '912861817', 'MADRID', '', ''),
(281, '', 'IERP MOTOBUYKERS SL', '1', '$', 430000298, '', '', 'B65400905', '', 'BARCELONA', '', ''),
(282, '', 'ADZOL MEDIA', '1', '$', 430000299, '', 'Lucas.toledo@adzolmedia.com', '513461035', '35121139904', 'ALG…S', '', ''),
(283, '', 'EASYFAIRS IBERIA SL', '1', '$', 430000300, '', '', 'B85454700', '', 'MADRID', '', ''),
(284, '', 'MARK AFFILIATION S¿RL', '1', '$', 430000301, '', '', '281811629', '', '', '', ''),
(285, '', 'RUBEN BAJO GALLEGO', '1', '$', 430000302, '', 'soporte@reformayuda.com', '02264955F', '918091764', 'TOLEDO', '', ''),
(286, '', 'BOWNTY', '1', '$', 430000303, '', '', '33251017', '', 'Denmark', '', ''),
(287, 'RELEVANT TRAFFIC SPAIN S.L.', 'RELEVANT TRAFFIC SPAIN SL', '1', '$', 430000304, '', 'administracion_rt@relevanttraffic.com', 'B84875459', '663201684', 'MADRID', '', ''),
(288, '', 'ASEGURACE S.A. CORREDURÕA DE SEG.', '1', '$', 430000305, '', 'nuria.abad@metlife.es', 'A78314150', '', 'MADRID', '', ''),
(289, '', 'MELIA HOTELS INTER. PRODIGIOS SA', '1', '$', 430000306, '', '', 'A07995434', '', 'ILLES BALEARS', '', ''),
(290, 'Legalitas Asistencia Legal S.L.', 'LEG¡LITAS ASISTENCIA LEGAL S.L.', '1', '$', 430000307, '', 'marta.borrachero@legalitas.es', 'B82305848', '628598184', 'MADRID', '', ''),
(291, '', 'NEOLOTTO LTD', '1', '$', 430000308, '', 'info@neolotto.com ', '21542519', '', '', '', ''),
(292, '', 'BILLY PERFORMANCE NERWORK SLU', '1', '$', 430000309, '', '', 'B66478223', '', 'BARCELONA', '', ''),
(293, '', 'EDITORIAL OCEANO', '1', '$', 430000310, '', 'ventas@oceano.com', 'B62751151', '93-2802020', 'Barcelona', '', ''),
(294, '', 'BIDDERPLACE GMBH', '1', '$', 430000311, '', '', '297751357', '', 'BERLIN', '', ''),
(295, '', 'ADEXPERIENCE', '1', '$', 430000312, '', 'stephane@adexperience.com', '31537890873', '', 'Levallois Perret', '', ''),
(296, '', 'NEOMOBILE S.P.A.', '1', '$', 430000313, '', '', '2927500542', '', 'ROMA', '', ''),
(297, '', 'BUONGIORNO UK LTD', '1', '$', 430000314, '', ' invoicing@taplinks.com', '839864272', '', 'LONDON', '', ''),
(298, '', 'TAPGERINE LLC', '1', '$', 430000315, '', 'vitaliy@tapgerine.com', '27-2965094', '', 'NEVADA', '', ''),
(299, '', 'WISTER - SERVICE AFFILIATION FACTUR', '1', '$', 430000316, '', '', '40447878950', '', 'PARIS', '', ''),
(300, '', 'XTRADE LTD', '1', '$', 430000317, '', 'Luciad@xtrade.com', '1859473', '663481538', 'Tortola', '', ''),
(301, '', 'GO-RILLA DIGITAL ADVERTISING LTD', '1', '$', 430000318, '', 'finance@go-rilla.mobi', '515016640', '+972-5-8860695', '', '', ''),
(302, 'Spain Business School S.L.', 'SPAIN BUSINESS SCHOOL S.L', '1', '$', 430000319, '', 'jlpeino@spainbs.com', 'B86457009', '', 'MADRID', '', ''),
(303, 'Finance Ads', 'FINANCE†ADS INTERNATIONAL GMBH', '1', '$', 430000320, '', 'ignacio.peinado@financeads.com', '299418367', '+34 960 130 218', 'Berlin', '', ''),
(304, '', 'MOBFOX MOBILE ADVERTISING GMBH', '1', '$', 430000321, '', '', 'U66270527', '', 'VIENNA', '', ''),
(305, '', 'ADQUOTA SPAIN SL', '1', '$', 430000322, '', '', 'B85897239', '915234528', 'MADRID', '', ''),
(306, '', 'PERFORMANCE REVENUES', '1', '$', 430000323, '', 'makis@performancerevenues.com', '514562909', '+972.72.234.660', '', '', ''),
(307, '', 'MOBEETECH B.V.', '1', '$', 430000324, '', '', '821123944B01', '+31(0)765146788', 'BREDA', '', ''),
(308, '', 'CELLON LTD. (MCR)', '1', '$', 430000325, '', 'Nir@mcr-m.com', '514235852', '+972-(0)3-60060', '', '', ''),
(309, '', 'MOBILE CASHOUT', '1', '$', 430000326, '', '', '3177685HH', '', '', '', ''),
(310, '', 'MINDSHARE†SPAIN†SA', '1', '$', 430000327, '', '', 'A78913019', '', 'MADRID', '', ''),
(311, '', 'SPONSOR BOOST (PRESTASOFT SARL)', '1', '$', 430000328, '', 'linh@sponsorboost.com', '93401486519', '', 'Nice', '', ''),
(312, '', 'INMOBI PTE. LIMITED', '1', '$', 430000329, '', '', '200811408G', '', '', '', ''),
(313, '', 'JOBS AND TALENT SL', '1', '$', 430000330, '', 'sergio.villacorta@jobandtalent.com', 'B85384808', '915782099', 'MADRID', '', ''),
(314, 'LEMON SKY SP”LKA Z OGRANICZONA ODP', 'LEMON SKY SP”LKA Z OGRANICZONA ODPOWIEDZ', '1', '$', 430000331, '', 'katarzyna.iwanich@lemonskyjwt.com', '5862236051', '', '', '', ''),
(315, '', 'KAWIIL GLOBAL ENERGY SERVICES SL', '1', '$', 430000332, '', '', 'B87095089', '', 'MADRID', '', ''),
(316, '', 'MAKE MARKETING Y COMUNICACI”N S.L', '1', '$', 430000333, '', 'administracion@make.es', 'B82861428', '913090209', 'MADRID', '', ''),
(317, 'INRUN', 'INRUN', '1', '$', 430000334, '', 'orestes@inruncatalyst.com', '48360487C', '636715589', 'MADRID', '', ''),
(318, '', 'LTRL GESTION INTERNET SL', '1', '$', 430000335, '', '', 'B93244408', '', 'M¡LAGA', '', ''),
(319, '', 'UNICEF COMIT… ESPA—OL', '1', '$', 430000336, '', '', 'G84451087', '913789555', 'MADRID', '', ''),
(320, '', 'MOUNTMEDIASOLUTIONS B.V', '1', '$', 430000337, '', '', '854984690B01', '0031 765146788', 'AT TERHEIJDEN', '', ''),
(321, 'Batlink Distribution Limited', 'BATLINK DISTRIBUTION LIMITED', '1', '$', 430000338, '', 'adv@msconsult.it', '113603557', '', 'LONDON', '', ''),
(322, '', 'CONTENT COMMERCE FACTORY SL', '1', '$', 430000339, '', '', 'B87299996', '911169539', 'MADRID', '', ''),
(323, '', 'SLIMSPOTS', '1', '$', 430000340, '', 'register@slimspots.com', '264640070', '+49 531-257 333', 'Braunschweig', '', ''),
(324, '', 'LAVINIA ESPA—A SELECCION, S.L.U. ', '1', '$', 430000341, '', '', 'B83546119', '', 'MADRID', '', ''),
(325, '', 'SANCHEZ LANDERAS SL (VAS)', '1', '$', 430000342, '', 'pilar@zapatosvas.com', 'B80606494', '915060681', 'MADRID', '', ''),
(326, 'Across S.r.l.', 'ACROSS†SRL', '1', '$', 430000343, '', 'amministrazione@across.it', '10599350013', '', '', '', ''),
(327, 'Abacus Marketing Solutions SL', 'ABACUS MARKETING SOLUTIONS S.L', '1', '$', 430000344, '', 'finance@abacus-marketing.com', 'B66170135', '', 'Barcelona', '', ''),
(328, '', 'TIME MARKETING BETS S.L.', '1', '$', 430000345, '', '', 'B87382594', '', 'MADRID', '', ''),
(329, '', 'SALUDONNET SPAIN S.L. ', '1', '$', 430000346, '', 'alejandro.canora@saludonnet.com', 'B83214643', '91.636.42.92', 'MADRID', '', ''),
(330, '', 'FRAPIVI MAYORISTAS S.L', '1', '$', 430000347, '', 'pilarcecilio@gmail.com', 'B87379921', '', 'MADRID', '', ''),
(331, 'Beauty Luxe Distributions S.L.', 'BEAUTY LUXE DISTRIBUTIONS S.L.', '1', '$', 430000348, '', 'jorge@perfumesclub.com', 'B57818197', '', 'ILLES BALEARS', '', ''),
(332, '', 'ORANGEBUDDIES MEDIA INTERNATIONAL B.V. ', '1', '$', 430000349, '', 'auto-invoices@orangebuddies.com', '852848808B01', '', '', '', ''),
(333, 'Auto1 Group GmbH', 'AUTO1 GROUP GMBH', '1', '$', 430000350, '', 'anna.schulz@auto1.com', '286118911', '', '', '', ''),
(334, 'Eoniac Tec (HK INTERNET GROUP LIMITED)', 'HK INTERNET GROUP LIMITED', '1', '$', 430000351, '', 'manuel.peguero@eoniantec.com', '1703880', '931594627', '', '', ''),
(335, '', 'TASKPHONE S.A', '1', '$', 430000352, '', 'raquel.carnicero@grupoinmark.com', 'A81352817', '677909447', 'MADRID', '', ''),
(336, 'GroupM Publicidad Worlwide, S.A.', 'GROUPM PUBLICIDAD WORLDWIDE, S.A', '1', '$', 430000353, '', 'vendorqueries.spain@wppfinanceplus.com', 'A81922791', '913089510', 'MADRID', '', ''),
(337, '', 'PICKAWIN S.L.', '1', '$', 430000354, '', '', 'B86915360', '660876353', 'MADRID', '', ''),
(338, '', 'SOLUCIONES DE OCIO DIGITAL KAMPAII, S.L.', '1', '$', 430000355, '', 'Samuel@kampaii.com', 'B87058640', '', 'MADRID', '', ''),
(339, 'SESAM Ltd', 'SESAM LTD', '1', '$', 430000356, '', 't.yin@b-fly.biz', 'EU826411144', '', '', '', ''),
(340, '', 'IGLUE MEDIA SL', '1', '$', 430000357, '', '', 'B85001709', '', 'MADRID', '', ''),
(341, '', 'GLISPA BILLING DETAILS', '1', '$', 430000358, '', 'billing@glispamedia.com', '814998388', '', '', '', ''),
(342, '', 'MPIRE NETWORK INC.', '1', '$', 430000359, '', 'superna.naik@mpirenetwork.com', '821340577', '', '', '', ''),
(343, '', 'OPTIMALIA SERVICIOS S.L.U.', '1', '$', 430000360, '', 'nievescanete@optimalia.es', 'B86232469', '', 'MADRID', '', ''),
(344, 'Neo Ogilvy SA', 'NEO OGILVY S.A', '1', '$', 430000361, '', 'sofia.diazdeteran@ogilvy.com;', 'A58707670', '913089510', 'MADRID', '', ''),
(345, 'Conversant International Limited', 'CONVERSANT INTERNATIONAL LIMITED', '1', '$', 430000362, '', 'PLumbreras@conversantmedia.com', '9690375D', '', '', '', ''),
(346, '', 'VICTOR MANUEL GARCIA COMES', '1', '$', 430000363, '', 'garcia@demalia.com', '53358119M', '', 'VALENCIA/VAL…NCIA', '', ''),
(347, '', 'MEDIADORES EN TELECOMUNICACIONES S.L.U', '1', '$', 430000364, '', 'eduardo.reig@gmail.com', 'B98699242', '', 'VALENCIA/VAL…NCIA', '', ''),
(348, 'Inflecto Media Ltd.', 'INFLECTO MEDIA LTD', '1', '$', 430000365, '', 'louisa@inflectomedia.com', '980397385', '', '', '', ''),
(349, 'LEADSOLUTION INTELIGENCIA MARKETING DIGITAL SCP', 'LEADSOLUTION INTELIGENCIA MK  DIGITAL SC', '1', '$', 430000366, '', 'delia@leadsolution.es', 'J66459082', '960626200', 'VALENCIA/VAL…NCIA', '', ''),
(350, 'AdviceMe srl', 'ADVICEME SRL', '1', '$', 430000367, '', 'amministrazione@adviceme.it', '1542400559', '', '', '', ''),
(351, 'VIVUS FINANCE S.A.U.', 'VIVUS FINANCE SAU', '1', '$', 430000368, '', 'affiliates@4finance.com', 'A86521309', '', 'MADRID', '', ''),
(352, '', 'GESDELLA LLAMADAS S.L.', '1', '$', 430000369, '', 'jose.perez@gesdella.com', 'B86018124', '692068325', 'MADRID', '', ''),
(353, 'International Personal Finance Spain Digital S.A.U', 'INTERNATIONAL PERSONAL FINANCE SPAIN DIG', '1', '$', 430000370, '', 'Finanzas.es@ipfdigital.com', 'A87026274', '', 'MADRID', '', ''),
(354, '', 'WEBORAMA IBERICA S.L', '1', '$', 430000371, '', 'patricia.isidro@weborama.com', 'B84422401', '915233330', 'MADRID', '', ''),
(355, '', 'NATEXO ITALIA SRL', '1', '$', 430000372, '', 'Invoicing_Italia@natexo.com', '8007240966', '+33(0)148567031', '', '', ''),
(356, '', 'CRITEO ESPA—A S.L.', '1', '$', 430000373, '', 'suppliermadrid@criteo.com', 'B87086559', '', 'Madrid', '', ''),
(357, 'brillen.de Optik AG', 'BRILLEN DE OPTIK AG', '1', '$', 430000374, '', 'i.lopez@gafas.es', '291219087', '', 'Brandenburg', '', ''),
(358, 'The Yellow Box', 'SAS THE YELLOW BOX', '1', '$', 430000375, '', ' Sophie.marques@the-ybox.com', '26812987204', '621460841', '', '', ''),
(359, 'Innovations, Inc - dba London Bridge Performance Media', 'MP INNOVATION (LONDON BRIDGE)', '1', '$', 430000376, '', '', 'D28ABEE9D1', '', 'New York', '', ''),
(360, 'Escuela Internacional de Profesionales y Empresas Business School S.L.', 'ESCUELA INTERNACIONAL DE PROFESIONALES ', '1', '$', 430000377, '', 'igarcia@imf-formacion.com', 'B87097671', '91-3645157 ', 'MADRID', '', ''),
(361, 'Instituto Europeo EstÈtico', 'INSTITUTO EUROPEO EST…TICO SL', '1', '$', 430000378, '', 'pzandueta@clinicaszurich.com ', 'B85728483', '917370699', 'MADRID', '', ''),
(362, 'SUPERMONEY S.R.L', 'SUPERMONEY S.R.L.', '1', '$', 430000379, '', 'amministrazione@mailsupermoney.eu', '8883390968', '', '', '', ''),
(363, '', 'CONTAQ SOLUTIONS SL', '1', '$', 430000380, '', 'jorge.gonzalez@iccs.es', 'B93365039', '', 'M¡LAGA', '', ''),
(364, 'eMarketingSolutions', 'EMARKETINGSOLUTIONS,ONLINE MARKETING S.L', '1', '$', 430000381, '', 'jesus.sanchez@emarketingsolutions.es', 'B87445128', '608705286', 'MADRID', '', ''),
(365, '', 'VIAJES TUTTOCARD', '1', '$', 430000382, '', 'victorchamizo@adsalsagroup.com', 'B54227707', '', 'Alicante', '', ''),
(366, 'Entusiasmo y Mucho Valor (Fisherman)', 'ENTUSIAMO Y MUCHO VALOR, S.L.', '1', '$', 430000383, '', 'juanjo.izquierdo@emvdos.es', 'B85808376', '915199121', 'MADRID', '', ''),
(367, 'FIne Line Services Asia Ltd', 'FINE LINE SERVICES ASIA LTD', '1', '$', 430000384, '', 'katarzyna.iwanich@friendlymediagroup.com', '2292626', '603607862', '', '', ''),
(368, '', 'SPAIN BUSINESS SCHOOL CONSUL Y SERV SL', '1', '$', 430000385, '', 'jlpeino@spainbs.com', 'B87479812', '', 'MADRID', '', ''),
(369, '', 'INTEGRATED CALL CENTRE SERVICES S.L.', '1', '$', 430000386, '', 'administracion@iccs.es', 'B92266915', '951088080', 'M¡LAGA', '', ''),
(370, 'Maverick Media', 'MAVERICK MEDIA SL', '1', '$', 430000387, '', 'caro@maverickmedia.eu', 'B65798688', '933189510', 'BARCELONA', '', ''),
(371, '', '4F SALES INC', '1', '$', 430000388, '', 'affiliates@4finance.com', '5697577', '', 'FLORIDA', '', ''),
(372, 'NEXT CORREDURÕA DE SEGUROS TELEM¡TICOS, S.L.', 'NEXT CORREDURÕA DE SEGUROS TELEMATICOS, ', '1', '$', 430000389, '', 'dcuenca@gruponext.es', 'B14973010', '917895349', 'MADRID', '', ''),
(373, 'LinkaLeads S.L.', 'LINKALEADS, S.L.L.', '1', '$', 430000390, '', 'susana@linkaleads.com ', 'B22404149', '', 'HUESCA', '', ''),
(374, '', 'ROIANDCO CONSULTING, S.L.', '1', '$', 430000391, '', 'sbea@roiandco.com', 'B66692377', '', 'BARCELONA', '', ''),
(375, '', 'PROYECTOS EGONCU SL', '1', '$', 430000392, '', 'nadalesfr@oficinalocal.segurcaixaAdeslas.es', 'B93311512', '', 'M¡LAGA', '', ''),
(376, '', 'CONTESTA TELESERVICIOS, S.A.', '1', '$', 430000393, '', 'rogelio.carrero@contesta.es', 'A82950312', '', 'MADRID', '', ''),
(377, '', 'MASTER ZEN, S.L.', '1', '$', 430000394, '', 'luismaria@masterzen.es', 'B87154944', '', 'MADRID', '', ''),
(378, '2-30 M…DIA', '2-30 M…DIA', '1', '$', 430000395, '', 'comptabilite@deux-trente.com', '81751283938', '177122478', '', '', ''),
(379, 'Publiespacio Media Consulting SL', 'PUBLIESPACIO MEDIA CONSULTING SL', '1', '$', 430000396, '', 'fernando@publiespacio.com', 'B84365311', '', 'MADRID', '', ''),
(380, 'OCU EDICIONES S.A.', 'OCU EDICIONES, S.A. ', '1', '$', 430000397, '', 'mreyes@ocu.org', 'A78602091', '917226031', 'MADRID', '', ''),
(381, 'VIVA AQUA SERVICE SPAIN S.A.(Aquaservice))', 'VIVA AQUA SERVICE SPAIN SA', '1', '$', 430000398, '', 'evamaria.marcos@aquaservice.com', 'A41810920', '961415500', 'VALENCIA/VAL…NCIA', '', ''),
(382, '', 'PRIME TIME CONSULTING S.L.', '1', '$', 430000399, '', 'Daniel.vecina@prtconsulting.es', 'B84882588', '', 'MADRID', '', ''),
(383, 'NETTHINK IBERIA SL', 'NETTHINK IBERIA, S.L.U.', '1', '$', 430000400, '', 'raquel.gomez@iprospect.es', 'B82314089', '91-3536200', 'MADRID', '', ''),
(384, '', 'SALESLAND S.L.', '1', '$', 430000401, '', 'francisco.cano@kaptalc.es', 'B82738675', '', 'MADRID', '', ''),
(385, 'REPARALIA S.A.U.', 'REPARALIA SAU', '1', '$', 430000402, '', 'proveedores@reparalia.es', 'A82451410', '91-3277201', 'MADRID', '', ''),
(386, 'Cash Converters EspaÒa SL', 'CASH CONVERTERS ESPA—A S.L.', '1', '$', 430000403, '', 'spintado@cash-converters.es', 'B61135851', '952907169', 'MADRID', '', ''),
(387, 'Travelscan AG', 'TRAVELSCAN AG', '1', '$', 430000404, '', 'giovanni.ciaccio@hotelscan.com', '166760645', '', '', '', ''),
(388, 'Habitissimo SL', 'HABITISSIMO, S.L.', '1', '$', 430000405, '', 'cnaveda@habitissimo.com', 'B65100240', '9714398514', 'ILLES BALEARS', '', ''),
(389, 'Logitravel S.L.', 'LOGITRAVEL, S.L.', '1', '$', 430000406, '', 'invoice@logitravel.com', 'B57288193', '971080125.EX101', 'ILLES BALEARS', '', ''),
(390, 'Conversant Europe Limited', 'CONVERSANT EUROPE LIMITED', '1', '$', 430000407, '', 'mcalvar@cj.com', '752320754', '33146519170', '', '', ''),
(391, 'Acquajet SL', 'SOCIEDAD ESPA—OLA DE MAQUINAS PARA AGUA', '1', '$', 430000408, '', 'ocuervovazquez@acquajet.com', 'B06304984', '902-154557', 'SEVILLA', '', ''),
(392, 'Westwing Group GMBH', 'WESTWING GROUP GMBH', '1', '$', 430000409, '', 'daniel.fernandez@westwing.de', '279806558', '489550544', 'Munich', '', '');
INSERT INTO `advertisers` (`id`, `nombre`, `social`, `id_am`, `money`, `cod_client`, `nation`, `contact_mail`, `CifDni`, `phone`, `state`, `notes`, `address`) VALUES
(393, '40 & FAB LTD', '40 & FAB LIMITED', '1', '$', 430000410, '', 'marketing@dianaveggenze.com', '142095334', '', '', '', ''),
(394, 'Welmory Limited', 'WELMORY LIMITED', '1', '$', 430000411, '', 'm.zeligowska@adwellmedia.com ', '10245903J', '', 'CHIPRE', '', ''),
(395, '', 'TRIBOO MEDIA SRL', '1', '$', 430000412, '', 'fatture@leonardoadv.it', '6933670967', '+39 02 64.74.14', 'MILANO', '', ''),
(396, 'EducaOnline, S.L', 'EDUCAONLINE, S.L', '1', '$', 430000413, '', 'gorchs@dep.net  ', 'B62257589', '932151365', 'BARCELONA', '', ''),
(397, 'Datawad s.r.l.', 'DATAWAD S.R.L.', '1', '$', 430000414, '', 'facturation@datawad.com', '8853400961', '', 'MILANO', '', ''),
(398, 'ONTWICE INTERACTIVE SERVICES SL', 'ONTWICE INTERACTIVE SERVICE SL', '1', '$', 430000415, '', 'sergio.yuste@contversion.com', 'B84894575', '682695557', 'MADRID', '', ''),
(399, 'Sparkling Adv SL', 'SPARKLING ADV SLU', '1', '$', 430000416, '', 'matildesparkling@gmail.com', 'B66737958', '674185820', 'BARCELONA', '', ''),
(400, 'GAFAS EN RED DE ”PTICAS SL', 'GAFAS EN RED DE ”PTICAS SL', '1', '$', 430000417, '', 'contabilidad@gafas.es', 'B73833758', '', 'MURCIA', '', ''),
(401, 'EDENRED ESPA—A SA', 'EDENRED ESPA—A SA', '1', '$', 430000418, '', 'Eduardo.ZAMANILLO@edenred.com', 'A78881190', '', 'MADRID', '', ''),
(402, 'e-Sixt GmbH & Co. KG', 'E-SIXT GMBH & CO. AUTOVERMIETUNG', '1', '$', 430000419, '', 'laura.rosado-rey@sixt.com', '814276027', '', 'GERMANY', '', ''),
(403, 'FRANCE GOURMET DIFFUSION', 'FRANCE GOURMET DIFISION (FGD)', '1', '$', 430000420, '', 'contact@ventealapropriete.com', '43480451939', '', 'FRANCE', '', ''),
(404, 'Companies House', 'COMPANIES HOUSE', '1', '$', 430000421, '', ' display-invo@secretescapes.com', '103138366', '', 'LONDON', '', ''),
(405, '', 'AVANZA OUTBOUND SERVICES, SL AVANZA', '1', '$', 430000422, '', 'albertobastida@kaptalc.es', 'B63541890', '610450515', 'MADRID', '', ''),
(406, 'LinkAppeal S.r.l.s.', 'LINKAPPEAL SRLS', '1', '$', 430000423, '', 'amministrazione@linkappeal.it', '7341001217', '', 'NAPOLI', '', ''),
(407, 'IDFINANCE SPAIN SL', 'IDFINANCE SPAIN SL', '1', '$', 430000424, '', 'afiliados@moneyman.es', 'B66487190', '+34 934617231', 'BARCELONA', '', ''),
(408, 'English Worldwide', 'ENGLISH WORLDWIDE', '1', '$', 430000425, '', 'rperez@abaenglish.com', 'B64401482', '+34 934091396', 'BARCELONA', '', ''),
(409, 'Aprendum Compra Colectiva S.L', 'APRENDUM COMPRA COLECTIVA', '1', '$', 430000426, '', 'jsalcedo@aprendum.com', 'B86277787', '676482080', 'MADRID', '', ''),
(410, 'COOKIEPATH SLU', 'COOKEYPATH SLU', '1', '$', 430000427, '', 'cuentas@cookeypath.com', 'B87515722', '', 'MADRID', '', ''),
(411, 'Comefruta Internet SL', 'COMEFRUTA INTERNET SL', '1', '$', 430000428, '', 'joseluis@comefruta.es', 'B86121696', '', '', '', ''),
(412, 'Viciano Servicios Juridicos (Vabogados)', 'VICIANO SERVICIOS JURIDICOS, S.L.P.', '1', '$', 430000429, '', 'sve@viciano.com', 'J15865397', '696872611', 'MADRID', '', ''),
(413, '', 'THYSSENKRUPP ENCASA SRL SUCURSAL ESPA—A', '1', '$', 430000430, '', 'belen.galan@tkec.es', 'W0056443E', '914922292', 'MADRID', '', ''),
(414, 'Adventure Conseil', 'ADVENTURE CONSEIL', '1', '$', 430000431, '', 'adventureconseil@receiptbank.me', '3793819673', '', '', '', ''),
(415, 'Iris Media agencia de medios SL', 'IRIS MEDIA AGENCIA DE MEDIOS S.L.', '1', '$', 430000432, '', 'nfernandez@irismedia.es', 'B84584705', '91-7250965', 'MADRID', '', ''),
(416, '', 'EXPECTA INGENIERIA', '1', '$', 430000433, '', 'crespo@expectaingenieria.com', 'A87049904', '910064936', 'MADRID', '', ''),
(417, 'Brico Prive', 'BRICOPRIVE', '1', '$', 430000434, '', 'marie@bricoprive.com', '22788504629', '05 61 70 52 27', 'LUnion', '', ''),
(418, 'Societe MaFringue SARL', 'SOCIETE MA FRINGE SARL', '1', '$', 430000435, '', 'invoicing@mafringue.com', '86810334177', '33 (0)148567031', 'Paris', '', ''),
(419, 'ADPulse', 'LOCAL MEDIA', '1', '$', 430000436, '', 'gdoumou@local-media.com', '21534188024', '01 73 03 46 19 ', 'Paris', '', ''),
(420, 'Luce ADV srl', 'LUCE ADV SRL', '1', '$', 430000437, '', 'fatturazione@luceadv.it', '7594841210', '', 'Napoli', '', ''),
(421, 'DIGITAL METRICS SRL (GEKO ADV)', 'DIGITAL METRICS†S.R.L.', '1', '$', 430000438, '', 'invoice@digitalmetrics.eu', '9091511007', '0606 54832176', 'ROMA', '', ''),
(422, '', 'VAUGHAN INTENSIVOS RESIDENCIALES', '1', '$', 430000439, '', 'margarita.caro@grupovaughan.com', 'B84820919', '', 'Madrid', '', ''),
(423, 'CARDATA SA', 'CARDATA', '1', '$', 430000440, '', 'tatiana@cardata.fr', '28500942990', '', 'Le Chesnay', '', ''),
(424, 'WHG (International Limited)', 'WHG (INTERNATIONAL) LIMITED', '1', '$', 430000441, '', 'or.bronstein@williamhill.com', '99191', '972(73)7333299', '', '', ''),
(425, '', 'OMNITELECOM LTD.', '1', '$', 430000442, '', 'Isaac@omnitelecom.com', '513470971', '16 093 183 126', '', '', ''),
(426, 'Hello Media Group SL', 'HELLO MEDIA GROUP, S.L.', '1', '$', 430000443, '', 'facturacion@hellomedia.com', 'B86899630', '91 193 66 50 ', 'MADRID', '', ''),
(427, '', 'QUALYTEL ANDALUCIA, S.L.U.', '1', '$', 430000444, '', 'beatriz.fernandez@arvato.es', 'A41954959', '913253860', 'SEVILLA', '', ''),
(428, 'Intredo Ltd', 'INTREDOMEDIA LTD', '1', '$', 430000445, '', 'a.bartkowiak@intredo.com', '23423523', '', '', '', ''),
(429, 'Data Factory', 'THE DATA FACTORY', '1', '$', 430000446, '', 'accounts-payable@thedata-factory.com', 'B66411067', '', 'BARCELONA', '', ''),
(430, 'INVERSI”N EN PROINDIVISOS,S.L.', 'INVERSI”N EN PROINDIVISOS,S.L.', '1', '$', 430000447, '', 'gip@giproindivisos.com', 'B87205407', '900101108', 'MADRID', '', ''),
(431, 'MediaNoe', 'MEDIANOE', '1', '$', 430000448, '', 'factures@medianoe.com', '59519273155', '+33†0†183694682', 'PARIS', '', ''),
(432, 'The Click Lab S.L.', 'THE CLICKLAB', '1', '$', 430000449, '', 'invoicing@theclicklab-advertising.com', 'B66108002', '931704014', 'BARCELONA', '', ''),
(433, 'Notify', 'NOTIFY', '1', '$', 430000450, '', 'lmorales@adlead.fr', '3490992948', '33 0145613030', '', '', ''),
(434, 'Tradeo Ltd', 'TRADEO LTD', '1', '$', 430000451, '', 'suport@tradeo.com', '514515337', '44203695855', 'TEL AVIV', '', ''),
(435, '', 'AHORRADORES DIGITALES', '1', '$', 430000452, '', 'luis@adgravity.com', 'B87559399', '910523002', 'Madrid', '', ''),
(436, 'Macho Beard Company, S.L.', 'MACHO BEARD COMPANY SL', '1', '$', 430000453, '', 'nestoralriols@machobeardcompany.com', 'B66331281', '', 'BARCELONA', '', ''),
(437, '22BIS MEDIA', '22 BIS MEDIA ', '1', '$', 430000454, '', 'wilfried.valmorin@22bismedia.com', '3799089586', '33(0)174313620', 'Boulogne-Billancourt', '', ''),
(438, '', 'THEWONDERTRIP SL', '1', '$', 430000455, '', 'cblanco@thewondertrip.com', 'B87415071', '', 'MADRID', '', ''),
(439, 'Brainlang SL', 'BRAINLANG SL', '1', '$', 430000456, '', 'cesar@brainlang.com', 'B87267472', '627708432', 'MADRID', '', ''),
(440, 'Alain Afflelou EspaÒa, S.A.', 'ALAIN AFFLELOU ESPA—A S.A.U', '1', '$', 430000457, '', 'vmagalhaes@afflelou.es', 'A83759019', '', 'MADRID', '', ''),
(441, 'IAP LTD', 'IAP LTD', '1', '$', 430000458, '', 'marketing@ia-patrimoine.com', 'CD2178/16', '33182887530', '', '', ''),
(442, 'AMV HISPANIA CORREDURIA DE SEGUROS S.L.', 'AMV HISPANIA CORREDURIA DE SEGUROS, SL', '1', '$', 430000459, '', 'facturacion@amvseguros.es', 'B83204586', '902125750', 'MADRID', '', ''),
(443, 'Socketines S.L', 'SOCKETINES', '1', '$', 430000460, '', 'jorge@socketines.com', 'B87025052', '670930892', 'Madrid', '', ''),
(444, 'Ornet Ventures SARL', 'ORNET VENTURES SARL', '1', '$', 430000461, '', 'julien@credit-conso.org', '72532573573', '+33 1 85090129', '', '', ''),
(445, 'IW MEDIA LTD', 'IW MEDIA LTD', '1', '$', 430000462, '', 'bayer@iwoza.com', '430000462', '4312297743', 'TORTOLA', '', ''),
(446, 'Buy Premier Leads', 'BUY PREMIER LEADS', '1', '$', 430000463, '', 'facturacion@buypremierleads.com', 'B93501534', '', 'M¡LAGA', '', ''),
(447, 'Leadiya Ltd', 'LEADIYA LTD', '1', '$', 430000464, '', 'hossam@leadiya.com', 'A16209137050', '21625231144', '', '', ''),
(448, '', 'WHG SERVICES LIMITED', '1', '$', 430000465, '', 'alina.botnyayev@williamhill.com', '99191', '972(73)7333299', 'London', '', ''),
(449, '', 'MEDONIA MARKETING LTD', '1', '$', 430000466, '', 'shira@playmillionpartners.com', '1661003', '', 'Tortola', '', ''),
(450, 'NeoDevis', 'PKY SERVICES SARL (NEO-DEVIS)', '1', '$', 430000467, '', 'contact@neo-devis.com', '1502050818', '09 60 13 09 07', '', '', ''),
(451, '', 'RICHFIELD CAPITAL LTD', '1', '$', 430000468, '', 'amelie.c@247traffic.com', '430000468', '353 15134970', 'BELICE', '', ''),
(452, '', 'UNY SRL', '1', '$', 430000469, '', 'miguelrodriguez@unygroup.eu', '4215790405', '687-212840', 'RICCIONE', '', ''),
(453, '', 'SALUDONNET GLOBAL MARKET S.L. ', '1', '$', 430000470, '', 'alejandro.canora@saludonnet.com', 'B87013298', '34 91.636.42.92', 'MADRID', '', ''),
(454, '', 'AMNISTIA INTENACIONAL', '1', '$', 430000471, '', 'rciacco@es.amnesty.org', 'G28667152', '913101277 ext65', 'MADRID', '', ''),
(455, 'Marketing Netpartners Limited', 'MARKETING NETPARTNERS LIMITED', '1', '$', 430000472, '', 'desislava.dodeva@netopartners.com', '1836700', '', '', '', ''),
(456, 'Leadeal Marketing', 'SARL LEADEAL MARKETING', '1', '$', 430000473, '', 'lloubere@leadeal-marketing.com', '84520205592', '', 'PARIS', '', ''),
(457, 'ANTEVENIO SRL Italia', 'ANTEVENIO SRL ITALIA SRL', '1', '$', 430000474, '', 'mgurnari@antevenio.com', '4218260968', '', 'MILANO', '', ''),
(458, '', 'FENIX†DEALS† SLU', '1', '$', 430000475, '', '', 'B87471496', '', 'MADRID', '', ''),
(459, '', 'WONDEOTEC SA', '1', '$', 430000476, '', 'finance@wondeotec.com', '513099743', '+35 300401582', 'Porto', '', ''),
(460, 'Mutui Per La Casa', 'BACK OFFICE SRL', '1', '$', 430000477, '', 'sara.colnaghi@mutuiperlacasa.com', '10030581002', '39 02 89295-203', '', '', ''),
(461, 'Ga Media', 'GA MEDIA  EURL ', '1', '$', 430000478, '', 'contact@ga-media.fr', '24807959176', '330618032955', '', '', ''),
(462, 'Betmedia Soluciones, S.L.', 'BETMEDIA SOLUCIONES', '1', '$', 430000479, '', 'Martafreire@hastoplay.com', 'B70264312', '649.747.065', 'Galicia', '', ''),
(463, 'COOKINGTKC Atlanta Gate sl', 'ATLANTA GATE S.L', '1', '$', 430000480, '', 'yfernandez@cookingtkc.com', 'B64802895', '93-4875551', 'Barcelona', '', ''),
(464, 'Triboo Spa', 'TRIBOO S.P.A.', '1', '$', 430000481, '', 'fatture_editori@leonardoadv.it', '3273450969', '', 'MILANO', '', ''),
(465, '', 'SWARMIZ SL', '1', '$', 430000482, '', 'invoicing@swarmiz.com', 'B66725995', '', 'BARCELONA', '', ''),
(466, 'WebPerformance', 'WEBPERFORMANCE SRL', '1', '$', 430000483, '', 'amministrazione@webperformance.it', '2186380222', '', '', '', ''),
(467, '', 'VAROP SERVICIOS JURÕDICOS, S.L.P.', '1', '$', 430000484, '', 'info@vabogados.com', 'B87664272', '', 'MADRID', '', ''),
(468, 'DevisProx', 'SAS DEVISPROX', '1', '$', 430000485, '', 'lea@devisprox.com', '91502709769', '', '', '', ''),
(469, 'Traffic Advisor Ltd', 'TRAFFIC ADVISOR LTD', '1', '$', 430000486, '', 'augusto@trafficadv.co.uk', '215735024', '', '', '', ''),
(470, 'Webreflex', 'WEBREFLEXE', '1', '$', 430000487, '', 'affiliate@webreflexe.com', '91492362876', '', '', '', ''),
(471, 'AXA Global Direct Seguros y Reaseguros S.A.U', 'AXA GLOBAL DIRECT SEGUROS Y REASEGUROS ', '1', '$', 430000488, '', 'laura.bravo.garcia@directseguros.es', 'A81357246', '', 'MADRID', '', ''),
(472, '', 'DATACENTRIC', '1', '$', 430000489, '', 'graido@datacentric.es', 'A80845050', '913822002ext224', 'MADRID', '', ''),
(473, 'Feebbo solutions SL', 'FEEBBO SOLUTIONS S.L.', '1', '$', 430000490, '', 'tech@feebbo.com', 'B13515622', '', 'CIUDAD REAL', '', ''),
(474, 'GEO ALTERNATIVA, S.L.', 'GEO ALTERNATIVA SL', '1', '$', 430000491, '', 'andrea.peino@mipodo.com', 'B87382644', '', 'MADRID', '', ''),
(475, 'Bonzai Digital', 'SASU BONZAI DIGITAL', '1', '$', 430000492, '', 'mbaudouin@bonzai.digital', '26514635671', '', '', '', ''),
(476, '', 'TEKKA SPA ', '1', '$', 430000493, '', ' m.costa@tekka.it', '9479150014', ' +39 344 127607', '', '', ''),
(477, 'InstaVets Servicio Online SL', 'INSTAVETS SERVICIO ONLINE SL', '1', '$', 430000494, '', ' alex.abando@instavets.com', 'B87216396', '650000528', 'MADRID', '', ''),
(478, '', 'LEAN ABOGADOS', '1', '$', 430000495, '', 'contabilidad@abaci.es', 'G87679890', '+34 932176882', 'MADRID', '', ''),
(479, 'Matiere Grise', 'MATIERE GRISE, SAS', '1', '$', 430000496, '', 'valerie@matiere-grise.fr', '22394485973', '645750512', 'STRASBOURG', '', ''),
(480, 'Acciones y Servicios de Telemarketing, S.L.', 'ACCIONES Y SERVICIOS DE TELEMARKETING†', '1', '$', 430000497, '', 'administracionleon@telemark-spain.com', 'B24624801', '660880705', 'LE”N', '', ''),
(481, 'Bysidecar S.L.', 'BYSIDECAR, S.L.', '1', '$', 430000498, '', 'angelescasas@bysidecar.com', 'B70466057', '650404205', 'A CORU—A', '', ''),
(482, 'Madz Digital Business', 'MADZ DIGITAL BUSINESS, S.L.', '1', '$', 430000499, '', 'nancy.decastro@madzdigitalbusiness.com', 'B87741229', '', 'MADRID', '', ''),
(483, 'AdvertiseMe', 'ADVERTISE ME', '1', '$', 430000500, '', 'mdien@advertise-me.net', '47503747040', '+33 04 86 68 5 ', '', '', ''),
(484, '', 'LEADSOLUTION INTELIGENCIA DIGITAL S', '1', '$', 430000501, '', 'finanzas@leadsolution.es', 'B98816499', '+34 960 626 200', 'VALENCIA/VAL…NCIA', '', ''),
(485, '', 'CRECERA CALL CENTER SERVICES', '1', '$', 430000502, '', 'jose.escobedo@ventask.com', 'B86873783', '67567115', 'MADRID', '', ''),
(486, 'MediaComm', 'MEDIACOMM LECCE SAS', '1', '$', 430000503, '', '', '4789990753', '0039 3939346105', '', '', ''),
(487, 'Motorpress IbÈrica S.A.', 'MOTORPRESS IBERICA SA', '1', '$', 430000504, '', 'mjulia@mpib.es', 'A82090952', '91 347 00 46', 'MADRID', '', ''),
(488, 'Lead the Lead S.L.', 'LEAD THE LEADS SL', '1', '$', 430000505, '', 'aida@leadtheleads.com', 'B66592528', '', 'BARCELONA', '', ''),
(489, 'Avent Media', 'AVENT MEDIA', '1', '$', 430000506, '', 'compta@avent-media.fr', '90492104500', '0033 141792494', 'Levallois Perret', '', ''),
(490, 'Evolution Marketing', 'EVOLUTION MARKETING', '1', '$', 430000507, '', 'michele@evolutionland.eu', '473102553', '32 (0)493783886', '', '', ''),
(491, 'Kovetz Online', 'KOVETZ ONLINE', '1', '$', 430000508, '', 'comptabilite@kovetz-online.com', '5143343630', '33 178407100', '', '', ''),
(492, 'Istituto Bancario del Lavoro S.p.A.', 'ISTITUTO BANCARIO DEL LAVORO S.P.A.', '1', '$', 430000509, '', 'enrico.gigliotti@iblbanca.it', '897081006', '06/4666911', '', '', ''),
(493, '', 'DIME MARKETING S.L.', '1', '$', 430000510, '', '', 'B86871928', '', 'MADRID', '', ''),
(494, '', 'DIGITAL DARTS GROUP, S.L.', '1', '$', 430000511, '', 'jestevez@digitaldartsco.com', 'B70468392', '', 'GALICIA', '', ''),
(495, 'TEYAM… 360 S.L.', 'TEYAM… 360, S.L.', '1', '$', 430000512, '', 'oarnaldo@teyame.es', 'B86691540', '', '', '', ''),
(496, '', 'FEDERICO CORDA', '1', '$', 430000513, '', 'fcorda@performind.eu', 'Y2437473Q', '', 'BARCELONA', '', ''),
(497, 'Emergia', 'EMERGIA EXCELLENCE, SL', '1', '$', 430000514, '', 'afmonzon@emergiacc.com', 'B65520876', '679546333', 'BARCELONA', '', ''),
(498, '', 'TELEMARK BPO, SL', '1', '$', 430000515, '', 'Laura.sastre@tlmark.com', 'B24632267', '660880705', 'LE”N', '', ''),
(499, '', 'PRECIO OPTIMO SELECCION, SL', '1', '$', 430000516, '', 'daniel.vecina@prtconsulting.es', 'B86652732', '659342428', 'MADRID', '', ''),
(500, 'VIAJES EL CORTE INGLES, SA', 'VIAJES EL CORTE INGLES, SA', '1', '$', 430000517, '', 'mariofernandez@viajeseci.es', 'A28229813', '', 'Madrid', '', ''),
(501, 'BMIND Sales Maker Company S.L', 'BMIND SALES MAKER COMPANY S.L.', '1', '$', 430000518, '', 'billing@businessmind.es', 'B87363891', '', 'MADRID', '', ''),
(502, '', 'FRANCE CONSOMMABLE SRL', '1', '$', 430000519, '', 'alexandre.jasmin@sfr.fr', '39807580493', '', '', '', ''),
(503, 'The Story Tailors', 'THE STORY TAILORS, SL', '1', '$', 430000520, '', 'soporte@lamagiademinombre.com', 'B66606617', '93 368 23 84', 'BARCELONA', '', ''),
(504, 'Occhiali24.it SRL', 'OCCHIALI24.IT GMBH', '1', '$', 430000521, '', 'm.gonzalez@gafas.es', '2934890217', '', '', '', ''),
(505, '', 'CAPRI TELECOMUNICACIONES 2016 S.L.', '1', '$', 430000522, '', 'p.bonilla@v3click.com', 'B87488474', '', 'MADRID', '', ''),
(506, '', 'BASIC FIT SPAIN S.A.U', '1', '$', 430000523, '', 'francisco.lopez@basic-fit.es', 'A82553447', '91 425 10 50', 'MADRID', '', ''),
(507, '', 'KREDITECH SPAIN S.L.', '1', '$', 430000524, '', 'xavier.romero@kreditech.com', 'B65895252', '', 'MADRID', '', ''),
(508, 'Didactia Digital', 'DID¡CTICA DIGITAL, SL', '1', '$', 430000525, '', 'iconsul@didacticadigital.es', 'B65032070', '93 473 52 99 ', 'BARCELONA', '', ''),
(509, 'Viwom Video Marketing S.L.', 'VIWOM VIDEO MARKETING SL', '1', '$', 430000526, '', 'balino@viwom.com', 'B26510826', '', 'LA RIOJA', '', ''),
(510, 'Arriaga Asesoramiento JurÌdico y EconÛmico S.L.', 'ARRIAGA ASESORAM.JURÕDICO Y ECON”MICO SL', '1', '$', 430000527, '', 'ealfaya@arriagaasociados.com', 'B98488117', '', 'MADRID', '', ''),
(511, '', 'ATIENZA ASESORES S.L.U.', '1', '$', 430000528, '', '', 'B86667540', '', 'Madrid', '', ''),
(512, '', 'BENDITA EXTREMADURA S.L.', '1', '$', 430000529, '', 'lpzvqz.julio.jgb@gmail.com', 'B86884509', '', 'Madrid', '', ''),
(513, '', 'REGALOS E IDEAS DE GOMA SL', '1', '$', 430000530, '', '', 'B86571726', '', 'MADRID', '', ''),
(514, 'Nest4Innovation (Adstrategy)', 'NEST4INNOVATION LDA', '1', '$', 430000531, '', 'Mauro.ferreira@adstrategy.pt', '513241558', '', '', '', ''),
(515, '', 'COMODON DESCANSO, SL', '1', '$', 430000532, '', 'info@colchocomodon.es', 'B73708216', '968752655', 'MURCIA', '', ''),
(516, '', 'GLOBAL ADVANCED SOLUTIONS LIMITED', '1', '$', 430000533, '', '', '20170405RT', '', '', '', ''),
(517, '', 'TEAM BOOKING, S.L.', '1', '$', 430000534, '', 'zigor.lapera@groupinghotels.com', 'B87210670', '678613580', 'MADRID', '', ''),
(518, '', 'SOLUCIONES INTEGRALES DE FORMACI”N S.A.', '1', '$', 430000535, '', 'joseantonio.nunez@structuralia.com', 'A82914417', '91 490 42 00', 'MADRID', '', ''),
(519, 'SEP EYEWEAR S.L.', 'SEP EYEWEAR SL.', '1', '$', 430000536, '', 'sergio@millerandmarc.com', 'B87363867', '', 'MADRID', '', ''),
(520, '', 'ADSALSA INTERACTIVE S.L.', '1', '$', 430000537, '', 'mireia.garcia@adsalsagroup.com', 'B54769682', '', 'ALICANTE/ALACANT', '', ''),
(521, '', 'ACTION CROSS SLU', '1', '$', 430000538, '', '', 'B82822438', '', 'MADRID', '', ''),
(522, '', 'FEDERICO CORDA', '1', '$', 43000059, '', 'fcorda@performind.eu', 'Y2437473Q', '', 'BARCELONA', '', ''),
(523, '', 'QUALYTEL ANDALUCIA, S.L.U.', '1', '$', 43000439, '', 'juanpablo.leal@arvato.es', 'A41954959', '913253860', 'SEVILLA', '', ''),
(524, '', 'KARPE DEAL S.L.', '1', '$', 433000001, '', 'blanero@karpedeal.com', 'B86305513', '', 'Madrid', '', ''),
(525, 'SMART SHOP NETWORK S.L.', 'SMART SHOP NETWORK S.L.', '1', '$', 433000002, '', '', 'B86305596', '', 'Madrid', '', ''),
(526, '', 'MAXIDAT SL', '1', '$', 433000003, '', '', 'B86185204', '', 'Madrid', '', ''),
(527, '', 'FENIX DATA SL', '1', '$', 433000004, '', '', 'B86185170', '', 'Madrid', '', ''),
(528, '', 'DUJOK ICONSULTING SL', '1', '$', 433000005, '', 'cristina.alcantarilla@dujok.com', 'B86287794', '', 'Madrid', '', ''),
(529, '', 'FAST TRACK NETWORK SL', '1', '$', 433000006, '', 'elena.rubio@fasttracknet.es', 'B86296712', '', 'Madrid', '', ''),
(530, '', 'BORSENYA SLU', '1', '$', 433000007, '', '', 'B86810421', '', 'Madrid', '', ''),
(531, 'Content Commerce Factory S.L.', 'CONTENT COMMERCE FACTORY SL', '1', '$', 433000008, '', '', 'B87299996', '911169539', 'MADRID', '', ''),
(532, 'DIGITAL DISTRIBUTION MANAGEMENT', 'DIGITAL DISTRIBUTION MANAGEMENT SL ', '1', '$', 434000000, '', 'thomas.franconieri@digidis.net', 'B84276773', '', 'Madrid', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `affiliates`
--

CREATE TABLE `affiliates` (
  `id` int(4) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `social` varchar(80) NOT NULL,
  `affm` varchar(30) NOT NULL,
  `money` varchar(2) DEFAULT '$',
  `cod_client` int(12) DEFAULT NULL,
  `nation` varchar(50) DEFAULT NULL,
  `contact_mail` varchar(200) DEFAULT NULL,
  `CifDni` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `notes` varchar(1000) DEFAULT 'Notes...',
  `address` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `affiliates`
--

INSERT INTO `affiliates` (`id`, `nombre`, `social`, `affm`, `money`, `cod_client`, `nation`, `contact_mail`, `CifDni`, `phone`, `state`, `notes`, `address`) VALUES
(1, 'Appaniac', 'Appaniac LTD', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(2, 'Allitap', 'Aircyb Limited', '8', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(3, 'Mobco', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(4, 'Cost2action', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(5, 'Avazu', 'Avazu (Brunei) afiliación', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(6, 'Digital Pow Pow', 'Xertive Global media LTD', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(8, 'Mozoo', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(9, 'Misky Ads', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(10, 'Stingrad', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(11, 'IncentMobi', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(12, 'SabiaMedia', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(13, 'Wextmedia', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(14, 'Ströer Mobile Performance CPA', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(15, 'Affle CPA', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(16, 'Crazy4Media CPA', 'Crazy4Media Online LTD', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(17, 'Midco', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(18, 'iWoop CPA', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(19, 'IncentMobi CPA', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(20, 'Publited', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(21, 'Linkadia CPA', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(22, 'Bido-Studio.co', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(23, 'Gombio', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(24, 'Bidsopt', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(25, 'Add Value Media', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(26, 'Revenuemob', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(27, 'CashOnMobi', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(28, 'CPI Traffic', 'CPITRAFFIC.COM', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(29, 'iWoop CPI', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(30, 'Hexcan', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(31, 'CrowMobi - Sax Limited', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(32, 'Zhengfang Wang', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(33, 'Adsfast', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(34, 'BestApps', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(35, 'ONEAPI - SVG Media', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(36, 'Publited CPI', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(37, 'SabotageAds', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(38, 'Linkadia CPI', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(39, 'Trooperads CPI', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(40, 'Voluum', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(41, 'Native X', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(42, 'SelfAdvertiser', '', '6', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(43, 'Minimob', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(44, 'App Booster', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(45, 'Adcash', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(46, 'Revmob CPI', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(47, 'DCypher Media', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(48, 'Instal.com', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(49, 'Spiroox', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(50, 'Opera', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(52, 'Smartlead', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(53, 'Mobchain', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(54, 'Offerseven', 'Offerseven', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(55, 'Cosmic', '                      ', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(56, 'Avazu Inc', 'Avazu Inc', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(57, 'Billymob', 'Billymob', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(58, 'Bucksense', 'Bucksense', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(59, 'Clickadu', 'Markopros Media LLP', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(60, 'Kimia CPA', 'Kimia', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(61, 'Mobipium CPA', 'Mobipium CPA', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(62, 'MobiVisits', 'MobiVisits', '6', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(63, 'Mobquid', 'Mobquid', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(64, 'Reporo', 'Reporo', '7', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(65, 'ADXMI', 'Youmi Technology (HK) Ltd', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(67, 'Gasmobi', 'Gasmobi', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(68, 'Haka Mobi', 'Haka Mobi', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(69, 'Advance Mobile', 'Advance Mobile', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(70, 'ReachEffect', 'ReachEffect', '6', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(71, 'Exoclick', 'Exoclick', '7', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(72, 'Matomy - My DSP', 'Matomy - My DSP', '7', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(73, 'SelfAdvertiser', 'SelfAdvertiser', '6', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(74, 'Zeropark', 'Zeropark', '6', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(75, 'Trillium Interactive', 'Trillium Interactive', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(76, 'Glispa', 'Glispa', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(77, 'PopAds', 'Tomksoft S.A.', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(78, 'Arkeero', 'Rock Internet S.L.', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(79, 'BravAds', 'Bravads Strategic Online Advertising', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(80, 'RevLinker', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(81, 'Click2Comission CPA', 'Click2Comission CPA', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(82, 'Clariad CPA', 'Clariad Performance SA DE CV', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(83, 'Clickky CPA', 'Clickky LLC', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(84, 'Kimia CPA 2', 'Kimia CPA 2', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(85, 'PropellerAds', 'PropellerAds', '7', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(86, 'PlugRush', 'JMT Nordic', '7', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(87, 'MobAds', '', '7', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(88, 'Reach Effect', '', '6', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(89, 'Avazu Mobile DSP', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(90, 'ActiveRevenue', 'Info Bridge Ltd', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(91, 'JuicyAds', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(92, 'Matomy myDSP', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(93, 'FivePPC', '', '7', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(94, 'PopMyAds', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(95, 'AdXpansion', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(96, 'RevMob', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(97, 'Old Zero Park', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(98, 'AirPush', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(99, 'Hilltopads', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(100, 'Focuus', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(101, 'Inmobi', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(102, 'Traffic Shop', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(103, 'MediaHub', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(104, 'Buzzcity', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(105, 'Bidder Trafico', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(106, 'Adiquity', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(107, 'Fyber', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(108, 'Winclap', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(109, 'StartApp', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(110, 'AdSupply', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(111, 'Appreciate', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(112, 'TrafficJunky', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(113, 'Pocketmath', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(114, 'Traffic Factory', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(115, 'Affise', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(116, 'Adfly', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(117, 'EroAdvertising', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(118, 'Adultmoda', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(119, 'PopCash', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(120, 'RevMob 2 TEST', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(121, 'Mobicow', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(122, 'Spotify', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(123, 'LeadBolt', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(124, 'MobFox', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(125, 'Adivity', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(126, 'Appnext', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(127, 'Liquid', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(128, 'Adamo', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(129, 'Decisive', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(130, 'Ezmob', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(131, 'DNTX', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(132, 'No Affiliate', 'No Affiliate', 'No Affiliate', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(133, 'Affiliates', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(134, 'Appamplify.com', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(135, 'EffectMobi', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(136, 'SocialPubli', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(137, 'CloudMob', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(138, 'Techno Experience', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(139, 'Appromoters', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(166, 'Collectcent', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(167, 'MoiAds', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(168, 'Vykonia', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(169, 'Sushmita Dwivedi', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(170, 'WingoAds', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(171, 'Adbirdmedia', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(172, 'CloudMob2', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(173, 'Adsjoy', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(174, 'Smashmyads', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(175, 'Appalgo', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(176, 'TONIC', '', '2', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(177, 'TrafficForce', '', '2', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campaigns_month_close`
--

CREATE TABLE `campaigns_month_close` (
  `id` int(6) NOT NULL,
  `client` varchar(500) NOT NULL,
  `internal_mail` decimal(10,2) DEFAULT '0.00',
  `external_mail` decimal(10,2) DEFAULT '0.00',
  `invest_fb` decimal(10,2) DEFAULT '0.00',
  `fee_fb` decimal(10,2) DEFAULT '0.00',
  `setup_fb` decimal(10,2) DEFAULT '0.00',
  `invest_adwords` decimal(10,2) DEFAULT '0.00',
  `fee_adwords` decimal(10,2) DEFAULT '0.00',
  `setup_adwords` decimal(10,2) DEFAULT '0.00',
  `invest_rtb` decimal(10,2) DEFAULT '0.00',
  `fee_rtb` decimal(10,2) DEFAULT '0.00',
  `setup_rtb` decimal(10,2) DEFAULT '0.00',
  `invest_amazon` decimal(10,2) DEFAULT '0.00',
  `fee_amazon` decimal(10,2) DEFAULT '0.00',
  `setup_amazon` decimal(10,2) DEFAULT '0.00',
  `tracking` decimal(10,2) DEFAULT '0.00',
  `design` decimal(10,2) DEFAULT '0.00',
  `contents` decimal(10,2) DEFAULT '0.00',
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `campaigns_month_close`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `costes`
--

CREATE TABLE `costes` (
  `id` int(5) NOT NULL,
  `date` date NOT NULL,
  `affiliate` int(5) NOT NULL,
  `affm` int(5) DEFAULT NULL,
  `totaldollar` decimal(10,2) NOT NULL DEFAULT '0.00',
  `totaleuro` decimal(10,2) DEFAULT '0.00',
  `plataff` decimal(10,2) DEFAULT '0.00',
  `discrepancia` decimal(10,2) DEFAULT '0.00',
  `scrubs` decimal(10,2) DEFAULT '0.00',
  `onhold` decimal(10,2) DEFAULT '0.00',
  `validacion` decimal(10,2) DEFAULT '0.00',
  `status` int(2) DEFAULT NULL,
  `comments` varchar(100) DEFAULT NULL,
  `fact_number` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `costes`
--

INSERT INTO `costes` (`id`, `date`, `affiliate`, `affm`, `totaldollar`, `totaleuro`, `plataff`, `discrepancia`, `scrubs`, `onhold`, `validacion`, `status`, `comments`, `fact_number`) VALUES
(2, '2017-05-01', 25, 62, '1.82', '0.00', '0.00', '0.00', '0.00', '0.00', '1.82', 1, 'Ok', NULL),
(3, '2017-05-01', 65, 1, '1218.10', '0.00', '0.00', '0.00', '0.00', '0.00', '1218.10', 1, '', NULL),
(4, '2017-05-01', 15, 1, '5.03', '0.00', '0.00', '0.00', '0.00', '0.00', '5.03', 1, '', NULL),
(5, '2017-05-01', 1, 23, '3.29', '0.00', '0.00', '0.00', '0.00', '0.00', '3.29', 1, '', NULL),
(6, '2017-05-01', 27, 1, '1.50', '0.00', '0.00', '0.00', '0.00', '0.00', '1.50', 1, '', NULL),
(7, '2017-05-01', 66, 1, '256.87', '0.00', '0.00', '0.00', '0.00', '0.00', '256.87', 2, 'Kamila\'s Fault', NULL),
(8, '2017-05-01', 67, 1, '9.09', '0.00', '0.00', '0.00', '0.00', '0.00', '9.09', 1, '', NULL),
(9, '2017-05-01', 68, 1, '1.76', '0.00', '0.00', '0.00', '0.00', '0.00', '1.76', 1, '', NULL),
(10, '2017-05-01', 69, 1, '2.62', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '', NULL),
(11, '2017-05-01', 11, 1, '154.30', '0.00', '0.00', '0.00', '0.00', '0.00', '154.30', 1, '', NULL),
(12, '2017-05-01', 60, 1, '4713.08', '0.00', '0.00', '0.00', '0.00', '0.00', '4713.08', 1, '', NULL),
(13, '2017-05-01', 21, 1, '7.29', '0.00', '0.00', '0.00', '0.00', '0.00', '7.29', 1, '', NULL),
(14, '2017-05-01', 61, 1, '52.30', '0.00', '0.00', '0.00', '0.00', '0.00', '52.30', 1, '', NULL),
(15, '2017-05-01', 49, 23, '6072.12', '0.00', '0.00', '0.00', '0.00', '0.00', '6072.12', 1, '', NULL),
(16, '2017-05-01', 74, 69, '3562.67', '0.00', '3580.18', '17.51', '0.00', '0.00', '3580.18', 1, '', NULL),
(17, '2017-05-01', 64, 1, '235.14', '0.00', '258.62', '23.48', '0.00', '0.00', '258.62', 1, '', NULL),
(18, '2017-05-01', 70, 1, '736.03', '0.00', '0.00', '0.00', '0.00', '0.00', '736.03', 1, '', NULL),
(19, '2017-05-01', 71, 1, '291.61', '0.00', '329.75', '38.14', '0.00', '0.00', '329.75', 1, '', NULL),
(20, '2017-05-01', 72, 1, '210.08', '0.00', '220.84', '10.76', '0.00', '0.00', '220.84', 1, '', NULL),
(21, '2017-05-01', 42, 1, '100.04', '0.00', '0.00', '0.00', '0.00', '0.00', '100.04', 1, '', NULL),
(22, '2017-06-01', 60, 48, '11882.10', '0.00', '10556.50', '1325.60', '0.00', '0.00', '11882.10', 1, '', NULL),
(23, '2017-06-01', 49, 54, '9345.46', '0.00', '0.00', '0.00', '0.00', '0.00', '9345.46', 1, 'Total Cost = Validation + IVA PROVISION 8189.24 €', NULL),
(24, '2017-06-01', 65, 49, '6238.05', '0.00', '0.00', '0.00', '0.00', '0.00', '6238.05', 1, 'PROVISION 5466.22 €', NULL),
(25, '2017-06-01', 25, 52, '661.20', '0.00', '0.00', '0.00', '0.00', '0.00', '661.20', 1, 'Total Cost = Validation + IVA - Se confirma al Aff sin Validación de ADV PROVISION 579.39 €', NULL),
(27, '2017-06-01', 3, 51, '98.00', '0.00', '0.00', '0.00', '0.00', '0.00', '98.00', 1, '', NULL),
(28, '2017-06-01', 12, 51, '129.36', '0.00', '0.00', '0.00', '0.00', '0.00', '129.36', 1, 'Se confirma al Aff sin validación ADV PROVISION 113.35 €', NULL),
(29, '2017-06-01', 66, 52, '99.17', '0.00', '0.00', '0.00', '0.00', '0.00', '99.17', 1, 'Juntos CPI (91.81) + CPA (7.36) - Se confirma al Aff sin validación ADV', NULL),
(30, '2017-06-01', 11, 48, '53.40', '0.00', '0.00', '0.00', '0.00', '0.00', '53.40', 1, '', NULL),
(31, '2017-06-01', 61, 53, '3.43', '0.00', '0.00', '0.00', '0.00', '0.00', '3.43', 1, '', NULL),
(32, '2017-06-01', 15, 23, '1.30', '0.00', '0.00', '0.00', '0.00', '0.00', '1.30', 1, '', NULL),
(33, '2017-06-01', 21, 56, '0.60', '0.00', '0.00', '0.00', '0.00', '0.00', '0.60', 1, '', NULL),
(34, '2017-06-01', 76, 54, '51.60', '0.00', '0.00', '0.00', '0.00', '0.00', '51.60', 1, 'Se confirma al Aff sin validación ADV', NULL),
(35, '2017-06-01', 75, 23, '0.27', '0.00', '0.00', '0.00', '0.00', '0.00', '0.27', 1, '', NULL),
(36, '2017-06-01', 74, 55, '8607.11', '0.00', '8616.98', '9.87', '0.00', '0.00', '8616.98', 1, 'Voluum (voluum vs zeropark discrepancy)', NULL),
(37, '2017-06-01', 42, 23, '329.35', '0.00', '352.33', '22.98', '0.00', '0.00', '352.33', 1, 'Voluum (discrepancia, error al actualizar amontos)', NULL),
(38, '2017-06-01', 70, 51, '204.15', '0.00', '229.06', '24.91', '0.00', '0.00', '204.15', 1, 'Voluum (amonto de discrapancia tendra refund) PROVISION 178.89 €', NULL),
(39, '2017-06-01', 71, 51, '42.96', '38.92', '44.32', '1.36', '0.00', '0.00', '44.32', 1, 'Voluum - Discrepancia por ExchangeRates', NULL),
(40, '2017-06-01', 72, 55, '25.93', '0.00', '25.94', '0.01', '0.00', '0.00', '25.94', 1, 'Voluum', NULL),
(41, '2017-06-01', 62, 23, '6.33', '0.00', '6.33', '0.00', '0.00', '0.00', '6.33', 1, 'Voluum', NULL),
(42, '2017-06-01', 77, 23, '25.37', '0.00', '25.37', '0.01', '0.00', '0.00', '25.37', 1, 'Voluum', NULL),
(43, '2017-02-01', 45, 51, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '17464.68', 1, 'Total costs of february', NULL),
(44, '2017-03-01', 65, 23, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '43079.54', 1, 'Total costs of March. It\'s not ADXMI', NULL),
(45, '2017-04-01', 65, 23, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '19056.38', 1, 'Total costs of April. Not ADXMI', NULL),
(46, '2017-07-01', 49, 1, '19192.08', '0.00', '0.00', '0.00', '6840.00', '0.00', '12352.55', 2, '', NULL),
(49, '2017-07-01', 3, NULL, '31.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(50, '2017-07-01', 54, NULL, '26.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(52, '2017-07-01', 60, NULL, '34.32', '0.00', '0.00', '0.00', '1091.25', '0.00', '34.49', 2, NULL, NULL),
(53, '2017-07-01', 69, NULL, '1.80', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(59, '2017-07-01', 78, 23, '5513.70', '0.00', '0.00', '0.00', '2182.50', '0.00', '3331.20', 1, 'F1700479 - 3286.20$', NULL),
(60, '2017-07-01', 81, NULL, '323.65', '0.00', '0.00', '0.00', '0.00', '0.00', '323.65', 1, NULL, NULL),
(62, '2017-07-01', 21, NULL, '0.60', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(65, '2017-07-01', 65, 1, '2270.63', '0.00', '0.00', '0.00', '0.00', '0.00', '2270.63', 1, 'YMAD-20170801', NULL),
(69, '2017-07-01', 74, 84, '8223.07', '0.00', '8242.96', '19.89', '0.00', '0.00', '8242.96', 1, 'Voluum', NULL),
(70, '2017-07-01', 42, 86, '442.83', '0.00', '447.56', '4.73', '0.00', '0.00', '447.56', 1, 'Voluum', NULL),
(71, '2017-07-01', 71, 52, '289.34', '239.56', '0.00', '0.00', '0.00', '0.00', '285.00', 1, 'Voluum', NULL),
(72, '2017-07-01', 70, 86, '224.19', '0.00', '226.58', '2.39', '0.00', '0.00', '226.58', 1, 'Voluum', NULL),
(73, '2017-07-01', 85, 83, '167.65', '0.00', '176.17', '8.52', '0.00', '0.00', '176.17', 1, 'Voluum', NULL),
(74, '2017-07-01', 72, 83, '37.24', '0.00', '37.82', '0.58', '0.00', '0.00', '37.24', 1, 'Voluum', NULL),
(75, '2017-07-01', 86, 73, '20.01', '0.00', '20.01', '0.00', '0.00', '0.00', '20.01', 1, 'Voluum', NULL),
(76, '2017-07-01', 64, 85, '18.02', '0.00', '18.08', '0.06', '0.00', '0.00', '18.08', 1, 'Voluum', NULL),
(119, '2017-08-01', 74, 23, '9145.07', '0.00', '9144.94', '0.00', '0.00', '0.00', '9144.94', 1, 'Discrepancia muy baja', NULL),
(120, '2017-08-01', 71, 23, '1111.43', '0.00', '1108.29', '0.00', '0.00', '0.00', '1108.29', 1, 'Discrepancia por cambio de divisa', NULL),
(121, '2017-08-01', 87, 23, '2308.27', '0.00', '2308.79', '0.00', '0.00', '0.00', '2308.79', 1, 'INV-62188,INV-63301', NULL),
(122, '2017-08-01', 85, 23, '978.42', '0.00', '1018.43', '0.00', '0.00', '0.00', '1018.43', 1, 'voluum', NULL),
(123, '2017-08-01', 64, 23, '526.90', '0.00', '528.50', '0.00', '0.00', '0.00', '528.50', 1, 'INV-63292, INV-62178', NULL),
(124, '2017-08-01', 90, 1, '207.79', '0.00', '205.48', '0.00', '0.00', '0.00', '205.48', 1, 'Discrepancia por horarios', '123'),
(125, '2017-08-01', 88, 83, '585.96', '0.00', '586.28', '0.00', '0.00', '0.00', '586.28', 1, 'Discrepancia muy baja', NULL),
(126, '2017-08-01', 89, 23, '181.01', '0.00', '192.64', '0.00', '0.00', '0.00', '192.64', 1, 'Discrepancia con informe - Voluum', NULL),
(127, '2017-08-01', 42, 23, '321.98', '0.00', '322.11', '0.00', '0.00', '0.00', '322.11', 1, 'Discrepancia muy baja', NULL),
(129, '2017-08-01', 93, 80, '2.25', '0.00', '2.25', '0.00', '0.00', '0.00', '2.25', 1, 'voluum', NULL),
(130, '2017-08-01', 86, 71, '108.70', '0.00', '108.74', '0.00', '0.00', '0.00', '108.74', 1, 'voluum', NULL),
(131, '2017-08-01', 91, 82, '28.76', '0.00', '28.76', '0.00', '0.00', '0.00', '28.76', 1, 'voluum', NULL),
(132, '2017-08-01', 94, 82, '9.78', '0.00', '11.27', '0.00', '0.00', '0.00', '11.27', 1, 'Discrepancia por horario', NULL),
(133, '2017-08-01', 62, 83, '137.56', '0.00', '137.56', '0.00', '0.00', '0.00', '137.56', 1, 'voluum', NULL),
(134, '2017-08-01', 95, 82, '0.44', '0.00', '0.66', '0.00', '0.00', '0.00', '0.66', 1, 'voluum', NULL),
(137, '2017-08-01', 92, 23, '59.33', '0.00', '59.32', '0.00', '0.00', '0.00', '59.32', 1, 'voluum', NULL),
(190, '2017-08-01', 65, 1, '13258.65', '0.00', '0.00', '0.00', '483.21', '0.00', '12775.44', 2, 'Aff', NULL),
(191, '2017-08-01', 78, 1, '3214.98', '0.00', '0.00', '0.00', '0.00', '0.00', '3214.98', 1, 'Aff. F1700494', NULL),
(192, '2017-08-01', 75, 1, '3006.00', '0.00', '2556.00', '0.00', '0.00', '0.00', '2556.00', 1, 'Aff', NULL),
(193, '2017-08-01', 49, 1, '2591.65', '0.00', '0.00', '0.00', '0.00', '0.00', '2591.63', 1, 'Aff. FRA-2017-1626 2657.54€', NULL),
(194, '2017-08-01', 137, 1, '450.08', '0.00', '447.08', '0.00', '0.00', '0.00', '447.08', 1, 'Aff', NULL),
(195, '2017-08-01', 60, 1, '130.77', '0.00', '0.00', '0.00', '0.00', '0.00', '130.59', 1, 'Aff - pago en Euros 110', NULL),
(197, '2017-08-01', 79, 1, '51.69', '0.00', '0.00', '0.00', '0.00', '0.00', '51.69', 1, 'Aff', NULL),
(198, '2017-08-01', 54, 1, '13.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, 'Aff', NULL),
(199, '2017-08-01', 61, 1, '11.04', '0.00', '0.00', '0.00', '0.00', '0.00', '11.04', 1, 'Aff', NULL),
(200, '2017-08-01', 82, 1, '6.30', '0.00', '0.00', '0.00', '0.00', '0.00', '6.30', 1, 'Aff', NULL),
(201, '2017-08-01', 134, 1, '3.75', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, 'Aff', NULL),
(203, '2017-08-01', 80, 1, '1.53', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, 'Aff', NULL),
(204, '2017-08-01', 15, 1, '1.30', '0.00', '0.00', '0.00', '0.00', '0.00', '1.30', 1, 'Aff', NULL),
(205, '2017-08-01', 3, 1, '0.63', '0.00', '0.00', '0.00', '0.00', '0.00', '0.63', 1, 'Aff', NULL),
(268, '2017-09-01', 74, 1, '9179.11', '0.00', '0.00', '0.00', '0.00', '0.00', '1232.00', 2, 'voluum', ''),
(269, '2017-09-01', 90, 1, '1320.29', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, 'voluum', ''),
(270, '2017-09-01', 85, NULL, '3625.74', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(271, '2017-09-01', 87, NULL, '1517.61', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(272, '2017-09-01', 86, 1, '538.21', '0.00', '0.00', '0.00', '0.00', '0.00', '232.00', 2, 'voluum', ''),
(273, '2017-09-01', 71, NULL, '389.79', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(274, '2017-09-01', 110, 1, '339.89', '0.00', '0.00', '0.00', '0.00', '0.00', '1231.00', 1, 'voluum', ''),
(275, '2017-09-01', 64, NULL, '264.25', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(276, '2017-09-01', 88, NULL, '562.78', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(277, '2017-09-01', 102, NULL, '154.93', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(278, '2017-09-01', 117, NULL, '192.92', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(279, '2017-09-01', 93, NULL, '7.54', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(280, '2017-09-01', 119, 1, '30.00', '0.00', '0.00', '0.00', '0.00', '0.00', '323.00', 1, 'voluum', ''),
(281, '2017-09-01', 176, NULL, '97.90', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(282, '2017-09-01', 94, NULL, '14.50', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(283, '2017-09-01', 111, 1, '618.46', '0.00', '0.00', '0.00', '0.00', '0.00', '123.00', 2, 'voluum', ''),
(284, '2017-09-01', 91, NULL, '31.06', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(285, '2017-09-01', 62, 1, '49.36', '0.00', '0.00', '0.00', '0.00', '0.00', '123.00', 2, 'voluum', ''),
(286, '2017-09-01', 95, 1, '113.38', '0.00', '0.00', '0.00', '0.00', '0.00', '640.00', 2, 'voluum', ''),
(287, '2017-09-01', 42, NULL, '2.22', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(288, '2017-09-01', 73, NULL, '2.22', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(289, '2017-09-01', 177, 1, '15.98', '0.00', '0.00', '0.00', '0.00', '0.00', '1232.00', 2, 'voluum', ''),
(290, '2017-09-01', 112, 1, '73.03', '0.00', '0.00', '0.00', '0.00', '0.00', '123.00', 2, 'voluum', ''),
(291, '2017-09-01', 96, NULL, '84.47', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(292, '2017-09-01', 123, NULL, '108.80', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(293, '2017-09-01', 59, NULL, '12.60', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(294, '2017-09-01', 49, NULL, '1293.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(295, '2017-09-01', 79, 1, '10.00', '0.00', '0.00', '0.00', '0.00', '0.00', '300.00', 2, '', ''),
(296, '2017-09-01', 57, 1, '5.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '', ''),
(297, '2017-09-01', 60, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(298, '2017-09-01', 84, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(299, '2017-09-01', 69, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2323.00', 2, '', ''),
(300, '2017-09-01', 82, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(301, '2017-09-01', 61, 1, '38.00', '0.00', '0.00', '0.00', '0.00', '0.00', '333.00', 2, '', ''),
(302, '2017-09-01', 15, 4, '1.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '', ''),
(303, '2017-09-01', 25, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '', ''),
(304, '2017-09-01', 166, NULL, '291.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(305, '2017-09-01', 80, NULL, '25.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(306, '2017-09-01', 78, 5, '166.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '', ''),
(307, '2017-09-01', 167, 1, '2769.00', '0.00', '0.00', '0.00', '0.00', '0.00', '3232.00', 2, '', ''),
(308, '2017-09-01', 81, NULL, '7.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(309, '2017-09-01', 168, 1, '11.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2300.00', 2, '', ''),
(310, '2017-09-01', 23, NULL, '1203.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(311, '2017-09-01', 82, NULL, '29.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(312, '2017-09-01', 65, 1, '10643.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '', ''),
(313, '2017-09-01', 169, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '3323.00', 2, '', ''),
(314, '2017-09-01', 170, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '23.00', 2, '', ''),
(315, '2017-09-01', 171, 5, '3.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 2, '', ''),
(316, '2017-09-01', 75, NULL, '750.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(317, '2017-09-01', 172, NULL, '3.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(318, '2017-09-01', 173, 4, '3566.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '', ''),
(319, '2017-09-01', 137, NULL, '874.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(320, '2017-09-01', 172, NULL, '874.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(321, '2017-09-01', 174, NULL, '15.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(322, '2017-09-01', 175, 8, '6.00', '0.00', '0.00', '0.00', '0.00', '0.00', '123.00', 2, '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doc_number`
--

CREATE TABLE `doc_number` (
  `id` int(2) NOT NULL,
  `increment_value` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `doc_number`
--

INSERT INTO `doc_number` (`id`, `increment_value`) VALUES
(1, 98);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incomes`
--

CREATE TABLE `incomes` (
  `id` int(4) UNSIGNED ZEROFILL NOT NULL,
  `advertiser` varchar(50) DEFAULT '0',
  `social` int(5) DEFAULT NULL,
  `am` varchar(50) DEFAULT '3',
  `clicks` int(12) DEFAULT '0',
  `conversion` int(12) DEFAULT '0',
  `revenue` decimal(10,2) DEFAULT '0.00',
  `platform_revenue` decimal(10,2) DEFAULT '0.00',
  `difference` decimal(10,2) DEFAULT '0.00',
  `scrubs` decimal(10,2) DEFAULT '0.00',
  `hold` decimal(10,2) DEFAULT '0.00',
  `validated` decimal(10,2) DEFAULT '0.00',
  `accumulated` decimal(10,2) DEFAULT '0.00',
  `status` varchar(50) DEFAULT '0',
  `notas_am` varchar(200) DEFAULT 'Notes...',
  `notas_finance` varchar(200) DEFAULT 'Notes...',
  `date` date DEFAULT NULL,
  `increment` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `incomes`
--

INSERT INTO `incomes` (`id`, `advertiser`, `social`, `am`, `clicks`, `conversion`, `revenue`, `platform_revenue`, `difference`, `scrubs`, `hold`, `validated`, `accumulated`, `status`, `notas_am`, `notas_finance`, `date`, `increment`) VALUES
(0001, '162', NULL, '354', 0, 11782, '8594.00', '0.00', '0.00', '4441.40', '0.00', '8114.90', '0.00', '7', 'validado por mail Camilla - scrubs en excel', 'FACTURA MOB 870', '2017-02-01', 0),
(0002, '2', NULL, '50', 0, 107, '3834.00', '5996.00', '2162.00', '0.00', '0.00', '5996.00', '0.00', '7', 'Notes...', 'Factura MOB 850: 7339,10 USD que incluye Febrero y varios meses anteriores', '2017-02-01', 0),
(0003, '3', NULL, '354', 0, 1494, '3435.00', '3517.85', '82.85', '2135.70', '0.00', '1382.15', '0.00', '7', 'validado total - scrubs', 'FACTURA MOB 872', '2017-02-01', 0),
(0004, '163', NULL, '0', 0, 2724, '3130.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'queda 0, el total es Mobquid ', 'Notes...', '2017-02-01', 0),
(0005, '4', NULL, '179', 0, 254, '1892.00', '0.00', '0.00', '0.00', '0.00', '2671.00', '0.00', '7', 'Notes...', 'Factura MOB 856', '2017-02-01', 0),
(0006, '57', NULL, '354', 0, 464, '1349.00', '0.00', '0.00', '0.00', '0.00', '1349.90', '0.00', '7', 'Confirman el 15th (no puedo sacar reporte en la plataforma)', 'FACTURA MOB 873', '2017-02-01', 0),
(0007, '28', NULL, '354', 0, 734, '1345.00', '2283.91', '938.91', '0.00', '0.00', '2283.91', '0.00', '7', 'No scrubs yet(15th)', 'FACTURA MOB 871', '2017-02-01', 0),
(0008, '9', NULL, '0', 0, 726, '820.00', '683.20', '-136.80', '0.00', '0.00', '799.11', '0.00', '7', '', 'Factura MOB 892', '2017-02-01', 0),
(0009, '165', NULL, '354', 0, 232, '796.00', '887.85', '91.85', '0.00', '0.00', '716.10', '0.00', '7', '', 'Factura MOB881', '2017-02-01', 0),
(0010, '166', NULL, '179', 0, 111, '766.00', '0.00', '0.00', '0.00', '0.00', '773.00', '0.00', '7', 'Notes...', 'El importe de 773  se ha facturado en Euros, no USD', '2017-02-01', 0),
(0011, '172', NULL, '354', 0, 473, '637.00', '639.20', '2.20', '0.00', '0.00', '639.20', '0.00', '7', 'Facturar Diciembre ($47.85) y Enero ($4.00', 'Fact.Diciembre MOB878 / Factura Enero MOB879 / Factura Febrero MOB880', '2017-02-01', 0),
(0012, '83', NULL, '179', 0, 65, '604.00', '0.00', '0.00', '0.00', '0.00', '608.18', '0.00', '7', 'Notes...', 'Factura MOB 858', '2017-02-01', 0),
(0013, '43', NULL, '179', 0, 189, '521.00', '0.00', '0.00', '0.00', '0.00', '644.40', '0.00', '7', '67.90 USD en hasoffers', 'Factura MOB 845', '2017-02-01', 0),
(0014, '164', NULL, '179', 0, 908, '417.00', '0.00', '0.00', '0.00', '0.00', '443.70', '0.00', '7', 'Notes...', 'Factura MOB 860', '2017-02-01', 0),
(0015, '50', NULL, '354', 0, 114, '245.00', '386.66', '141.66', '0.00', '0.00', '386.66', '0.00', '7', 'AM OOO hasta 17th', 'FACTURA MOB 874', '2017-02-01', 0),
(0016, '10', NULL, '0', 0, 10, '240.00', '0.00', '0.00', '0.00', '0.00', '421.89', '0.00', '7', 'Se genera la factura en su plataforma. Este importe corresponde a CPA + CPI', 'Factura MOB 893', '2017-02-01', 0),
(0017, '61', NULL, '0', 0, 217, '235.00', '235.77', '0.77', '232.00', '0.00', '3.77', '0.00', '2', 'Operaciones no acepta el scrub por estar fuera de fecha. Mandar factura: 235.77', 'Notes...', '2017-02-01', 0),
(0018, '173', NULL, '0', 0, 48, '211.00', '211.50', '0.50', '193.50', '0.00', '18.00', '0.00', '2', 'Se acumula', 'Notes...', '2017-02-01', 0),
(0019, '19', NULL, '354', 0, 210, '162.00', '457.76', '295.76', '21.60', '0.00', '459.16', '0.00', '7', '', 'Factura MOB 882', '2017-02-01', 0),
(0020, '151', NULL, '0', 0, 150, '147.00', '0.00', '0.00', '0.00', '0.00', '147.00', '0.00', '7', 'Notes...', 'Factura MOB 894', '2017-02-01', 0),
(0021, '11', NULL, '179', 0, 133, '146.00', '0.00', '0.00', '0.00', '0.00', '174.90', '0.00', '7', 'Notes...', 'FACTURA MOB 842.', '2017-02-01', 0),
(0022, '174', NULL, '354', 0, 122, '111.00', '113.12', '2.12', '103.55', '0.00', '113.12', '0.00', '7', 'Scrub:103.55. El AM no se acuerda, me lo ha confirmado todo.', 'FACTURA MOB 877', '2017-02-01', 0),
(0023, '41', NULL, '354', 0, 45, '109.00', '118.79', '9.79', '0.00', '0.00', '118.79', '0.00', '7', 'validado por mail a amparo', 'FACTURA MOB 875', '2017-02-01', 0),
(0024, '136', NULL, '0', 0, 56, '86.00', '94.35', '8.35', '94.35', '0.00', '0.00', '0.00', '2', 'VALIDADO: 0.00$. TODO SCRUB. Pruebas de fraude mandadas a afiliación. Ellos no mandan screenshots de placements', 'Notes...', '2017-02-01', 0),
(0025, '42', NULL, '179', 0, 210, '84.00', '0.00', '0.00', '0.00', '0.00', '156.40', '0.00', '7', 'esta factura es de ENERO!!! hay que hace la factura de Febrero por  153.2', 'Factura MOB 839', '2017-02-01', 0),
(0026, '138', NULL, '0', 0, 57, '68.00', '68.40', '0.40', '0.00', '0.00', '68.40', '0.00', '2', 'Minimo: 100$', 'Notes...', '2017-02-01', 0),
(0027, '147', NULL, '0', 0, 137, '54.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0028, '150', NULL, '0', 0, 73, '48.00', '46.19', '-1.81', '0.00', '0.00', '46.19', '46.19', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0029, '13', NULL, '0', 0, 5, '46.00', '0.00', '0.00', '0.00', '0.00', '46.00', '0.00', '2', 'hay diferencia con Rafa, igual no pagan por error nuestro', 'Notes...', '2017-02-01', 0),
(0030, '143', NULL, '0', 0, 34, '46.00', '46.40', '0.40', '0.00', '0.00', '46.40', '0.00', '2', 'Minimo: 250$', 'Notes...', '2017-02-01', 0),
(0031, '32', NULL, '0', 0, 52, '45.00', '46.30', '1.30', '0.00', '0.00', '46.30', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0032, '153', NULL, '0', 0, 21, '34.00', '36.20', '2.20', '0.00', '0.00', '36.20', '0.00', '7', '', 'Factura MOB 937 que incluye Febrero y Marzo', '2017-02-01', 0),
(0033, '54', NULL, '179', 0, 17, '29.00', '36.02', '7.02', '0.00', '0.00', '36.02', '0.00', '7', 'Notes...', 'Factura MOB 862', '2017-02-01', 0),
(0034, '40', NULL, '0', 0, 33, '29.00', '65.99', '36.99', '0.00', '0.00', '0.00', '0.00', '2', 'Se genera la factura en su plataforma. Mirar Avazu CPA.', 'Notes...', '2017-02-01', 0),
(0035, '17', NULL, '0', 0, 36, '29.00', '0.00', '0.00', '0.00', '9.00', '37.75', '0.00', '2', '18,2 HO hay que añadir', 'Notes...', '2017-02-01', 0),
(0036, '1', NULL, '179', 0, 92, '27.00', '0.00', '0.00', '0.00', '0.00', '27.60', '0.00', '7', 'Notes...', 'NEOMOBILE SPA  Via della Sierra Nevada, 60 00144 - Roma, Italia  Vat Number      IT02927500542', '2017-02-01', 0),
(0037, '176', NULL, '0', 0, 12, '19.00', '23.64', '4.64', '0.00', '0.00', '23.64', '0.00', '2', '', 'Notes...', '2017-02-01', 0),
(0038, '75', NULL, '0', 0, 15, '18.00', '18.88', '0.88', '0.00', '0.00', '18.88', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0039, '60', NULL, '0', 0, 20, '27.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'añadido HO - se factura con CPI', 'Notes...', '2017-02-01', 0),
(0040, '146', NULL, '179', 0, 3, '17.00', '15.74', '-1.26', '0.00', '0.00', '15.74', '0.00', '7', 'Notes...', 'Factura MOB 864', '2017-02-01', 0),
(0041, '14', NULL, '37', 0, 8, '15.00', '0.00', '0.00', '0.00', '0.00', '27.40', '0.00', '7', '+ 37,7  HO', '3D MOBILE 3DM SAS - Factura MOB 857 / Abono AMOB74 / Factura MOB 883', '2017-02-01', 0),
(0042, '167', NULL, '0', 0, 13, '13.00', '13.93', '0.93', '13.93', '0.00', '0.00', '0.00', '2', 'Validado 0.00', 'Notes...', '2017-02-01', 0),
(0043, '159', NULL, '73', 0, 25, '13.00', '4388.51', '4375.51', '0.00', '0.00', '4398.67', '0.00', '7', 'Confirmados', 'FACTURA MOB 887 ESTA EN EUROS!!!', '2017-02-01', 0),
(0044, '148', NULL, '0', 0, 17, '11.00', '16.42', '5.42', '0.00', '0.00', '16.42', '0.00', '2', 'Confirman el 15th', 'Notes...', '2017-02-01', 0),
(0045, '20', NULL, '179', 0, 13, '9.00', '0.00', '0.00', '0.00', '0.00', '40.80', '0.00', '7', 'Notes...', 'Factura MOB 861', '2017-02-01', 0),
(0046, '36', NULL, '0', 0, 14, '7.00', '7.18', '0.18', '0.00', '0.00', '7.18', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0047, '26', NULL, '0', 0, 2, '5.00', '0.00', '0.00', '0.00', '0.00', '5.00', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0048, '161', NULL, '0', 0, 3, '5.00', '0.00', '0.00', '0.00', '0.00', '5.00', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0049, '139', NULL, '0', 0, 13, '4.00', '0.00', '0.00', '0.00', '0.00', '10.80', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0050, '135', NULL, '0', 0, 2, '2.00', '0.00', '0.00', '0.00', '0.00', '2.00', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0051, '141', NULL, '0', 0, 1, '21.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'revenue HO - no sabemos de que viene la diferencia', 'Notes...', '2017-02-01', 0),
(0052, '168', NULL, '0', 0, 1, '1.00', '0.00', '0.00', '0.00', '0.00', '1.00', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0053, '33', NULL, '0', 0, 1, '1.00', '0.00', '0.00', '0.00', '0.00', '1.00', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0054, '24', NULL, '0', 0, 1, '1.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0055, '152', NULL, '0', 0, 2, '1.00', '0.00', '0.00', '0.00', '0.00', '1.00', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0059, '131', NULL, '0', 0, 1, '21.28', '0.00', '0.00', '0.00', '0.00', '24.58', '0.00', '2', 'HO+Rafa', 'Notes...', '2017-02-01', 0),
(0064, '35', NULL, '0', 0, 1, '21.00', '0.00', '0.00', '0.00', '0.00', '21.00', '0.00', '2', 'Rev HO', 'Notes...', '2017-02-01', 0),
(0080, '5', NULL, '79', 0, 9, '0.00', '0.00', '0.00', '0.00', '0.00', '600.20', '0.00', '7', 'Desglose mail: 21$ Colombia - 9$ Honduras - México CykGames 569$ - Argentina CLaro 1.20$', 'Factura MOB 859', '2017-02-01', 0),
(0083, '171', NULL, '179', 0, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '304.00', '0.00', '7', 'Aun no está enviado!!!!', 'Factura MOB 852', '2017-02-01', 0),
(0086, '62', NULL, '3', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '117.60', '0.00', '7', 'CONFIRMADO', 'FACTURA MOB 876 - Nombre: Voiceweb Americas S.A. de C.V. Dirección: Colonia La Joya, Calle principal Bloque No. 1, Sector No. 1, Casa 2817, Tegucigalpa, Honduras. Ciudad: Tegucigalpa, Honduras. Código', '2017-02-01', 0),
(0087, '44', NULL, '342', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '10062.30', '0.00', '7', 'ESTA EN EUROS!!!!!!', 'FACTURA MOB 843 (28/02/17)', '2017-02-01', 0),
(0095, '21', NULL, '67', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '7.20', '0.00', '2', '', '', '2017-02-01', 0),
(0096, '80', NULL, '168', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '190.02', '0.00', '7', 'euros', '', '2017-02-01', 0),
(0098, '79', NULL, '155', 0, 0, '37.23', '0.00', '0.00', '0.00', '0.00', '37.23', '0.00', '2', '', '', '2017-02-01', 0),
(0100, '82', NULL, '304', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '36.00', '0.00', '2', 'Rev Rafa', '', '2017-02-01', 0),
(0368, '130', NULL, '0', 0, 3, '5.00', '0.00', '0.00', '0.00', '0.00', '5.01', '0.00', '2', 'Notes...', 'Notes...', '2017-03-01', 0),
(0369, '50', NULL, '0', 0, 260, '907.00', '908.01', '1.01', '0.00', '0.00', '908.01', '0.00', '7', 'will be delayed up until 20 April 2017 due to the Easter Break holidays.  kindly submit your invoice by 28 April 201', 'Factura MOB915', '2017-03-01', 0),
(0370, '353', NULL, '0', 0, 866, '1296.00', '0.00', '0.00', '1241.40', '0.00', '1296.00', '0.00', '10', 'No cuadra por conversiones declinadas. Scrub esperando + pruebas', 'Factura MOB 928', '2017-03-01', 0),
(0371, '28', NULL, '0', 0, 643, '1264.00', '2120.92', '856.92', '0.00', '0.00', '2120.92', '0.00', '7', 'Mnadar factura con el importe de platf.Adv', 'Factura MOB 912', '2017-03-01', 0),
(0372, '172', NULL, '0', 0, 3098, '6711.00', '0.00', '0.00', '0.00', '0.00', '6822.55', '0.00', '7', 'Notes...', 'Factura MOB910', '2017-03-01', 0),
(0375, '143', NULL, '0', 0, 295, '470.00', '0.00', '0.00', '0.00', '0.00', '470.00', '0.00', '7', 'until 20th', 'Factura MOB916', '2017-03-01', 0),
(0376, '138', NULL, '0', 0, 41, '49.00', '30.72', '-18.28', '0.00', '0.00', '30.72', '0.00', '2', 'Notes...', 'Notes...', '2017-03-01', 0),
(0377, '35', NULL, '0', 0, 9, '27.00', '27.00', '0.00', '0.00', '0.00', '27.00', '0.00', '2', 'Notes...', 'Notes...', '2017-03-01', 0),
(0379, '9', NULL, '0', 0, 1604, '1781.00', '1335.89', '-445.11', '0.00', '0.00', '1335.89', '0.00', '7', '', 'Factura MOB 913', '2017-03-01', 0),
(0381, '68', NULL, '0', 0, 154, '308.00', '0.00', '0.00', '0.00', '0.00', '357.73', '0.00', '7', 'until 20th', 'Factura MOB918', '2017-03-01', 0),
(0382, '170', NULL, '0', 0, 52, '52.00', '0.00', '0.00', '0.00', '0.00', '46.00', '0.00', '2', 'January 368.90 + March 46.00=414.90.  Please use invoice date 31-03-2017', 'Notes...', '2017-03-01', 0),
(0383, '39', NULL, '0', 0, 1, '4.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Acumular para mes siguiente', 'Notes...', '2017-03-01', 0),
(0384, '153', NULL, '0', 0, 704, '1047.00', '1057.75', '10.75', '351.40', '0.00', '706.35', '0.00', '7', 'Modificada cantidad a validar el 03.05. On hold 36.00$ sin report.Además, hay que facturar además 36,20$ de febrero', 'Factura MOB 937 : 742.55 USD que incluye Febrero y Marzo', '2017-03-01', 0),
(0385, '25', NULL, '0', 0, 2, '7.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Validado en Layroute CPA', 'Notes...', '2017-03-01', 0),
(0386, '360', NULL, '0', 0, 2, '7.00', '0.00', '0.00', '0.00', '0.00', '7.00', '0.00', '4', 'Notes...', 'Notes...', '2017-03-01', 0),
(0387, '356', NULL, '0', 0, 1, '1.00', '0.00', '0.00', '0.00', '0.00', '1.00', '0.00', '4', 'Notes...', 'Notes...', '2017-03-01', 0),
(0388, '3', NULL, '0', 0, 575, '1382.00', '0.00', '0.00', '1360.38', '0.00', '19.60', '0.00', '2', 'Notes...', 'Notes...', '2017-03-01', 0),
(0390, '157', NULL, '0', 0, 26, '17.00', '0.00', '0.00', '0.00', '0.00', '18.40', '0.00', '7', 'Notes...', 'Factura MOB 967 junto con abril', '2017-03-01', 0),
(0391, '163', NULL, '0', 0, 1624, '1867.00', '0.00', '0.00', '0.00', '0.00', '2039.60', '0.00', '7', 'Notes...', 'Factura MOB 904', '2017-03-01', 0),
(0392, '139', NULL, '0', 0, 5, '3.00', '0.00', '0.00', '0.00', '0.00', '3.00', '0.00', '2', 'Notes...', 'Notes...', '2017-03-01', 0),
(0394, '43', NULL, '0', 0, 1099, '1955.00', '0.00', '0.00', '0.00', '0.00', '2053.65', '0.00', '7', 'Glamour 548,4 - Mainstream 1505,25', 'Factura MOB 897', '2017-03-01', 0),
(0395, '359', NULL, '0', 0, 13, '32.00', '0.00', '0.00', '0.00', '0.00', '33.50', '0.00', '2', 'no tienen plataforma', 'Notes...', '2017-03-01', 0),
(0396, '60', NULL, '0', 0, 18, '18.00', '0.00', '0.00', '0.00', '0.00', '13.20', '0.00', '7', 'Notes...', 'Factura MOB1050', '2017-03-01', 0),
(0397, '160', NULL, '0', 0, 4, '3.00', '0.00', '0.00', '0.00', '0.00', '3.00', '0.00', '4', 'Notes...', 'Notes...', '2017-03-01', 0),
(0398, '134', NULL, '0', 0, 20, '96.00', '90.19', '-5.81', '0.00', '0.00', '90.19', '0.00', '7', 'Notes...', 'Factura MOB 1029', '2017-03-01', 0),
(0399, '140', NULL, '0', 0, 1, '2.00', '0.00', '0.00', '0.00', '0.00', '2.00', '0.00', '4', 'Notes...', 'Notes...', '2017-03-01', 0),
(0401, '319', 191, '138', 0, 1, '23.00', '0.00', '0.00', '0.00', '0.00', '23.00', '0.00', '10', 'Notes...', 'Factura MOB1069', '2017-03-01', 0),
(0402, '308', NULL, '0', 0, 27, '55.00', '0.00', '0.00', '0.00', '0.00', '66.00', '0.00', '7', 'Notes...', 'Factura MOB 944 que incluye tambien febrero y abril', '2017-03-01', 0),
(0404, '31', NULL, '0', 0, 7, '24.00', '0.00', '0.00', '0.00', '0.00', '51.40', '0.00', '2', 'Notes...', 'Notes...', '2017-03-01', 0),
(0405, '164', NULL, '0', 0, 217, '133.00', '0.00', '0.00', '0.00', '0.00', '145.53', '0.00', '7', 'enviar fv a: publishers@headwaydigital.com', 'Factura MOB911', '2017-03-01', 0),
(0407, '83', NULL, '0', 0, 15, '114.00', '149.44', '35.44', '0.00', '0.00', '149.44', '0.00', '7', 'Hay 2 cuentas de buongiorno. FACTURAR DESGLOSADO POR OFERTA', 'Factura MOB 903', '2017-03-01', 0),
(0408, '4', NULL, '0', 0, 1219, '10863.00', '0.00', '0.00', '0.00', '0.00', '11923.70', '0.00', '7', 'Notes...', 'Factura MOB 901', '2017-03-01', 0),
(0409, '11', NULL, '0', 0, 1175, '1291.00', '1315.60', '24.60', '0.00', '0.00', '1315.60', '0.00', '7', 'Notes...', 'Factura MOB 895', '2017-03-01', 0),
(0410, '155', NULL, '0', 0, 6, '10.00', '0.00', '0.00', '0.00', '0.00', '18.46', '0.00', '2', 'Notes...', 'Notes...', '2017-03-01', 0),
(0411, '1', NULL, '0', 0, 3, '1.00', '0.00', '0.00', '0.00', '0.00', '1.00', '0.00', '4', 'Notes...', 'Notes...', '2017-03-01', 0),
(0412, '306', NULL, '0', 0, 191, '114.00', '0.00', '0.00', '0.00', '0.00', '119.13', '0.00', '7', 'Notes...', 'Factura MOB919', '2017-03-01', 0),
(0413, '342', NULL, '0', 0, 10, '9.00', '0.00', '0.00', '0.00', '0.00', '9.00', '0.00', '2', 'Notes...', 'Notes...', '2017-03-01', 0),
(0415, '305', NULL, '0', 0, 1, '22.00', '0.00', '0.00', '0.00', '0.00', '22.00', '0.00', '7', 'la van a pagar con el monto de Mayo (no se envia factura)  crear factura a parte por nosotros', 'Facturado MOB 984', '2017-03-01', 0),
(0416, '24', NULL, '0', 0, 1477, '1799.00', '2195.00', '396.00', '563.40', '0.00', '1769.93', '0.00', '7', 'Antes estaba facturado NoOk$', 'Factura MOB 927', '2017-03-01', 0),
(0417, '57', NULL, '0', 0, 266, '878.00', '892.25', '14.25', '0.00', '0.00', '892.25', '0.00', '7', 'Send invoice to: billing@adxperience.com(Amparo, 12/04); mandado el email', 'Factura MOB914', '2017-03-01', 0),
(0418, '32', NULL, '0', 0, 6, '4.00', '0.00', '0.00', '0.00', '0.00', '4.90', '51.20', '2', 'Notes...', 'Notes...', '2017-03-01', 0),
(0419, '175', NULL, '0', 0, 8, '36.00', '0.00', '0.00', '0.00', '0.00', '40.50', '0.00', '7', 'Notes...', 'Factura MOB 979', '2017-03-01', 0),
(0420, '17', NULL, '0', 0, 3366, '4172.00', '4178.95', '6.95', '0.00', '0.00', '4178.95', '37.75', '10', 'van a pagar solo 2,197.69 por error nuestra plataforma', 'Factura MOB 926', '2017-03-01', 0),
(0421, '2', NULL, '0', 0, 7364, '20978.00', '20561.34', '-416.66', '7961.86', '10800.00', '12613.96', '0.00', '10', 'en validado está lo confirmado (1813.96$) más lo de on hold ya que llevamos demasiado tiempo esperando la confirmación', 'Facturan desde su plataforma - Factura MOB 925', '2017-03-01', 0),
(0422, '223', NULL, '0', 0, 28, '59.00', '0.00', '0.00', '0.00', '0.00', '59.03', '0.00', '2', 'please invoice to this company and add as description: Mobile Traffic   Company Name: Chilli ads, SA de CV Contact: Edgar Salazar E-mail: publishers@chilliads.com Phone: 0181-82-44-01-65 State: Nuevo ', 'Notes...', '2017-03-01', 0),
(0423, '14', NULL, '0', 0, 206, '267.80', '0.00', '0.00', '0.00', '0.00', '236.60', '0.00', '7', 'Notes...', 'Notes...', '2017-03-01', 0),
(0424, '10', NULL, '0', 0, 20, '480.00', '423.02', '-56.98', '0.00', '0.00', '0.00', '0.00', '2', 'Esperar que se genere la factura en su plataforma', 'no mandar factura - plataforma', '2017-03-01', 0),
(0425, '135', NULL, '0', 0, 224, '46.00', '0.00', '0.00', '0.00', '0.00', '43.00', '0.00', '2', 'invoicing@affle.com', '', '2017-03-01', 0),
(0427, '162', NULL, '0', 0, 18046, '11729.00', '0.00', '0.00', '11729.15', '0.00', '0.00', '0.00', '2', 'Numeros aprobados en Mobquid CPA. Todo esto es scrub.', 'Notes...', '2017-03-01', 0),
(0429, '147', NULL, '0', 0, 113, '50.31', '0.00', '0.00', '0.00', '0.00', '50.31', '50.31', '4', 'Notes...', 'Notes...', '2017-03-01', 0),
(0430, '142', NULL, '0', 0, 202, '171.00', '172.55', '1.55', '0.00', '0.00', '172.55', '0.00', '7', 'Confirman el 18th', 'Factura MOB920', '2017-03-01', 0),
(0431, '173', NULL, '0', 0, 494, '705.00', '0.00', '0.00', '336.24', '160.20', '199.71', '18.00', '7', 'Mes que viene revisar ON HOLD', 'Factura MOB 909', '2017-03-01', 0),
(0432, '171', NULL, '0', 0, 2868, '5908.00', '0.00', '0.00', '0.00', '0.00', '5765.50', '0.00', '7', 'Diferencia de 150$ - se supone que es por zona horaria, debería pasarse al abril', 'Factura MOB 899', '2017-03-01', 0),
(0434, '145', NULL, '0', 0, 32, '43.00', '0.00', '0.00', '0.00', '0.00', '43.50', '0.00', '2', 'Mandado el email, informaron que confirman el 19th', 'Notes...', '2017-03-01', 0),
(0438, '178', NULL, '0', 0, 1, '1.00', '0.00', '0.00', '0.00', '0.00', '1.00', '0.00', '4', 'Notes...', 'Notes...', '2017-03-01', 0),
(0439, '364', NULL, '0', 0, 4, '4.00', '0.00', '0.00', '0.00', '0.00', '4.00', '0.00', '4', 'Notes...', 'Notes...', '2017-03-01', 0),
(0448, '151', NULL, '0', 0, 1093, '606.00', '0.00', '0.00', '0.00', '0.00', '385.00', '0.00', '7', 'En scrubs hay más por overcap. Abonar factura anterior (385eur) y facturar la nueva cantidad', 'Factura MOB917', '2017-03-01', 0),
(0450, '42', NULL, '0', 0, 276, '110.00', '0.00', '0.00', '0.00', '0.00', '115.65', '0.00', '7', 'Notes...', 'Factura MOB 900', '2017-03-01', 0),
(0452, '83', NULL, '0', 0, 6, '51.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Hay 2 cuentas de buongiorno. Validado en la otra', 'Notes...', '2017-03-01', 0),
(0456, '146', NULL, '0', 0, 2, '3.00', '0.00', '0.00', '0.00', '0.00', '3.00', '0.00', '4', 'Notes...', 'Notes...', '2017-03-01', 0),
(0459, '371', NULL, '0', 0, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '9774.96', '0.00', '7', 'Notes...', 'Factura MOB 902', '2017-03-01', 0),
(0468, '320', 377, '67', 0, 0, '292.40', '0.00', '0.00', '0.00', '0.00', '295.11', '0.00', '7', '', 'Factura MOB 896', '2017-03-01', 0),
(0469, '20', 377, '372', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '4.80', '0.00', '2', '', '', '2017-03-01', 0),
(0472, '178', NULL, '0', 0, 1, '1.00', '0.00', '0.00', '0.00', '0.00', '1.00', '0.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0473, '170', NULL, '0', 0, 2, '2.00', '0.00', '0.00', '0.00', '0.00', '2.00', '48.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0474, '60', NULL, '0', 0, 5, '2.00', '0.00', '0.00', '0.00', '0.00', '2.00', '0.00', '7', 'Notes...', 'Notes...', '2017-04-01', 0),
(0475, '138', NULL, '0', 0, 1, '2.00', '0.00', '0.00', '0.00', '0.00', '2.00', '101.12', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0476, '143', NULL, '0', 0, 4, '3.00', '0.00', '0.00', '0.00', '0.00', '3.00', '49.40', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0477, '358', NULL, '0', 0, 1, '5.00', '0.00', '0.00', '0.00', '0.00', '5.00', '0.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0478, '373', NULL, '0', 0, 4, '5.00', '0.00', '0.00', '0.00', '0.00', '5.00', '0.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0479, '363', NULL, '0', 0, 11, '6.00', '6.70', '0.70', '0.00', '0.00', '6.70', '0.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0480, '48', NULL, '0', 0, 19, '9.00', '0.00', '0.00', '0.00', '0.00', '9.00', '0.00', '4', 'en su plataforma aparece 0.00', 'Notes...', '2017-04-01', 0),
(0481, '77', NULL, '0', 0, 67, '10.36', '10.36', '0.00', '0.00', '0.00', '10.36', '0.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0482, '306', NULL, '0', 0, 25, '14.00', '0.00', '0.00', '0.00', '0.00', '22.77', '0.00', '7', 'Notes...', 'Factura MOB 974', '2017-04-01', 0),
(0483, '159', NULL, '0', 0, 10, '214.43', '0.00', '0.00', '0.00', '0.00', '214.43', '0.00', '7', 'Notes...', 'Factura MOB 952', '2017-04-01', 0),
(0485, '157', NULL, '0', 0, 20, '16.00', '16.80', '0.80', '0.00', '0.00', '16.80', '0.00', '7', 'Notes...', 'Factura MOB 967 junto con Marzo', '2017-04-01', 0),
(0486, '342', NULL, '0', 0, 18, '16.00', '0.00', '0.00', '0.00', '0.00', '16.20', '25.20', '2', 'Notes...', 'Notes...', '2017-04-01', 0),
(0488, '134', NULL, '0', 0, 5, '24.00', '0.00', '0.00', '0.00', '0.00', '22.55', '0.00', '7', 'Notes...', 'Factura MOB 1030', '2017-04-01', 0),
(0489, '364', NULL, '0', 0, 24, '27.00', '27.60', '0.60', '0.00', '0.00', '27.60', '0.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0490, '160', NULL, '0', 0, 47, '30.00', '0.00', '0.00', '0.00', '0.00', '30.00', '0.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0491, '135', NULL, '0', 0, 187, '37.00', '0.00', '0.00', '0.00', '0.00', '35.00', '0.00', '7', 'mandar antes del 10. desglosar factura como mail mandado a cristina', 'Factura MOB944', '2017-04-01', 0),
(0492, '308', NULL, '0', 0, 11, '38.00', '0.00', '0.00', '0.00', '0.00', '34.23', '0.00', '7', 'Notes...', 'Factura MOB944 que incluye tambien febrero y marzo', '2017-04-01', 0),
(0493, '177', 415, '73', 0, 2, '39.00', '0.00', '0.00', '0.00', '0.00', '39.00', '13.50', '2', 'Notes...', 'Notes...', '2017-04-01', 0),
(0494, '19', NULL, '0', 0, 32, '47.00', '49.80', '2.80', '0.00', '0.00', '49.80', '49.80', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0495, '28', NULL, '0', 0, 45, '58.00', '103.11', '45.11', '0.00', '0.00', '103.11', '0.00', '10', 'Notes...', 'Factura MOB 956', '2017-04-01', 0),
(0496, '223', NULL, '0', 0, 20, '60.00', '0.00', '0.00', '0.00', '0.00', '60.00', '0.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0497, '9', NULL, '0', 0, 40, '67.00', '71.73', '4.73', '0.00', '0.00', '71.73', '71.73', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0499, '23', NULL, '0', 0, 99, '84.00', '0.00', '0.00', '0.00', '0.00', '99.00', '99.00', '2', 'pedido por mail', 'Notes...', '2017-04-01', 0),
(0500, '173', NULL, '0', 0, 36, '87.00', '87.39', '0.39', '0.00', '0.00', '87.39', '18.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0501, '164', NULL, '0', 0, 160, '98.00', '100.32', '2.32', '0.00', '0.00', '100.32', '0.00', '7', 'Notes...', 'Factura MOB 947', '2017-04-01', 0),
(0502, '319', 191, '138', 0, 5, '93.00', '0.00', '0.00', '0.00', '0.00', '70.00', '23.00', '7', 'pedidos por mail', 'Factura MOB 1069', '2017-04-01', 0),
(0503, '31', NULL, '0', 0, 52, '93.00', '0.00', '0.00', '0.00', '0.00', '105.31', '0.00', '7', 'Notes...', 'Factura MOB 945', '2017-04-01', 0),
(0504, '367', NULL, '0', 0, 25, '100.00', '0.00', '0.00', '0.00', '0.00', '100.00', '0.00', '7', 'Notes...', 'Factura MOB943', '2017-04-01', 0),
(0505, '35', NULL, '0', 0, 43, '107.00', '107.50', '0.50', '0.00', '0.00', '107.50', '48.00', '10', 'Notes...', 'Factura MOB 955.', '2017-04-01', 0),
(0506, '353', NULL, '0', 0, 124, '115.00', '124.00', '9.00', '0.00', '0.00', '124.00', '0.00', '10', 'Notes...', 'FActura MOB 953.', '2017-04-01', 0),
(0507, '368', NULL, '0', 0, 51, '122.00', '0.00', '0.00', '0.00', '0.00', '125.79', '0.00', '7', 'FORWARD MEDIA COMMUNICATIONS SA DE CV  Calle: AV. PROLONGACION PASEO DE LA REFORMA No. 1015 Int: PISO 24,  Col. DESARROLLO CUAJIMALPA DE MORELOS, DISRITO FEDERAL  CP 05348 RFC: FMC1412123B6', 'Factura MOB 931 enviar a grissel.Mayorga@corp.terra.com', '2017-04-01', 0),
(0508, '14', NULL, '0', 0, 102, '132.00', '0.00', '0.00', '0.00', '0.00', '133.90', '0.00', '7', 'Notes...', 'Factura MOB 930', '2017-04-01', 0),
(0509, '83', NULL, '0', 0, 42, '239.00', '262.10', '23.10', '0.00', '0.00', '262.10', '0.00', '7', 'Notes...', 'Factura MOB940', '2017-04-01', 0),
(0510, '175', NULL, '0', 0, 23, '241.00', '294.00', '53.00', '0.00', '0.00', '294.00', '0.00', '7', 'Notes...', 'Factura MOB 934', '2017-04-01', 0),
(0511, '380', NULL, '0', 0, 175, '315.00', '315.00', '0.00', '0.00', '0.00', '315.00', '0.00', '7', 'Notes...', 'Factura MOB 957', '2017-04-01', 0),
(0513, '24', NULL, '0', 0, 344, '367.00', '370.40', '3.40', '0.00', '260.40', '108.80', '0.00', '7', 'lo que está on hold todavía no ha sido confirmado', 'Factura MOB 954', '2017-04-01', 0),
(0514, '153', NULL, '0', 0, 140, '436.00', '439.60', '3.60', '422.40', '10.80', '0.00', '0.00', '10', 'Hablado con Cristina para anular factura. Scrub notificado el 15(festivo en Madrid)', '', '2017-04-01', 0),
(0515, '174', NULL, '0', 0, 485, '533.00', '471.90', '-61.10', '0.00', '0.00', '471.90', '0.00', '7', 'Validado: 471.90. Diferencia -4 horas. Factura mandada incorrecta (543.40)', 'Factura MOB 951./ Abono AMOB94 y Factura MOB959', '2017-04-01', 0),
(0516, '2', NULL, '0', 0, 342, '569.00', '569.26', '0.26', '0.00', '0.00', '569.26', '0.00', '10', 'casi seguro todo estará validado ok hasta el 20 de mayo', 'Factura MOB 950', '2017-04-01', 0),
(0517, '11', NULL, '0', 0, 615, '672.00', '0.00', '0.00', '0.00', '0.00', '668.80', '0.00', '7', 'Notes...', 'Factura MOB 929', '2017-04-01', 0),
(0518, '163', NULL, '0', 0, 1224, '1407.00', '0.00', '0.00', '0.00', '0.00', '1408.90', '0.00', '7', 'validado por Skype a Camilla - pedido por mail', 'Factura MOB 946', '2017-04-01', 0),
(0519, '172', NULL, '0', 0, 886, '1437.00', '1444.30', '7.30', '0.00', '0.00', '1444.30', '0.00', '10', 'Notes...', 'Factura MOB 949', '2017-04-01', 0),
(0520, '43', NULL, '0', 0, 1191, '1862.00', '0.00', '0.00', '0.00', '0.00', '1905.30', '0.00', '7', 'Notes...', 'Factura MOB 933', '2017-04-01', 0),
(0521, '171', NULL, '0', 0, 1385, '2858.00', '0.00', '0.00', '0.00', '0.00', '2800.00', '0.00', '7', 'Notes...', 'Factura MOB939', '2017-04-01', 0),
(0522, '4', NULL, '0', 0, 860, '7740.00', '0.00', '0.00', '0.00', '0.00', '7740.00', '0.00', '10', 'Notes...', 'Factura MOB 948', '2017-04-01', 0),
(0523, '42', NULL, '0', 0, 132, '75.00', '0.00', '0.00', '0.00', '0.00', '76.05', '0.00', '7', 'Notes...', 'Factura MOB 1033', '2017-04-01', 0),
(0526, '305', NULL, '0', 0, 22, '893.00', '0.00', '0.00', '0.00', '0.00', '902.00', '0.00', '7', 'Notes...', 'Factura MOB 932', '2017-04-01', 0),
(0528, '174', 174, '163', 0, 0, '71.50', '71.50', '0.00', '0.00', '0.00', '71.50', '0.00', '7', '', 'Factura MOB 936', '2017-03-01', 0),
(0529, '320', 380, '80', 0, 0, '102.00', '104.10', '2.10', '0.00', '0.00', '104.10', '83.00', '7', 'no pasa por Affise - confirmado Juliana', 'Factura MOB942', '2017-04-01', 0),
(0530, '308', 380, '27', 0, 0, '102.09', '0.00', '0.00', '0.00', '0.00', '102.09', '0.00', '7', 'Ya se ha creado factura para febrero (844) - hay que cambiarla por una con el importe validado aquí', 'Factura MOB 944 qye incluye tambien marzo y abril', '2017-02-01', 0),
(0531, '67', 380, '142', 1000, 1, '3.80', '3.80', '0.00', '0.00', '0.00', '3.80', '0.00', '2', '', '', '2017-04-01', 0),
(0532, '1', 352, '372', 0, 0, '152.80', '0.00', '0.00', '0.00', '0.00', '152.80', '0.00', '7', 'Actividad 2016 Neomobile ecuador - ver desglose mail ', 'Factura MOB961', '2017-05-01', 0),
(0538, '42', NULL, '0', 0, 373117, '31.60', '0.00', '0.00', '0.00', '0.00', '34.00', '0.00', '7', 'Notes...', 'Factura MOB 1034', '2017-05-01', 0),
(0540, '166', NULL, '0', 0, 18181, '8.00', '0.00', '0.00', '0.00', '0.00', '8.00', '0.00', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0542, '164', NULL, '0', 0, 42799, '0.60', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-05-01', 0),
(0543, '145', NULL, '0', 0, 148467, '0.25', '0.00', '0.00', '0.00', '0.00', '0.00', '43.50', '0', 'Notes...', 'Notes...', '2017-05-01', 0),
(0544, '4', NULL, '0', 0, 297091, '6561.00', '0.00', '0.00', '0.00', '0.00', '6723.00', '0.00', '7', 'pedido por mail', 'Factura MOB 968', '2017-05-01', 0),
(0545, '171', NULL, '0', 0, 766636, '5544.00', '0.00', '0.00', '0.00', '0.00', '5578.00', '0.00', '7', 'a partir de junio nuevos datos de facturación', 'Factura MOB 966', '2017-05-01', 0),
(0546, '31', NULL, '0', 0, 1007257, '3063.13', '0.00', '0.00', '0.00', '0.00', '3054.40', '0.00', '7', 'Notes...', 'Factura MOB 970', '2017-05-01', 0),
(0547, '83', NULL, '0', 0, 1424513, '2741.36', '2759.48', '18.12', '0.00', '0.00', '2759.48', '0.00', '7', 'Notes...', 'Factura MOB 969', '2017-05-01', 0),
(0548, '43', NULL, '0', 0, 4164610, '1708.25', '0.00', '0.00', '0.00', '0.00', '1704.50', '0.00', '7', 'Notes...', 'Factura MOB 965', '2017-05-01', 0),
(0549, '382', NULL, '0', 0, 2582794, '365.90', '0.00', '0.00', '0.00', '0.00', '387.90', '0.00', '7', 'Colombia: $ 91,50 - EUR 296,40 convertido todo a auer', 'Factura MOB973 Company Information 		  Company 	  Tekka S.p.A 	  Address 	  Lungodora Colletta75 	 	  10153 Torino ITALIA 	  VAT 	  IT09479150014 	  amministrazione 	  Simonetta 	  s.capaldi@tekka.it', '2017-05-01', 0),
(0550, '163', NULL, '0', 0, 212804, '626.75', '0.00', '0.00', '0.00', '0.00', '627.15', '0.00', '7', '', 'Factura MOB 971', '2017-05-01', 0),
(0551, '305', NULL, '0', 0, 239248, '475.00', '0.00', '0.00', '0.00', '0.00', '475.00', '0.00', '7', 'no hace falta enviar, la envian ellos de su plataforma,', 'Factura MOB 962', '2017-05-01', 0),
(0552, '308', NULL, '0', 0, 226414, '447.67', '221.96', '-225.71', '0.00', '0.00', '415.44', '0.00', '7', 'había un error en su panel y calculó mal el cpa x subs', 'Factura MOB976', '2017-05-01', 0),
(0553, '368', NULL, '0', 0, 713608, '414.39', '0.00', '0.00', '0.00', '0.00', '427.54', '0.00', '7', ' FORWARD MEDIA COMMUNICATIONS SA DE CV  Calle: AV. PROLONGACION PASEO DE LA REFORMA No. 1015 Int: PISO 24,  Col. DESARROLLO CUAJIMALPA DE MORELOS, DISRITO FEDERAL  CP 05348  RFC: FMC1412123B6', 'Factura MOB 964', '2017-05-01', 0),
(0554, '380', NULL, '0', 0, 24565, '342.00', '0.00', '0.00', '0.00', '0.00', '351.00', '0.00', '7', '', 'Factura MOB 977', '2017-05-01', 0),
(0555, '11', NULL, '0', 0, 798334, '301.40', '0.00', '0.00', '0.00', '0.00', '311.30', '0.00', '7', 'DIGITAL VIRGO ESPAÑA, S.A.  Address: 	  C/ Juan Ignacio Luca de Tena, 1, 3ª Planta, 28027, Madrid  ID TAX: 	  A80060924', 'Factura MOB 963', '2017-05-01', 0),
(0556, '383', NULL, '0', 0, 97103, '124.00', '0.00', '0.00', '0.00', '0.00', '130.00', '0.00', '7', 'Plusmobile - Comtel International LLC Company Address: 2250 Sun Trust Int\'l Center, One SE 3 rd - USA - Vat Number: 98- 041-9205', 'Factura MOB 978', '2017-05-01', 0),
(0557, '175', NULL, '0', 0, 76623, '52.50', '0.00', '0.00', '0.00', '0.00', '42.00', '0.00', '2', '', 'Notes...', '2017-05-01', 0),
(0558, '60', NULL, '0', 0, 101870, '44.55', '0.00', '0.00', '0.00', '0.00', '44.00', '0.00', '7', 'Notes...', 'Factura MOB1050', '2017-05-01', 0),
(0559, '160', NULL, '0', 0, 420107, '43.60', '0.00', '0.00', '0.00', '0.00', '43.60', '0.00', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0561, '306', NULL, '0', 0, 101281, '25.16', '0.00', '0.00', '0.00', '0.00', '25.06', '0.00', '7', 'Notes...', 'Factura MOB 974', '2017-05-01', 0),
(0562, '364', NULL, '0', 0, 4139, '18.00', '0.00', '0.00', '0.00', '0.00', '18.00', '0.00', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0563, '384', NULL, '0', 0, 91061, '17.10', '17.10', '0.00', '0.00', '0.00', '17.10', '0.00', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0564, '223', NULL, '0', 0, 64962, '15.00', '0.00', '0.00', '0.00', '0.00', '15.00', '0.00', '4', 'Notes...', 'Notes...', '2017-05-01', 0),
(0565, '28', NULL, '0', 0, 48767, '14.35', '2.70', '-11.65', '0.00', '0.00', '2.70', '0.00', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0566, '385', NULL, '0', 0, 248454, '12.40', '12.40', '0.00', '0.00', '0.00', '12.40', '0.00', '7', 'Notes...', 'Factura MOB 1022', '2017-05-01', 0),
(0567, '63', NULL, '0', 0, 3530, '10.00', '0.00', '0.00', '0.00', '0.00', '10.00', '0.00', '2', 'Mandar a Crazy4Media Mobile Advertising &amp; Billing C/Arquitectura Nº2 Torre 11 Planta 7, Mod: del 1 al 7. 41015 Sevilla Vat: B90125741 finance@crazy4mediamobile.com', 'Notes...', '2017-05-01', 0),
(0570, '48', NULL, '0', 0, 12088, '8.84', '0.00', '0.00', '0.00', '0.00', '40.30', '0.00', '7', 'no sirve mandar factura - min 100 acumular', 'Factura MOB994', '2017-05-01', 0),
(0571, '77', NULL, '0', 0, 37160, '8.40', '8.68', '0.28', '0.00', '0.00', '8.68', '19.04', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0572, '386', NULL, '0', 0, 218219, '3.37', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-05-01', 0),
(0573, '173', NULL, '0', 0, 2825300, '2.70', '2.70', '0.00', '0.00', '0.00', '2.70', '108.09', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0574, '135', NULL, '0', 0, 67223, '2.38', '3.13', '0.75', '0.00', '0.00', '3.13', '48.13', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0575, '57', NULL, '0', 0, 31, '2.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0576, '378', NULL, '0', 0, 31, '2.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0577, '367', NULL, '0', 0, 32642, '1.92', '0.00', '0.00', '0.00', '0.00', '1.82', '0.00', '4', 'Notes...', 'Notes...', '2017-05-01', 0),
(0578, '363', NULL, '0', 0, 16270, '1.80', '1.80', '0.00', '0.00', '0.00', '1.80', '0.00', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0579, '379', NULL, '0', 0, 33, '1.60', '0.00', '0.00', '0.00', '0.00', '1.60', '0.00', '4', 'Notes...', 'Notes...', '2017-05-01', 0),
(0580, '373', NULL, '0', 0, 22235, '1.35', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0581, '153', NULL, '0', 0, 348304, '1.32', '1.32', '0.00', '0.00', '0.00', '1.32', '38.88', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0582, '164', NULL, '0', 0, 42799, '0.67', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-05-01', 0),
(0583, '145', NULL, '0', 0, 148467, '0.28', '0.00', '0.00', '0.00', '0.00', '0.00', '43.50', '0', 'Notes...', 'Notes...', '2017-05-01', 0),
(0584, '361', NULL, '0', 0, 80660, '0.22', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-05-01', 0),
(0585, '387', NULL, '0', 0, 4, '0.20', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'test', 'Notes...', '2017-05-01', 0),
(0586, '382', 239, '376', 0, 0, '832.32', '0.00', '0.00', '0.00', '0.00', '704.77', '0.00', '7', 'Company  Tekka Digital SA Address Via Balestra 12  6900 Lugano (CH)  VAT  CHE-303.540.263', 'Factura MOB972   send the invoice to the address Amministrazione@tekkadigital.com  ', '2017-05-01', 0),
(0587, '320', 387, '376', 0, 0, '81.30', '0.00', '0.00', '0.00', '0.00', '83.00', '0.00', '2', '', '', '2017-05-01', 0),
(0588, '24', 387, '144', 1000, 1, '260.40', '260.40', '0.00', '0.00', '0.00', '260.40', '0.00', '7', 'se refiere a lo que estaba on hold el mes de abril (nunca han aportado pruebas de scrubs)', 'Factura MOB 975', '2017-05-01', 0),
(0589, '17', 387, '338', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '-1981.26', '0.00', '7', 'ABONO de la factura MOB926', 'Abono AMOB96', '2017-03-01', 0),
(0590, '2', 387, '338', 0, 0, '-6435.44', '0.00', '0.00', '0.00', '0.00', '-6435.44', '0.00', '7', 'para hacer el abono de la factura 925', 'Abono AMOB97', '2017-03-01', 0),
(0591, '83', NULL, '0', 0, 1895, '27851.37', '28207.57', '356.20', '0.00', '0.00', '28207.57', '0.00', '7', 'recordar hacer gesglose', 'Factura MOB997', '2017-06-01', 5),
(0592, '4', NULL, '0', 0, 688, '6174.00', '0.00', '0.00', '0.00', '0.00', '6723.00', '0.00', '7', '', 'Factura MOB998', '2017-06-01', 6),
(0593, '171', NULL, '0', 0, 1748, '3443.50', '0.00', '0.00', '0.00', '0.00', '3405.50', '0.00', '7', 'facturado a nombre de Feraz Media', 'Factura MOB986', '2017-06-01', 0),
(0594, '168', NULL, '0', 0, 279, '2528.75', '2545.57', '16.82', '0.00', '0.00', '2556.57', '1.00', '7', '', 'Factura MOB1000', '2017-06-01', 8),
(0595, '43', NULL, '0', 0, 1896, '2383.30', '0.00', '0.00', '0.00', '0.00', '2390.20', '0.00', '7', 'Notes...', 'Factura MOB993', '2017-06-01', 0),
(0596, '382', NULL, '0', 0, 1538, '1965.36', '0.00', '0.00', '0.00', '0.00', '715.00', '0.00', '7', 'USD 558,00+EUR 224,20 ', 'Factura MOB1006 y MOB1007 Provisión Tc 1,1412 Importe 626,53 €', '2017-06-01', 24),
(0597, '163', NULL, '0', 0, 1083, '1245.45', '0.00', '0.00', '0.00', '0.00', '1246.00', '0.00', '7', '', 'Factura MOB999', '2017-06-01', 7),
(0598, '379', NULL, '0', 0, 1586, '1258.40', '0.00', '0.00', '0.00', '0.00', '1258.40', '1.60', '7', 'no contesta, enviado el mensaje por whatsapp insistiendo', 'Factura MOB1004Provisión: Tc 1,1412 Importe 1102,69 €', '2017-06-01', 22),
(0599, '31', NULL, '0', 0, 653, '890.37', '0.00', '0.00', '0.00', '0.00', '1270.61', '51.40', '7', 'confirman sobre el 15th probablemente', 'Factura MOB1002', '2017-06-01', 9),
(0600, '383', NULL, '0', 0, 422, '422.00', '0.00', '0.00', '0.00', '0.00', '429.00', '0.00', '7', 'COLOMBIA Plusmobile Communications SA  Calle 94A # 11A ­ 27 of 402  Bogotá ­ Bogotá  111051 ­ Colombia Nit: 900041186-1', 'Factura MOB988', '2017-06-01', 0),
(0601, '305', NULL, '0', 0, 9, '359.92', '0.00', '0.00', '0.00', '0.00', '350.00', '0.00', '7', 'no hay que enviarles facturas', 'Factura MOB 985', '2017-06-01', 0),
(0602, '11', NULL, '0', 0, 310, '341.00', '0.00', '0.00', '0.00', '0.00', '331.10', '0.00', '7', 'Notes...', 'Factura MOB990', '2017-06-01', 0),
(0603, '368', NULL, '0', 0, 133, '339.53', '0.00', '0.00', '0.00', '0.00', '346.07', '0.00', '7', 'Notes...', 'Factura MOB989', '2017-06-01', 0),
(0604, '223', NULL, '0', 0, 42, '165.00', '0.00', '0.00', '0.00', '0.00', '165.00', '134.03', '7', 'Notes...', 'factura MOB1008 Provisión Tc 1,1412 Importe 144,58 €', '2017-06-01', 27),
(0605, '166', NULL, '0', 0, 16, '146.25', '0.00', '0.00', '0.00', '0.00', '128.00', '8.20', '7', 'Notes...', 'Factura MOB991', '2017-06-01', 0),
(0606, '385', NULL, '0', 0, 120, '118.00', '118.00', '0.00', '0.00', '0.00', '118.00', '12.40', '7', 'Notes...', 'Factura MOB1011-Provisión Tc 1,1412 Importe 103,39 €', '2017-06-01', 29),
(0607, '14', NULL, '0', 0, 94, '98.20', '0.00', '0.00', '98.20', '0.00', '0.00', '0.00', '2', 'ver scrubs', 'Notes...', '2017-06-01', 0),
(0609, '314', NULL, '0', 0, 19, '92.50', '0.00', '0.00', '0.00', '0.00', '81.56', '0.00', '7', 'facturar aunque es menos de 100$', 'Factura MOB992.', '2017-06-01', 0),
(0610, '175', NULL, '0', 0, 6, '61.50', '0.00', '0.00', '0.00', '0.00', '61.50', '42.00', '7', 'Notes...', 'Factura MOB1001', '2017-06-01', 0),
(0611, '48', NULL, '0', 0, 111, '57.72', '0.00', '0.00', '0.00', '0.00', '67.87', '9.00', '7', 'no se manda factura - min 100 se acumula con Mayo', 'Factura MOB994', '2017-06-01', 0),
(0612, '308', 194, '39', 0, 262, '53.31', '0.00', '0.00', '0.00', '0.00', '49.30', '0.00', '7', 'Notes...', 'Factura MOB1061', '2017-06-01', 0),
(0613, '386', NULL, '0', 0, 6, '37.71', '33.00', '-4.71', '0.00', '0.00', '33.00', '0.00', '2', 'Notes...', 'Notes...', '2017-06-01', 0),
(0614, '367', NULL, '0', 0, 39, '37.56', '0.00', '0.00', '0.00', '0.00', '37.56', '1.82', '7', 'Notes...', 'Factura MOB1038', '2017-06-01', 0),
(0615, '387', NULL, '0', 0, 45, '29.13', '29.25', '0.12', '0.00', '0.00', '29.25', '0.00', '2', 'Notes...', 'Notes...', '2017-06-01', 0),
(0616, '388', NULL, '0', 0, 4, '21.94', '0.00', '0.00', '0.00', '0.00', '21.87', '0.00', '2', 'Notes...', 'Notes...', '2017-06-01', 0),
(0617, '164', NULL, '0', 0, 33, '19.85', '0.00', '0.00', '0.00', '0.00', '44.28', '0.00', '2', 'Notes...', 'Notes...', '2017-06-01', 0),
(0618, '60', NULL, '0', 0, 7, '17.50', '0.00', '0.00', '0.00', '0.00', '17.50', '59.20', '7', 'Notes...', 'Factura MOB1050', '2017-06-01', 0),
(0619, '373', NULL, '0', 0, 4, '14.91', '13.96', '-0.95', '0.00', '0.00', '13.96', '5.00', '2', 'Notes...', 'Notes...', '2017-06-01', 0),
(0620, '134', NULL, '0', 0, 1, '11.43', '10.00', '-1.43', '0.00', '0.00', '10.00', '122.74', '7', 'revenue en $', 'Factura MOB 1031', '2017-06-01', 0),
(0621, '363', NULL, '0', 0, 9, '7.20', '0.00', '0.00', '0.00', '0.00', '7.20', '8.50', '4', 'Notes...', 'Notes...', '2017-06-01', 0),
(0622, '28', NULL, '0', 0, 9, '4.50', '5.50', '1.00', '0.00', '0.00', '5.50', '2.70', '2', 'Notes...', 'Notes...', '2017-06-01', 0),
(0623, '160', NULL, '0', 0, 3, '2.40', '0.00', '0.00', '0.00', '0.00', '2.40', '38.10', '4', 'Notes...', 'Notes...', '2017-06-01', 0),
(0624, '364', NULL, '0', 0, 2, '2.40', '0.00', '0.00', '0.00', '0.00', '2.40', '52.00', '2', 'Notes...', 'Notes...', '2017-06-01', 0),
(0625, '149', NULL, '0', 0, 1, '1.20', '0.00', '0.00', '0.00', '0.00', '1.20', '0.00', '4', 'Notes...', 'Notes...', '2017-06-01', 0),
(0627, '345', NULL, '0', 0, 1, '0.84', '0.00', '0.00', '0.00', '0.00', '0.84', '0.00', '4', 'Notes...', 'Notes...', '2017-06-01', 0),
(0628, '389', NULL, '0', 0, 1, '0.82', '0.00', '0.00', '0.00', '0.00', '0.82', '0.00', '4', 'Notes...', 'Notes...', '2017-06-01', 0),
(0629, '42', NULL, '0', 0, 1, '0.25', '0.00', '0.00', '0.00', '0.00', '0.25', '110.30', '2', 'Notes...', 'Notes...', '2017-06-01', 0),
(0630, '320', 389, '166', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '578.00', '0.00', '7', '', 'Factura MOB987', '2017-06-01', 0),
(0632, '391', 177, '329', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '1081.90', NULL, '7', '0 en affise porque no estaban separados los tekkas', 'Factura MOB1005 Amministrazione@tekkadigital.com PROVISION Tc 1,1412 Importe 948,04 €', '2017-06-01', 26),
(0633, '83', NULL, '0', 0, 2933, '31397.86', '0.00', '0.00', '14040.00', '0.00', '18192.46', '0.00', '7', 'para abonar (se factura BR por separado)', 'Factura MOB 1028/ AMOB111 /  MOB 1044', '2017-07-01', 48),
(0634, '4', NULL, '0', 0, 637, '5733.00', '0.00', '0.00', '0.00', '0.00', '5733.00', '0.00', '7', 'Notes...', 'Factura MOB1019', '2017-07-01', 41),
(0635, '43', NULL, '0', 0, 3507, '3334.70', '0.00', '0.00', '0.00', '0.00', '3330.25', '0.00', '7', 'Notes...', 'Factura MOB 1018', '2017-07-01', 38),
(0636, '392', NULL, '0', 0, 1376, '1440.23', '0.00', '0.00', '0.00', '0.00', '1256.65', '0.00', '7', 'Tekka Digital SA 	  Address 	  Via Balestra 12 	 	  6900 Lugano (CH) VAT  CHE-303.540.263  send the invoice to amministrazione@tekkadigital.com', 'Factura MOB1036', '2017-07-01', 53),
(0637, '368', NULL, '0', 0, 320, '778.08', '0.00', '0.00', '0.00', '0.00', '859.46', '0.00', '7', 'Notes...', 'Factura MOB 1015', '2017-07-01', 35),
(0638, '305', NULL, '0', 0, 18, '705.93', '0.00', '0.00', '0.00', '0.00', '595.88', '0.00', '7', 'No hay que enviar facturas, revenue en $', 'Factura MOB 1020', '2017-07-01', 43),
(0639, '314', NULL, '0', 0, 134, '670.00', '0.00', '0.00', '0.00', '0.00', '566.00', '0.00', '7', 'Revenue en $', 'Factura MOB 1016', '2017-07-01', 39),
(0640, '386', NULL, '0', 0, 77, '501.72', '0.00', '0.00', '0.00', '0.00', '880.00', '33.00', '7', '5.5€ * 160 leads jazztel', 'Factura MOB 1035', '2017-07-01', 49),
(0641, '168', NULL, '0', 0, 38, '494.45', '501.77', '7.32', '0.00', '0.00', '501.77', '0.00', '7', 'Notes...', 'Factura MOB 1045', '2017-07-01', 58),
(0642, '345', NULL, '0', 0, 261, '360.36', '0.00', '0.00', '0.00', '0.00', '368.30', '0.88', '7', 'Notes...', 'Factura MOB1026', '2017-07-01', 46),
(0643, '383', NULL, '0', 0, 263, '263.00', '0.00', '0.00', '0.00', '0.00', '269.00', '0.00', '7', 'poner en concepto \"campañas comtel\"- Facturar a COLOMBIA Plusmobile Communications SA  Calle 94A # 11A ­ 27 of 402  Bogotá ­ Bogotá  111051 ­ Colombia Nit: 900041186-1', 'Factura MOB 1017', '2017-07-01', 40),
(0644, '163', NULL, '0', 0, 207, '238.05', '0.00', '0.00', '0.00', '0.00', '231.05', '0.00', '7', 'Notes...', 'Factura MOB 1048', '2017-07-01', 59),
(0645, '388', NULL, '0', 0, 232, '209.35', '216.39', '7.04', '0.00', '0.00', '216.39', '21.87', '7', 'Notes...', 'Factura MOB 1023', '2017-07-01', 44),
(0646, '223', NULL, '0', 0, 42, '164.00', '0.00', '0.00', '0.00', '0.00', '171.00', '0.00', '7', 'Notes...', 'Factura MOB 1040', '2017-07-01', 55),
(0647, '164', NULL, '0', 0, 132, '156.28', '0.00', '0.00', '0.00', '0.00', '288.47', '44.28', '7', 'Notes...', 'Factura MOB1027', '2017-07-01', 47),
(0648, '48', NULL, '0', 0, 268, '139.36', '0.00', '0.00', '0.00', '0.00', '69.39', '0.00', '7', '80,07 € se suman para agosto', 'Factura MOB1047', '2017-07-01', NULL),
(0649, '304', NULL, '0', 0, 22, '88.87', '0.00', '0.00', '0.00', '0.00', '80.21', '0.00', '7', 'Notes...', 'Factura MOB 1025', '2017-07-01', NULL),
(0650, '2', NULL, '0', 0, 2, '72.00', '109.60', '37.60', '0.00', '0.00', '0.00', '0.00', '2', '', 'Notes...', '2017-07-01', NULL),
(0651, '134', NULL, '0', 0, 6, '71.08', '0.00', '0.00', '0.00', '0.00', '60.00', '122.74', '7', 'Notes...', 'Factura MOB 1032', '2017-07-01', NULL),
(0652, '367', NULL, '0', 0, 66, '66.96', '0.00', '0.00', '0.00', '0.00', '67.92', '39.38', '7', 'Notes...', 'Factura MOB1039', '2017-07-01', NULL),
(0653, '60', NULL, '0', 0, 6, '57.00', '0.00', '0.00', '0.00', '0.00', '57.00', '76.70', '7', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0654, '403', 183, '163', 0, 16, '48.00', '0.00', '0.00', '0.00', '0.00', '132.00', '0.00', '7', '', 'Factura MOB 1037', '2017-07-01', 54),
(0655, '308', 194, '39', 0, 96, '45.23', '0.00', '0.00', '0.00', '0.00', '38.45', '49.30', '7', 'revenue en $', 'Factura MOB 1061', '2017-07-01', NULL),
(0656, '175', NULL, '0', 0, 5, '43.00', '0.00', '0.00', '0.00', '0.00', '53.00', '0.00', '7', 'Notes...', 'Factura MOB 1041', '2017-07-01', NULL),
(0657, '382', NULL, '0', 0, 39, '29.26', '0.00', '0.00', '0.00', '0.00', '160.50', '0.00', '7', 'Notes...', 'Factura MOB 1024', '2017-07-01', 45),
(0658, '387', NULL, '0', 0, 8, '26.54', '26.42', '-0.12', '0.00', '0.00', '26.42', '29.25', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0659, '77', NULL, '0', 0, 4, '26.00', '26.00', '0.00', '0.00', '0.00', '26.00', '0.00', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0660, '393', NULL, '0', 0, 22, '23.40', '0.00', '0.00', '0.00', '0.00', '23.40', '0.00', '4', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0661, '394', NULL, '0', 0, 28, '19.19', '0.00', '0.00', '0.00', '0.00', '16.20', '0.00', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0662, '395', NULL, '0', 0, 1, '16.59', '14.00', '-2.59', '0.00', '0.00', '14.00', '0.00', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0663, '364', NULL, '0', 0, 11, '16.50', '17.60', '1.10', '0.00', '0.00', '17.60', '54.40', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0664, '396', NULL, '0', 0, 15, '16.45', '0.00', '0.00', '0.00', '0.00', '16.80', '0.00', '2', 'se factura cada país por separado, preguntar antes de facturar', 'Notes...', '2017-07-01', NULL),
(0665, '385', NULL, '0', 0, 13, '15.60', '17.70', '2.10', '0.00', '0.00', '17.70', '0.00', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0666, '397', NULL, '0', 0, 19, '8.82', '8.82', '0.00', '0.00', '0.00', '8.82', '0.00', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0667, '70', NULL, '0', 0, 2, '7.11', '0.00', '0.00', '0.00', '0.00', '7.11', '0.00', '4', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0668, '398', 191, '138', 0, 2, '4.50', '9.45', '4.95', '0.00', '0.00', '9.45', '0.00', '7', 'Notes...', 'Factura MOB1080', '2017-07-01', NULL),
(0669, '317', NULL, '0', 0, 2, '4.20', '4.20', '0.00', '0.00', '0.00', '4.20', '0.00', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0670, '399', NULL, '0', 0, 6, '3.00', '0.00', '0.00', '0.00', '0.00', '3.00', '0.00', '4', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0671, '372', NULL, '0', 0, 3, '3.00', '3.00', '0.00', '0.00', '0.00', '3.00', '0.00', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0672, '160', NULL, '0', 0, 1, '2.40', '1.60', '-0.80', '0.00', '0.00', '1.60', '40.50', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0673, '400', NULL, '0', 0, 13, '2.16', '0.00', '0.00', '0.00', '0.00', '1.00', '0.00', '2', 'Notes...', 'Notes...', '2017-07-01', NULL);
INSERT INTO `incomes` (`id`, `advertiser`, `social`, `am`, `clicks`, `conversion`, `revenue`, `platform_revenue`, `difference`, `scrubs`, `hold`, `validated`, `accumulated`, `status`, `notas_am`, `notas_finance`, `date`, `increment`) VALUES
(0674, '149', NULL, '0', 0, 1, '1.20', '0.00', '0.00', '0.00', '0.00', '1.20', '1.20', '4', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0675, '401', 182, '134', 49896, 9, '72.00', '72.00', '0.00', '0.00', '0.00', '72.00', '0.00', '7', 'facturar con datos de PT', 'Factura MOB 1021', '2017-07-01', NULL),
(0676, '159', 183, '304', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '1471.33', '0.00', '7', '1079,12vf+392,21orange', 'Factura MOB1051', '2017-07-01', 56),
(0677, '31', 183, '134', 0, 0, '254.78', '254.78', '0.00', '0.00', '0.00', '254.78', '0.00', '7', '', 'Factura MOB1046', '2017-07-01', 60),
(0678, '404', 185, '162', 0, 0, '61.36', '61.36', '0.00', '0.00', '0.00', '61.36', '0.00', '7', 'por facturar por separado', 'Factura MOB1049', '2017-07-01', NULL),
(0679, '42', 185, '162', 0, 0, '153.20', '153.20', '0.00', '0.00', '0.00', '153.20', '0.00', '7', '', 'Factura MOB 1043', '2017-02-01', NULL),
(0901, '177', 415, '134', 0, 0, '13.50', '13.50', '0.00', '0.00', '0.00', '13.50', '0.00', '2', '', '', '2017-03-01', NULL),
(1008, '368', 416, '128', 0, 3357, '8042.94', '0.00', '0.00', '874.10', '0.00', '8127.20', '0.00', '7', 'la diferencia se debe a dos pausas y un cambio de cap a lo largo de fin de semana que no fueron aplicados a tiempo, asi que no se quitará de ningun afiliado (ver hoja scrub)', 'Factura MOB1056', '2017-08-01', 67),
(1009, '403', 416, '128', 0, 2434, '7947.46', '0.00', '0.00', '0.00', '0.00', '7793.50', '0.00', '7', 'la diferencia por un error al montar una campaña (por eso el revenue sale como mezcla de $ y €)', 'Factura MOB 1066', '2017-08-01', 81),
(1010, '83', 194, '39', 0, 611, '7242.90', '0.00', '0.00', '0.00', '0.00', '7999.90', '0.00', '7', 'Notes...', 'Factura MOB 1067', '2017-08-01', 77),
(1011, '43', 416, '128', 0, 5024, '6077.59', '0.00', '0.00', '0.00', '0.00', '6089.35', '0.00', '7', 'incluye collectcent smartlink', 'Factura MOB1063', '2017-08-01', 66),
(1012, '4', 194, '39', 0, 370, '3332.00', '0.00', '0.00', '0.00', '0.00', '2808.24', '0.00', '7', 'revenue en ~$', 'Factura MOB 1064', '2017-08-01', 74),
(1013, '60', 191, '138', 0, 210, '1903.70', '0.00', '0.00', '0.00', '0.00', '1913.20', '0.00', '7', 'Notes...', 'Factura MOB 1073', '2017-08-01', 84),
(1014, '223', 194, '39', 0, 370, '1469.00', '0.00', '0.00', '0.00', '0.00', '1546.00', '0.00', '7', 'Notes...', 'Factura MOB 1065', '2017-08-01', 76),
(1015, '408', 416, '128', 0, 258, '1221.01', '0.00', '0.00', '0.00', '0.00', '1032.00', '0.00', '7', 'The invoice and payment requests will be issued on this page on the first day of each month for your earnings paid by advertisers.', 'Factura MOB 1057', '2017-08-01', 69),
(1016, '31', 191, '138', 0, 968, '1203.80', '1407.25', '203.45', '764.40', '0.00', '642.85', '0.00', '2', 'Notes...', 'el 17th sabremos pubid de scrub ', '2017-08-01', 94),
(1017, '398', 194, '39', 0, 722, '747.90', '606.15', '-141.75', '0.00', '0.00', '754.20', '9.45', '7', 'hay error en su plataforma, acordamos por mail', 'Factura MOB 1080', '2017-08-01', 89),
(1018, '345', 191, '138', 0, 476, '672.35', '0.00', '0.00', '0.00', '0.00', '672.91', '0.00', '7', 'Notes...', 'Factura MOB 1075', '2017-08-01', 86),
(1019, '168', 194, '39', 0, 57, '656.45', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '1', 'confirman hasta el 15th', 'Notes...', '2017-08-01', NULL),
(1020, '417', 416, '134', 0, 121, '605.00', '0.00', '0.00', '0.00', '0.00', '504.00', '0.00', '7', 'revenue en $', 'Factura MOB 1058 - hay que volver a hacerla', '2017-08-01', 63),
(1021, '319', 194, '304', 0, 18, '427.86', '0.00', '0.00', '0.00', '0.00', '360.00', '93.00', '7', 'Notes...', 'Factura MOB 1068', '2017-08-01', 83),
(1022, '175', 191, '138', 0, 42, '378.10', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '1', 'Notes...', 'por dmexco confirmaciones más tarde', '2017-08-01', NULL),
(1023, '401', 416, '128', 0, 29, '275.73', '0.00', '0.00', '0.00', '0.00', '271.00', '0.00', '7', 'Notes...', 'Factura MOB 1059', '2017-08-01', 62),
(1024, '383', 416, '2', 0, 247, '250.80', '0.00', '0.00', '0.00', '0.00', '948.80', '0.00', '7', 'Asunto tiene que ser  \"campañas comtel\" y que tenga fecha de septiembre. ', 'Factura MOB 1079 PLUSMOBILE COMMUNICATIONS SA 1110501 BOGOTA FACTURA BOGOTA-COLOMBIA  ID CIF/DNI: CO9000411861', '2017-08-01', 61),
(1025, '304', 191, '138', 0, 22, '184.80', '0.00', '0.00', '0.00', '0.00', '150.59', '0.00', '7', 'Company Name: Rock Internet, S.L. VAT: ESB86907953 Phone: +34 91 084 17 30 Billing Address: C/ Barquillo, 8 - 2ºizq. City: Madrid Postal Code: 28004 Email: billing@arkeero.com', 'Factura MOB 1076   Bank name: BSCH (Banco Santander Central Hispano) Bank address: Príncipe de Vergara, 277 - 28016- Madrid (oficina 2862) IBAN (Euros (€)):ES93 0049 2862 67 2914627049', '2017-08-01', 87),
(1026, '308', 416, '128', 0, 270, '177.35', '0.00', '0.00', '0.00', '0.00', '149.19', '87.75', '7', 'revenue en $', 'Factura MOB 1061', '2017-08-01', 71),
(1027, '177', 194, '3', 0, 9, '64.00', '0.00', '0.00', '0.00', '0.00', '64.00', '0.00', '7', 'aclarando la diferencia', 'Factura MOB 1070', '2017-08-01', NULL),
(1028, '171', 416, '128', 0, 20, '60.96', '0.00', '0.00', '0.00', '0.00', '60.00', '0.00', '7', 'Notes...', 'Factura MOB 1060', '2017-08-01', NULL),
(1029, '164', 191, '138', 0, 46, '60.48', '0.00', '0.00', '0.00', '0.00', '97.76', '0.00', '7', 'Notes...', 'Factura MOB 1071', '2017-08-01', NULL),
(1030, '388', 194, '39', 0, 141, '56.40', '1.20', '-55.20', '0.00', '0.00', '56.40', '0.00', '7', 'validado skype 07/09', 'Factura MOB 1072', '2017-08-01', NULL),
(1031, '305', 416, '128', 0, 2, '49.92', '0.00', '0.00', '0.00', '0.00', '42.00', '0.00', '2', 'revenue en $, no hay que enviar factura', 'Notes...', '2017-08-01', NULL),
(1032, '318', 194, '39', 0, 26, '41.70', '0.00', '0.00', '0.00', '0.00', '41.70', '0.00', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1033, '397', 194, '39', 0, 84, '41.16', '0.00', '0.00', '0.00', '0.00', '41.16', '8.82', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1034, '367', 194, '39', 0, 33, '38.90', '0.00', '0.00', '0.00', '0.00', '38.90', '0.00', '7', 'Notes...', 'Factura MOB 1077', '2017-08-01', NULL),
(1035, '394', 416, '128', 0, 14, '38.74', '48.80', '10.06', '0.00', '0.00', '48.80', '16.20', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1036, '2', 191, '138', 0, 1, '36.00', '120.80', '84.80', '0.00', '0.00', '0.00', '0.00', '2', 'generado por el error del adv', 'Notes...', '2017-08-01', NULL),
(1037, '409', 191, '138', 0, 6, '33.28', '22.29', '-10.99', '0.00', '0.00', '22.29', '0.00', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1038, '410', 191, '138', 0, 39, '19.00', '0.00', '0.00', '0.00', '0.00', '19.00', '0.00', '4', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1039, '416', 194, '2', 0, 15, '14.26', '0.00', '0.00', '0.00', '0.00', '12.16', '0.00', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1040, '134', 191, '138', 0, 1, '11.88', '10.00', '-1.88', '0.00', '0.00', '10.00', '8.00', '2', 'acumulado de febrero que se ha añadido ahora', 'Notes...', '2017-08-01', NULL),
(1041, '20', 191, '138', 0, 3, '6.65', '0.00', '0.00', '0.00', '0.00', '1.80', '0.00', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1042, '48', 416, '128', 0, 12, '6.24', '0.00', '0.00', '0.00', '0.00', '99.23', '0.00', '7', 'no hace falta mandar factura, ya está compensado el error de payout del mes pasado', 'Factura MOB 1062', '2017-08-01', NULL),
(1043, '400', 194, '39', 0, 36, '6.09', '0.00', '0.00', '0.00', '0.00', '6.27', '1.00', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1044, '393', 191, '138', 0, 8, '4.80', '0.00', '0.00', '0.00', '0.00', '4.80', '23.40', '2', 'basado en nuestros numeros', 'Notes...', '2017-08-01', NULL),
(1045, '411', 194, '39', 0, 13, '4.80', '0.00', '0.00', '0.00', '0.00', '32.55', '0.00', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1046, '364', 191, '138', 0, 3, '4.50', '4.80', '0.30', '0.00', '0.00', '4.80', '72.00', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1047, '160', 191, '138', 0, 1, '2.40', '1.60', '-0.80', '0.00', '0.00', '1.60', '42.10', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1048, '52', 191, '138', 0, 1, '1.85', '37.00', '35.15', '0.00', '0.00', '37.00', '0.00', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1049, '396', 191, '138', 0, 2, '1.70', '0.00', '0.00', '0.00', '0.00', '1.70', '16.80', '4', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1050, '412', 194, '39', 0, 1, '1.60', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1051, '406', 191, '138', 0, 1, '1.20', '0.00', '0.00', '0.00', '0.00', '1.20', '0.00', '4', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1052, '163', 191, '138', 0, 1, '0.85', '0.00', '0.00', '0.00', '0.00', '0.85', '0.00', '4', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1053, '405', 191, '138', 0, 1, '0.85', '0.00', '0.00', '0.00', '0.00', '0.85', '0.00', '4', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1054, '413', 191, '138', 0, 1, '0.71', '0.00', '0.00', '0.00', '0.00', '0.90', '0.00', '2', 'Webpilots España, S.L.  Pellaires, 30-38, Nave B  08019 Barcelona, España  N.I.F.: ESB62268339 finanzas@webpilots.com ', 'Notes...', '2017-08-01', NULL),
(1056, '415', 416, '128', 0, 37, '13.80', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'agregado y facturado en la cuenta de collectcent', 'Notes...', '2017-08-01', NULL),
(1057, '159', 371, '304', 0, 0, '4424.00', '0.00', '0.00', '0.00', '0.00', '4370.59', '0.00', '7', ' 4.370,59€ + IVA', 'Factura MOB 1074', '2017-08-01', 85),
(1058, '134', 191, '134', 0, 0, '0.00', '8.00', '8.00', '0.00', '0.00', '8.00', NULL, '2', '', '', '2017-02-01', NULL),
(1059, '83', 541, '3', 0, 1272, '12052.80', '0.00', '0.00', '0.00', '0.00', '12310.00', '0.00', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1060, '318', 541, '3', 0, 11188, '8554.94', '0.00', '0.00', '0.00', '0.00', '4244.00', '41.70', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1061, '368', NULL, '3', 0, 2240, '5607.80', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1062, '4', 541, '3', 0, 437, '4553.00', '0.00', '0.00', '0.00', '0.00', '4324.00', '0.00', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1063, '403', 541, '3', 0, 1003, '3265.00', '0.00', '0.00', '0.00', '0.00', '5555.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', 98),
(1064, '530', 541, '3', 0, 691, '3264.05', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1065, '43', 541, '3', 0, 2693, '2980.49', '0.00', '0.00', '0.00', '0.00', '23.00', '0.00', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1066, '164', NULL, '3', 0, 1995, '2205.13', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1067, '359', 541, '3', 0, 640, '2016.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1068, '531', NULL, '0', 0, 99, '1164.39', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1069, '168', NULL, '3', 0, 313, '1107.40', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1070, '417', 541, '3', 0, 200, '1000.00', '0.00', '0.00', '0.00', '0.00', '999.00', '0.00', '1', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1071, '398', NULL, '3', 0, 822, '878.60', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1072, '345', NULL, '3', 0, 1046, '878.03', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1073, '223', NULL, '3', 0, 208, '832.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1074, '409', 541, '3', 0, 62, '570.39', '0.00', '0.00', '0.00', '0.00', '0.00', '22.29', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1075, '415', 541, '3', 0, 677, '516.03', '0.00', '0.00', '0.00', '0.00', '233.00', '0.00', '1', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1076, '532', 541, '3', 0, 250, '260.05', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '9', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1077, '60', 541, '3', 0, 25, '237.50', '0.00', '0.00', '0.00', '0.00', '1232.00', '0.00', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1078, '177', NULL, '3', 0, 14, '198.40', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1079, '383', NULL, '3', 0, 141, '141.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1080, '20', 541, '3', 0, 130, '131.30', '0.00', '0.00', '0.00', '0.00', '0.00', '1.80', '4', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1081, '361', NULL, '3', 0, 31, '78.22', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1082, '533', NULL, '0', 0, 7, '70.86', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1083, '411', NULL, '3', 0, 118, '60.90', '0.00', '0.00', '0.00', '0.00', '0.00', '32.55', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1084, '534', 541, '3', 0, 40, '57.50', '0.00', '0.00', '0.00', '0.00', '333.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', 95),
(1085, '319', 541, '3', 0, 8, '55.50', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1086, '388', 541, '3', 0, 125, '50.00', '232.00', '0.00', '22.00', '0.00', '2323.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', 96),
(1087, '31', NULL, '3', 0, 11, '36.20', '0.00', '0.00', '0.00', '0.00', '0.00', '642.85', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1088, '535', 541, '3', 0, 30, '34.93', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1089, '407', 541, '3', 0, 27, '32.40', '0.00', '0.00', '0.00', '0.00', '1232.00', '0.00', '1', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1090, '329', NULL, '3', 0, 7, '30.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1091, '536', 541, '3', 0, 54, '23.85', '0.00', '0.00', '0.00', '0.00', '23.00', '0.00', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1092, '401', 541, '3', 0, 2, '21.26', '0.00', '0.00', '0.00', '0.00', '22.00', '0.00', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1093, '367', 541, '3', 0, 16, '17.90', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1094, '393', NULL, '3', 0, 8, '14.40', '0.00', '0.00', '0.00', '0.00', '0.00', '28.20', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1095, '308', 541, '3', 0, 73, '11.82', '0.00', '0.00', '0.00', '0.00', '2323.00', '0.00', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1096, '166', NULL, '3', 0, 1, '9.45', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1097, '537', 541, '3', 0, 3, '9.00', '0.00', '0.00', '0.00', '0.00', '2300.00', '0.00', '9', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1098, '538', 541, '3', 0, 11, '9.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1099, '539', NULL, '0', 0, 26, '8.74', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1100, '394', NULL, '3', 0, 2, '8.62', '0.00', '0.00', '0.00', '0.00', '0.00', '65.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1101, '304', 541, '3', 0, 1, '8.40', '0.00', '0.00', '0.00', '0.00', '1232.00', '0.00', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1102, '386', NULL, '3', 0, 1, '8.27', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1103, '540', NULL, '0', 0, 3, '8.27', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1104, '410', NULL, '3', 0, 14, '7.00', '0.00', '0.00', '0.00', '0.00', '0.00', '19.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1105, '139', NULL, '3', 0, 6, '6.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1106, '364', NULL, '3', 0, 2, '3.70', '0.00', '0.00', '0.00', '0.00', '0.00', '76.80', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1107, '541', NULL, '0', 0, 4, '2.70', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1108, '416', NULL, '3', 0, 4, '1.98', '0.00', '0.00', '0.00', '0.00', '0.00', '12.16', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1109, '405', 541, '3', 0, 2, '1.70', '0.00', '0.00', '0.00', '0.00', '123.00', '0.85', '1', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1110, '149', 541, '3', 0, 1, '1.20', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1111, '312', 541, '3', 0, 1, '0.60', '0.00', '0.00', '0.00', '0.00', '3323.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', 97),
(1112, '397', 541, '3', 0, 1, '0.49', '0.00', '0.00', '0.00', '0.00', '0.00', '49.98', '10', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1113, '387', NULL, '3', 0, 1, '0.12', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission`
--

CREATE TABLE `permission` (
  `permission_id` int(11) NOT NULL,
  `permission_description` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permission`
--

INSERT INTO `permission` (`permission_id`, `permission_description`) VALUES
(1, 'create incomes'),
(2, 'edit incomes'),
(3, 'edit notes AM'),
(4, 'edit notes FN'),
(5, 'total access'),
(6, 'only read'),
(7, 'costes edit'),
(8, 'costes create');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reason_scrubs`
--

CREATE TABLE `reason_scrubs` (
  `id` int(2) NOT NULL,
  `value` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `reason_scrubs`
--

INSERT INTO `reason_scrubs` (`id`, `value`) VALUES
(1, 'KPI not reached'),
(2, 'RR'),
(3, 'Overcap'),
(4, 'Fraud'),
(5, 'On Hold'),
(6, 'Rebrokering');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(50) NOT NULL,
  `role_description` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `role`
--

INSERT INTO `role` (`role_id`, `role_name`, `role_description`) VALUES
(1, 'Contabilidad', 'Validar Fact+Prov,No edita Costes, notas Finanzas'),
(2, 'Account Manager', 'No edita Costes. Valida Facturas. Notas AM'),
(3, 'Prod', 'All in costes. rest only read'),
(4, 'read only', 'Solo lectura en todo ninguna edición.'),
(5, 'total access', 'full edition');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_permission`
--

CREATE TABLE `role_permission` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `role_permission`
--

INSERT INTO `role_permission` (`role_id`, `permission_id`) VALUES
(1, 4),
(2, 2),
(2, 3),
(2, 1),
(3, 7),
(3, 8),
(4, 6),
(5, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `scrubs`
--

CREATE TABLE `scrubs` (
  `id` int(10) NOT NULL,
  `date` date NOT NULL,
  `date_recived` date NOT NULL,
  `status_adv` int(2) NOT NULL,
  `advertiser` int(5) NOT NULL,
  `offer` varchar(100) NOT NULL,
  `affiliate` int(3) NOT NULL,
  `conversions` int(10) NOT NULL,
  `revenue_disc` decimal(10,2) NOT NULL,
  `cost_disc` decimal(10,2) DEFAULT NULL,
  `traffic_team` int(2) NOT NULL,
  `status_aff` int(2) NOT NULL,
  `reason` int(2) NOT NULL,
  `notas_am` varchar(500) NOT NULL DEFAULT 'Notes...',
  `notas_aff` varchar(500) NOT NULL DEFAULT 'Notes...'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `scrubs`
--

INSERT INTO `scrubs` (`id`, `date`, `date_recived`, `status_adv`, `advertiser`, `offer`, `affiliate`, `conversions`, `revenue_disc`, `cost_disc`, `traffic_team`, `status_aff`, `reason`, `notas_am`, `notas_aff`) VALUES
(1, '2017-03-01', '2017-03-13', 1, 68, 'CleanMaster_US_Android_41497', 1, 41, '40.30', NULL, 1, 1, 1, 'Not passed GAID', 'Comunicado. A la espera de reporte'),
(2, '2017-03-01', '2017-03-21', 1, 173, 'TOUTIAOSHIPIN_CN_iOS_89481', 11, 84, '147.60', NULL, 1, 2, 2, 'sent screenshot + 2 excels', ''),
(3, '2017-03-01', '2017-03-14', 1, 151, 'Verychic_FR_iOS', 25, 518, '518.00', NULL, 1, 2, 4, 'sent by email (fake address)', '  sent detailed report (05/04)'),
(4, '2017-03-01', '2017-03-21', 1, 151, 'Cheerz_FR_iOS', 29, 243, '306.18', NULL, 1, 1, 3, '', 'Comunicado. Screenshot'),
(5, '2017-03-01', '2017-03-16', 1, 378, 'LetGo_TR_iOS_51081', 29, 100, '70.00', NULL, 1, 1, 4, 'aparte rebrokering', 'Comunicado'),
(6, '2017-03-01', '2017-03-15', 1, 3, 'Conquest_KR_Android_43730', 29, 574, '1360.38', NULL, 1, 1, 2, '', ''),
(7, '2017-03-01', '2017-04-06', 1, 173, 'Jinritoutiao_CN_iOS_89576', 37, 192, '288.00', NULL, 1, 1, 2, 'aparte incent traffic', 'comunicado.'),
(8, '2017-03-01', '2017-04-07', 1, 2, 'CradleOfEmpires_US_iOS_222', 51, 2344, '5320.88', NULL, 1, 2, 1, '', ''),
(9, '2017-03-01', '2017-04-07', 1, 2, 'ZakaZaka_RU_and_126', 52, 0, '0.00', NULL, 1, 2, 1, 'Only part of the KPIs has been reached', ''),
(10, '2017-03-01', '2017-04-07', 1, 2, 'ZakaZaka_RU_and_126', 48, 704, '537.63', NULL, 2, 1, 1, 'Only part of the KPIs has been reached - no tenemos split por publisher por eso propongo pagar a Smartlead y descontar todo Instal', ''),
(11, '2017-03-01', '2017-04-07', 1, 2, 'ZakaZaka_RU_iOS_125', 46, 4, '3.80', NULL, 2, 1, 1, '', ''),
(12, '2017-03-01', '2017-04-07', 1, 2, 'ZakaZaka_RU_iOS_125', 27, 102, '96.90', NULL, 1, 2, 1, '', ''),
(13, '2017-03-01', '2017-04-07', 1, 2, 'ZakaZaka_RU_iOS_125', 52, 761, '722.95', NULL, 1, 2, 1, '', ''),
(14, '2017-03-01', '2017-04-11', 2, 162, 'Wadi_UAE and SA_iOS', 53, 132, '171.60', NULL, 1, 2, 1, '', 'Proof sent by email (THEY WILL PAY 135 CONVERSIONS FROM THE TOTAL)'),
(15, '2017-03-01', '2017-04-11', 2, 162, 'Wadi_UAE and SA_iOS', 5, 1579, '2052.70', NULL, 1, 1, 1, 'also incent traffic (see CRs per day)', 'Proof sent by email (THEY WILL PAY 135 CONVERSIONS FROM THE TOTAL)'),
(16, '2017-03-01', '2017-04-11', 2, 162, 'Wadi_UAE and SA_iOS', 6, 1686, '2191.80', NULL, 1, 1, 1, '', 'Proof sent by email (THEY WILL PAY 135 CONVERSIONS FROM THE TOTAL)'),
(17, '2017-03-01', '2017-04-11', 2, 162, 'Wadi_UAE and SA_Android', 5, 9009, '4504.05', NULL, 1, 1, 1, 'also incent traffic (see CRs per day)', 'Proof sent by email (THEY WILL PAY 135 CONVERSIONS FROM THE TOTAL)'),
(18, '2017-03-01', '2017-04-11', 2, 162, 'Wadi_UAE and SA_Android', 28, 333, '166.00', NULL, 1, 2, 1, 'also incent traffic (see CRs per day)', 'Proof sent by email (THEY WILL PAY 135 CONVERSIONS FROM THE TOTAL)'),
(19, '2017-03-01', '2017-04-11', 2, 162, 'Wadi_UAE and SA_Android', 30, 5306, '2643.00', NULL, 1, 2, 1, 'also incent traffic (see CRs per day)', 'Proof sent by email (THEY WILL PAY 135 CONVERSIONS FROM THE TOTAL)'),
(21, '2017-03-01', '2017-04-14', 1, 173, 'EmpireWar:AgeofHero_US_iOS_88945', 52, 26, '60.84', NULL, 1, 2, 1, 'sent by email (screenshot attached)', ''),
(24, '2017-03-01', '2017-04-19', 2, 353, 'HUOSHANSHIPIN2_CN_iOS_23128', 54, 119, '178.50', NULL, 1, 2, 4, 'esperando report. Mandada factura no OK', ''),
(25, '2017-03-01', '2017-04-19', 2, 353, 'HUOSHANSHIPIN2_CN_iOS_23128', 29, 181, '271.50', NULL, 1, 1, 4, 'esperando report. Mandada factura no OK', 'Validado hacia Aff'),
(26, '2017-03-01', '2017-04-19', 2, 353, 'HUOSHANSHIPIN2_CN_iOS_23128', 30, 340, '510.00', NULL, 1, 2, 4, 'esperando report. Mandada factura no OK', ''),
(27, '2017-03-01', '2017-04-19', 2, 353, 'HUOSHANSHIPIN2_CN_iOS_23128', 52, 10, '15.00', NULL, 1, 2, 4, 'esperando report. Mandada factura no OK', ''),
(28, '2017-03-01', '2017-04-19', 2, 24, 'Heydealer_KR_iOS_463', 6, 49, '129.85', NULL, 1, 2, 5, '', ''),
(30, '2017-03-01', '2017-04-19', 1, 24, 'Pokerstars_ES_iOS_510', 52, 103, '309.00', NULL, 1, 2, 1, '', ''),
(31, '2017-03-01', '2017-04-19', 2, 24, 'Namshi_SA_iOS_258', 6, 212, '254.40', NULL, 1, 2, 1, 'facturado en mayo por falta de pruebas', ''),
(32, '2017-03-01', '2017-04-20', 1, 17, 'UpLiveVideo_CN_iOS_75202753', 28, 1491, '1960.30', NULL, 1, 2, 1, ' (scrub por enviar tráfico de la versión mas viaja de la solicitada)', ''),
(33, '2017-03-01', '2017-04-20', 1, 17, 'GameOfWar_DE_and_75063581', 48, 41, '66.09', NULL, 2, 2, 1, '', ''),
(36, '2017-03-01', '2017-04-20', 2, 2, 'JollyChic_SA/KW/AE_iOS_124', 6, 486, '806.96', NULL, 1, 2, 1, '', ''),
(37, '2017-03-01', '2017-04-20', 2, 2, 'JollyChic_SA/AE/KW_and_123', 43, 503, '352.10', NULL, 2, 2, 1, '', ''),
(49, '2017-03-01', '2017-04-18', 1, 153, 'CGTN_US_iOS_8952', 54, 241, '296.60', NULL, 1, 2, 1, 'Report mandado por email', ''),
(50, '2017-03-01', '2017-04-24', 2, 153, 'Gardenscapes_US_iOS_8811', 28, 18, '35.10', NULL, 1, 2, 1, 'NO ACEPTADO- falta report', ''),
(51, '2017-03-01', '2017-04-18', 1, 153, 'CGTN_US_iOS_8952', 11, 10, '14.00', NULL, 1, 2, 1, 'Report sent by email. Aparte incent traffic', ''),
(52, '2017-04-01', '2017-05-16', 1, 24, 'Namshi_SA_iOS_258', 6, 218, '260.40', NULL, 1, 1, 5, 'se rehace la factura, pagan la mitad - no pagar a Digital Pow Wow', ''),
(53, '2017-04-01', '2017-05-15', 1, 153, 'Astromusme_JP_Android_8857', 52, 110, '348.80', NULL, 1, 1, 1, 'mandadas pruebas por email', ''),
(54, '2017-04-01', '2017-05-15', 1, 153, 'Astromusme_JP_Android_8857', 1, 14, '44.80', NULL, 1, 1, 1, 'mandadas pruebas por email', ''),
(55, '2017-04-01', '2017-05-15', 1, 153, 'Astromusme_JP_Android_8857', 37, 10, '28.80', NULL, 1, 1, 1, 'mandadas pruebas por email', ''),
(56, '2017-06-01', '2017-06-05', 1, 14, 'CPA - Perú -Astros- Entel', 60, 94, '98.20', NULL, 1, 1, 4, '', ''),
(57, '2017-07-01', '2017-07-19', 1, 83, 'France - SFR - Pacman', 49, 638, '9555.00', '7177.50', 1, 2, 4, '', ''),
(58, '2017-07-01', '2017-07-19', 1, 83, 'France - Gameasy - SFR', 60, 101, '1515.00', '1091.25', 1, 2, 4, '', ''),
(59, '2017-07-27', '2017-07-27', 1, 83, 'France - Gameasy - SFR', 78, 198, '2970.00', '2227.50', 1, 1, 4, '', ''),
(60, '2017-08-01', '2017-09-01', 1, 368, '', 132, 0, '84.26', '0.00', 2, 2, 3, '', ''),
(61, '2017-09-01', '2017-10-04', 1, 214, 'Offer_Simpsons_SP', 128, 123, '12300.00', '323.00', 2, 2, 1, '', ''),
(62, '2017-09-01', '2017-09-22', 1, 423, 'Offer_game_battle_123', 45, 32323, '7654.00', '233.00', 4, 1, 5, '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status`
--

CREATE TABLE `status` (
  `id` int(5) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `code` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `status`
--

INSERT INTO `status` (`id`, `name`, `code`) VALUES
(1, '', 'white'),
(2, 'Validado', 'blue'),
(4, 'Validado NOK', 'red'),
(7, 'Facturado', 'green'),
(9, 'Provisionado', 'yellow'),
(10, 'Facturado NOK', 'red');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_adv`
--

CREATE TABLE `status_adv` (
  `id` int(3) NOT NULL,
  `value` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `status_adv`
--

INSERT INTO `status_adv` (`id`, `value`) VALUES
(1, 'ok'),
(2, 'negociacion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_aff`
--

CREATE TABLE `status_aff` (
  `id` int(3) NOT NULL,
  `value` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `status_aff`
--

INSERT INTO `status_aff` (`id`, `value`) VALUES
(1, 'ok'),
(2, 'negociacion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `traffic_team`
--

CREATE TABLE `traffic_team` (
  `id` int(2) NOT NULL,
  `value` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `traffic_team`
--

INSERT INTO `traffic_team` (`id`, `value`) VALUES
(1, 'Affiliation'),
(2, 'Media Buying'),
(3, 'Traffic Team'),
(4, 'Diego Duarte'),
(5, 'Ruben Macías'),
(6, 'Juliana Borges'),
(7, 'Irene Camarena'),
(8, 'Claudia Plazaola');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `email` tinytext NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(30) NOT NULL,
  `created` datetime NOT NULL,
  `attempt` varchar(15) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `name`, `created`, `attempt`) VALUES
(2, 'billy', 'sergio.suarez@advancemobile.net', '$2y$10$Z6YpYRBai1N1E.HJFwijkuF.tSWnrwUwF0BBB9pan4a.q1LQ9EyC.', 'sergio suarez', '2017-03-06 18:50:48', '0'),
(3, 'Ramon', 'ramon.granero@advancemobile.net', '$2y$10$3CL4ODxb7zpriNQCfMHJG.LZw6vVBSr0WRNbEogceyZYMDv0hYNJ6', 'Ramón', '2017-03-07 13:23:00', '0'),
(15, 'javiulacia', 'javi.deulacia@fasttracknet.es', '$2y$10$K1CVvK5K5pqKrDvAXZCFiO0MYRgUT8VnmgBIGsrfcYcOc38H6qcxC', 'Javi de Ulacia', '2017-05-10 08:42:22', '0'),
(21, 'agordo', 'abgordo@netsales.es', '$2y$10$edNt50VyAxHx6N2Uur9I0uc8zW.wGsllUb83w04oCTGcLThXroQOG', 'Ana Belen', '2017-10-05 11:00:17', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_devices`
--

CREATE TABLE `user_devices` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL COMMENT 'The user''s ID',
  `token` varchar(15) NOT NULL COMMENT 'A unique token for the user''s device',
  `last_access` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_role`
--

CREATE TABLE `user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user_role`
--

INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
(2, 5),
(3, 5),
(15, 5),
(21, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_tokens`
--

CREATE TABLE `user_tokens` (
  `token` varchar(40) NOT NULL COMMENT 'The generated unique token',
  `uid` int(11) NOT NULL COMMENT 'The User ID',
  `requested` varchar(20) NOT NULL COMMENT 'The date when token was created'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accountmanager`
--
ALTER TABLE `accountmanager`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `advertisers`
--
ALTER TABLE `advertisers`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `affiliates`
--
ALTER TABLE `affiliates`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `campaigns_month_close`
--
ALTER TABLE `campaigns_month_close`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `costes`
--
ALTER TABLE `costes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `doc_number`
--
ALTER TABLE `doc_number`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `incomes`
--
ALTER TABLE `incomes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`permission_id`);

--
-- Indices de la tabla `reason_scrubs`
--
ALTER TABLE `reason_scrubs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indices de la tabla `role_permission`
--
ALTER TABLE `role_permission`
  ADD KEY `role_id` (`role_id`),
  ADD KEY `permission_id` (`permission_id`);

--
-- Indices de la tabla `scrubs`
--
ALTER TABLE `scrubs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status_adv`
--
ALTER TABLE `status_adv`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status_aff`
--
ALTER TABLE `status_aff`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `traffic_team`
--
ALTER TABLE `traffic_team`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_devices`
--
ALTER TABLE `user_devices`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_role`
--
ALTER TABLE `user_role`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `accountmanager`
--
ALTER TABLE `accountmanager`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `advertisers`
--
ALTER TABLE `advertisers`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=533;
--
-- AUTO_INCREMENT de la tabla `affiliates`
--
ALTER TABLE `affiliates`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;
--
-- AUTO_INCREMENT de la tabla `campaigns_month_close`
--
ALTER TABLE `campaigns_month_close`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `costes`
--
ALTER TABLE `costes`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=323;
--
-- AUTO_INCREMENT de la tabla `doc_number`
--
ALTER TABLE `doc_number`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `incomes`
--
ALTER TABLE `incomes`
  MODIFY `id` int(4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1114;
--
-- AUTO_INCREMENT de la tabla `permission`
--
ALTER TABLE `permission`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `reason_scrubs`
--
ALTER TABLE `reason_scrubs`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `scrubs`
--
ALTER TABLE `scrubs`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT de la tabla `status`
--
ALTER TABLE `status`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `status_adv`
--
ALTER TABLE `status_adv`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `status_aff`
--
ALTER TABLE `status_aff`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `traffic_team`
--
ALTER TABLE `traffic_team`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `user_devices`
--
ALTER TABLE `user_devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `role_permission`
--
ALTER TABLE `role_permission`
  ADD CONSTRAINT `role_permission_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`),
  ADD CONSTRAINT `role_permission_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`permission_id`);

--
-- Filtros para la tabla `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`);
