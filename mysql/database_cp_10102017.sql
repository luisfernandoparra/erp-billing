-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:8889
-- Tiempo de generación: 10-10-2017 a las 10:28:28
-- Versión del servidor: 5.6.35
-- Versión de PHP: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `netsaleserp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accountmanager`
--

CREATE TABLE `accountmanager` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `accountmanager`
--

INSERT INTO `accountmanager` (`id`, `nombre`) VALUES
(2, 'Kamila Krajnik'),
(3, 'Camilla Rolando');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `advertisers`
--

CREATE TABLE `advertisers` (
  `id` int(2) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `social` varchar(200) DEFAULT NULL,
  `id_am` varchar(30) NOT NULL DEFAULT 'Camilla',
  `money` varchar(2) NOT NULL DEFAULT '$',
  `cod_client` int(12) DEFAULT NULL,
  `nation` varchar(50) DEFAULT NULL,
  `contact_mail` varchar(200) DEFAULT NULL,
  `CifDni` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `notes` varchar(1000) DEFAULT 'Notes...',
  `address` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `advertisers`
--

INSERT INTO `advertisers` (`id`, `nombre`, `social`, `id_am`, `money`, `cod_client`, `nation`, `contact_mail`, `CifDni`, `phone`, `state`, `notes`, `address`) VALUES
(1, 'Neomobile', 'NEOMOBILE S.P.A.', '3', '$', 430000003, 'IT', 'magdalena.pavia@neomobile.com', '', '', '', '', ''),
(2, 'Admitad', 'ADMITAD GMBH', '2', '$', 430000182, '', 'i.chigrin@admitad.com', '267669275', '', '', '', 'Address…'),
(3, 'Applift', 'APPLIFT GMBH', '3', '$', 430000091, 'GERMANY', 'cg@applift.com', '284616493', '', 'BERLIN', '', 'Address…'),
(4, 'Digital Galaxy', 'DIGITAL GALAXY S.R.L.', '2', '€', 430000086, 'ITALY', 'silvia.mattana@imp-srl.com', '10834050014', '', 'TORINO', '', 'Address…'),
(5, 'Naranya CPA ', 'OMNIMEDIA LIVE S.A. DE C.V (NARANYA MX)', '3', '$', 430000154, '', 'karina.castillo@naranya.com', ' OLI030318FKA', '', 'SAN PEDRO GARZA GARCIA', '', 'Address…'),
(6, 'Wadogo', NULL, '3', '$', NULL, NULL, NULL, '', '', '', '', ''),
(8, 'Click2commission CPI', 'MARKOPROS MEDIA LLP', '2', '$', 430000244, 'IN', 'accounts@click2commission.com', '', '', '', '', ''),
(9, 'Instal (OLD)', 'INSTAL.COM (TBD, SRL)', '3', '$', 430000069, 'ITALY', 'finance@instal.com', '05866260481', '', 'FLORENCE', '', 'Address…'),
(10, 'Avazu CPA', 'AVAZU INC.', '3', '$', 430000063, 'BN', 'sis.timberg@avazu.net', '', '', '', '', ''),
(11, 'Digital Virgo', 'DIGITAL VIRGO ESPAÑA SA', '2', '$', 430000084, 'ES', 'smonteiro@digitalvirgo.com', '', '', '', '', ''),
(12, 'Go Wide', NULL, '3', '$', NULL, NULL, NULL, '', '', '', '', ''),
(13, 'Ximobi ', 'XIMOBI INTERNET, SL', '3', '$', 430000125, 'SPAIN', 'antonio.alvarez@ximobi.es', 'B86518883', '', 'MADRID', '', 'Address…'),
(14, '3DM', '3D MOBILE 3DM SAS', '3', '$', 430000181, 'COLOMBIA', 'miguelo@3dm.com.co;equiasua@bmdigital.co', '900307626-4', '', 'BOGOTA', '', 'Address…'),
(15, 'Surikate ', 'MOZOO SK UK LIMITED ', '2', '$', 430000028, 'UNITED KINGDOM', 'ana@surikate.com;adeola@mozoo.com', '144825114', '', 'LONDON', '', 'Address…'),
(16, 'MobVista CPI', 'MOBVISTA INTERNATIONAL TECHNOLOGY LIMITE', '3', '$', 430000034, '', 'mars@mobvista.com;masha.chen@mobvista.com ', '6418718100012', '', 'KOWLOON', '', 'Address…'),
(17, 'Adstract', 'COOK MEDIA LTD. (ADSTRACT)', '3', '$', 430000301, '', 'Invoices@adstract.com', '86167', '', '', '', 'Address…'),
(18, 'Mobipuff', 'ADZ2LEAD MEDIA PRIVATE LIMITED', '3', '$', 430000072, 'IN', 'Umrao.singh@mobipuff.com', '', '', '', '', ''),
(19, 'Appcoach', 'APPCOACHS CO., LTD.', '3', '$', 430000046, '', 'finance@appcoachs.com', '9689008', '', '', '', 'Address…'),
(20, 'Image Perfect ', 'M L IMAGE PERFECT CONCEPTS PVT LTD', '3', '$', 430000083, 'INDIA', 'shailesh.sinha@imageperfect.in', 'AAGCM3508C', '', 'DELHI ', '', 'Address…'),
(21, 'InlabDigital ', 'INTERNET LABORATORY DIGITAL, SL (DOMOBIA', '3', '$', 430000116, 'SPAIN', 'alberto@inlabdigital.com', 'B87433579', '', 'MADRID', '', 'Address…'),
(22, 'Appalgo', 'APPALGO L.T.D', '3', '$', 430000212, '', 'Ela@appalgo.com', '514950047', '', '', '', 'Address…'),
(23, 'Moblin', 'DATOMO LTD (MOBLIN)', '3', '$', 430000207, '', 'Finance@Moblin.com;veredt@moblin.com;adops@moblin.com', '513765115', '', '', '', 'Address…'),
(24, 'AddValue Media CPI', 'ADD VALUE MEDIA S.L.', '2', '$', 430000079, 'ES', 'services@addvaluemedia.com', '', '', '', '', ''),
(25, 'Layroute ', 'LAYROUTE TECHNOLOGIES PRIVATE LTD', '3', '$', 430000209, '', 'rishab.kaushik@layroute.com', 'AACCL7646QSD0', '', '', '', 'Address…'),
(26, 'AdsFast', 'ADSFAST LLC', '3', '$', 430000213, '', ' finance@adsfast.com', '813480606', '', '', 'Pertenece a la UE pero no está registrado en VIES.', 'Address…'),
(27, 'Headway CPA', 'Gigales', '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(28, 'MiniMob', 'MINIMOB PTE LTD', '3', '$', 430000247, '', 'd.syros@minimob.com', '201113289D', '', '', '', 'Address…'),
(29, 'Naranya CPI ', 'NARANYA COLOMBIA, LTDA ', '3', '$', 430000153, '', 'karina.castillo@naranya.com  ', '900.043.114-0', '', '', '', 'Address…'),
(30, 'SekoMedia', 'SEKOMEDIA B.V.', '3', '$', NULL, NULL, NULL, '', '', '', '', ''),
(31, 'Rekket CPA ', 'REKKET NETWORK LTD', '2', '$', 430000169, '', 'finance@rekket.com', '1710335', '', '', '', 'Address…'),
(32, 'MappStreet ', 'NETERNALITY LTD. (MAPPSTREET)', '3', '$', 430000192, '', 'david.budd@neternality.com', '59024', '', '', '', 'Address…'),
(33, 'Sabia Media ', 'SABIA MEDIA INC', '3', '$', 430000197, 'FLORIDA', 'moshe@sabiamedia.com', '47-2771345', '', 'MIAMI', '', 'Address…'),
(34, 'Stroer', NULL, '3', '$', NULL, NULL, NULL, '', '', '', '', ''),
(35, 'Blackfox', 'MCOM INC. ( BLACKFOX)', '3', '$', 430000243, '', 'Amla@Mcom.io', '461806216', '', '', '', 'Address…'),
(36, 'SmartLead ', 'LOPANZIA INVESTMENTS LTD  (SMARTLEAD)', '3', '$', 430000186, '', 'billing@smartlead.mobi', '1547783', '', '', '', 'Address…'),
(37, 'ClicksMob', 'CLICKSMOB, INC.', '2', '$', 430000047, '', 'finance@clicksmob.com', '461883113', '', '', '', 'Address…'),
(38, 'InlabDigital CPA', NULL, '3', '$', NULL, NULL, NULL, '', '', '', '', ''),
(39, 'Eptonic', 'EPTONIC LTD', '3', '$', 430000137, '', 'Hannah@eptonic.com', '1858889', '', '', '', 'Address…'),
(40, 'Avazu', 'AVAZU INC.', '3', '$', 430000063, '', 'sis.timberg@avazu.net', 'NBD/10815', '', 'BRUNEI', '', 'Address…'),
(41, 'Digital Turbine', 'DIGITAL TURBINE MEDIA INC', '3', '$', 430000195, '', 'polina.p.l@digitalturbine.com', '262346340', '', '', '', 'Address…'),
(42, 'Marvel Media', 'MARVEL MEDIA SDN BHD', '2', '$', 430000158, 'Lepas', 'nicole@marvelmedia.com;jnchuah@marvelmedia.com', '936422-V', '', 'BAYAN', 'minimo facturación 1000$', 'Address…'),
(43, 'Collectcent', 'COLLECTCENT DIGITAL MEDIA PVT LTD', '2', '$', 430000018, 'HARYANA', 'invoice@collectcent.com;sanjeev@collectcent.com', 'AAFCCC5047D', '', 'GURGAON', '', 'Address…'),
(44, 'OnMobile CPA', NULL, '3', '€', NULL, NULL, NULL, '', '', '', '', ''),
(46, 'Mobisummer ', 'WHATECH MOBILE CO., LIMITED (MOBISUMMER)', '3', '$', 430000168, 'CHINA', 'hugh@mobisummer.com', '2143039', '', 'WAN CHAI', '', 'Address…'),
(47, 'Traffikey ', 'TRAFFIKEY (SVAROG LTD)', '3', '$', 430000167, '', 'moshe@traffikey.com', '514656453', '', '', '', 'Address…'),
(48, 'KIMIA CPA', 'KIMIA', '3', '$', 430000054, 'ES', 'ntripero@kimiasol.com', '', '', '', '', ''),
(49, 'Adooya Crossrider CPI', NULL, '3', '$', NULL, NULL, NULL, '', '', '', '', ''),
(50, 'Art of Click', 'ART OF CLICK PTE. LTD.', '3', '$', 430000141, '', 'billing@artofclick.com', '2011138990', '', '', '', 'Address…'),
(51, 'Matomy Media Group ', 'MATOMY MEDIA GROUP LTD.', '3', '$', 430000148, 'ISRAEL', 'sheila.b@matomy.com', '513795427', '', 'TEL AVIV', '', 'Address…'),
(52, 'BravAds', 'BRAVADS MEDIA SRL', '3', '$', 430000143, '', 'finance@bravads.com', '217741050010', '', '', '', 'Address…'),
(53, 'Affle', 'AFFLE INDIA PVT. LTD.', '3', '$', 430000009, 'Haryana', 'invoicing@affle.com;affiliate@affle.com', 'MUMT04558B', '', '', '', 'Address…'),
(54, 'Rekket', 'REKKET NETWORK LTD', '2', '$', 430000169, 'VG', 'finance@rekket.com', '', '', '', '', ''),
(55, 'Mobligo', 'AMONETIZE LTD ( MOBLIGO)', '3', '$', 430000049, 'ISRAEL', 'aviv.p@amonetize.com', '514751254', '', 'ISRAEL', '', 'Address…'),
(56, 'Spyke Media', 'SPYKE MEDIA GMBH', '3', '$', 430000102, 'GERMANY', 'm.vassalos@spykemedia.com', '815407710', '', 'ALEMANIA', '', 'Address…'),
(57, 'Adxperience', 'SAS ADXPERIENCE', '2', '$', 430000203, 'Levallois Perret', 'julie@adxperience.com;billing@adxperience.com', '31537890873', '', '', '', 'Address…'),
(58, 'Stingrad', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(59, 'Grand Oro LP ', 'GRAND ORO LP', '3', '$', 430000226, '', 'victoria@grandorolp.com;vladimir@grandorolp.com', 'SL028677', '', '', '', 'Address…'),
(60, 'Click2Commission CPA', 'MARKOPROS MEDIA LLP (CLICK2COMMISSION)', '2', '$', 430000244, '', 'accounts@click2commission.com', 'AAXFM0535H', '', '', '', 'Address…'),
(61, 'WinClap ', 'WINCLAP S.A', '3', '$', 430000114, '', 'Mateo.delgado@winclap.com', '30714537829', '', '', '', 'Address…'),
(62, 'Televida ', 'VOICEWEB AMERICAS S.A. DE C.V.', '3', '$', 430000246, '', 'aherrera@televida.biz', '0501900819588', '', '', '', 'Address…'),
(64, 'Linkadia', 'LINKADIA MEDIA S.L', '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(65, 'Cliq Digital', 'ARTIQ MOBILE BV', '3', '$', 430000043, '', 'invoices@cliqdigital.com', '812906810B01', '', '', '', 'Address…'),
(66, 'Headway Digital CPA', 'Gigales', '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(67, 'Mobco Cake', NULL, '3', '$', NULL, NULL, NULL, '', '', '', '', ''),
(68, 'Offerseven', 'OFFERSEVEN LLC', '3', '$', 430000045, '', 'anna@offerseven.com', '471634716', '', '', '', 'Address…'),
(69, 'VIP Response CPA', NULL, '3', '$', NULL, NULL, NULL, '', '', '', '', ''),
(70, 'Telecoming', 'TELECOMING, SA', '3', '$', 430000142, 'SPAIN', 'rvillegas@telecoming.com', 'A64067549', '', 'MADRID', '', 'Address…'),
(72, 'UC Union ', 'UCWEB INC.  (UC UNION)', '3', '$', 430000058, '', 'linhong.zlh@alibaba-inc.com', 'PTC047717', '', '', 'Es de la CEE pero no está en el Vies', 'Address…'),
(73, 'Epom Market', 'EPOM LTD', '3', '$', 430000163, '', 's.lazaryan@epom.com', '16229', '', '', '', 'Address…'),
(74, 'Rocket10', 'STONE BIRD INC.  (ROCKET10) ', '3', '$', 430000189, '', 'af@rocket10.com', '164438', '', 'MAHE', '', 'Address…'),
(75, 'Mobrider', NULL, '3', '$', NULL, NULL, NULL, '', '', '', '', ''),
(76, 'Aragon Advertising', NULL, 'Julia Carraro', '$', NULL, NULL, NULL, '', '', '', '', ''),
(77, 'Adxmi', 'YOUMI TECHNOLOGY (HK) LIMITED (ADXMI)', '3', '$', 430000170, '', 'chenzijie@youmi.net', '2133619', '', '', '', 'Address…'),
(79, 'Mobidea', 'MOBIDEA LIMITED', '3', '$', 430000052, 'CHIPRE', 'nuno.ribeiro@mobidea.com', '99000112J', '', 'NICOSIA', 'Pertenece a la UE pero no está registrado en VIES.', 'Address…'),
(80, 'Gameloft', 'GAMELOFT SE', '3', '€', 430000095, 'FR', 'ines.torres@gameloft.com;monica.mikuski@gameloft.com;PetreCatalin.Ilie@gameloft.com', '', '', '', '', ''),
(81, 'Imagine ADS', NULL, 'Julia Carraro', '$', NULL, NULL, NULL, '', '', '', '', ''),
(82, 'PSMOVILES', NULL, 'Julia Carraro', '$', NULL, NULL, NULL, '', '', '', '', ''),
(83, 'Buongiorno (Taplinks)', 'BUONGIORNO UK LTD', '2', '$', 430000004, 'UNITED KINGDOM', ' invoicing@taplinks.com', '839864272', '', '', 'minimo facturación 200$ - desglosar campañas en cada factura', 'Address…'),
(84, 'Msales', 'MSALES LTD.', '3', '$', 430000021, 'MALTA', 'finance@msales.com', '22481629', '', 'ST. JULIANS', '', 'Address…'),
(85, 'Kimia', 'KIMIA SOLUTIONS S.L', '3', '$', 430000054, 'SPAIN', 'ntripero@kimiasol.com', 'B84790187', '', 'MADRID', 'envían su factura, no enviar', 'Address…'),
(128, 'Ad3traffic ', 'FRANCISCO JAVIER GAYET CANOS (AD3TRAFFIC', '3', '$', 430000216, 'SPAIN', 'aff@ad3traffic.com', '19005594G', '', 'CASTELLON', '', 'Address…'),
(129, 'Adacts', NULL, '3', '$', NULL, NULL, NULL, '', '', '', '', ''),
(130, 'Addictive Ads', 'ADDICTIVE ADS INC.', '3', '$', 430000198, '', 'accounting@addictiveads.com', '820934792RT00', '', '', '', 'Address…'),
(131, 'Adooya Crossrider CPA', 'DEFINITIMEDIA LTD', '3', '$', 430000044, 'ISRAEL', 'ofer.mayer@definitimedia.com', '514770957', '+972722216565', 'TEL AVIV', '', 'Address…'),
(132, 'Adstacks', 'ADSTACKS MEDIA', '3', '$', 430000224, '', 'ritesh@adstacks.in;finance@adstacks.in', 'DELA41722C', '', '', '', 'Address…'),
(133, 'Adwool', 'ADWOOL LTD.', '3', '$', 430000070, 'UNITED KINGDOM', 'affiliates@adwool.com', '1464924305', '', 'LONDON', '', 'Address…'),
(134, 'Affimobiz', 'TORNIKA SAS', '3', '€', 430000061, 'FRANCE', 'accounting@affimobiz.com', '2793980822', '', 'VINCENNES', 'Es de la UE pero no está en el VIES', 'Address…'),
(135, 'Affle CPA', 'AFFLE INDIA PVT. LTD.', '3', '$', 430000009, 'IN', 'invoicing@affle.com;affiliate@affle.com', '', '', '', '', ''),
(136, 'AlliTapp', 'AIRCYB LTD (ALLITAPP)', '3', '$', 430000173, '', 'alme@allitapp.com', '2237777', '', '', '', 'Address…'),
(137, 'App Promoters', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(138, 'Appave', 'SIN CITY INTERACTIVE LLC (APPAVE)', '3', '$', 430000089, 'CALIFORNIA', 'payments@appave.mobi', '473050359', '', 'LAS VEGAS', '', 'Address…'),
(139, 'Basebone', 'BASEBONE LTD', '3', '$', 430000242, 'UNITED KINGDOM', 'zeus.lara@basebone.com;sylwia.stepniak@basebone.com', '198691244', '', 'LONDON', '', 'Address…'),
(140, 'CliqDigital', 'ARTIQ MOBILE BV', '3', '$', 430000043, 'NL', 'invoices@cliqdigital.com', '', '', '', '', ''),
(141, 'CPITraffic (OLD)', 'CPITRAFFIC LIMITED', '3', '$', 430000199, '', 'finance@cpitraffic.com', '430000199', '', '', '', 'Address…'),
(142, 'Crowmobi', 'SAX LIMITED ( CROWMOBI )', '3', '$', 430000249, 'CHINA', 'pavel.ryazhskikh@crowmobi.com', '2146141', '', 'HONG KONG', '', 'Address…'),
(143, 'DCyphermedia', 'DCYPHERMEDIA MEDIA B.V.', '3', '$', 430000185, '', 'invoice@dcyphermedia.com', '855954401B01', '', '', '', 'Address…'),
(144, 'DobleVia Latam', 'SOUND S.A', '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(145, 'Doozymob', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(146, 'Glize', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(147, 'Gold360 ', 'TECHFLOW LTDA. (GOLD 360)', '3', '$', 430000248, '', 'finances@gold360partners.com', '2222951700019', '55 51 3508-0010', '', '', 'Address…'),
(148, 'Go-Rilla', 'GO-RILLA DIGITAL ADVERTISING LTD', '2', '$', 430000006, 'ISRAEL', 'finance@go-rilla.mobi', '515016640', '+972-5-8860695', 'GIVATAYIM', '', 'Address…'),
(149, 'IDB Group', 'INTERBAND ENTERPRISES LIMITED (IDB GROUP', '2', '$', 430000217, '', 'ivana.bosnjak@idbgroup.net', '4237232', '', '', '', 'Address…'),
(150, 'JungleTap', 'JUNGLETAP', '2', '$', 430000050, 'DOMINICAN REPUBLIC', 'margo@jungletap.com', 'CD2453/15', '', 'COMMONWEALTH OF DOMINICA', '', 'Address…'),
(151, 'Kwanko FR', '0', '3', '€', 0, '0', '0', '', '', '', '', ''),
(152, 'Laddez ', 'LADDEZ GROUP LTD', '3', '$', 430000106, 'SEYCHELLES', 'aleksey@laddez.com', '176960', '', 'SEYCHELLES', '', 'Address…'),
(153, 'Leadzinmarketing AFFTRACK', 'SNJ TECHNOLOGY PRIVATE LIMITED', '3', '$', 430000100, 'INDIA', 'pooja@leadgenetics.in', 'AAVCS3495H', '', 'INDIA', '', 'Address…'),
(154, 'Linkadia CPA ', 'LINKADIA MEDIA S.L', '3', '$', 430000178, 'SPAIN', 'asainz@linkadia.com', 'B87009304', '', 'MADRID', '', 'Address…'),
(155, 'Maverick CPA ', 'MAVERICK MEDIA SL', '3', '$', 430000130, 'SPAIN', 'caro@maverickmedia.eu', 'B65798688', '', 'BARCELONA', '', 'Address…'),
(156, 'Miri Media', NULL, 'Julia Carraro', '$', NULL, NULL, NULL, '', '', '', '', ''),
(157, 'MiskyAds ', 'MISKY ADS SIA', '3', '$', 430000149, '', 'billing@misky-ads.com', '40103986912', '', '', '', 'Address…'),
(158, 'MobChain ', 'MOBCHAIN INC.', '3', '$', 430000219, '', ' finance@mobchain.net', '155607756', '', '', '', 'Address…'),
(159, 'Mobilogia', 'MOBILOGIA', '3', '$', 430000017, 'SPAIN', 'esaiz@mobilogia.com', 'B19293968', '633 09 65 24', 'GUADALAJARA', '', 'Address…'),
(160, 'Mobipium', 'ADZ2LEAD MEDIA PRIVATE LIMITED', '3', '$', 430000072, '', 'Umrao.singh@mobipuff.com', 'AANCA3873B', '', '', '', 'Address…'),
(161, 'Mobobeat CPI', NULL, 'Julia Carraro', '$', NULL, NULL, NULL, '', '', '', '', ''),
(162, 'Mobquid', 'MOBQUID', '3', '$', 430000208, '', 'sales@mobquid.com', 'ABAFM8474FSD0', '', '', '', 'Address…'),
(163, 'Mobquid CPA', 'MOBQUID', 'Kamila Krajinik', '$', 430000208, 'IN', 'sales@mobquid.com', '', '', '', '', ''),
(164, 'MoBrain (Headway)', 'GIGALES SA', '2', '$', 430000228, 'URUGUAY', 'finanance.europe@headwaydigital.com', '217208190012', '', 'MONTEVIDEO', '', 'Address…'),
(165, 'Mobsai ', 'MOBSAI', '3', '$', 430000183, 'INDIA', 'Vaishali@mobsai.com', 'AKCPR4987KSD0', '', 'PUNE', '', 'Address…'),
(166, 'Nanalab', 'NANA LAB SRL ', '3', '€', 430000215, 'ITALY', 'elena.gigli@nanalab.com', '09996640018', '', 'TORINO', '', 'Address…'),
(167, 'Persona.ly', 'PERSONA.LY', '3', '$', 430000071, '', 'michal@persona.ly', '514071281', '', 'ISRAEL', '', 'Address…'),
(168, 'ReGaming', 'REGAMING LLC', '2', '$', 430000164, '', 'valeria.s@regaming.com', '81-1275085', '', '', '', 'Address…'),
(169, 'Revenuemob', 'MOBILE REPRESENTATION INTERNATI', '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(170, 'Seko Media ', 'SEKOMEDIA B.V.', '3', '$', 430000165, '', 'koen@sekomedia.com', '855704779B01', '', '', '', 'Address…'),
(171, 'Softerik', 'SOFTERIK TECHNOLOGIES SA DE CV', '3', '$', 430000245, '', 'finanzas@softerik.com.mx', 'STE140429TE1', '', 'BENITO JUAREZ', '', 'Address…'),
(172, 'StartApp', 'STARTAPP INC', '3', '$', 430000162, '', 'jacqueline.sb@startapp.com', '39-2077584', '', '', '', 'Address…'),
(173, 'Superads', 'SUPERADS TECHNOLOGY LIMITED', '3', '$', 430000225, '', 'rachel@superads.cn', '2474469', '', '', '', 'Address…'),
(174, 'Tracsion ', 'TRACSION, INC.', '3', '$', 430000147, '', 'yifan@tracsion.com', '46-4731571', '', '', '', 'Address…'),
(175, 'Trooperads', 'TROOPERADS LLC ', '2', '$', 430000231, 'US', 'horacio@trooperads.com;finance@trooperads.com', '', '', '', '', ''),
(176, 'Zenna', 'BASTET HOLDING LTD (aka ZENNA)', '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(177, 'NRS Group', 'Giant Mobile Society', '2', '$', 0, '', 'miguel@giant-mob.com ', '', '', '', '', ''),
(178, 'Appromoters', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(179, 'Crownmobi', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(180, 'Binbit', 'BINBIT CORPORATE', '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(181, 'Hitcell ', 'HITCELL LIMITED', '3', '$', 430000206, '', 'billing@hitcell.com', '2274432', '', '', '', 'Address…'),
(182, 'Hang my ads ', 'HANG MY ADS LDA', '3', '$', 430000115, 'PORTUGAL', 'Billing@hangmyads.com', '513065369', '', 'LISBOA', '', 'Address…'),
(183, 'Uonline Media', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(185, 'Ritm Media Group', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(187, 'ADAttrack ', 'EBS MEDIA LTD (ADATTRACK)', '3', '$', 430000103, 'CHINA', 'brijesh@adattract.com', 'AKUPK5070C', '', 'HONG KONG', '', 'Address…'),
(188, 'Codrim', 'CODRIM INFORMATION TECHNOLOGY LTD', '3', '$', 430000200, 'CHINA', 'guo.zhuge@codrim.net', '6457332800004', '008618820783641', 'CHINA', '', 'Address…'),
(189, 'CPH Media', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(190, 'Inflecto Media', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(191, 'Sharepop', 'SHAREPOP APAC', '3', '$', 430000210, '', 'sangcheol.kim@sharepop.com', '2208890332', '', '', '', 'Address…'),
(194, 'Vamosmedia', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(195, 'Sunjoymedia', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(210, 'Creative Clicks ', 'MOBILE MINDED BV', '3', '$', 430000094, 'NETHERLANDS', 'omer.adato@creativeclicks.com', '822783010B01', '', 'THE NETHERLANDS', '', 'Address…'),
(211, '3ginstall', '3G INSTALL', '3', '$', 430000151, '', 'cfo@3ginstall.com', '309753044', '', '', '', 'Address…'),
(212, 'Actualsales', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(213, 'Ad-Topia', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(214, 'Ad4game', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(215, 'Adhit', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(216, 'Adshoard', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(217, 'AdTaily Group', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(218, 'Adzest', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(219, 'Affiliate Window', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(220, 'Affilinet', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(221, 'Affizzy', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(222, 'App Traffic Media', 'APP TRAFFIC MEDIA S.L', '3', '$', 430000145, 'SPAIN', 'javier.guerrero@apptrafficmedia.com', 'B66759796', '', 'BARCELONA', '', 'Address…'),
(223, 'Chilli Ads', 'NED SERVICIOS PROF. SA DE CV (CHILLIADS)', '3', '$', 430000119, 'MEXICO', 'publishers@chilliads.com', 'NSP1507147D1', '', 'SAN PEDRO GARZA GARCIA NL - NUEVO LEON', '', 'Address…'),
(224, 'Cityads', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(225, 'Edge360 Limited', 'EDGE360 LIMITED', '3', '$', 430000176, 'UNITED KINGDOM', 'finance@edge360.co.uk', '216451625', '', 'LONDON', '', 'Address…'),
(226, 'Eternal Galaxy (Rambomobi)', 'ETERNAL GALAXY LIMITED', '2', '$', 430000059, '', 'yuko@eternalgalaxy-tech.com', '1721100', '', 'SHENZHEN', '', 'Address…'),
(227, 'FactorAds', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(228, 'Flame Digital', 'FLAME DIGITAL LIMITED', '2', '$', 430000074, 'UNITED KINGDOM', 'finance@flame-digital.com', '198265264', '', 'LONDON', '', 'Address…'),
(229, 'FSI Media', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(230, 'Gappex', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(231, 'Ginger Media', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(232, 'Global Wide Media', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(233, 'Infinitly', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(234, 'Linkadia cpi', 'LINKADIA MEDIA S.L', '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(235, 'Market360', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(236, 'Media White ', 'MEDIA WHITE FZ LLC', '3', '$', 430000118, 'ARAB EMIRATES', 'freda@mediawhite.com', '91234', '', 'DUBAI', '', 'Address…'),
(237, 'MediaMob', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(238, 'Mind Magic Media', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(239, 'Mobair', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(240, 'Mobeleader', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(241, 'Mobilda', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(242, 'Mobiletraffic.de', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(243, 'Ninja MObile', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(244, 'PlatformLead', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(245, 'Plaza affiliate', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(246, 'PlusCPI', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(247, 'RainyDay Marketing', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(248, 'Retail9', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(250, 'S4M', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(251, 'SteelFuse', 'STEELFUSE INC.', '3', '$', 430000138, '', 'Matt@steelfuse.com', '47-5481302', '', '', '', 'Address…'),
(252, 'Taiga Mobile', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(253, 'Tradedoubler', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(254, 'Tradetracker', NULL, '2', '$', NULL, NULL, NULL, '', '', '', '', ''),
(255, 'Zucks', 'ZUCKS INC', '3', '$', 430000196, 'JAPAN', 'reward@voyagegroup.info', '81354592061', '', 'TOKYO', '', 'Address…'),
(304, 'Arkeero', 'ROCK INTERNET S.L. (ARKEERO)', '3', '€', 430000042, 'SPAIN', 'dmanea@arkeero.com', 'B86907953', '', 'MADRID', '', 'Address…'),
(305, 'Bangmobile (CPA)', 'MEDIA MARKETING INTERNET (BANGMOBILE)', '2', '€', 430000065, 'SPAIN', 'jorge.imedio@medianetpay.com;sara@medianetpay.com', 'B98620297', '', 'MADRID', '', 'Address…'),
(306, 'BaoAds', 'BAOADS S.C.', '2', '$', 430000194, 'MEXICO', 'cdelrivero@baoads.com', 'BAO150924HR1', '', 'TAMPICO', '', 'Address…'),
(307, 'Bidderplace', 'BIDDERPLACE GMBH', '3', '$', 430000013, 'GERMANY', 'bruno@bidderplace.com', '297751357', '', '', '', 'Address…'),
(308, 'Billymob', 'BILLY PERFORMANCE NERWORK SLU', '2', '€', 430000031, 'SPAIN', 'judit.ortiz@billymob.com', 'B66478223', '', 'BARCELONA', '', 'Address…'),
(310, 'BrokerBabe', 'BIRDVIEW MOBILE AG (BROKERBABE)', '2', '$', 430000038, 'SUIZA', 'kike@brokerbabe.com; sandy.buttigieg@bootecmarketing.com', '116356101', '', 'SUIZA', '', 'Address…'),
(312, 'Clickdealer', NULL, 'Julia Carraro', '$', NULL, NULL, NULL, '', '', '', '', ''),
(313, 'Concepto Movil', NULL, 'Julia Carraro', '$', NULL, NULL, NULL, '', '', '', '', ''),
(314, 'Crazy4Media OLD', 'CRAZY4MEDIA ONLINE LTD', '2', '€', 430000205, 'MALTA', 'juanma@crazy4mediaonline.com', '19837513', '', 'SLIEMA', '', 'Address…'),
(317, 'DobleVia Latam CPA', 'SOUND S.A. (DOBLEVIA )', '3', '$', 430000218, '', 'johana@doblevialatam.com', '800302737', '', '', '', 'Address…'),
(318, 'DSNR Media Group', 'DSNR MEDIA GROUP LTD', '3', '$', 430000041, 'ISRAEL', 'hodaya.bohbot@dsnrmg.com', '514043140', '', 'RA\'ANANA', '* Original Invoices.*Invoice should be sent until the 15th of the following month.*Currency of invoice should match the currency specified in the Insertion Order (IO). * In case currency is EUR/GBP, please use the Representative rate (Last day of media period) to calculate invoice amount. * Invoices', 'Address…'),
(319, 'Freenet Digital (Vene) ', 'MOTILITY GMBH', '3', '€', 430000096, 'GERMANY', 'Pamela.Gonzalez@freenetdigital.com', '272610000', '', 'BERLIN', '', 'Address…'),
(320, 'Game Loft', 'GAMELOFT SE', '3', '$', 430000095, 'FRANCE', 'ines.torres@gameloft.com;monica.mikuski@gameloft.com;PetreCatalin.Ilie@gameloft.com', '96429338130', '', 'PARIS', '', 'Address…'),
(324, 'Leadbit (CPA)', NULL, 'Julia Carraro', '$', NULL, NULL, NULL, '', '', '', '', ''),
(326, 'Linkaleads', NULL, 'Julia Carraro', '$', NULL, NULL, NULL, '', '', '', '', ''),
(327, 'Lomadee (Buscape)', NULL, 'Julia Carraro', '$', NULL, NULL, NULL, '', '', '', '', ''),
(329, 'MCR', 'CELLON LTD. (MCR)', '3', '$', 430000010, '', 'Nir@mcr-m.com', '514235852', '+972-(0)3-60060', 'RAMAT GAN', '', 'Address…'),
(330, 'Mobeetech CPA', 'MOBEETECH B.V.', '3', '$', 430000005, 'NETHERLANDS', 'mark.rademaker@mobeetech.com;barth.groeneveld@mobeetech.com', '821123944B01', '+31(0)765146788', 'BREDA', '', 'Address…'),
(332, 'Mobile4Fun', NULL, 'Julia Carraro', '$', NULL, NULL, NULL, '', '', '', '', ''),
(335, 'Mobobeat ', 'MARKETING & BILLING SOLUTIONS SL (MOBOB.', '3', '$', 430000221, 'SPAIN', 'billing@mobobeat.com', 'B02462158', '', 'ALBACETE', '', 'Address…'),
(338, 'Movile ', 'MOVILE INTERNET MOVEL S/A', '3', '$', 430000286, 'BRAZIL', 'beatriz.mendes@movile.com', '08654191/0001', '+551121630600', 'CAMPINAS', '', 'Address…'),
(342, 'Pmovil ', 'PMOVIL PERU S.A.C.', '3', '$', 430000152, '', 'antonio.vittori@pmovil.com', '20392629913', '', '', '', 'Address…'),
(343, 'SGM ', 'SGM MEDIA GMBH', '3', '$', 430000032, 'GERMANY', 'info@sexgoesmobile.com', '244489818', '', 'BREMEN', '', 'Address…'),
(344, 'Smartlead CPA', NULL, '3', '$', NULL, NULL, NULL, '', '', '', '', ''),
(345, 'Spiroox', 'SPIROOX MEDIA, S.L.', '2', '$', 430000078, 'SPAIN', 'daniel.gonzalez@spiroox.com', 'B85503662', '', 'MADRID', '', 'Address…'),
(346, 'Toro Advertising', NULL, 'Julia Carraro', '$', NULL, NULL, NULL, '', '', '', '', ''),
(347, 'Upstream ', 'UPSTREAM BRASIL PROMOCOES COMERCIAIS LTD', '3', '$', 430000134, 'BRAZIL', 'rodrigo.sciammarella@upstreamsystems.com', '10262881', '', 'RIO DE JANEIRO', '', 'Address…'),
(348, 'Vittalia', 'VITTALIA INTERNET S.L', '3', '$', 430000085, 'SPAIN', 'Finance@vittalia.com', 'B86217031', '', 'MADRID', '', 'Address…'),
(349, 'Webclicks CPA', NULL, 'Julia Carraro', '$', NULL, NULL, NULL, '', '', '', '', ''),
(350, 'WeweMedia CPA', NULL, 'Julia Carraro', '$', NULL, NULL, NULL, '', '', '', '', ''),
(351, 'Appsilon', 'APPSILON CO. LTD', '3', '$', 430000088, 'SOUTH KOREA', 'partner@appsilon.kr', '1870700024', '', 'COREA DEL SUR', '', 'Address…'),
(352, 'MiriMedia', NULL, '3', '$', NULL, NULL, NULL, '', '', '', '', ''),
(353, 'ChickenAds', 'CHICKENADS CO. LTD.', '3', '$', 430000191, 'CHINA', 'finace@chickenads.com', '223701274', '', 'HONG KONG', '', 'Address…'),
(354, 'Geenapp', 'GEENAPP INTERNET SL', '3', '$', 430000087, 'SPAIN', 'we@geenappmail.com;estefania@geenapp.com', 'B66029323', '', 'BARCELONA', '', 'Address…'),
(355, 'JustMob', 'FAREWIN S.A', '3', '$', 430000222, '', 'finance@justmob.mobi;gborras@justmob.mobi', '21748460001', '', '', '', 'Address…'),
(356, 'Appnostiq', '', '2', '', NULL, NULL, NULL, '', '', '', '', ''),
(357, 'Go Wide (Comboapp)', 'COMBOAPP, INC. D.B.A. GOWIDE', '3', '$', 430000105, 'UNITED STATES', 'dnorov@gowide.com;akhasin@gowide.com', '464309981', '', 'EEUU', '', 'Address…'),
(358, 'Songo', 'SONGO MEDIA LTD ', '3', '$', 430000296, 'ISRAEL', 'tal.shifman@songo.com', '514293620', '+972-3-6209614', 'TEL AVIV', '', 'Address…'),
(359, 'Gombio', '', '2', '', NULL, NULL, NULL, '', '', '', '', ''),
(360, 'LayRoute CPA', '', '3', '', NULL, NULL, NULL, '', '', '', '', ''),
(361, 'Publited', 'Afovel S.A.', '3', '', NULL, NULL, NULL, '', '', '', '', ''),
(362, 'Mobiklix', '', '2', '', NULL, NULL, NULL, '', '', '', '', ''),
(363, 'AdsMatic', 'Ontario INC.', '3', '$', NULL, NULL, NULL, '', '', '', '', ''),
(364, 'S2S softsys', 'S2S SOFTSYS PVT LTD', '3', '$', NULL, NULL, NULL, '', '', '', '', ''),
(365, 'Sparta Mobile', 'SPARTA MOBILE LIMITED', '3', '$', 430000161, '', 'dmitriy@spartamobile.com', '66185377', '', '', '', 'Address…'),
(366, 'AffiliaXe', '', '3', '$', NULL, NULL, NULL, '', '', '', '', ''),
(367, 'Logan Media', 'LGN ADVERTISING GROUP SA DE CV', '2', '$', 430000232, '', 'invoices@loganmedia.mobi', 'LAG120730DGA', '', '', '', 'Address…'),
(368, 'Terra', 'FORWARD MEDIA COMMUNICATIONS SA DE CV', '3', '$', 430000230, 'MEXICO', 'grissel.Mayorga@corp.terra.com', '1412123B6', '', 'CUAJIMALPA DE MORELOS', '', 'Address…'),
(369, 'IdVert Group', '', '3', '', NULL, NULL, NULL, '', '', '', '', ''),
(370, 'Jetmobo', '', '3', '', NULL, NULL, NULL, '', '', '', '', ''),
(371, 'Mobilogia CPI', 'MOBILOGIA', '3', '€', 430000017, 'ES', 'esaiz@mobilogia.com', '', '', '', '', ''),
(372, 'Adzmedia', '', '3', '$', NULL, NULL, NULL, '', '', '', '', ''),
(373, 'BitterStrawberry', '', '3', '€', NULL, NULL, NULL, '', '', '', '', ''),
(374, 'AppAmplify', '', '3', '', NULL, NULL, NULL, '', '', '', '', ''),
(375, 'PlayWing', '', '3', '', NULL, NULL, NULL, '', '', '', '', ''),
(376, 'On Mobile ', 'ONMOBILE GLOBAL ESPAÑA, SL', '3', '$', 430000241, 'SPAIN', 'jose.pradavales@onmobile.com;nuria.campa@onmobile.com', 'B86498110', '', 'MADRID', '', 'Address…'),
(377, 'Appaniac', '', '3', '', NULL, NULL, NULL, '', '', '', '', ''),
(379, 'Giuliana Flores ', 'GIULIANA COMERCIO DE FLORES', '3', '$', 430000303, 'BRAZIL', '', '6738971800019', '', 'SAO PAULO', '', 'Address…'),
(380, 'MotorMobile', 'MOTORMOBILE LLC ', '2', '$', 430000234, '', 'eugene.bagatsky@motormobile.com;billing@motormobile.com ', '814383796', '380638483558', '', '', 'Address…'),
(381, 'Mobco', 'MOBCO MEDIA', '2', '$', 430000039, 'ISRAEL', 'billing@mobcomedia.com', '312475114', '', '', '', 'Address…'),
(382, 'Tekka (MX) ', 'TEKKA  DIGITAL SA', '3', '$', 430000237, '', 'amministrazione@tekkadigital.com  ', '303540263', '', '', '', 'Address…'),
(383, 'Plusmobile ', 'PLUSMOBILE COMMUNICATIONS SA', '3', '$', 430000304, 'COLOMBIA', 'stephanie.alderete@plusmobile.com', '9000411861', '', 'BOGOTA', '', 'Address…'),
(384, 'EvoLeads', 'EVOMARKETPLACE, INC', '3', '$', 430000171, '', 'kate@evoleads.com', '4047RT0001', '', '', '', 'Address…'),
(385, 'Mottnow', 'TAGDATIC CORP', '3', '$', 430000305, 'FLORIDA', 'contact@mottnow.com', '371761697', '', 'MIAMI', '', 'Address…'),
(386, 'Netsales', 'Netsales', '3', '€', NULL, NULL, NULL, '', '', '', '', ''),
(387, 'Mundomedia', 'MUNDOmedia Ltd.', '3', '$', NULL, NULL, NULL, '', '', '', '', ''),
(388, 'Braverymob', 'BRAVEYRMOB LIMITED', '3', '$', 430000252, 'CHINA', 'sheila@braverymob.com', '390692200883', '', 'HONG KONG', '', 'Address…'),
(389, 'JustMob', 'FAREWIN S.A', '3', '$', 430000222, 'UY', 'finance@justmob.mobi;gborras@justmob.mobi', '', '', '', '', ''),
(391, 'Tekka (CO/RU/IN) ', 'TEKKA SPA ', '3', '€', 430000236, 'ITALY', 's.capaldi@tekka.it', '09479150014', ' +39 344 127607', 'TORINO', '', 'Address…'),
(392, 'Tekka (CO/RU/IN)', '', '3', '€', NULL, NULL, NULL, '', '', '', '', ''),
(393, 'Microcb', '', '3', '', NULL, NULL, NULL, '', '', '', '', ''),
(394, 'RevLinker', '', '3', '€', NULL, NULL, NULL, '', '', '', '', ''),
(396, 'Coco Mobile', '', '2', '', NULL, NULL, NULL, '', '', '', '', ''),
(397, 'Aditmedia', '', '3', '', NULL, NULL, NULL, '', '', '', '', ''),
(398, 'Wewemedia (Affle)', '', '3', '', NULL, NULL, NULL, '', '', '', '', ''),
(399, 'Neomobile APAC ', 'NEOMOBILE S.P.A.', '3', '$', 430000003, 'ITALY', 'magdalena.pavia@neomobile.com', '02927500542', '', 'ROMA', '', 'Address…'),
(400, 'Advivify', '', '3', '', NULL, NULL, NULL, '', '', '', '', ''),
(401, 'Digital Virgo PT', 'DIGITAL ONLINE DEVELOPMENT  S.L.U', '3', '$', 430000239, 'SPAIN', 'vrojo@digitalvirgo.com.es', 'B85753960', '', 'MADRID', '', 'Address…'),
(402, 'Digital Virgo ES', 'DIGITAL VIRGO ESPAÑA, S.A.', '2', '$', 430000084, 'SPAIN', 'smonteiro@digitalvirgo.com', 'A80060924', '', 'MADRID', '', 'Address…'),
(403, 'Feraz Media', 'FERAZ MEDIA ENTERTAIMENT S DE RL DE CV ', '2', '$', 430000250, '', 'akaram@ferazme.com;facturacion@ferazme.com;agalimberti@ferazme.com', 'FME101022EZ8', '', '', '', 'Address…'),
(404, 'Buongiorno BR', 'Buongiorno BR', '2', '$', 0, 'BR', 'paula.caamano@docomodigital.com', '', '', '', '', ''),
(405, 'Coco Mobile PA', 'Coco Mobile PA', '2', '$', 0, 'CO', 'liney.mogollon@cocomobile.co', '', '', '', '', ''),
(406, 'Coco Mobile EC', 'Coco Mobile EC', '2', '$', 0, 'CO', 'liney.mogollon@cocomobile.co', '', '', '', '', ''),
(407, 'Coco Mobile PE', 'Coco Mobile PE', '2', '$', 0, 'CO', 'liney.mogollon@cocomobile.co', '', '', '', '', ''),
(408, 'Kwanko ES', '0', '3', '€', 0, 'FR', 'luis.rubiao@kwanko.com', '', '', '', '', ''),
(409, 'Affil4you', '', '3', '', NULL, NULL, NULL, '', '', '', '', ''),
(410, 'Media Interactiva', '', '3', '', NULL, NULL, NULL, '', '', '', '', ''),
(411, 'Vacastudios', '', '3', '', NULL, NULL, NULL, '', '', '', '', ''),
(412, 'Awem', '', '3', '', NULL, NULL, NULL, '', '', '', '', ''),
(413, 'Webpilots', 'Webpilots España, S.L.', '3', '€', 0, 'ES', 'finanzas@webpilots.com', '', '', '', '', ''),
(414, 'Appsflyer', '', '3', '', NULL, NULL, NULL, '', '', '', '', ''),
(415, 'Collectcent Smartlink', '', '3', '', NULL, NULL, NULL, '', '', '', '', ''),
(416, 'Mobusi', 'MOBUSI MOBILE ADVERTISING S.L.', '3', '$', 430000240, 'SPAIN', 'sonia.sanjuan@mobusi.com', 'B86439577', '', 'MADRID', 'envían su factura, no enviar', 'Address…'),
(417, 'Crazy4Media', 'CRAZY4MEDIA MOBILE ADVERTISING & BILLING', '3', '$', 430000302, 'SPAIN', 'finance@crazy4mediamobile.com;susanaramirez@crazy4mediamobile.com', 'B90125741', '', 'SEVILLA', '', 'Address…'),
(418, 'ActionPay', 'ACTIONPAY ADVERTISING NETWORK LTD', '3', '$', 430000179, 'IRELAND', 'jessica.atallah@actionpay.com.br', '3356937FH', '', 'DUBLIN', '', 'Address…'),
(419, 'ADATHA', 'ADATHA MEDIA PVT LTD', '3', '$', 430000175, '', 'ratheesh@adatha.com', '102907', '', '', '', 'Address…'),
(420, 'AdCrimson', 'ADCRIMSON, LLC', '3', '$', 430000157, '', 'billing@adcrimson.com', '47-4276825', '', '', '', 'Address…'),
(421, 'Addict Mobile', 'ADDICT MOBILE', '3', '$', 430000235, '', 'finance@addict-mobile.com', '75810256347', '33613850503', '', '', 'Address…'),
(422, 'AddValue Media CPA', 'ADD VALUE MEDIA S.L.', '2', '$', 430000079, 'SPAIN', 'Services@addvaluemedia.com ', 'B86729126', '', 'MADRID', '', 'Address…'),
(423, 'Adooya (Definitimedia) ', 'DEFINITIMEDIA LTD', '3', '$', 430000131, 'ISRAEL', 'finance@adooya.com', '514770957', '+972722216565', 'TEL AVIV', '', 'Address…'),
(424, 'Appthis', 'APPTHIS LLC ', '3', '$', 430000098, 'DENVER', 'finance@appthis.com', '465155543', '', 'DENVER', '', 'Address…'),
(425, 'Baba Network', 'BABA NETWORK', '3', '$', 430000117, 'ISRAEL', 'elad.n@babnetwork.net', '514953926', '', 'ISRAEL 5252269', '', 'Address…'),
(426, 'Basebone (OLD)', 'BASEBONE SLU', '3', '$', 430000092, 'SPAIN', 'zeus.lara@basebone.com;sylwia.stepniak@basebone.com', 'B92790898', '', 'MARBELLA', '', 'Address…'),
(427, 'Bidmotion ', 'HYDRANE SAS (BIDMOTION)', '3', '$', 430000109, '', 'at@bidmotion.com', '14808515209', '', '', '', 'Address…'),
(428, 'Blind Ferret', 'BLIND FERRET ENTERTAINMENT', '3', '$', 430000172, '', 'josh@blindferretmedia.com', '843802943RC01', '', '', '', 'Address…'),
(429, 'Brusmedia', 'BRUS MEDIA', '3', '$', 430000160, '', 'sophie@brusmedia.com', '5016312013', '', '', '', 'Address…'),
(430, 'Buongiorno BR (Taplinks)', 'BUONGIORNO MYALERT BRASIL SERVIÇOS', '3', '$', 430000076, 'BRAZIL', 'vanessa.goncalves@buongiorno.com', 'CNPJ 07.776.9', '', 'SAO PAULO', 'minimo facturación 200$', 'Address…'),
(431, 'Clariad', 'CLARIAD PERFORMANCE S.A. DE C.V.', '3', '$', 430000067, 'MEXICO', 'hector.guerrero@clariad.com', 'CPE1206085U0', '', 'MEXICO', '', 'Address…'),
(432, 'CPITraffic ', 'CPITRAFFIC.COM', '3', '$', 430000204, 'CHINA', 'finance@cpitraffic.com', '2386324', '', '', '', 'Address…'),
(433, 'Crobo ', 'CROBO GMBH', '3', '$', 430000030, 'GERMANY', 'nejla.resid@crobo.com; billing@crobo.com', '281528178', '', 'BERLIN', '', 'Address…'),
(434, 'Curate Mobile', 'CURATE MOBILE LTD', '3', '$', 430000101, 'CANADA', 'jay@curatemobile.com', '806556999RT00', '', 'TORONTO', '', 'Address…'),
(435, 'Darriens', 'D’ARRIENS S.A', '3', '$', 430000066, 'URUGUAY', 'omri@darriens.com', '217217830010', '', 'MONTEVIDEO', '', 'Address…'),
(436, 'Digital Virgo BR', 'DIGITAL VIRGO DO BRASIL', '3', '$', 430000051, 'BRAZIL', 'smonteiro@digitalvirgo.com', '1192599800011', '', 'SAO PAULO', '', 'Address…'),
(437, 'Gasmobi', 'ROI PERFORMANCE MEDIA ,S.L', '3', '$', 430000027, 'SPAIN', 'victor.duran@gasmobi.com', 'B87320487', '622589367', 'ALCOBENDAS', '', 'Address…'),
(438, 'Glispa', 'GLISPA BILLING DETAILS', '3', '$', 430000037, 'GERMANY', 'billing@glispamedia.com', '814998388', '', 'BERLIN', '', 'Address…'),
(439, 'Iconpeak ', 'FIBERMOBI PERFORM. MKETING GMBH-ICONPEAK', '3', '$', 430000111, 'GERMANY', 'partners+1@iconpeak.com', '289357781', '', 'BERLIN', '', 'Address…'),
(440, 'Instal', 'INSTAL SRL', '3', '$', 430000229, '', 'finance@instal.com', '06681730484', '', '', '', 'Address…'),
(441, 'iWoop ', 'IWOOP LIMITED', '3', '$', 430000127, '', 'alex@iwoop.com', '2344595', '', '', '', 'Address…'),
(442, 'JudoAds ', 'JUDOADS LTD', '3', '$', 430000104, 'ISRAEL', 'ido@judoads.com', '515221430', '', 'TEL-AVIV', '', 'Address…'),
(443, 'Khing ', 'KHING LTD', '3', '$', 430000278, 'UNITED KINGDOM', 'invoicing@khingdom.com', '175221227', '', 'LONDON', '', 'Address…'),
(444, 'Kissmyads/ Stroer', 'STRÖER MOBILE PERFORMANCE (KISSMYADS)', '3', '$', 430000020, 'GERMANY', 'i.gallardo@stroeermobileperformance.com;s.kremer@stroeermobileperformance.com', '285830904', '', 'COLOGNE', '', 'Address…'),
(445, 'Kwanko France ', 'KWANKO S.A', '3', '€', 430000201, 'FRANCE', 'marine.deshayes@kwanko.com;julien.contaret@kwanko.com;luis.rubiao@kwanko.com', '50440546885', '', 'BOULOGNE BILLANCOURT', 'aprobar factura de su plataforma, no se envia', 'Address…'),
(446, 'Leadbolt ', 'LEADBOLT PTY LTD', '3', '$', 430000144, 'AUSTRALIA', 'daniel.small@leadbolt.com', 'ABN6314403', '', '', '', 'Address…'),
(447, 'Lollitap ', 'LOLLITAP LP', '3', '$', 430000129, '', 'Alex@lollitap.com', '6280682631', '', '', '', 'Address…'),
(448, 'MagicTap ', 'MAGICTAP SOLUTIONS PVT LTD', '3', '$', 430000159, '', 'farzana@magictap.mobi', '08080609232', '', '', '', 'Address…'),
(449, 'MaxBounty ', 'MAXBOUNTY', '3', '$', 430000214, '', 'brodyc@maxbounty.com', '736346321', '', '', '', 'Address…'),
(450, 'Mister Bell ', 'SARL MISTER BELL', '3', '$', 430000128, '', 'facturation@misterbell.com', '67528044415', '', '', '', 'Address…'),
(451, 'Mobaloo ', 'MOBALOO SL.', '3', '$', 430000068, 'SPAIN', 'isastre@mobaloo.com', 'B86014313', '', 'ALCALÁ DE HENARES', '', 'Address…'),
(452, 'Mobco CPA ', 'VIEWALL INVESTMENTS LIMITED', '3', '$', 430000112, 'CHIPRE', 'billing@mobcomedia.com', '10349250V', '', 'NICOSIA', '', 'Address…'),
(453, 'Mobilecashout ', 'INNOVATIVE MOBILE SOLUTIONS LIMITED', '3', '$', 430000011, '', 'oliver@mobilecashout.com', '3177685HH', '', 'WESTMEATH', '', 'Address…'),
(454, 'Mobiletraffic.de ', 'WEMAX GROUP GMBH & CO. KG (MOBILE TRAFFI', '3', '$', 430000113, 'GERMANY', 'Advertisers@mobiletraffic.de', '281524971', '', 'BERLIN', '', 'Address…'),
(455, 'Mobisense ', 'MOBISENSE A SENSE DIGITAL VENTURE', '3', '$', 430000190, '', 'trapti@mobisense.in; media@mobisense.in', 'AAVCS7097PPS0', '', '', '', 'Address…'),
(456, 'MobiteMedia ', 'WEB PICK-INTERNET HOLDING LTD.', '3', '$', 430000121, '', 'albert.s@mobitemedia.com', '514586619', '', '', '', 'Address…'),
(457, 'MobPartner', 'SAS MOBPARTNER', '3', '$', 430000029, 'FRANCE', 'accounts@advancemobile.net', '76484374533', '', 'PARIS', '', 'Address…'),
(458, 'Mobupps ', 'MOBUPPS INTERNATIONAL LTD.', '3', '$', 430000166, 'ISRAEL', 'betina@mobupps.com', '514429232', '', 'ISRAEL', '', 'Address…'),
(459, 'Mpire ', 'MPIRE NETWORK INC.', '3', '$', 430000033, '', 'superna.naik@mpirenetwork.com', '821340577', '', 'MARKHAM, ON ', '', 'Address…'),
(460, 'On Mobile (OLD) ', 'FONESTARZ MEDIA LTD (2 DAY YUK LTD)', '3', '$', 430000193, '', 'jose.pradavales@onmobile.com;nuria.campa@onmobile.com', '867141610', '', '', '', 'Address…'),
(461, 'Pimp my Clicks ', 'PIMT MY CLICKS', '3', '$', 430000099, 'ISRAEL', 'michal@pimpmyclicks.com', '2040424690', '', 'ISRAEL', '', 'Address…'),
(462, 'Plusmobile (OLD)', 'COMTEL INTERNATIONAL LLC (PLUSMOBILE)', '3', '$', 430000251, '', 'stephanie.alderete@plusmobile.com', '98 0419205', '', '', 'no se envía factura', 'Address…'),
(463, 'Pocket media ', 'POCKET SALES BV. (POCKET MEDIA)', '3', '$', 430000060, '', 'Finance@pocketmedia.mobi', '852367600B01', '', 'AMSTERDAM', '', 'Address…'),
(464, 'Purify ', 'PURIFY DIGITAL LIMITED', '3', '$', 430000090, 'UNITED KINGDOM', 'dariya.andreeva@purifydigital.com;accounts@purifydigital.com;ivelina.stoyanova@purifydigital.com', '175435396', '', 'LONDRES', '', 'Address…'),
(465, 'Revmob ', 'MOBILE REPRESENTATION INTERNATI.(REVMOB)', '3', '$', 430000110, 'BRITISH VIRGIN ISLAND', 'billing@revmob.com', '1743006', '', 'ROAD TOWN ', '', 'Address…'),
(466, 'Rulead ', 'RULEAD LP', '3', '$', 430000155, '', 'finance@rulead.com', 'SL022628', '', '', '', 'Address…'),
(467, 'Shark Games', 'SHARK GAMES CORP', '3', '$', 430000150, '', 'd.radchenko@sharkgames.com', '0369873249808', '', '', '', 'Address…'),
(468, 'Sprocketster', 'SPROCKETSTER MEDIA', '3', '$', 430000056, '', 'accounting@sprocketster.com;dee@sprocketster.com', '272667228', '', '', '', 'Address…'),
(469, 'Swift ', 'JBS SA   (SWIFT)', '3', '$', 430000140, 'BRAZIL', 'susana.felix@jbs.com.br', '02916265/0027', '', 'SAO PAULO', '', 'Address…'),
(470, 'Tapgerine', 'TAPGERINE LLC', '3', '$', 430000002, 'NEVADA', 'vitaliy@tapgerine.com', '27-2965094', '', '664 PEACHY CANYON CIRCLE ', '', 'Address…'),
(471, 'Terra (OLD)', 'ASESFIS (TERRA)', '3', '$', 430000132, '', 'karina.hernandez@terc.terra.com', '15070389864', '', '', '', 'Address…'),
(472, 'Terra (OLD)', 'ASESFIS S DE RL DE CV', '3', '$', 430000174, 'MEXICO', 'paulina.Gonzalez@terc.terra.com', 'ASE1507104QA', '', 'ESTADO DE MEXICO', '', 'Address…'),
(473, 'The Wink Initiative ', 'THE WINK INITIATIVE INC.', '3', '$', 430000073, '', 'lynn@twkmobile.com', '461764503', '', '', '', 'Address…'),
(474, 'Trooperads', 'TROOPERADS LLC ', '2', '$', 430000231, 'FLORIDA', 'media@trooperads.com;horacio@trooperads.com;finance@trooperads.com', '364825227', '', 'FLORIDA', '', 'Address…'),
(475, 'Twistbox ', 'TWISTBOX ENTERTAINMENT', '3', '$', 430000025, 'CALIFORNIA', 'lkhodaverdy@twistbox.com', '800058995', '+18183016200', 'LOS ÁNGELES', '', 'Address…'),
(476, 'Umiqo ', 'UMIQO GLOBAL SL', '3', '$', 430000026, 'SPAIN', 'david@umiqo.com', 'B66298332', '+34 662 682 727', 'BARCELONA', '', 'Address…'),
(477, 'Vip Response ', 'VIP RESPONSE B.V.', '3', '$', 430000184, 'Ev Utrecht', 'shareen@vipresponse.es', '852627725B01', '', 'EV UTRECHT', '', 'Address…'),
(478, 'William Hill ', 'WHG  INTERNATIONAL  LTD. (WILLIAM HILL)', '3', '$', 430000187, '', 'lior.maleh@williamhill.com', '99191', '972(73)7333299', '', '', 'Address…'),
(479, 'Woobi', 'SOCIAL TOKENS LTD.', '3', '$', 430000053, 'ISRAEL', 'finance@woobi.com', '514234673', '', '', '', 'Address…'),
(480, 'YeahMobi', 'CLICK TECH LIMITED (YEAHMOBI)', '3', '$', 430000108, 'CHINA', 'accounting@yeahmobi.com', '6505054500007', '', 'HONG-KONG', '', 'Address…'),
(481, 'YOC ', 'YOC AG', '3', '$', 430000177, 'GERMANY', 'rohan.nair@yoc.com', '211843798', '', 'BERLIN', '', 'Address…'),
(482, 'Zeight', 'ZEIGHT (GLASGOW CALEDONIAN UNIVERSITY)', '3', '$', 430000202, 'UNITED KINGDOM', 'jp@zeight.co', '596789450', '(+1) 773.335.08', 'SCOTLAND, GLASGOW', '', 'Address…'),
(483, 'POPADS INTERNATIONAL LIMITED', 'POPADS INTERNATIONAL LIMITED', '3', '$', 430000238, '', '', 'IC20150629', '', '', '', 'Address…'),
(484, 'DIGI117 LTD.', 'DIGI117 LTD.', '3', '$', 430000227, '', 'smarynich@gowide.com', '824885057', '', '', '', 'Address…'),
(485, 'IMGNT LTD', 'IMGNT LTD', '3', '$', 430000156, '', 'support@monetizeplus.com', '515344554', '', '', '', 'Address…'),
(486, 'ADSALES, LDA.', 'ADSALES, LDA.', '3', '$', 430000016, '', 'andre.lopes@salesengineonline.com', '509036228', '(+351)210179400', '', '', 'Address…'),
(487, 'PLAYBEAT', 'PLAYBEAT', '3', '$', 430000211, '', 'finance@playbeat.com', '472327364', '', '', '', 'Address…'),
(488, 'ADX DIGITAL PTE LTD', 'ADX DIGITAL PTE LTD', '3', '$', 430000223, '', 'finance@justmob.mobi;gborras@justmob.mobi', '21748460001', '', '', '', 'Address…'),
(489, 'EPAGO, LTD', 'EPAGO, LTD', '3', '$', 430000107, '', 'billing@epago.com', '201300047', '', '', '', 'Address…'),
(490, 'INMOBI PTE. LIMITED', 'INMOBI PTE. LIMITED', '3', '$', 430000007, '', 'archana.s@inmobi.com;accounts.receivable@inmobi.com', '200811408G', '', '', '', 'Address…'),
(491, 'NEOMOBILE PERU S.A.C.', 'NEOMOBILE PERU S.A.C.', '3', '$', 430000135, '', 'invoice.andean@neomobile.com', '1792394910001', '', 'SAN BORJA', '', 'Address…'),
(492, 'NEOMOBILECUADOR S.A.', 'NEOMOBILECUADOR S.A.', '3', '$', 430000136, '', 'invoice.andean@neomobile.com', '1792394910001', '', '', '', 'Address…'),
(493, 'MOBILE MARKETING ASSOCIATION', 'MOBILE MARKETING ASSOCIATION', '3', '$', 430000139, '', 'finance@mmaglobal.com', '13-4161254', '', '', '', 'Address…'),
(494, 'MINIMOB (CY) LTD', 'MINIMOB (CY) LTD', '3', '$', 430000300, '', 'l.mourmouris@internetq.com', '10342536I', '', '', '', 'Address…'),
(495, 'GOLDKIWI MEDIA S.A. ', 'GOLDKIWI MEDIA S.A. ', '3', '$', 430000036, '', 'mkaraagac@goldkiwi.com', '0866204258', '', '', '', 'Address…'),
(496, 'DIGITAL VIRGO ARGENTINA', 'DIGITAL VIRGO ARGENTINA', '3', '$', 430000126, 'ARGENTINA', 'g,devicentis@digitalvirgo.com', '30708627409', '', 'BUENOS AIRES', '', 'Address…'),
(497, 'MOUNTMEDIASOLUTIONS B.V', 'MOUNTMEDIASOLUTIONS B.V', '3', '$', 430000014, 'AT TERHEIJDEN', '', '854984690B01', '0031 765146788', 'AT TERHEIJDEN', '', 'Address…'),
(498, 'NEOMOBILE DO BRASIL TECNOLOGIA', 'NEOMOBILE DO BRASIL TECNOLOGIA', '3', '$', 430000040, 'BRAZIL', 'juliano.valerio@neomobile.com', '09.143.138/00', '', 'SAO PAULO', '', 'Address…'),
(499, 'APPSUNION THECNOLOGIES INTERNA', 'APPSUNION THECNOLOGIES INTERNATIONAL PVT', '3', '$', 430000062, 'CHINA', 'Amy@appsunion.com', '61771513-000', '', 'HONG KONG', '', 'Address…'),
(500, 'NEOMOBILE COLOMBIA S.A.S.', 'NEOMOBILE COLOMBIA S.A.S.', '3', '$', 430000120, 'COLOMBIA', 'lizbeth.urbiola@neomobile.com', '9006041543', '', 'BOGOTÁ', '', 'Address…'),
(501, 'MIDCORP INVESTMENTS, LLC', 'MIDCORP INVESTMENTS, LLC', '3', '$', 430000097, 'FLORIDA', 'info@midcorp.guru', '464519905', '', 'MIAMI', '', 'Address…'),
(502, 'WISTER - SERVICE AFFILIATION F', 'WISTER - SERVICE AFFILIATION FACTUR', '3', '$', 430000081, 'FRANCE', 'richard@wister.fr', '40447878950', '', 'PARIS', '', 'Address…'),
(503, 'SLIMSPOTS', 'SLIMSPOTS', '3', '$', 430000008, 'GERMANY', 'register@slimspots.com', '264640070', '+49 531-257 333', 'Braunschweig', '', 'Address…'),
(504, 'W2M GMBH', 'W2M GMBH', '3', '$', 430000080, 'GERMANY', 'ys.shon@w2mobile.com', '249563317', '', 'COLOGNE', '', 'Address…'),
(505, 'POLWAY LIMITED ( MOBSUITE)', 'POLWAY LIMITED ( MOBSUITE)', '3', '$', 430000075, 'IRELAND', 'affiliate@mobsuite.com', '3239717KH', '', 'DUBLIN', '', 'Address…'),
(506, 'XPLOSION LTD.', 'XPLOSION LTD.', '3', '$', 430000023, 'Isle Of Man', '', '003480517', '', 'ISLE OF MAN', '', 'Address…'),
(507, 'TAKOOMI LTD', 'TAKOOMI LTD', '3', '$', 430000019, 'ISRAEL', '', '514688522', '', 'TEL AVIV', '', 'Address…'),
(508, 'ALFONSO ROSSI', 'ALFONSO ROSSI', '3', '$', 430000180, 'ITALY', 'tuliorossi64@gmail.com', 'MI2208058', '', 'MILANO', '', 'Address…'),
(509, 'MEGAMOBILE SOLUTIONS SDN BHD', 'MEGAMOBILE SOLUTIONS SDN BHD', '3', '$', 430000146, 'MALAYSIA', 'shinngee@megamobile.com.my', '754681-T', '', 'PETALING JAYA', '', 'Address…'),
(510, 'NEOMOBILE MÉXICO, S DE RL DE C', 'NEOMOBILE MÉXICO, S DE RL DE CV', '3', '$', 430000077, 'MEXICO', 'omar.camacho@neomobile.com', 'AMO060503UV2', '', 'MEXICO', '', 'Address…'),
(511, 'BINBIT CORPORATE', 'BINBIT CORPORATE', '3', '$', 430000093, 'MEXICO', 'grodriguez@binbit.com', '201022094K', '', 'MEXICO', '', 'Address…'),
(512, 'FRIST MOBILE AFFILIATE S.A', 'FRIST MOBILE AFFILIATE S.A', '3', '$', 430000055, 'PANAMA', 'Laura@fristmobilecash.com', '155598528', '', 'PANAMA', '', 'Address…'),
(513, 'FRK-SERVIÇOS DE MARKETING NA I', 'FRK-SERVIÇOS DE MARKETING NA INTERNET,SA', '3', '$', 430000123, 'PORTUGAL', 'finance@frk-agency.com', '509404863', '', 'LISBOA', '', 'Address…'),
(514, 'MOBILE ENTERTAINMENT TECHNOLOG', 'MOBILE ENTERTAINMENT TECHNOLOGY PTE LTD', '3', '$', 430000024, 'SINGAPORE', 'billing@met.sg', '201022094K', '', 'SINGAPORE', '', 'Address…'),
(515, 'BYRON NETWORKS S.L (ILOVECPA)', 'BYRON NETWORKS S.L (ILOVECPA)', '3', '$', 430000048, 'SPAIN', 'maria.e@ilovecpa.com', 'B93150324', '', 'BENALMÁDENA', '', 'Address…'),
(516, 'DESARROLLOS DIFDY SL', 'DESARROLLOS DIFDY SL', '3', '$', 430000233, 'SPAIN', '', 'B87158374', '', 'MAJADAHONDA', '', 'Address…'),
(517, 'ADQUOTA SPAIN SL', 'ADQUOTA SPAIN SL', '3', '$', 430000012, 'SPAIN', 'pedro.manzanares@adqspain.com', 'B85897239', '915234528', 'MADRID', '', 'Address…'),
(518, 'IBRANDS MEDIOS INTERACTIVOS, S', 'IBRANDS MEDIOS INTERACTIVOS, S.L.', '3', '$', 430000001, 'SPAIN', 'marta.quintas@ibrands.es', 'B85718880', '', 'MADRID', '', 'Address…'),
(519, 'NEOMOBILE SPAIN SLU', 'NEOMOBILE SPAIN SLU', '3', '$', 430000082, 'SPAIN', 'isabela.uehara@neomobile.com', 'B85425361', '', 'MADRID', '', 'Address…'),
(520, 'EUROCIO FREENTIME SL', 'EUROCIO FREENTIME SL', '3', '$', 430000283, 'SPAIN', 'victor.hernandez@eurocios.com', 'B65878555', '93 667 56 52', 'BARCELONA', '', 'Address…'),
(521, 'PRODUCTOS SERVICIOS MOVILES (P', 'PRODUCTOS SERVICIOS MOVILES (PSMOVILES)', '3', '$', 430000133, 'SPAIN', 'borja.calvet@psmoviles.com', 'B24478885', '', 'MADRID', '', 'Address…'),
(522, 'KITMAKER ENTETAIMENT SL', 'KITMAKER ENTETAIMENT SL', '3', '$', 430000057, 'SPAIN', 'ccarbonell@kitmaker.com', 'A57670028', '', 'PALMA DE MALLORCA', '', 'Address…');
INSERT INTO `advertisers` (`id`, `nombre`, `social`, `id_am`, `money`, `cod_client`, `nation`, `contact_mail`, `CifDni`, `phone`, `state`, `notes`, `address`) VALUES
(523, 'RETRO MEDYA INTERAKTIF TEKNOLO', 'RETRO MEDYA INTERAKTIF TEKNOLOJILER', '3', '$', 430000188, 'TURKEY', 'alejandra.becerra@neomobile.com;isabela.uehara@neomobile.com;marketing.emea@neomobile.com;', '7340444998', '', 'ESTAMBUL', '', 'Address…'),
(524, 'RETRO MEDYA INTERAKTIF TEKNOLO', 'RETRO MEDYA INTERAKTIF TEKNOLOJILERI A.S', '3', '$', 430000124, 'TURKEY', 'isabela.uehara@neomobile.com;marketing.emea@neomobile.com;alejandra.becerra@neomobile.com ', '7340444998', '', 'ESTAMBUL', '', 'Address…'),
(525, 'BASTET HOLDING LTD (AKA ZENNA)', 'BASTET HOLDING LTD (AKA ZENNA)', '3', '$', 430000220, 'UNITED KINGDOM', 'kollkate@zennaapps.com;finance@zennaapps.com ', '1790277', '', 'BRITISH VIRGIN ISLANDS ', '', 'Address…'),
(526, 'MAD4MOBILE LIMITED', 'MAD4MOBILE LIMITED', '3', '$', 430000064, 'UNITED KINGDOM', 'james@mad4mobile.com', '116732133', '', 'MANCHESTER', '', 'Address…'),
(527, 'HEADWAY DIGITAL', 'HEADWAY DIGITAL', '3', '$', 430000035, 'URUGUAY', 'diego.mangiarotti@headwaydigital.com;thomas.lehardy@headwaydigital.com', '217208190012', '', 'MONTEVIDEO', '', 'Address…'),
(528, 'ADZOL MEDIA', 'ADZOL MEDIA', '3', '$', 430000299, '', 'Lucas.toledo@adzolmedia.com', '513461035', '+35121139904', '', '', 'Address…'),
(529, 'Juliana Ribeiro Borges', '', '3', '', 430000122, 'BRASIL', 'juliana.borges@advancemobile.net', 'FH140880', '', '', 'Notes...', ''),
(530, 'Kwanko Spain', '', '3', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(531, 'Neomobile ES', '', '3', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(532, 'MoiAds', '', '3', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(533, 'mSolutions', '', '3', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(534, 'AdsJoy', '', '3', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(535, 'Momads', '', '3', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(536, 'Adbird Media', '', '3', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(537, 'Adssapp', '', '3', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(538, 'Gadmobe', '', '3', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(539, 'Vykonia', '', '3', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(540, 'Mobiground', '', '3', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(541, 'Neomobile BR', '', '3', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `affiliates`
--

CREATE TABLE `affiliates` (
  `id` int(4) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `social` varchar(80) NOT NULL,
  `affm` varchar(30) NOT NULL,
  `money` varchar(2) DEFAULT '$',
  `cod_client` int(12) DEFAULT NULL,
  `nation` varchar(50) DEFAULT NULL,
  `contact_mail` varchar(200) DEFAULT NULL,
  `CifDni` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `notes` varchar(1000) DEFAULT 'Notes...',
  `address` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `affiliates`
--

INSERT INTO `affiliates` (`id`, `nombre`, `social`, `affm`, `money`, `cod_client`, `nation`, `contact_mail`, `CifDni`, `phone`, `state`, `notes`, `address`) VALUES
(1, 'Appaniac', 'Appaniac LTD', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(2, 'Allitap', 'Aircyb Limited', '8', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(3, 'Mobco', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(4, 'Cost2action', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(5, 'Avazu', 'Avazu (Brunei) afiliación', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(6, 'Digital Pow Pow', 'Xertive Global media LTD', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(8, 'Mozoo', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(9, 'Misky Ads', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(10, 'Stingrad', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(11, 'IncentMobi', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(12, 'SabiaMedia', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(13, 'Wextmedia', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(14, 'Ströer Mobile Performance CPA', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(15, 'Affle CPA', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(16, 'Crazy4Media CPA', 'Crazy4Media Online LTD', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(17, 'Midco', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(18, 'iWoop CPA', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(19, 'IncentMobi CPA', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(20, 'Publited', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(21, 'Linkadia CPA', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(22, 'Bido-Studio.co', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(23, 'Gombio', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(24, 'Bidsopt', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(25, 'Add Value Media', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(26, 'Revenuemob', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(27, 'CashOnMobi', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(28, 'CPI Traffic', 'CPITRAFFIC.COM', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(29, 'iWoop CPI', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(30, 'Hexcan', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(31, 'CrowMobi - Sax Limited', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(32, 'Zhengfang Wang', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(33, 'Adsfast', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(34, 'BestApps', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(35, 'ONEAPI - SVG Media', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(36, 'Publited CPI', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(37, 'SabotageAds', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(38, 'Linkadia CPI', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(39, 'Trooperads CPI', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(40, 'Voluum', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(41, 'Native X', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(42, 'SelfAdvertiser', '', '6', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(43, 'Minimob', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(44, 'App Booster', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(45, 'Adcash', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(46, 'Revmob CPI', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(47, 'DCypher Media', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(48, 'Instal.com', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(49, 'Spiroox', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(50, 'Opera', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(52, 'Smartlead', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(53, 'Mobchain', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(54, 'Offerseven', 'Offerseven', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(55, 'Cosmic', '                      ', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(56, 'Avazu Inc', 'Avazu Inc', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(57, 'Billymob', 'Billymob', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(58, 'Bucksense', 'Bucksense', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(59, 'Clickadu', 'Markopros Media LLP', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(60, 'Kimia CPA', 'Kimia', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(61, 'Mobipium CPA', 'Mobipium CPA', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(62, 'MobiVisits', 'MobiVisits', '6', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(63, 'Mobquid', 'Mobquid', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(64, 'Reporo', 'Reporo', '7', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(65, 'ADXMI', 'Youmi Technology (HK) Ltd', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(67, 'Gasmobi', 'Gasmobi', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(68, 'Haka Mobi', 'Haka Mobi', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(69, 'Advance Mobile', 'Advance Mobile', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(70, 'ReachEffect', 'ReachEffect', '6', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(71, 'Exoclick', 'Exoclick', '7', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(72, 'Matomy - My DSP', 'Matomy - My DSP', '7', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(73, 'SelfAdvertiser', 'SelfAdvertiser', '6', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(74, 'Zeropark', 'Zeropark', '6', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(75, 'Trillium Interactive', 'Trillium Interactive', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(76, 'Glispa', 'Glispa', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(77, 'PopAds', 'Tomksoft S.A.', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(78, 'Arkeero', 'Rock Internet S.L.', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(79, 'BravAds', 'Bravads Strategic Online Advertising', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(80, 'RevLinker', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(81, 'Click2Comission CPA', 'Click2Comission CPA', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(82, 'Clariad CPA', 'Clariad Performance SA DE CV', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(83, 'Clickky CPA', 'Clickky LLC', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(84, 'Kimia CPA 2', 'Kimia CPA 2', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(85, 'PropellerAds', 'PropellerAds', '7', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(86, 'PlugRush', 'JMT Nordic', '7', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(87, 'MobAds', '', '7', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(88, 'Reach Effect', '', '6', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(89, 'Avazu Mobile DSP', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(90, 'ActiveRevenue', 'Info Bridge Ltd', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(91, 'JuicyAds', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(92, 'Matomy myDSP', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(93, 'FivePPC', '', '7', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(94, 'PopMyAds', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(95, 'AdXpansion', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(96, 'RevMob', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(97, 'Old Zero Park', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(98, 'AirPush', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(99, 'Hilltopads', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(100, 'Focuus', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(101, 'Inmobi', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(102, 'Traffic Shop', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(103, 'MediaHub', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(104, 'Buzzcity', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(105, 'Bidder Trafico', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(106, 'Adiquity', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(107, 'Fyber', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(108, 'Winclap', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(109, 'StartApp', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(110, 'AdSupply', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(111, 'Appreciate', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(112, 'TrafficJunky', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(113, 'Pocketmath', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(114, 'Traffic Factory', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(115, 'Affise', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(116, 'Adfly', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(117, 'EroAdvertising', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(118, 'Adultmoda', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(119, 'PopCash', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(120, 'RevMob 2 TEST', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(121, 'Mobicow', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(122, 'Spotify', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(123, 'LeadBolt', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(124, 'MobFox', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(125, 'Adivity', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(126, 'Appnext', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(127, 'Liquid', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(128, 'Adamo', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(129, 'Decisive', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(130, 'Ezmob', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(131, 'DNTX', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(132, 'No Affiliate', 'No Affiliate', 'No Affiliate', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(133, 'Affiliates', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(134, 'Appamplify.com', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(135, 'EffectMobi', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(136, 'SocialPubli', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(137, 'CloudMob', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(138, 'Techno Experience', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(139, 'Appromoters', '', '3', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(166, 'Collectcent', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(167, 'MoiAds', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(168, 'Vykonia', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(169, 'Sushmita Dwivedi', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(170, 'WingoAds', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(171, 'Adbirdmedia', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(172, 'CloudMob2', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(173, 'Adsjoy', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(174, 'Smashmyads', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(175, 'Appalgo', '', '4', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(176, 'TONIC', '', '2', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL),
(177, 'TrafficForce', '', '2', '$', NULL, NULL, NULL, NULL, NULL, NULL, 'Notes...', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `costes`
--

CREATE TABLE `costes` (
  `id` int(5) NOT NULL,
  `date` date NOT NULL,
  `affiliate` int(5) NOT NULL,
  `affm` int(5) DEFAULT NULL,
  `totaldollar` decimal(10,2) NOT NULL DEFAULT '0.00',
  `totaleuro` decimal(10,2) DEFAULT '0.00',
  `plataff` decimal(10,2) DEFAULT '0.00',
  `discrepancia` decimal(10,2) DEFAULT '0.00',
  `scrubs` decimal(10,2) DEFAULT '0.00',
  `onhold` decimal(10,2) DEFAULT '0.00',
  `validacion` decimal(10,2) DEFAULT '0.00',
  `status` int(2) DEFAULT NULL,
  `comments` varchar(100) DEFAULT NULL,
  `fact_number` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `costes`
--

INSERT INTO `costes` (`id`, `date`, `affiliate`, `affm`, `totaldollar`, `totaleuro`, `plataff`, `discrepancia`, `scrubs`, `onhold`, `validacion`, `status`, `comments`, `fact_number`) VALUES
(2, '2017-05-01', 25, 62, '1.82', '0.00', '0.00', '0.00', '0.00', '0.00', '1.82', 1, 'Ok', NULL),
(3, '2017-05-01', 65, 1, '1218.10', '0.00', '0.00', '0.00', '0.00', '0.00', '1218.10', 1, '', NULL),
(4, '2017-05-01', 15, 1, '5.03', '0.00', '0.00', '0.00', '0.00', '0.00', '5.03', 1, '', NULL),
(5, '2017-05-01', 1, 23, '3.29', '0.00', '0.00', '0.00', '0.00', '0.00', '3.29', 1, '', NULL),
(6, '2017-05-01', 27, 1, '1.50', '0.00', '0.00', '0.00', '0.00', '0.00', '1.50', 1, '', NULL),
(7, '2017-05-01', 66, 1, '256.87', '0.00', '0.00', '0.00', '0.00', '0.00', '256.87', 2, 'Kamila\'s Fault', NULL),
(8, '2017-05-01', 67, 1, '9.09', '0.00', '0.00', '0.00', '0.00', '0.00', '9.09', 1, '', NULL),
(9, '2017-05-01', 68, 1, '1.76', '0.00', '0.00', '0.00', '0.00', '0.00', '1.76', 1, '', NULL),
(10, '2017-05-01', 69, 1, '2.62', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '', NULL),
(11, '2017-05-01', 11, 1, '154.30', '0.00', '0.00', '0.00', '0.00', '0.00', '154.30', 1, '', NULL),
(12, '2017-05-01', 60, 1, '4713.08', '0.00', '0.00', '0.00', '0.00', '0.00', '4713.08', 1, '', NULL),
(13, '2017-05-01', 21, 1, '7.29', '0.00', '0.00', '0.00', '0.00', '0.00', '7.29', 1, '', NULL),
(14, '2017-05-01', 61, 1, '52.30', '0.00', '0.00', '0.00', '0.00', '0.00', '52.30', 1, '', NULL),
(15, '2017-05-01', 49, 23, '6072.12', '0.00', '0.00', '0.00', '0.00', '0.00', '6072.12', 1, '', NULL),
(16, '2017-05-01', 74, 69, '3562.67', '0.00', '3580.18', '17.51', '0.00', '0.00', '3580.18', 1, '', NULL),
(17, '2017-05-01', 64, 1, '235.14', '0.00', '258.62', '23.48', '0.00', '0.00', '258.62', 1, '', NULL),
(18, '2017-05-01', 70, 1, '736.03', '0.00', '0.00', '0.00', '0.00', '0.00', '736.03', 1, '', NULL),
(19, '2017-05-01', 71, 1, '291.61', '0.00', '329.75', '38.14', '0.00', '0.00', '329.75', 1, '', NULL),
(20, '2017-05-01', 72, 1, '210.08', '0.00', '220.84', '10.76', '0.00', '0.00', '220.84', 1, '', NULL),
(21, '2017-05-01', 42, 1, '100.04', '0.00', '0.00', '0.00', '0.00', '0.00', '100.04', 1, '', NULL),
(22, '2017-06-01', 60, 48, '11882.10', '0.00', '10556.50', '1325.60', '0.00', '0.00', '11882.10', 1, '', NULL),
(23, '2017-06-01', 49, 54, '9345.46', '0.00', '0.00', '0.00', '0.00', '0.00', '9345.46', 1, 'Total Cost = Validation + IVA PROVISION 8189.24 €', NULL),
(24, '2017-06-01', 65, 49, '6238.05', '0.00', '0.00', '0.00', '0.00', '0.00', '6238.05', 1, 'PROVISION 5466.22 €', NULL),
(25, '2017-06-01', 25, 52, '661.20', '0.00', '0.00', '0.00', '0.00', '0.00', '661.20', 1, 'Total Cost = Validation + IVA - Se confirma al Aff sin Validación de ADV PROVISION 579.39 €', NULL),
(27, '2017-06-01', 3, 51, '98.00', '0.00', '0.00', '0.00', '0.00', '0.00', '98.00', 1, '', NULL),
(28, '2017-06-01', 12, 51, '129.36', '0.00', '0.00', '0.00', '0.00', '0.00', '129.36', 1, 'Se confirma al Aff sin validación ADV PROVISION 113.35 €', NULL),
(29, '2017-06-01', 66, 52, '99.17', '0.00', '0.00', '0.00', '0.00', '0.00', '99.17', 1, 'Juntos CPI (91.81) + CPA (7.36) - Se confirma al Aff sin validación ADV', NULL),
(30, '2017-06-01', 11, 48, '53.40', '0.00', '0.00', '0.00', '0.00', '0.00', '53.40', 1, '', NULL),
(31, '2017-06-01', 61, 53, '3.43', '0.00', '0.00', '0.00', '0.00', '0.00', '3.43', 1, '', NULL),
(32, '2017-06-01', 15, 23, '1.30', '0.00', '0.00', '0.00', '0.00', '0.00', '1.30', 1, '', NULL),
(33, '2017-06-01', 21, 56, '0.60', '0.00', '0.00', '0.00', '0.00', '0.00', '0.60', 1, '', NULL),
(34, '2017-06-01', 76, 54, '51.60', '0.00', '0.00', '0.00', '0.00', '0.00', '51.60', 1, 'Se confirma al Aff sin validación ADV', NULL),
(35, '2017-06-01', 75, 23, '0.27', '0.00', '0.00', '0.00', '0.00', '0.00', '0.27', 1, '', NULL),
(36, '2017-06-01', 74, 55, '8607.11', '0.00', '8616.98', '9.87', '0.00', '0.00', '8616.98', 1, 'Voluum (voluum vs zeropark discrepancy)', NULL),
(37, '2017-06-01', 42, 23, '329.35', '0.00', '352.33', '22.98', '0.00', '0.00', '352.33', 1, 'Voluum (discrepancia, error al actualizar amontos)', NULL),
(38, '2017-06-01', 70, 51, '204.15', '0.00', '229.06', '24.91', '0.00', '0.00', '204.15', 1, 'Voluum (amonto de discrapancia tendra refund) PROVISION 178.89 €', NULL),
(39, '2017-06-01', 71, 51, '42.96', '38.92', '44.32', '1.36', '0.00', '0.00', '44.32', 1, 'Voluum - Discrepancia por ExchangeRates', NULL),
(40, '2017-06-01', 72, 55, '25.93', '0.00', '25.94', '0.01', '0.00', '0.00', '25.94', 1, 'Voluum', NULL),
(41, '2017-06-01', 62, 23, '6.33', '0.00', '6.33', '0.00', '0.00', '0.00', '6.33', 1, 'Voluum', NULL),
(42, '2017-06-01', 77, 23, '25.37', '0.00', '25.37', '0.01', '0.00', '0.00', '25.37', 1, 'Voluum', NULL),
(43, '2017-02-01', 45, 51, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '17464.68', 1, 'Total costs of february', NULL),
(44, '2017-03-01', 65, 23, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '43079.54', 1, 'Total costs of March. It\'s not ADXMI', NULL),
(45, '2017-04-01', 65, 23, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '19056.38', 1, 'Total costs of April. Not ADXMI', NULL),
(46, '2017-07-01', 49, 1, '19192.08', '0.00', '0.00', '0.00', '6840.00', '0.00', '12352.55', 2, '', NULL),
(49, '2017-07-01', 3, NULL, '31.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(50, '2017-07-01', 54, NULL, '26.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(52, '2017-07-01', 60, NULL, '34.32', '0.00', '0.00', '0.00', '1091.25', '0.00', '34.49', 2, NULL, NULL),
(53, '2017-07-01', 69, NULL, '1.80', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(59, '2017-07-01', 78, 23, '5513.70', '0.00', '0.00', '0.00', '2182.50', '0.00', '3331.20', 1, 'F1700479 - 3286.20$', NULL),
(60, '2017-07-01', 81, NULL, '323.65', '0.00', '0.00', '0.00', '0.00', '0.00', '323.65', 1, NULL, NULL),
(62, '2017-07-01', 21, NULL, '0.60', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(65, '2017-07-01', 65, 1, '2270.63', '0.00', '0.00', '0.00', '0.00', '0.00', '2270.63', 1, 'YMAD-20170801', NULL),
(69, '2017-07-01', 74, 84, '8223.07', '0.00', '8242.96', '19.89', '0.00', '0.00', '8242.96', 1, 'Voluum', NULL),
(70, '2017-07-01', 42, 86, '442.83', '0.00', '447.56', '4.73', '0.00', '0.00', '447.56', 1, 'Voluum', NULL),
(71, '2017-07-01', 71, 52, '289.34', '239.56', '0.00', '0.00', '0.00', '0.00', '285.00', 1, 'Voluum', NULL),
(72, '2017-07-01', 70, 86, '224.19', '0.00', '226.58', '2.39', '0.00', '0.00', '226.58', 1, 'Voluum', NULL),
(73, '2017-07-01', 85, 83, '167.65', '0.00', '176.17', '8.52', '0.00', '0.00', '176.17', 1, 'Voluum', NULL),
(74, '2017-07-01', 72, 83, '37.24', '0.00', '37.82', '0.58', '0.00', '0.00', '37.24', 1, 'Voluum', NULL),
(75, '2017-07-01', 86, 73, '20.01', '0.00', '20.01', '0.00', '0.00', '0.00', '20.01', 1, 'Voluum', NULL),
(76, '2017-07-01', 64, 85, '18.02', '0.00', '18.08', '0.06', '0.00', '0.00', '18.08', 1, 'Voluum', NULL),
(119, '2017-08-01', 74, 23, '9145.07', '0.00', '9144.94', '0.00', '0.00', '0.00', '9144.94', 1, 'Discrepancia muy baja', NULL),
(120, '2017-08-01', 71, 23, '1111.43', '0.00', '1108.29', '0.00', '0.00', '0.00', '1108.29', 1, 'Discrepancia por cambio de divisa', NULL),
(121, '2017-08-01', 87, 23, '2308.27', '0.00', '2308.79', '0.00', '0.00', '0.00', '2308.79', 1, 'INV-62188,INV-63301', NULL),
(122, '2017-08-01', 85, 23, '978.42', '0.00', '1018.43', '0.00', '0.00', '0.00', '1018.43', 1, 'voluum', NULL),
(123, '2017-08-01', 64, 23, '526.90', '0.00', '528.50', '0.00', '0.00', '0.00', '528.50', 1, 'INV-63292, INV-62178', NULL),
(124, '2017-08-01', 90, 1, '207.79', '0.00', '205.48', '0.00', '0.00', '0.00', '205.48', 1, 'Discrepancia por horarios', '123'),
(125, '2017-08-01', 88, 83, '585.96', '0.00', '586.28', '0.00', '0.00', '0.00', '586.28', 1, 'Discrepancia muy baja', NULL),
(126, '2017-08-01', 89, 23, '181.01', '0.00', '192.64', '0.00', '0.00', '0.00', '192.64', 1, 'Discrepancia con informe - Voluum', NULL),
(127, '2017-08-01', 42, 23, '321.98', '0.00', '322.11', '0.00', '0.00', '0.00', '322.11', 1, 'Discrepancia muy baja', NULL),
(129, '2017-08-01', 93, 80, '2.25', '0.00', '2.25', '0.00', '0.00', '0.00', '2.25', 1, 'voluum', NULL),
(130, '2017-08-01', 86, 71, '108.70', '0.00', '108.74', '0.00', '0.00', '0.00', '108.74', 1, 'voluum', NULL),
(131, '2017-08-01', 91, 82, '28.76', '0.00', '28.76', '0.00', '0.00', '0.00', '28.76', 1, 'voluum', NULL),
(132, '2017-08-01', 94, 82, '9.78', '0.00', '11.27', '0.00', '0.00', '0.00', '11.27', 1, 'Discrepancia por horario', NULL),
(133, '2017-08-01', 62, 83, '137.56', '0.00', '137.56', '0.00', '0.00', '0.00', '137.56', 1, 'voluum', NULL),
(134, '2017-08-01', 95, 82, '0.44', '0.00', '0.66', '0.00', '0.00', '0.00', '0.66', 1, 'voluum', NULL),
(137, '2017-08-01', 92, 23, '59.33', '0.00', '59.32', '0.00', '0.00', '0.00', '59.32', 1, 'voluum', NULL),
(190, '2017-08-01', 65, 1, '13258.65', '0.00', '0.00', '0.00', '483.21', '0.00', '12775.44', 2, 'Aff', NULL),
(191, '2017-08-01', 78, 1, '3214.98', '0.00', '0.00', '0.00', '0.00', '0.00', '3214.98', 1, 'Aff. F1700494', NULL),
(192, '2017-08-01', 75, 1, '3006.00', '0.00', '2556.00', '0.00', '0.00', '0.00', '2556.00', 1, 'Aff', NULL),
(193, '2017-08-01', 49, 1, '2591.65', '0.00', '0.00', '0.00', '0.00', '0.00', '2591.63', 1, 'Aff. FRA-2017-1626 2657.54€', NULL),
(194, '2017-08-01', 137, 1, '450.08', '0.00', '447.08', '0.00', '0.00', '0.00', '447.08', 1, 'Aff', NULL),
(195, '2017-08-01', 60, 1, '130.77', '0.00', '0.00', '0.00', '0.00', '0.00', '130.59', 1, 'Aff - pago en Euros 110', NULL),
(197, '2017-08-01', 79, 1, '51.69', '0.00', '0.00', '0.00', '0.00', '0.00', '51.69', 1, 'Aff', NULL),
(198, '2017-08-01', 54, 1, '13.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, 'Aff', NULL),
(199, '2017-08-01', 61, 1, '11.04', '0.00', '0.00', '0.00', '0.00', '0.00', '11.04', 1, 'Aff', NULL),
(200, '2017-08-01', 82, 1, '6.30', '0.00', '0.00', '0.00', '0.00', '0.00', '6.30', 1, 'Aff', NULL),
(201, '2017-08-01', 134, 1, '3.75', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, 'Aff', NULL),
(203, '2017-08-01', 80, 1, '1.53', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, 'Aff', NULL),
(204, '2017-08-01', 15, 1, '1.30', '0.00', '0.00', '0.00', '0.00', '0.00', '1.30', 1, 'Aff', NULL),
(205, '2017-08-01', 3, 1, '0.63', '0.00', '0.00', '0.00', '0.00', '0.00', '0.63', 1, 'Aff', NULL),
(268, '2017-09-01', 74, 1, '9179.11', '0.00', '0.00', '0.00', '0.00', '0.00', '1232.00', 2, 'voluum', ''),
(269, '2017-09-01', 90, 1, '1320.29', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, 'voluum', ''),
(270, '2017-09-01', 85, NULL, '3625.74', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(271, '2017-09-01', 87, NULL, '1517.61', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(272, '2017-09-01', 86, 1, '538.21', '0.00', '0.00', '0.00', '0.00', '0.00', '232.00', 2, 'voluum', ''),
(273, '2017-09-01', 71, NULL, '389.79', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(274, '2017-09-01', 110, 1, '339.89', '0.00', '0.00', '0.00', '0.00', '0.00', '1231.00', 1, 'voluum', ''),
(275, '2017-09-01', 64, NULL, '264.25', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(276, '2017-09-01', 88, NULL, '562.78', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(277, '2017-09-01', 102, NULL, '154.93', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(278, '2017-09-01', 117, NULL, '192.92', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(279, '2017-09-01', 93, NULL, '7.54', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(280, '2017-09-01', 119, 1, '30.00', '0.00', '0.00', '0.00', '0.00', '0.00', '323.00', 1, 'voluum', ''),
(281, '2017-09-01', 176, NULL, '97.90', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(282, '2017-09-01', 94, NULL, '14.50', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(283, '2017-09-01', 111, 1, '618.46', '0.00', '0.00', '0.00', '0.00', '0.00', '123.00', 2, 'voluum', ''),
(284, '2017-09-01', 91, NULL, '31.06', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(285, '2017-09-01', 62, 1, '49.36', '0.00', '0.00', '0.00', '0.00', '0.00', '123.00', 2, 'voluum', ''),
(286, '2017-09-01', 95, 1, '113.38', '0.00', '0.00', '0.00', '0.00', '0.00', '640.00', 2, 'voluum', ''),
(287, '2017-09-01', 42, NULL, '2.22', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(288, '2017-09-01', 73, NULL, '2.22', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(289, '2017-09-01', 177, 1, '15.98', '0.00', '0.00', '0.00', '0.00', '0.00', '1232.00', 2, 'voluum', ''),
(290, '2017-09-01', 112, 1, '73.03', '0.00', '0.00', '0.00', '0.00', '0.00', '123.00', 2, 'voluum', ''),
(291, '2017-09-01', 96, NULL, '84.47', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(292, '2017-09-01', 123, NULL, '108.80', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(293, '2017-09-01', 59, NULL, '12.60', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, 'voluum', NULL),
(294, '2017-09-01', 49, NULL, '1293.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(295, '2017-09-01', 79, 1, '10.00', '0.00', '0.00', '0.00', '0.00', '0.00', '300.00', 2, '', ''),
(296, '2017-09-01', 57, 1, '5.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '', ''),
(297, '2017-09-01', 60, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(298, '2017-09-01', 84, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(299, '2017-09-01', 69, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2323.00', 2, '', ''),
(300, '2017-09-01', 82, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(301, '2017-09-01', 61, 1, '38.00', '0.00', '0.00', '0.00', '0.00', '0.00', '333.00', 2, '', ''),
(302, '2017-09-01', 15, 4, '1.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '', ''),
(303, '2017-09-01', 25, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '', ''),
(304, '2017-09-01', 166, NULL, '291.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(305, '2017-09-01', 80, NULL, '25.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(306, '2017-09-01', 78, 5, '166.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '', ''),
(307, '2017-09-01', 167, 1, '2769.00', '0.00', '0.00', '0.00', '0.00', '0.00', '3232.00', 2, '', ''),
(308, '2017-09-01', 81, NULL, '7.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(309, '2017-09-01', 168, 1, '11.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2300.00', 2, '', ''),
(310, '2017-09-01', 23, NULL, '1203.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(311, '2017-09-01', 82, NULL, '29.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(312, '2017-09-01', 65, 1, '10643.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '', ''),
(313, '2017-09-01', 169, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '3323.00', 2, '', ''),
(314, '2017-09-01', 170, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '23.00', 2, '', ''),
(315, '2017-09-01', 171, 5, '3.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 2, '', ''),
(316, '2017-09-01', 75, NULL, '750.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(317, '2017-09-01', 172, NULL, '3.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(318, '2017-09-01', 173, 4, '3566.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '', ''),
(319, '2017-09-01', 137, NULL, '874.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(320, '2017-09-01', 172, NULL, '874.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(321, '2017-09-01', 174, NULL, '15.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, NULL),
(322, '2017-09-01', 175, 8, '6.00', '0.00', '0.00', '0.00', '0.00', '0.00', '123.00', 2, '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doc_number`
--

CREATE TABLE `doc_number` (
  `id` int(2) NOT NULL,
  `increment_value` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `doc_number`
--

INSERT INTO `doc_number` (`id`, `increment_value`) VALUES
(1, 98);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incomes`
--

CREATE TABLE `incomes` (
  `id` int(4) UNSIGNED ZEROFILL NOT NULL,
  `advertiser` varchar(50) DEFAULT '0',
  `social` int(5) DEFAULT NULL,
  `am` varchar(50) DEFAULT '3',
  `clicks` int(12) DEFAULT '0',
  `conversion` int(12) DEFAULT '0',
  `revenue` decimal(10,2) DEFAULT '0.00',
  `platform_revenue` decimal(10,2) DEFAULT '0.00',
  `difference` decimal(10,2) DEFAULT '0.00',
  `scrubs` decimal(10,2) DEFAULT '0.00',
  `hold` decimal(10,2) DEFAULT '0.00',
  `validated` decimal(10,2) DEFAULT '0.00',
  `accumulated` decimal(10,2) DEFAULT '0.00',
  `status` varchar(50) DEFAULT '0',
  `notas_am` varchar(200) DEFAULT 'Notes...',
  `notas_finance` varchar(200) DEFAULT 'Notes...',
  `date` date DEFAULT NULL,
  `increment` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `incomes`
--

INSERT INTO `incomes` (`id`, `advertiser`, `social`, `am`, `clicks`, `conversion`, `revenue`, `platform_revenue`, `difference`, `scrubs`, `hold`, `validated`, `accumulated`, `status`, `notas_am`, `notas_finance`, `date`, `increment`) VALUES
(0001, '162', NULL, '354', 0, 11782, '8594.00', '0.00', '0.00', '4441.40', '0.00', '8114.90', '0.00', '7', 'validado por mail Camilla - scrubs en excel', 'FACTURA MOB 870', '2017-02-01', 0),
(0002, '2', NULL, '50', 0, 107, '3834.00', '5996.00', '2162.00', '0.00', '0.00', '5996.00', '0.00', '7', 'Notes...', 'Factura MOB 850: 7339,10 USD que incluye Febrero y varios meses anteriores', '2017-02-01', 0),
(0003, '3', NULL, '354', 0, 1494, '3435.00', '3517.85', '82.85', '2135.70', '0.00', '1382.15', '0.00', '7', 'validado total - scrubs', 'FACTURA MOB 872', '2017-02-01', 0),
(0004, '163', NULL, '0', 0, 2724, '3130.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'queda 0, el total es Mobquid ', 'Notes...', '2017-02-01', 0),
(0005, '4', NULL, '179', 0, 254, '1892.00', '0.00', '0.00', '0.00', '0.00', '2671.00', '0.00', '7', 'Notes...', 'Factura MOB 856', '2017-02-01', 0),
(0006, '57', NULL, '354', 0, 464, '1349.00', '0.00', '0.00', '0.00', '0.00', '1349.90', '0.00', '7', 'Confirman el 15th (no puedo sacar reporte en la plataforma)', 'FACTURA MOB 873', '2017-02-01', 0),
(0007, '28', NULL, '354', 0, 734, '1345.00', '2283.91', '938.91', '0.00', '0.00', '2283.91', '0.00', '7', 'No scrubs yet(15th)', 'FACTURA MOB 871', '2017-02-01', 0),
(0008, '9', NULL, '0', 0, 726, '820.00', '683.20', '-136.80', '0.00', '0.00', '799.11', '0.00', '7', '', 'Factura MOB 892', '2017-02-01', 0),
(0009, '165', NULL, '354', 0, 232, '796.00', '887.85', '91.85', '0.00', '0.00', '716.10', '0.00', '7', '', 'Factura MOB881', '2017-02-01', 0),
(0010, '166', NULL, '179', 0, 111, '766.00', '0.00', '0.00', '0.00', '0.00', '773.00', '0.00', '7', 'Notes...', 'El importe de 773  se ha facturado en Euros, no USD', '2017-02-01', 0),
(0011, '172', NULL, '354', 0, 473, '637.00', '639.20', '2.20', '0.00', '0.00', '639.20', '0.00', '7', 'Facturar Diciembre ($47.85) y Enero ($4.00', 'Fact.Diciembre MOB878 / Factura Enero MOB879 / Factura Febrero MOB880', '2017-02-01', 0),
(0012, '83', NULL, '179', 0, 65, '604.00', '0.00', '0.00', '0.00', '0.00', '608.18', '0.00', '7', 'Notes...', 'Factura MOB 858', '2017-02-01', 0),
(0013, '43', NULL, '179', 0, 189, '521.00', '0.00', '0.00', '0.00', '0.00', '644.40', '0.00', '7', '67.90 USD en hasoffers', 'Factura MOB 845', '2017-02-01', 0),
(0014, '164', NULL, '179', 0, 908, '417.00', '0.00', '0.00', '0.00', '0.00', '443.70', '0.00', '7', 'Notes...', 'Factura MOB 860', '2017-02-01', 0),
(0015, '50', NULL, '354', 0, 114, '245.00', '386.66', '141.66', '0.00', '0.00', '386.66', '0.00', '7', 'AM OOO hasta 17th', 'FACTURA MOB 874', '2017-02-01', 0),
(0016, '10', NULL, '0', 0, 10, '240.00', '0.00', '0.00', '0.00', '0.00', '421.89', '0.00', '7', 'Se genera la factura en su plataforma. Este importe corresponde a CPA + CPI', 'Factura MOB 893', '2017-02-01', 0),
(0017, '61', NULL, '0', 0, 217, '235.00', '235.77', '0.77', '232.00', '0.00', '3.77', '0.00', '2', 'Operaciones no acepta el scrub por estar fuera de fecha. Mandar factura: 235.77', 'Notes...', '2017-02-01', 0),
(0018, '173', NULL, '0', 0, 48, '211.00', '211.50', '0.50', '193.50', '0.00', '18.00', '0.00', '2', 'Se acumula', 'Notes...', '2017-02-01', 0),
(0019, '19', NULL, '354', 0, 210, '162.00', '457.76', '295.76', '21.60', '0.00', '459.16', '0.00', '7', '', 'Factura MOB 882', '2017-02-01', 0),
(0020, '151', NULL, '0', 0, 150, '147.00', '0.00', '0.00', '0.00', '0.00', '147.00', '0.00', '7', 'Notes...', 'Factura MOB 894', '2017-02-01', 0),
(0021, '11', NULL, '179', 0, 133, '146.00', '0.00', '0.00', '0.00', '0.00', '174.90', '0.00', '7', 'Notes...', 'FACTURA MOB 842.', '2017-02-01', 0),
(0022, '174', NULL, '354', 0, 122, '111.00', '113.12', '2.12', '103.55', '0.00', '113.12', '0.00', '7', 'Scrub:103.55. El AM no se acuerda, me lo ha confirmado todo.', 'FACTURA MOB 877', '2017-02-01', 0),
(0023, '41', NULL, '354', 0, 45, '109.00', '118.79', '9.79', '0.00', '0.00', '118.79', '0.00', '7', 'validado por mail a amparo', 'FACTURA MOB 875', '2017-02-01', 0),
(0024, '136', NULL, '0', 0, 56, '86.00', '94.35', '8.35', '94.35', '0.00', '0.00', '0.00', '2', 'VALIDADO: 0.00$. TODO SCRUB. Pruebas de fraude mandadas a afiliación. Ellos no mandan screenshots de placements', 'Notes...', '2017-02-01', 0),
(0025, '42', NULL, '179', 0, 210, '84.00', '0.00', '0.00', '0.00', '0.00', '156.40', '0.00', '7', 'esta factura es de ENERO!!! hay que hace la factura de Febrero por  153.2', 'Factura MOB 839', '2017-02-01', 0),
(0026, '138', NULL, '0', 0, 57, '68.00', '68.40', '0.40', '0.00', '0.00', '68.40', '0.00', '2', 'Minimo: 100$', 'Notes...', '2017-02-01', 0),
(0027, '147', NULL, '0', 0, 137, '54.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0028, '150', NULL, '0', 0, 73, '48.00', '46.19', '-1.81', '0.00', '0.00', '46.19', '46.19', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0029, '13', NULL, '0', 0, 5, '46.00', '0.00', '0.00', '0.00', '0.00', '46.00', '0.00', '2', 'hay diferencia con Rafa, igual no pagan por error nuestro', 'Notes...', '2017-02-01', 0),
(0030, '143', NULL, '0', 0, 34, '46.00', '46.40', '0.40', '0.00', '0.00', '46.40', '0.00', '2', 'Minimo: 250$', 'Notes...', '2017-02-01', 0),
(0031, '32', NULL, '0', 0, 52, '45.00', '46.30', '1.30', '0.00', '0.00', '46.30', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0032, '153', NULL, '0', 0, 21, '34.00', '36.20', '2.20', '0.00', '0.00', '36.20', '0.00', '7', '', 'Factura MOB 937 que incluye Febrero y Marzo', '2017-02-01', 0),
(0033, '54', NULL, '179', 0, 17, '29.00', '36.02', '7.02', '0.00', '0.00', '36.02', '0.00', '7', 'Notes...', 'Factura MOB 862', '2017-02-01', 0),
(0034, '40', NULL, '0', 0, 33, '29.00', '65.99', '36.99', '0.00', '0.00', '0.00', '0.00', '2', 'Se genera la factura en su plataforma. Mirar Avazu CPA.', 'Notes...', '2017-02-01', 0),
(0035, '17', NULL, '0', 0, 36, '29.00', '0.00', '0.00', '0.00', '9.00', '37.75', '0.00', '2', '18,2 HO hay que añadir', 'Notes...', '2017-02-01', 0),
(0036, '1', NULL, '179', 0, 92, '27.00', '0.00', '0.00', '0.00', '0.00', '27.60', '0.00', '7', 'Notes...', 'NEOMOBILE SPA  Via della Sierra Nevada, 60 00144 - Roma, Italia  Vat Number      IT02927500542', '2017-02-01', 0),
(0037, '176', NULL, '0', 0, 12, '19.00', '23.64', '4.64', '0.00', '0.00', '23.64', '0.00', '2', '', 'Notes...', '2017-02-01', 0),
(0038, '75', NULL, '0', 0, 15, '18.00', '18.88', '0.88', '0.00', '0.00', '18.88', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0039, '60', NULL, '0', 0, 20, '27.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'añadido HO - se factura con CPI', 'Notes...', '2017-02-01', 0),
(0040, '146', NULL, '179', 0, 3, '17.00', '15.74', '-1.26', '0.00', '0.00', '15.74', '0.00', '7', 'Notes...', 'Factura MOB 864', '2017-02-01', 0),
(0041, '14', NULL, '37', 0, 8, '15.00', '0.00', '0.00', '0.00', '0.00', '27.40', '0.00', '7', '+ 37,7  HO', '3D MOBILE 3DM SAS - Factura MOB 857 / Abono AMOB74 / Factura MOB 883', '2017-02-01', 0),
(0042, '167', NULL, '0', 0, 13, '13.00', '13.93', '0.93', '13.93', '0.00', '0.00', '0.00', '2', 'Validado 0.00', 'Notes...', '2017-02-01', 0),
(0043, '159', NULL, '73', 0, 25, '13.00', '4388.51', '4375.51', '0.00', '0.00', '4398.67', '0.00', '7', 'Confirmados', 'FACTURA MOB 887 ESTA EN EUROS!!!', '2017-02-01', 0),
(0044, '148', NULL, '0', 0, 17, '11.00', '16.42', '5.42', '0.00', '0.00', '16.42', '0.00', '2', 'Confirman el 15th', 'Notes...', '2017-02-01', 0),
(0045, '20', NULL, '179', 0, 13, '9.00', '0.00', '0.00', '0.00', '0.00', '40.80', '0.00', '7', 'Notes...', 'Factura MOB 861', '2017-02-01', 0),
(0046, '36', NULL, '0', 0, 14, '7.00', '7.18', '0.18', '0.00', '0.00', '7.18', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0047, '26', NULL, '0', 0, 2, '5.00', '0.00', '0.00', '0.00', '0.00', '5.00', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0048, '161', NULL, '0', 0, 3, '5.00', '0.00', '0.00', '0.00', '0.00', '5.00', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0049, '139', NULL, '0', 0, 13, '4.00', '0.00', '0.00', '0.00', '0.00', '10.80', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0050, '135', NULL, '0', 0, 2, '2.00', '0.00', '0.00', '0.00', '0.00', '2.00', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0051, '141', NULL, '0', 0, 1, '21.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'revenue HO - no sabemos de que viene la diferencia', 'Notes...', '2017-02-01', 0),
(0052, '168', NULL, '0', 0, 1, '1.00', '0.00', '0.00', '0.00', '0.00', '1.00', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0053, '33', NULL, '0', 0, 1, '1.00', '0.00', '0.00', '0.00', '0.00', '1.00', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0054, '24', NULL, '0', 0, 1, '1.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0055, '152', NULL, '0', 0, 2, '1.00', '0.00', '0.00', '0.00', '0.00', '1.00', '0.00', '2', 'Notes...', 'Notes...', '2017-02-01', 0),
(0059, '131', NULL, '0', 0, 1, '21.28', '0.00', '0.00', '0.00', '0.00', '24.58', '0.00', '2', 'HO+Rafa', 'Notes...', '2017-02-01', 0),
(0064, '35', NULL, '0', 0, 1, '21.00', '0.00', '0.00', '0.00', '0.00', '21.00', '0.00', '2', 'Rev HO', 'Notes...', '2017-02-01', 0),
(0080, '5', NULL, '79', 0, 9, '0.00', '0.00', '0.00', '0.00', '0.00', '600.20', '0.00', '7', 'Desglose mail: 21$ Colombia - 9$ Honduras - México CykGames 569$ - Argentina CLaro 1.20$', 'Factura MOB 859', '2017-02-01', 0),
(0083, '171', NULL, '179', 0, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '304.00', '0.00', '7', 'Aun no está enviado!!!!', 'Factura MOB 852', '2017-02-01', 0),
(0086, '62', NULL, '3', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '117.60', '0.00', '7', 'CONFIRMADO', 'FACTURA MOB 876 - Nombre: Voiceweb Americas S.A. de C.V. Dirección: Colonia La Joya, Calle principal Bloque No. 1, Sector No. 1, Casa 2817, Tegucigalpa, Honduras. Ciudad: Tegucigalpa, Honduras. Código', '2017-02-01', 0),
(0087, '44', NULL, '342', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '10062.30', '0.00', '7', 'ESTA EN EUROS!!!!!!', 'FACTURA MOB 843 (28/02/17)', '2017-02-01', 0),
(0095, '21', NULL, '67', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '7.20', '0.00', '2', '', '', '2017-02-01', 0),
(0096, '80', NULL, '168', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '190.02', '0.00', '7', 'euros', '', '2017-02-01', 0),
(0098, '79', NULL, '155', 0, 0, '37.23', '0.00', '0.00', '0.00', '0.00', '37.23', '0.00', '2', '', '', '2017-02-01', 0),
(0100, '82', NULL, '304', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '36.00', '0.00', '2', 'Rev Rafa', '', '2017-02-01', 0),
(0368, '130', NULL, '0', 0, 3, '5.00', '0.00', '0.00', '0.00', '0.00', '5.01', '0.00', '2', 'Notes...', 'Notes...', '2017-03-01', 0),
(0369, '50', NULL, '0', 0, 260, '907.00', '908.01', '1.01', '0.00', '0.00', '908.01', '0.00', '7', 'will be delayed up until 20 April 2017 due to the Easter Break holidays.  kindly submit your invoice by 28 April 201', 'Factura MOB915', '2017-03-01', 0),
(0370, '353', NULL, '0', 0, 866, '1296.00', '0.00', '0.00', '1241.40', '0.00', '1296.00', '0.00', '10', 'No cuadra por conversiones declinadas. Scrub esperando + pruebas', 'Factura MOB 928', '2017-03-01', 0),
(0371, '28', NULL, '0', 0, 643, '1264.00', '2120.92', '856.92', '0.00', '0.00', '2120.92', '0.00', '7', 'Mnadar factura con el importe de platf.Adv', 'Factura MOB 912', '2017-03-01', 0),
(0372, '172', NULL, '0', 0, 3098, '6711.00', '0.00', '0.00', '0.00', '0.00', '6822.55', '0.00', '7', 'Notes...', 'Factura MOB910', '2017-03-01', 0),
(0375, '143', NULL, '0', 0, 295, '470.00', '0.00', '0.00', '0.00', '0.00', '470.00', '0.00', '7', 'until 20th', 'Factura MOB916', '2017-03-01', 0),
(0376, '138', NULL, '0', 0, 41, '49.00', '30.72', '-18.28', '0.00', '0.00', '30.72', '0.00', '2', 'Notes...', 'Notes...', '2017-03-01', 0),
(0377, '35', NULL, '0', 0, 9, '27.00', '27.00', '0.00', '0.00', '0.00', '27.00', '0.00', '2', 'Notes...', 'Notes...', '2017-03-01', 0),
(0379, '9', NULL, '0', 0, 1604, '1781.00', '1335.89', '-445.11', '0.00', '0.00', '1335.89', '0.00', '7', '', 'Factura MOB 913', '2017-03-01', 0),
(0381, '68', NULL, '0', 0, 154, '308.00', '0.00', '0.00', '0.00', '0.00', '357.73', '0.00', '7', 'until 20th', 'Factura MOB918', '2017-03-01', 0),
(0382, '170', NULL, '0', 0, 52, '52.00', '0.00', '0.00', '0.00', '0.00', '46.00', '0.00', '2', 'January 368.90 + March 46.00=414.90.  Please use invoice date 31-03-2017', 'Notes...', '2017-03-01', 0),
(0383, '39', NULL, '0', 0, 1, '4.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Acumular para mes siguiente', 'Notes...', '2017-03-01', 0),
(0384, '153', NULL, '0', 0, 704, '1047.00', '1057.75', '10.75', '351.40', '0.00', '706.35', '0.00', '7', 'Modificada cantidad a validar el 03.05. On hold 36.00$ sin report.Además, hay que facturar además 36,20$ de febrero', 'Factura MOB 937 : 742.55 USD que incluye Febrero y Marzo', '2017-03-01', 0),
(0385, '25', NULL, '0', 0, 2, '7.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Validado en Layroute CPA', 'Notes...', '2017-03-01', 0),
(0386, '360', NULL, '0', 0, 2, '7.00', '0.00', '0.00', '0.00', '0.00', '7.00', '0.00', '4', 'Notes...', 'Notes...', '2017-03-01', 0),
(0387, '356', NULL, '0', 0, 1, '1.00', '0.00', '0.00', '0.00', '0.00', '1.00', '0.00', '4', 'Notes...', 'Notes...', '2017-03-01', 0),
(0388, '3', NULL, '0', 0, 575, '1382.00', '0.00', '0.00', '1360.38', '0.00', '19.60', '0.00', '2', 'Notes...', 'Notes...', '2017-03-01', 0),
(0390, '157', NULL, '0', 0, 26, '17.00', '0.00', '0.00', '0.00', '0.00', '18.40', '0.00', '7', 'Notes...', 'Factura MOB 967 junto con abril', '2017-03-01', 0),
(0391, '163', NULL, '0', 0, 1624, '1867.00', '0.00', '0.00', '0.00', '0.00', '2039.60', '0.00', '7', 'Notes...', 'Factura MOB 904', '2017-03-01', 0),
(0392, '139', NULL, '0', 0, 5, '3.00', '0.00', '0.00', '0.00', '0.00', '3.00', '0.00', '2', 'Notes...', 'Notes...', '2017-03-01', 0),
(0394, '43', NULL, '0', 0, 1099, '1955.00', '0.00', '0.00', '0.00', '0.00', '2053.65', '0.00', '7', 'Glamour 548,4 - Mainstream 1505,25', 'Factura MOB 897', '2017-03-01', 0),
(0395, '359', NULL, '0', 0, 13, '32.00', '0.00', '0.00', '0.00', '0.00', '33.50', '0.00', '2', 'no tienen plataforma', 'Notes...', '2017-03-01', 0),
(0396, '60', NULL, '0', 0, 18, '18.00', '0.00', '0.00', '0.00', '0.00', '13.20', '0.00', '7', 'Notes...', 'Factura MOB1050', '2017-03-01', 0),
(0397, '160', NULL, '0', 0, 4, '3.00', '0.00', '0.00', '0.00', '0.00', '3.00', '0.00', '4', 'Notes...', 'Notes...', '2017-03-01', 0),
(0398, '134', NULL, '0', 0, 20, '96.00', '90.19', '-5.81', '0.00', '0.00', '90.19', '0.00', '7', 'Notes...', 'Factura MOB 1029', '2017-03-01', 0),
(0399, '140', NULL, '0', 0, 1, '2.00', '0.00', '0.00', '0.00', '0.00', '2.00', '0.00', '4', 'Notes...', 'Notes...', '2017-03-01', 0),
(0401, '319', 191, '138', 0, 1, '23.00', '0.00', '0.00', '0.00', '0.00', '23.00', '0.00', '10', 'Notes...', 'Factura MOB1069', '2017-03-01', 0),
(0402, '308', NULL, '0', 0, 27, '55.00', '0.00', '0.00', '0.00', '0.00', '66.00', '0.00', '7', 'Notes...', 'Factura MOB 944 que incluye tambien febrero y abril', '2017-03-01', 0),
(0404, '31', NULL, '0', 0, 7, '24.00', '0.00', '0.00', '0.00', '0.00', '51.40', '0.00', '2', 'Notes...', 'Notes...', '2017-03-01', 0),
(0405, '164', NULL, '0', 0, 217, '133.00', '0.00', '0.00', '0.00', '0.00', '145.53', '0.00', '7', 'enviar fv a: publishers@headwaydigital.com', 'Factura MOB911', '2017-03-01', 0),
(0407, '83', NULL, '0', 0, 15, '114.00', '149.44', '35.44', '0.00', '0.00', '149.44', '0.00', '7', 'Hay 2 cuentas de buongiorno. FACTURAR DESGLOSADO POR OFERTA', 'Factura MOB 903', '2017-03-01', 0),
(0408, '4', NULL, '0', 0, 1219, '10863.00', '0.00', '0.00', '0.00', '0.00', '11923.70', '0.00', '7', 'Notes...', 'Factura MOB 901', '2017-03-01', 0),
(0409, '11', NULL, '0', 0, 1175, '1291.00', '1315.60', '24.60', '0.00', '0.00', '1315.60', '0.00', '7', 'Notes...', 'Factura MOB 895', '2017-03-01', 0),
(0410, '155', NULL, '0', 0, 6, '10.00', '0.00', '0.00', '0.00', '0.00', '18.46', '0.00', '2', 'Notes...', 'Notes...', '2017-03-01', 0),
(0411, '1', NULL, '0', 0, 3, '1.00', '0.00', '0.00', '0.00', '0.00', '1.00', '0.00', '4', 'Notes...', 'Notes...', '2017-03-01', 0),
(0412, '306', NULL, '0', 0, 191, '114.00', '0.00', '0.00', '0.00', '0.00', '119.13', '0.00', '7', 'Notes...', 'Factura MOB919', '2017-03-01', 0),
(0413, '342', NULL, '0', 0, 10, '9.00', '0.00', '0.00', '0.00', '0.00', '9.00', '0.00', '2', 'Notes...', 'Notes...', '2017-03-01', 0),
(0415, '305', NULL, '0', 0, 1, '22.00', '0.00', '0.00', '0.00', '0.00', '22.00', '0.00', '7', 'la van a pagar con el monto de Mayo (no se envia factura)  crear factura a parte por nosotros', 'Facturado MOB 984', '2017-03-01', 0),
(0416, '24', NULL, '0', 0, 1477, '1799.00', '2195.00', '396.00', '563.40', '0.00', '1769.93', '0.00', '7', 'Antes estaba facturado NoOk$', 'Factura MOB 927', '2017-03-01', 0),
(0417, '57', NULL, '0', 0, 266, '878.00', '892.25', '14.25', '0.00', '0.00', '892.25', '0.00', '7', 'Send invoice to: billing@adxperience.com(Amparo, 12/04); mandado el email', 'Factura MOB914', '2017-03-01', 0),
(0418, '32', NULL, '0', 0, 6, '4.00', '0.00', '0.00', '0.00', '0.00', '4.90', '51.20', '2', 'Notes...', 'Notes...', '2017-03-01', 0),
(0419, '175', NULL, '0', 0, 8, '36.00', '0.00', '0.00', '0.00', '0.00', '40.50', '0.00', '7', 'Notes...', 'Factura MOB 979', '2017-03-01', 0),
(0420, '17', NULL, '0', 0, 3366, '4172.00', '4178.95', '6.95', '0.00', '0.00', '4178.95', '37.75', '10', 'van a pagar solo 2,197.69 por error nuestra plataforma', 'Factura MOB 926', '2017-03-01', 0),
(0421, '2', NULL, '0', 0, 7364, '20978.00', '20561.34', '-416.66', '7961.86', '10800.00', '12613.96', '0.00', '10', 'en validado está lo confirmado (1813.96$) más lo de on hold ya que llevamos demasiado tiempo esperando la confirmación', 'Facturan desde su plataforma - Factura MOB 925', '2017-03-01', 0),
(0422, '223', NULL, '0', 0, 28, '59.00', '0.00', '0.00', '0.00', '0.00', '59.03', '0.00', '2', 'please invoice to this company and add as description: Mobile Traffic   Company Name: Chilli ads, SA de CV Contact: Edgar Salazar E-mail: publishers@chilliads.com Phone: 0181-82-44-01-65 State: Nuevo ', 'Notes...', '2017-03-01', 0),
(0423, '14', NULL, '0', 0, 206, '267.80', '0.00', '0.00', '0.00', '0.00', '236.60', '0.00', '7', 'Notes...', 'Notes...', '2017-03-01', 0),
(0424, '10', NULL, '0', 0, 20, '480.00', '423.02', '-56.98', '0.00', '0.00', '0.00', '0.00', '2', 'Esperar que se genere la factura en su plataforma', 'no mandar factura - plataforma', '2017-03-01', 0),
(0425, '135', NULL, '0', 0, 224, '46.00', '0.00', '0.00', '0.00', '0.00', '43.00', '0.00', '2', 'invoicing@affle.com', '', '2017-03-01', 0),
(0427, '162', NULL, '0', 0, 18046, '11729.00', '0.00', '0.00', '11729.15', '0.00', '0.00', '0.00', '2', 'Numeros aprobados en Mobquid CPA. Todo esto es scrub.', 'Notes...', '2017-03-01', 0),
(0429, '147', NULL, '0', 0, 113, '50.31', '0.00', '0.00', '0.00', '0.00', '50.31', '50.31', '4', 'Notes...', 'Notes...', '2017-03-01', 0),
(0430, '142', NULL, '0', 0, 202, '171.00', '172.55', '1.55', '0.00', '0.00', '172.55', '0.00', '7', 'Confirman el 18th', 'Factura MOB920', '2017-03-01', 0),
(0431, '173', NULL, '0', 0, 494, '705.00', '0.00', '0.00', '336.24', '160.20', '199.71', '18.00', '7', 'Mes que viene revisar ON HOLD', 'Factura MOB 909', '2017-03-01', 0),
(0432, '171', NULL, '0', 0, 2868, '5908.00', '0.00', '0.00', '0.00', '0.00', '5765.50', '0.00', '7', 'Diferencia de 150$ - se supone que es por zona horaria, debería pasarse al abril', 'Factura MOB 899', '2017-03-01', 0),
(0434, '145', NULL, '0', 0, 32, '43.00', '0.00', '0.00', '0.00', '0.00', '43.50', '0.00', '2', 'Mandado el email, informaron que confirman el 19th', 'Notes...', '2017-03-01', 0),
(0438, '178', NULL, '0', 0, 1, '1.00', '0.00', '0.00', '0.00', '0.00', '1.00', '0.00', '4', 'Notes...', 'Notes...', '2017-03-01', 0),
(0439, '364', NULL, '0', 0, 4, '4.00', '0.00', '0.00', '0.00', '0.00', '4.00', '0.00', '4', 'Notes...', 'Notes...', '2017-03-01', 0),
(0448, '151', NULL, '0', 0, 1093, '606.00', '0.00', '0.00', '0.00', '0.00', '385.00', '0.00', '7', 'En scrubs hay más por overcap. Abonar factura anterior (385eur) y facturar la nueva cantidad', 'Factura MOB917', '2017-03-01', 0),
(0450, '42', NULL, '0', 0, 276, '110.00', '0.00', '0.00', '0.00', '0.00', '115.65', '0.00', '7', 'Notes...', 'Factura MOB 900', '2017-03-01', 0),
(0452, '83', NULL, '0', 0, 6, '51.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Hay 2 cuentas de buongiorno. Validado en la otra', 'Notes...', '2017-03-01', 0),
(0456, '146', NULL, '0', 0, 2, '3.00', '0.00', '0.00', '0.00', '0.00', '3.00', '0.00', '4', 'Notes...', 'Notes...', '2017-03-01', 0),
(0459, '371', NULL, '0', 0, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '9774.96', '0.00', '7', 'Notes...', 'Factura MOB 902', '2017-03-01', 0),
(0468, '320', 377, '67', 0, 0, '292.40', '0.00', '0.00', '0.00', '0.00', '295.11', '0.00', '7', '', 'Factura MOB 896', '2017-03-01', 0),
(0469, '20', 377, '372', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '4.80', '0.00', '2', '', '', '2017-03-01', 0),
(0472, '178', NULL, '0', 0, 1, '1.00', '0.00', '0.00', '0.00', '0.00', '1.00', '0.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0473, '170', NULL, '0', 0, 2, '2.00', '0.00', '0.00', '0.00', '0.00', '2.00', '48.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0474, '60', NULL, '0', 0, 5, '2.00', '0.00', '0.00', '0.00', '0.00', '2.00', '0.00', '7', 'Notes...', 'Notes...', '2017-04-01', 0),
(0475, '138', NULL, '0', 0, 1, '2.00', '0.00', '0.00', '0.00', '0.00', '2.00', '101.12', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0476, '143', NULL, '0', 0, 4, '3.00', '0.00', '0.00', '0.00', '0.00', '3.00', '49.40', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0477, '358', NULL, '0', 0, 1, '5.00', '0.00', '0.00', '0.00', '0.00', '5.00', '0.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0478, '373', NULL, '0', 0, 4, '5.00', '0.00', '0.00', '0.00', '0.00', '5.00', '0.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0479, '363', NULL, '0', 0, 11, '6.00', '6.70', '0.70', '0.00', '0.00', '6.70', '0.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0480, '48', NULL, '0', 0, 19, '9.00', '0.00', '0.00', '0.00', '0.00', '9.00', '0.00', '4', 'en su plataforma aparece 0.00', 'Notes...', '2017-04-01', 0),
(0481, '77', NULL, '0', 0, 67, '10.36', '10.36', '0.00', '0.00', '0.00', '10.36', '0.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0482, '306', NULL, '0', 0, 25, '14.00', '0.00', '0.00', '0.00', '0.00', '22.77', '0.00', '7', 'Notes...', 'Factura MOB 974', '2017-04-01', 0),
(0483, '159', NULL, '0', 0, 10, '214.43', '0.00', '0.00', '0.00', '0.00', '214.43', '0.00', '7', 'Notes...', 'Factura MOB 952', '2017-04-01', 0),
(0485, '157', NULL, '0', 0, 20, '16.00', '16.80', '0.80', '0.00', '0.00', '16.80', '0.00', '7', 'Notes...', 'Factura MOB 967 junto con Marzo', '2017-04-01', 0),
(0486, '342', NULL, '0', 0, 18, '16.00', '0.00', '0.00', '0.00', '0.00', '16.20', '25.20', '2', 'Notes...', 'Notes...', '2017-04-01', 0),
(0488, '134', NULL, '0', 0, 5, '24.00', '0.00', '0.00', '0.00', '0.00', '22.55', '0.00', '7', 'Notes...', 'Factura MOB 1030', '2017-04-01', 0),
(0489, '364', NULL, '0', 0, 24, '27.00', '27.60', '0.60', '0.00', '0.00', '27.60', '0.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0490, '160', NULL, '0', 0, 47, '30.00', '0.00', '0.00', '0.00', '0.00', '30.00', '0.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0491, '135', NULL, '0', 0, 187, '37.00', '0.00', '0.00', '0.00', '0.00', '35.00', '0.00', '7', 'mandar antes del 10. desglosar factura como mail mandado a cristina', 'Factura MOB944', '2017-04-01', 0),
(0492, '308', NULL, '0', 0, 11, '38.00', '0.00', '0.00', '0.00', '0.00', '34.23', '0.00', '7', 'Notes...', 'Factura MOB944 que incluye tambien febrero y marzo', '2017-04-01', 0),
(0493, '177', 415, '73', 0, 2, '39.00', '0.00', '0.00', '0.00', '0.00', '39.00', '13.50', '2', 'Notes...', 'Notes...', '2017-04-01', 0),
(0494, '19', NULL, '0', 0, 32, '47.00', '49.80', '2.80', '0.00', '0.00', '49.80', '49.80', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0495, '28', NULL, '0', 0, 45, '58.00', '103.11', '45.11', '0.00', '0.00', '103.11', '0.00', '10', 'Notes...', 'Factura MOB 956', '2017-04-01', 0),
(0496, '223', NULL, '0', 0, 20, '60.00', '0.00', '0.00', '0.00', '0.00', '60.00', '0.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0497, '9', NULL, '0', 0, 40, '67.00', '71.73', '4.73', '0.00', '0.00', '71.73', '71.73', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0499, '23', NULL, '0', 0, 99, '84.00', '0.00', '0.00', '0.00', '0.00', '99.00', '99.00', '2', 'pedido por mail', 'Notes...', '2017-04-01', 0),
(0500, '173', NULL, '0', 0, 36, '87.00', '87.39', '0.39', '0.00', '0.00', '87.39', '18.00', '4', 'Notes...', 'Notes...', '2017-04-01', 0),
(0501, '164', NULL, '0', 0, 160, '98.00', '100.32', '2.32', '0.00', '0.00', '100.32', '0.00', '7', 'Notes...', 'Factura MOB 947', '2017-04-01', 0),
(0502, '319', 191, '138', 0, 5, '93.00', '0.00', '0.00', '0.00', '0.00', '70.00', '23.00', '7', 'pedidos por mail', 'Factura MOB 1069', '2017-04-01', 0),
(0503, '31', NULL, '0', 0, 52, '93.00', '0.00', '0.00', '0.00', '0.00', '105.31', '0.00', '7', 'Notes...', 'Factura MOB 945', '2017-04-01', 0),
(0504, '367', NULL, '0', 0, 25, '100.00', '0.00', '0.00', '0.00', '0.00', '100.00', '0.00', '7', 'Notes...', 'Factura MOB943', '2017-04-01', 0),
(0505, '35', NULL, '0', 0, 43, '107.00', '107.50', '0.50', '0.00', '0.00', '107.50', '48.00', '10', 'Notes...', 'Factura MOB 955.', '2017-04-01', 0),
(0506, '353', NULL, '0', 0, 124, '115.00', '124.00', '9.00', '0.00', '0.00', '124.00', '0.00', '10', 'Notes...', 'FActura MOB 953.', '2017-04-01', 0),
(0507, '368', NULL, '0', 0, 51, '122.00', '0.00', '0.00', '0.00', '0.00', '125.79', '0.00', '7', 'FORWARD MEDIA COMMUNICATIONS SA DE CV  Calle: AV. PROLONGACION PASEO DE LA REFORMA No. 1015 Int: PISO 24,  Col. DESARROLLO CUAJIMALPA DE MORELOS, DISRITO FEDERAL  CP 05348 RFC: FMC1412123B6', 'Factura MOB 931 enviar a grissel.Mayorga@corp.terra.com', '2017-04-01', 0),
(0508, '14', NULL, '0', 0, 102, '132.00', '0.00', '0.00', '0.00', '0.00', '133.90', '0.00', '7', 'Notes...', 'Factura MOB 930', '2017-04-01', 0),
(0509, '83', NULL, '0', 0, 42, '239.00', '262.10', '23.10', '0.00', '0.00', '262.10', '0.00', '7', 'Notes...', 'Factura MOB940', '2017-04-01', 0),
(0510, '175', NULL, '0', 0, 23, '241.00', '294.00', '53.00', '0.00', '0.00', '294.00', '0.00', '7', 'Notes...', 'Factura MOB 934', '2017-04-01', 0),
(0511, '380', NULL, '0', 0, 175, '315.00', '315.00', '0.00', '0.00', '0.00', '315.00', '0.00', '7', 'Notes...', 'Factura MOB 957', '2017-04-01', 0),
(0513, '24', NULL, '0', 0, 344, '367.00', '370.40', '3.40', '0.00', '260.40', '108.80', '0.00', '7', 'lo que está on hold todavía no ha sido confirmado', 'Factura MOB 954', '2017-04-01', 0),
(0514, '153', NULL, '0', 0, 140, '436.00', '439.60', '3.60', '422.40', '10.80', '0.00', '0.00', '10', 'Hablado con Cristina para anular factura. Scrub notificado el 15(festivo en Madrid)', '', '2017-04-01', 0),
(0515, '174', NULL, '0', 0, 485, '533.00', '471.90', '-61.10', '0.00', '0.00', '471.90', '0.00', '7', 'Validado: 471.90. Diferencia -4 horas. Factura mandada incorrecta (543.40)', 'Factura MOB 951./ Abono AMOB94 y Factura MOB959', '2017-04-01', 0),
(0516, '2', NULL, '0', 0, 342, '569.00', '569.26', '0.26', '0.00', '0.00', '569.26', '0.00', '10', 'casi seguro todo estará validado ok hasta el 20 de mayo', 'Factura MOB 950', '2017-04-01', 0),
(0517, '11', NULL, '0', 0, 615, '672.00', '0.00', '0.00', '0.00', '0.00', '668.80', '0.00', '7', 'Notes...', 'Factura MOB 929', '2017-04-01', 0),
(0518, '163', NULL, '0', 0, 1224, '1407.00', '0.00', '0.00', '0.00', '0.00', '1408.90', '0.00', '7', 'validado por Skype a Camilla - pedido por mail', 'Factura MOB 946', '2017-04-01', 0),
(0519, '172', NULL, '0', 0, 886, '1437.00', '1444.30', '7.30', '0.00', '0.00', '1444.30', '0.00', '10', 'Notes...', 'Factura MOB 949', '2017-04-01', 0),
(0520, '43', NULL, '0', 0, 1191, '1862.00', '0.00', '0.00', '0.00', '0.00', '1905.30', '0.00', '7', 'Notes...', 'Factura MOB 933', '2017-04-01', 0),
(0521, '171', NULL, '0', 0, 1385, '2858.00', '0.00', '0.00', '0.00', '0.00', '2800.00', '0.00', '7', 'Notes...', 'Factura MOB939', '2017-04-01', 0),
(0522, '4', NULL, '0', 0, 860, '7740.00', '0.00', '0.00', '0.00', '0.00', '7740.00', '0.00', '10', 'Notes...', 'Factura MOB 948', '2017-04-01', 0),
(0523, '42', NULL, '0', 0, 132, '75.00', '0.00', '0.00', '0.00', '0.00', '76.05', '0.00', '7', 'Notes...', 'Factura MOB 1033', '2017-04-01', 0),
(0526, '305', NULL, '0', 0, 22, '893.00', '0.00', '0.00', '0.00', '0.00', '902.00', '0.00', '7', 'Notes...', 'Factura MOB 932', '2017-04-01', 0),
(0528, '174', 174, '163', 0, 0, '71.50', '71.50', '0.00', '0.00', '0.00', '71.50', '0.00', '7', '', 'Factura MOB 936', '2017-03-01', 0),
(0529, '320', 380, '80', 0, 0, '102.00', '104.10', '2.10', '0.00', '0.00', '104.10', '83.00', '7', 'no pasa por Affise - confirmado Juliana', 'Factura MOB942', '2017-04-01', 0),
(0530, '308', 380, '27', 0, 0, '102.09', '0.00', '0.00', '0.00', '0.00', '102.09', '0.00', '7', 'Ya se ha creado factura para febrero (844) - hay que cambiarla por una con el importe validado aquí', 'Factura MOB 944 qye incluye tambien marzo y abril', '2017-02-01', 0),
(0531, '67', 380, '142', 1000, 1, '3.80', '3.80', '0.00', '0.00', '0.00', '3.80', '0.00', '2', '', '', '2017-04-01', 0),
(0532, '1', 352, '372', 0, 0, '152.80', '0.00', '0.00', '0.00', '0.00', '152.80', '0.00', '7', 'Actividad 2016 Neomobile ecuador - ver desglose mail ', 'Factura MOB961', '2017-05-01', 0),
(0538, '42', NULL, '0', 0, 373117, '31.60', '0.00', '0.00', '0.00', '0.00', '34.00', '0.00', '7', 'Notes...', 'Factura MOB 1034', '2017-05-01', 0),
(0540, '166', NULL, '0', 0, 18181, '8.00', '0.00', '0.00', '0.00', '0.00', '8.00', '0.00', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0542, '164', NULL, '0', 0, 42799, '0.60', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-05-01', 0),
(0543, '145', NULL, '0', 0, 148467, '0.25', '0.00', '0.00', '0.00', '0.00', '0.00', '43.50', '0', 'Notes...', 'Notes...', '2017-05-01', 0),
(0544, '4', NULL, '0', 0, 297091, '6561.00', '0.00', '0.00', '0.00', '0.00', '6723.00', '0.00', '7', 'pedido por mail', 'Factura MOB 968', '2017-05-01', 0),
(0545, '171', NULL, '0', 0, 766636, '5544.00', '0.00', '0.00', '0.00', '0.00', '5578.00', '0.00', '7', 'a partir de junio nuevos datos de facturación', 'Factura MOB 966', '2017-05-01', 0),
(0546, '31', NULL, '0', 0, 1007257, '3063.13', '0.00', '0.00', '0.00', '0.00', '3054.40', '0.00', '7', 'Notes...', 'Factura MOB 970', '2017-05-01', 0),
(0547, '83', NULL, '0', 0, 1424513, '2741.36', '2759.48', '18.12', '0.00', '0.00', '2759.48', '0.00', '7', 'Notes...', 'Factura MOB 969', '2017-05-01', 0),
(0548, '43', NULL, '0', 0, 4164610, '1708.25', '0.00', '0.00', '0.00', '0.00', '1704.50', '0.00', '7', 'Notes...', 'Factura MOB 965', '2017-05-01', 0),
(0549, '382', NULL, '0', 0, 2582794, '365.90', '0.00', '0.00', '0.00', '0.00', '387.90', '0.00', '7', 'Colombia: $ 91,50 - EUR 296,40 convertido todo a auer', 'Factura MOB973 Company Information 		  Company 	  Tekka S.p.A 	  Address 	  Lungodora Colletta75 	 	  10153 Torino ITALIA 	  VAT 	  IT09479150014 	  amministrazione 	  Simonetta 	  s.capaldi@tekka.it', '2017-05-01', 0),
(0550, '163', NULL, '0', 0, 212804, '626.75', '0.00', '0.00', '0.00', '0.00', '627.15', '0.00', '7', '', 'Factura MOB 971', '2017-05-01', 0),
(0551, '305', NULL, '0', 0, 239248, '475.00', '0.00', '0.00', '0.00', '0.00', '475.00', '0.00', '7', 'no hace falta enviar, la envian ellos de su plataforma,', 'Factura MOB 962', '2017-05-01', 0),
(0552, '308', NULL, '0', 0, 226414, '447.67', '221.96', '-225.71', '0.00', '0.00', '415.44', '0.00', '7', 'había un error en su panel y calculó mal el cpa x subs', 'Factura MOB976', '2017-05-01', 0),
(0553, '368', NULL, '0', 0, 713608, '414.39', '0.00', '0.00', '0.00', '0.00', '427.54', '0.00', '7', ' FORWARD MEDIA COMMUNICATIONS SA DE CV  Calle: AV. PROLONGACION PASEO DE LA REFORMA No. 1015 Int: PISO 24,  Col. DESARROLLO CUAJIMALPA DE MORELOS, DISRITO FEDERAL  CP 05348  RFC: FMC1412123B6', 'Factura MOB 964', '2017-05-01', 0),
(0554, '380', NULL, '0', 0, 24565, '342.00', '0.00', '0.00', '0.00', '0.00', '351.00', '0.00', '7', '', 'Factura MOB 977', '2017-05-01', 0),
(0555, '11', NULL, '0', 0, 798334, '301.40', '0.00', '0.00', '0.00', '0.00', '311.30', '0.00', '7', 'DIGITAL VIRGO ESPAÑA, S.A.  Address: 	  C/ Juan Ignacio Luca de Tena, 1, 3ª Planta, 28027, Madrid  ID TAX: 	  A80060924', 'Factura MOB 963', '2017-05-01', 0),
(0556, '383', NULL, '0', 0, 97103, '124.00', '0.00', '0.00', '0.00', '0.00', '130.00', '0.00', '7', 'Plusmobile - Comtel International LLC Company Address: 2250 Sun Trust Int\'l Center, One SE 3 rd - USA - Vat Number: 98- 041-9205', 'Factura MOB 978', '2017-05-01', 0),
(0557, '175', NULL, '0', 0, 76623, '52.50', '0.00', '0.00', '0.00', '0.00', '42.00', '0.00', '2', '', 'Notes...', '2017-05-01', 0),
(0558, '60', NULL, '0', 0, 101870, '44.55', '0.00', '0.00', '0.00', '0.00', '44.00', '0.00', '7', 'Notes...', 'Factura MOB1050', '2017-05-01', 0),
(0559, '160', NULL, '0', 0, 420107, '43.60', '0.00', '0.00', '0.00', '0.00', '43.60', '0.00', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0561, '306', NULL, '0', 0, 101281, '25.16', '0.00', '0.00', '0.00', '0.00', '25.06', '0.00', '7', 'Notes...', 'Factura MOB 974', '2017-05-01', 0),
(0562, '364', NULL, '0', 0, 4139, '18.00', '0.00', '0.00', '0.00', '0.00', '18.00', '0.00', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0563, '384', NULL, '0', 0, 91061, '17.10', '17.10', '0.00', '0.00', '0.00', '17.10', '0.00', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0564, '223', NULL, '0', 0, 64962, '15.00', '0.00', '0.00', '0.00', '0.00', '15.00', '0.00', '4', 'Notes...', 'Notes...', '2017-05-01', 0),
(0565, '28', NULL, '0', 0, 48767, '14.35', '2.70', '-11.65', '0.00', '0.00', '2.70', '0.00', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0566, '385', NULL, '0', 0, 248454, '12.40', '12.40', '0.00', '0.00', '0.00', '12.40', '0.00', '7', 'Notes...', 'Factura MOB 1022', '2017-05-01', 0),
(0567, '63', NULL, '0', 0, 3530, '10.00', '0.00', '0.00', '0.00', '0.00', '10.00', '0.00', '2', 'Mandar a Crazy4Media Mobile Advertising &amp; Billing C/Arquitectura Nº2 Torre 11 Planta 7, Mod: del 1 al 7. 41015 Sevilla Vat: B90125741 finance@crazy4mediamobile.com', 'Notes...', '2017-05-01', 0),
(0570, '48', NULL, '0', 0, 12088, '8.84', '0.00', '0.00', '0.00', '0.00', '40.30', '0.00', '7', 'no sirve mandar factura - min 100 acumular', 'Factura MOB994', '2017-05-01', 0),
(0571, '77', NULL, '0', 0, 37160, '8.40', '8.68', '0.28', '0.00', '0.00', '8.68', '19.04', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0572, '386', NULL, '0', 0, 218219, '3.37', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-05-01', 0),
(0573, '173', NULL, '0', 0, 2825300, '2.70', '2.70', '0.00', '0.00', '0.00', '2.70', '108.09', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0574, '135', NULL, '0', 0, 67223, '2.38', '3.13', '0.75', '0.00', '0.00', '3.13', '48.13', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0575, '57', NULL, '0', 0, 31, '2.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0576, '378', NULL, '0', 0, 31, '2.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0577, '367', NULL, '0', 0, 32642, '1.92', '0.00', '0.00', '0.00', '0.00', '1.82', '0.00', '4', 'Notes...', 'Notes...', '2017-05-01', 0),
(0578, '363', NULL, '0', 0, 16270, '1.80', '1.80', '0.00', '0.00', '0.00', '1.80', '0.00', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0579, '379', NULL, '0', 0, 33, '1.60', '0.00', '0.00', '0.00', '0.00', '1.60', '0.00', '4', 'Notes...', 'Notes...', '2017-05-01', 0),
(0580, '373', NULL, '0', 0, 22235, '1.35', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0581, '153', NULL, '0', 0, 348304, '1.32', '1.32', '0.00', '0.00', '0.00', '1.32', '38.88', '2', 'Notes...', 'Notes...', '2017-05-01', 0),
(0582, '164', NULL, '0', 0, 42799, '0.67', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-05-01', 0),
(0583, '145', NULL, '0', 0, 148467, '0.28', '0.00', '0.00', '0.00', '0.00', '0.00', '43.50', '0', 'Notes...', 'Notes...', '2017-05-01', 0),
(0584, '361', NULL, '0', 0, 80660, '0.22', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-05-01', 0),
(0585, '387', NULL, '0', 0, 4, '0.20', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'test', 'Notes...', '2017-05-01', 0),
(0586, '382', 239, '376', 0, 0, '832.32', '0.00', '0.00', '0.00', '0.00', '704.77', '0.00', '7', 'Company  Tekka Digital SA Address Via Balestra 12  6900 Lugano (CH)  VAT  CHE-303.540.263', 'Factura MOB972   send the invoice to the address Amministrazione@tekkadigital.com  ', '2017-05-01', 0),
(0587, '320', 387, '376', 0, 0, '81.30', '0.00', '0.00', '0.00', '0.00', '83.00', '0.00', '2', '', '', '2017-05-01', 0),
(0588, '24', 387, '144', 1000, 1, '260.40', '260.40', '0.00', '0.00', '0.00', '260.40', '0.00', '7', 'se refiere a lo que estaba on hold el mes de abril (nunca han aportado pruebas de scrubs)', 'Factura MOB 975', '2017-05-01', 0),
(0589, '17', 387, '338', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '-1981.26', '0.00', '7', 'ABONO de la factura MOB926', 'Abono AMOB96', '2017-03-01', 0),
(0590, '2', 387, '338', 0, 0, '-6435.44', '0.00', '0.00', '0.00', '0.00', '-6435.44', '0.00', '7', 'para hacer el abono de la factura 925', 'Abono AMOB97', '2017-03-01', 0),
(0591, '83', NULL, '0', 0, 1895, '27851.37', '28207.57', '356.20', '0.00', '0.00', '28207.57', '0.00', '7', 'recordar hacer gesglose', 'Factura MOB997', '2017-06-01', 5),
(0592, '4', NULL, '0', 0, 688, '6174.00', '0.00', '0.00', '0.00', '0.00', '6723.00', '0.00', '7', '', 'Factura MOB998', '2017-06-01', 6),
(0593, '171', NULL, '0', 0, 1748, '3443.50', '0.00', '0.00', '0.00', '0.00', '3405.50', '0.00', '7', 'facturado a nombre de Feraz Media', 'Factura MOB986', '2017-06-01', 0),
(0594, '168', NULL, '0', 0, 279, '2528.75', '2545.57', '16.82', '0.00', '0.00', '2556.57', '1.00', '7', '', 'Factura MOB1000', '2017-06-01', 8),
(0595, '43', NULL, '0', 0, 1896, '2383.30', '0.00', '0.00', '0.00', '0.00', '2390.20', '0.00', '7', 'Notes...', 'Factura MOB993', '2017-06-01', 0),
(0596, '382', NULL, '0', 0, 1538, '1965.36', '0.00', '0.00', '0.00', '0.00', '715.00', '0.00', '7', 'USD 558,00+EUR 224,20 ', 'Factura MOB1006 y MOB1007 Provisión Tc 1,1412 Importe 626,53 €', '2017-06-01', 24),
(0597, '163', NULL, '0', 0, 1083, '1245.45', '0.00', '0.00', '0.00', '0.00', '1246.00', '0.00', '7', '', 'Factura MOB999', '2017-06-01', 7),
(0598, '379', NULL, '0', 0, 1586, '1258.40', '0.00', '0.00', '0.00', '0.00', '1258.40', '1.60', '7', 'no contesta, enviado el mensaje por whatsapp insistiendo', 'Factura MOB1004Provisión: Tc 1,1412 Importe 1102,69 €', '2017-06-01', 22),
(0599, '31', NULL, '0', 0, 653, '890.37', '0.00', '0.00', '0.00', '0.00', '1270.61', '51.40', '7', 'confirman sobre el 15th probablemente', 'Factura MOB1002', '2017-06-01', 9),
(0600, '383', NULL, '0', 0, 422, '422.00', '0.00', '0.00', '0.00', '0.00', '429.00', '0.00', '7', 'COLOMBIA Plusmobile Communications SA  Calle 94A # 11A ­ 27 of 402  Bogotá ­ Bogotá  111051 ­ Colombia Nit: 900041186-1', 'Factura MOB988', '2017-06-01', 0),
(0601, '305', NULL, '0', 0, 9, '359.92', '0.00', '0.00', '0.00', '0.00', '350.00', '0.00', '7', 'no hay que enviarles facturas', 'Factura MOB 985', '2017-06-01', 0),
(0602, '11', NULL, '0', 0, 310, '341.00', '0.00', '0.00', '0.00', '0.00', '331.10', '0.00', '7', 'Notes...', 'Factura MOB990', '2017-06-01', 0),
(0603, '368', NULL, '0', 0, 133, '339.53', '0.00', '0.00', '0.00', '0.00', '346.07', '0.00', '7', 'Notes...', 'Factura MOB989', '2017-06-01', 0),
(0604, '223', NULL, '0', 0, 42, '165.00', '0.00', '0.00', '0.00', '0.00', '165.00', '134.03', '7', 'Notes...', 'factura MOB1008 Provisión Tc 1,1412 Importe 144,58 €', '2017-06-01', 27),
(0605, '166', NULL, '0', 0, 16, '146.25', '0.00', '0.00', '0.00', '0.00', '128.00', '8.20', '7', 'Notes...', 'Factura MOB991', '2017-06-01', 0),
(0606, '385', NULL, '0', 0, 120, '118.00', '118.00', '0.00', '0.00', '0.00', '118.00', '12.40', '7', 'Notes...', 'Factura MOB1011-Provisión Tc 1,1412 Importe 103,39 €', '2017-06-01', 29),
(0607, '14', NULL, '0', 0, 94, '98.20', '0.00', '0.00', '98.20', '0.00', '0.00', '0.00', '2', 'ver scrubs', 'Notes...', '2017-06-01', 0),
(0609, '314', NULL, '0', 0, 19, '92.50', '0.00', '0.00', '0.00', '0.00', '81.56', '0.00', '7', 'facturar aunque es menos de 100$', 'Factura MOB992.', '2017-06-01', 0),
(0610, '175', NULL, '0', 0, 6, '61.50', '0.00', '0.00', '0.00', '0.00', '61.50', '42.00', '7', 'Notes...', 'Factura MOB1001', '2017-06-01', 0),
(0611, '48', NULL, '0', 0, 111, '57.72', '0.00', '0.00', '0.00', '0.00', '67.87', '9.00', '7', 'no se manda factura - min 100 se acumula con Mayo', 'Factura MOB994', '2017-06-01', 0),
(0612, '308', 194, '39', 0, 262, '53.31', '0.00', '0.00', '0.00', '0.00', '49.30', '0.00', '7', 'Notes...', 'Factura MOB1061', '2017-06-01', 0),
(0613, '386', NULL, '0', 0, 6, '37.71', '33.00', '-4.71', '0.00', '0.00', '33.00', '0.00', '2', 'Notes...', 'Notes...', '2017-06-01', 0),
(0614, '367', NULL, '0', 0, 39, '37.56', '0.00', '0.00', '0.00', '0.00', '37.56', '1.82', '7', 'Notes...', 'Factura MOB1038', '2017-06-01', 0),
(0615, '387', NULL, '0', 0, 45, '29.13', '29.25', '0.12', '0.00', '0.00', '29.25', '0.00', '2', 'Notes...', 'Notes...', '2017-06-01', 0),
(0616, '388', NULL, '0', 0, 4, '21.94', '0.00', '0.00', '0.00', '0.00', '21.87', '0.00', '2', 'Notes...', 'Notes...', '2017-06-01', 0),
(0617, '164', NULL, '0', 0, 33, '19.85', '0.00', '0.00', '0.00', '0.00', '44.28', '0.00', '2', 'Notes...', 'Notes...', '2017-06-01', 0),
(0618, '60', NULL, '0', 0, 7, '17.50', '0.00', '0.00', '0.00', '0.00', '17.50', '59.20', '7', 'Notes...', 'Factura MOB1050', '2017-06-01', 0),
(0619, '373', NULL, '0', 0, 4, '14.91', '13.96', '-0.95', '0.00', '0.00', '13.96', '5.00', '2', 'Notes...', 'Notes...', '2017-06-01', 0),
(0620, '134', NULL, '0', 0, 1, '11.43', '10.00', '-1.43', '0.00', '0.00', '10.00', '122.74', '7', 'revenue en $', 'Factura MOB 1031', '2017-06-01', 0),
(0621, '363', NULL, '0', 0, 9, '7.20', '0.00', '0.00', '0.00', '0.00', '7.20', '8.50', '4', 'Notes...', 'Notes...', '2017-06-01', 0),
(0622, '28', NULL, '0', 0, 9, '4.50', '5.50', '1.00', '0.00', '0.00', '5.50', '2.70', '2', 'Notes...', 'Notes...', '2017-06-01', 0),
(0623, '160', NULL, '0', 0, 3, '2.40', '0.00', '0.00', '0.00', '0.00', '2.40', '38.10', '4', 'Notes...', 'Notes...', '2017-06-01', 0),
(0624, '364', NULL, '0', 0, 2, '2.40', '0.00', '0.00', '0.00', '0.00', '2.40', '52.00', '2', 'Notes...', 'Notes...', '2017-06-01', 0),
(0625, '149', NULL, '0', 0, 1, '1.20', '0.00', '0.00', '0.00', '0.00', '1.20', '0.00', '4', 'Notes...', 'Notes...', '2017-06-01', 0),
(0627, '345', NULL, '0', 0, 1, '0.84', '0.00', '0.00', '0.00', '0.00', '0.84', '0.00', '4', 'Notes...', 'Notes...', '2017-06-01', 0),
(0628, '389', NULL, '0', 0, 1, '0.82', '0.00', '0.00', '0.00', '0.00', '0.82', '0.00', '4', 'Notes...', 'Notes...', '2017-06-01', 0),
(0629, '42', NULL, '0', 0, 1, '0.25', '0.00', '0.00', '0.00', '0.00', '0.25', '110.30', '2', 'Notes...', 'Notes...', '2017-06-01', 0),
(0630, '320', 389, '166', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '578.00', '0.00', '7', '', 'Factura MOB987', '2017-06-01', 0),
(0632, '391', 177, '329', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '1081.90', NULL, '7', '0 en affise porque no estaban separados los tekkas', 'Factura MOB1005 Amministrazione@tekkadigital.com PROVISION Tc 1,1412 Importe 948,04 €', '2017-06-01', 26),
(0633, '83', NULL, '0', 0, 2933, '31397.86', '0.00', '0.00', '14040.00', '0.00', '18192.46', '0.00', '7', 'para abonar (se factura BR por separado)', 'Factura MOB 1028/ AMOB111 /  MOB 1044', '2017-07-01', 48),
(0634, '4', NULL, '0', 0, 637, '5733.00', '0.00', '0.00', '0.00', '0.00', '5733.00', '0.00', '7', 'Notes...', 'Factura MOB1019', '2017-07-01', 41),
(0635, '43', NULL, '0', 0, 3507, '3334.70', '0.00', '0.00', '0.00', '0.00', '3330.25', '0.00', '7', 'Notes...', 'Factura MOB 1018', '2017-07-01', 38),
(0636, '392', NULL, '0', 0, 1376, '1440.23', '0.00', '0.00', '0.00', '0.00', '1256.65', '0.00', '7', 'Tekka Digital SA 	  Address 	  Via Balestra 12 	 	  6900 Lugano (CH) VAT  CHE-303.540.263  send the invoice to amministrazione@tekkadigital.com', 'Factura MOB1036', '2017-07-01', 53),
(0637, '368', NULL, '0', 0, 320, '778.08', '0.00', '0.00', '0.00', '0.00', '859.46', '0.00', '7', 'Notes...', 'Factura MOB 1015', '2017-07-01', 35),
(0638, '305', NULL, '0', 0, 18, '705.93', '0.00', '0.00', '0.00', '0.00', '595.88', '0.00', '7', 'No hay que enviar facturas, revenue en $', 'Factura MOB 1020', '2017-07-01', 43),
(0639, '314', NULL, '0', 0, 134, '670.00', '0.00', '0.00', '0.00', '0.00', '566.00', '0.00', '7', 'Revenue en $', 'Factura MOB 1016', '2017-07-01', 39),
(0640, '386', NULL, '0', 0, 77, '501.72', '0.00', '0.00', '0.00', '0.00', '880.00', '33.00', '7', '5.5€ * 160 leads jazztel', 'Factura MOB 1035', '2017-07-01', 49),
(0641, '168', NULL, '0', 0, 38, '494.45', '501.77', '7.32', '0.00', '0.00', '501.77', '0.00', '7', 'Notes...', 'Factura MOB 1045', '2017-07-01', 58),
(0642, '345', NULL, '0', 0, 261, '360.36', '0.00', '0.00', '0.00', '0.00', '368.30', '0.88', '7', 'Notes...', 'Factura MOB1026', '2017-07-01', 46),
(0643, '383', NULL, '0', 0, 263, '263.00', '0.00', '0.00', '0.00', '0.00', '269.00', '0.00', '7', 'poner en concepto \"campañas comtel\"- Facturar a COLOMBIA Plusmobile Communications SA  Calle 94A # 11A ­ 27 of 402  Bogotá ­ Bogotá  111051 ­ Colombia Nit: 900041186-1', 'Factura MOB 1017', '2017-07-01', 40),
(0644, '163', NULL, '0', 0, 207, '238.05', '0.00', '0.00', '0.00', '0.00', '231.05', '0.00', '7', 'Notes...', 'Factura MOB 1048', '2017-07-01', 59),
(0645, '388', NULL, '0', 0, 232, '209.35', '216.39', '7.04', '0.00', '0.00', '216.39', '21.87', '7', 'Notes...', 'Factura MOB 1023', '2017-07-01', 44),
(0646, '223', NULL, '0', 0, 42, '164.00', '0.00', '0.00', '0.00', '0.00', '171.00', '0.00', '7', 'Notes...', 'Factura MOB 1040', '2017-07-01', 55),
(0647, '164', NULL, '0', 0, 132, '156.28', '0.00', '0.00', '0.00', '0.00', '288.47', '44.28', '7', 'Notes...', 'Factura MOB1027', '2017-07-01', 47),
(0648, '48', NULL, '0', 0, 268, '139.36', '0.00', '0.00', '0.00', '0.00', '69.39', '0.00', '7', '80,07 € se suman para agosto', 'Factura MOB1047', '2017-07-01', NULL),
(0649, '304', NULL, '0', 0, 22, '88.87', '0.00', '0.00', '0.00', '0.00', '80.21', '0.00', '7', 'Notes...', 'Factura MOB 1025', '2017-07-01', NULL),
(0650, '2', NULL, '0', 0, 2, '72.00', '109.60', '37.60', '0.00', '0.00', '0.00', '0.00', '2', '', 'Notes...', '2017-07-01', NULL),
(0651, '134', NULL, '0', 0, 6, '71.08', '0.00', '0.00', '0.00', '0.00', '60.00', '122.74', '7', 'Notes...', 'Factura MOB 1032', '2017-07-01', NULL),
(0652, '367', NULL, '0', 0, 66, '66.96', '0.00', '0.00', '0.00', '0.00', '67.92', '39.38', '7', 'Notes...', 'Factura MOB1039', '2017-07-01', NULL),
(0653, '60', NULL, '0', 0, 6, '57.00', '0.00', '0.00', '0.00', '0.00', '57.00', '76.70', '7', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0654, '403', 183, '163', 0, 16, '48.00', '0.00', '0.00', '0.00', '0.00', '132.00', '0.00', '7', '', 'Factura MOB 1037', '2017-07-01', 54),
(0655, '308', 194, '39', 0, 96, '45.23', '0.00', '0.00', '0.00', '0.00', '38.45', '49.30', '7', 'revenue en $', 'Factura MOB 1061', '2017-07-01', NULL),
(0656, '175', NULL, '0', 0, 5, '43.00', '0.00', '0.00', '0.00', '0.00', '53.00', '0.00', '7', 'Notes...', 'Factura MOB 1041', '2017-07-01', NULL),
(0657, '382', NULL, '0', 0, 39, '29.26', '0.00', '0.00', '0.00', '0.00', '160.50', '0.00', '7', 'Notes...', 'Factura MOB 1024', '2017-07-01', 45),
(0658, '387', NULL, '0', 0, 8, '26.54', '26.42', '-0.12', '0.00', '0.00', '26.42', '29.25', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0659, '77', NULL, '0', 0, 4, '26.00', '26.00', '0.00', '0.00', '0.00', '26.00', '0.00', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0660, '393', NULL, '0', 0, 22, '23.40', '0.00', '0.00', '0.00', '0.00', '23.40', '0.00', '4', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0661, '394', NULL, '0', 0, 28, '19.19', '0.00', '0.00', '0.00', '0.00', '16.20', '0.00', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0662, '395', NULL, '0', 0, 1, '16.59', '14.00', '-2.59', '0.00', '0.00', '14.00', '0.00', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0663, '364', NULL, '0', 0, 11, '16.50', '17.60', '1.10', '0.00', '0.00', '17.60', '54.40', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0664, '396', NULL, '0', 0, 15, '16.45', '0.00', '0.00', '0.00', '0.00', '16.80', '0.00', '2', 'se factura cada país por separado, preguntar antes de facturar', 'Notes...', '2017-07-01', NULL),
(0665, '385', NULL, '0', 0, 13, '15.60', '17.70', '2.10', '0.00', '0.00', '17.70', '0.00', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0666, '397', NULL, '0', 0, 19, '8.82', '8.82', '0.00', '0.00', '0.00', '8.82', '0.00', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0667, '70', NULL, '0', 0, 2, '7.11', '0.00', '0.00', '0.00', '0.00', '7.11', '0.00', '4', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0668, '398', 191, '138', 0, 2, '4.50', '9.45', '4.95', '0.00', '0.00', '9.45', '0.00', '7', 'Notes...', 'Factura MOB1080', '2017-07-01', NULL),
(0669, '317', NULL, '0', 0, 2, '4.20', '4.20', '0.00', '0.00', '0.00', '4.20', '0.00', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0670, '399', NULL, '0', 0, 6, '3.00', '0.00', '0.00', '0.00', '0.00', '3.00', '0.00', '4', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0671, '372', NULL, '0', 0, 3, '3.00', '3.00', '0.00', '0.00', '0.00', '3.00', '0.00', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0672, '160', NULL, '0', 0, 1, '2.40', '1.60', '-0.80', '0.00', '0.00', '1.60', '40.50', '2', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0673, '400', NULL, '0', 0, 13, '2.16', '0.00', '0.00', '0.00', '0.00', '1.00', '0.00', '2', 'Notes...', 'Notes...', '2017-07-01', NULL);
INSERT INTO `incomes` (`id`, `advertiser`, `social`, `am`, `clicks`, `conversion`, `revenue`, `platform_revenue`, `difference`, `scrubs`, `hold`, `validated`, `accumulated`, `status`, `notas_am`, `notas_finance`, `date`, `increment`) VALUES
(0674, '149', NULL, '0', 0, 1, '1.20', '0.00', '0.00', '0.00', '0.00', '1.20', '1.20', '4', 'Notes...', 'Notes...', '2017-07-01', NULL),
(0675, '401', 182, '134', 49896, 9, '72.00', '72.00', '0.00', '0.00', '0.00', '72.00', '0.00', '7', 'facturar con datos de PT', 'Factura MOB 1021', '2017-07-01', NULL),
(0676, '159', 183, '304', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '1471.33', '0.00', '7', '1079,12vf+392,21orange', 'Factura MOB1051', '2017-07-01', 56),
(0677, '31', 183, '134', 0, 0, '254.78', '254.78', '0.00', '0.00', '0.00', '254.78', '0.00', '7', '', 'Factura MOB1046', '2017-07-01', 60),
(0678, '404', 185, '162', 0, 0, '61.36', '61.36', '0.00', '0.00', '0.00', '61.36', '0.00', '7', 'por facturar por separado', 'Factura MOB1049', '2017-07-01', NULL),
(0679, '42', 185, '162', 0, 0, '153.20', '153.20', '0.00', '0.00', '0.00', '153.20', '0.00', '7', '', 'Factura MOB 1043', '2017-02-01', NULL),
(0901, '177', 415, '134', 0, 0, '13.50', '13.50', '0.00', '0.00', '0.00', '13.50', '0.00', '2', '', '', '2017-03-01', NULL),
(1008, '368', 416, '128', 0, 3357, '8042.94', '0.00', '0.00', '874.10', '0.00', '8127.20', '0.00', '7', 'la diferencia se debe a dos pausas y un cambio de cap a lo largo de fin de semana que no fueron aplicados a tiempo, asi que no se quitará de ningun afiliado (ver hoja scrub)', 'Factura MOB1056', '2017-08-01', 67),
(1009, '403', 416, '128', 0, 2434, '7947.46', '0.00', '0.00', '0.00', '0.00', '7793.50', '0.00', '7', 'la diferencia por un error al montar una campaña (por eso el revenue sale como mezcla de $ y €)', 'Factura MOB 1066', '2017-08-01', 81),
(1010, '83', 194, '39', 0, 611, '7242.90', '0.00', '0.00', '0.00', '0.00', '7999.90', '0.00', '7', 'Notes...', 'Factura MOB 1067', '2017-08-01', 77),
(1011, '43', 416, '128', 0, 5024, '6077.59', '0.00', '0.00', '0.00', '0.00', '6089.35', '0.00', '7', 'incluye collectcent smartlink', 'Factura MOB1063', '2017-08-01', 66),
(1012, '4', 194, '39', 0, 370, '3332.00', '0.00', '0.00', '0.00', '0.00', '2808.24', '0.00', '7', 'revenue en ~$', 'Factura MOB 1064', '2017-08-01', 74),
(1013, '60', 191, '138', 0, 210, '1903.70', '0.00', '0.00', '0.00', '0.00', '1913.20', '0.00', '7', 'Notes...', 'Factura MOB 1073', '2017-08-01', 84),
(1014, '223', 194, '39', 0, 370, '1469.00', '0.00', '0.00', '0.00', '0.00', '1546.00', '0.00', '7', 'Notes...', 'Factura MOB 1065', '2017-08-01', 76),
(1015, '408', 416, '128', 0, 258, '1221.01', '0.00', '0.00', '0.00', '0.00', '1032.00', '0.00', '7', 'The invoice and payment requests will be issued on this page on the first day of each month for your earnings paid by advertisers.', 'Factura MOB 1057', '2017-08-01', 69),
(1016, '31', 191, '138', 0, 968, '1203.80', '1407.25', '203.45', '764.40', '0.00', '642.85', '0.00', '2', 'Notes...', 'el 17th sabremos pubid de scrub ', '2017-08-01', 94),
(1017, '398', 194, '39', 0, 722, '747.90', '606.15', '-141.75', '0.00', '0.00', '754.20', '9.45', '7', 'hay error en su plataforma, acordamos por mail', 'Factura MOB 1080', '2017-08-01', 89),
(1018, '345', 191, '138', 0, 476, '672.35', '0.00', '0.00', '0.00', '0.00', '672.91', '0.00', '7', 'Notes...', 'Factura MOB 1075', '2017-08-01', 86),
(1019, '168', 194, '39', 0, 57, '656.45', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '1', 'confirman hasta el 15th', 'Notes...', '2017-08-01', NULL),
(1020, '417', 416, '134', 0, 121, '605.00', '0.00', '0.00', '0.00', '0.00', '504.00', '0.00', '7', 'revenue en $', 'Factura MOB 1058 - hay que volver a hacerla', '2017-08-01', 63),
(1021, '319', 194, '304', 0, 18, '427.86', '0.00', '0.00', '0.00', '0.00', '360.00', '93.00', '7', 'Notes...', 'Factura MOB 1068', '2017-08-01', 83),
(1022, '175', 191, '138', 0, 42, '378.10', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '1', 'Notes...', 'por dmexco confirmaciones más tarde', '2017-08-01', NULL),
(1023, '401', 416, '128', 0, 29, '275.73', '0.00', '0.00', '0.00', '0.00', '271.00', '0.00', '7', 'Notes...', 'Factura MOB 1059', '2017-08-01', 62),
(1024, '383', 416, '2', 0, 247, '250.80', '0.00', '0.00', '0.00', '0.00', '948.80', '0.00', '7', 'Asunto tiene que ser  \"campañas comtel\" y que tenga fecha de septiembre. ', 'Factura MOB 1079 PLUSMOBILE COMMUNICATIONS SA 1110501 BOGOTA FACTURA BOGOTA-COLOMBIA  ID CIF/DNI: CO9000411861', '2017-08-01', 61),
(1025, '304', 191, '138', 0, 22, '184.80', '0.00', '0.00', '0.00', '0.00', '150.59', '0.00', '7', 'Company Name: Rock Internet, S.L. VAT: ESB86907953 Phone: +34 91 084 17 30 Billing Address: C/ Barquillo, 8 - 2ºizq. City: Madrid Postal Code: 28004 Email: billing@arkeero.com', 'Factura MOB 1076   Bank name: BSCH (Banco Santander Central Hispano) Bank address: Príncipe de Vergara, 277 - 28016- Madrid (oficina 2862) IBAN (Euros (€)):ES93 0049 2862 67 2914627049', '2017-08-01', 87),
(1026, '308', 416, '128', 0, 270, '177.35', '0.00', '0.00', '0.00', '0.00', '149.19', '87.75', '7', 'revenue en $', 'Factura MOB 1061', '2017-08-01', 71),
(1027, '177', 194, '3', 0, 9, '64.00', '0.00', '0.00', '0.00', '0.00', '64.00', '0.00', '7', 'aclarando la diferencia', 'Factura MOB 1070', '2017-08-01', NULL),
(1028, '171', 416, '128', 0, 20, '60.96', '0.00', '0.00', '0.00', '0.00', '60.00', '0.00', '7', 'Notes...', 'Factura MOB 1060', '2017-08-01', NULL),
(1029, '164', 191, '138', 0, 46, '60.48', '0.00', '0.00', '0.00', '0.00', '97.76', '0.00', '7', 'Notes...', 'Factura MOB 1071', '2017-08-01', NULL),
(1030, '388', 194, '39', 0, 141, '56.40', '1.20', '-55.20', '0.00', '0.00', '56.40', '0.00', '7', 'validado skype 07/09', 'Factura MOB 1072', '2017-08-01', NULL),
(1031, '305', 416, '128', 0, 2, '49.92', '0.00', '0.00', '0.00', '0.00', '42.00', '0.00', '2', 'revenue en $, no hay que enviar factura', 'Notes...', '2017-08-01', NULL),
(1032, '318', 194, '39', 0, 26, '41.70', '0.00', '0.00', '0.00', '0.00', '41.70', '0.00', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1033, '397', 194, '39', 0, 84, '41.16', '0.00', '0.00', '0.00', '0.00', '41.16', '8.82', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1034, '367', 194, '39', 0, 33, '38.90', '0.00', '0.00', '0.00', '0.00', '38.90', '0.00', '7', 'Notes...', 'Factura MOB 1077', '2017-08-01', NULL),
(1035, '394', 416, '128', 0, 14, '38.74', '48.80', '10.06', '0.00', '0.00', '48.80', '16.20', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1036, '2', 191, '138', 0, 1, '36.00', '120.80', '84.80', '0.00', '0.00', '0.00', '0.00', '2', 'generado por el error del adv', 'Notes...', '2017-08-01', NULL),
(1037, '409', 191, '138', 0, 6, '33.28', '22.29', '-10.99', '0.00', '0.00', '22.29', '0.00', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1038, '410', 191, '138', 0, 39, '19.00', '0.00', '0.00', '0.00', '0.00', '19.00', '0.00', '4', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1039, '416', 194, '2', 0, 15, '14.26', '0.00', '0.00', '0.00', '0.00', '12.16', '0.00', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1040, '134', 191, '138', 0, 1, '11.88', '10.00', '-1.88', '0.00', '0.00', '10.00', '8.00', '2', 'acumulado de febrero que se ha añadido ahora', 'Notes...', '2017-08-01', NULL),
(1041, '20', 191, '138', 0, 3, '6.65', '0.00', '0.00', '0.00', '0.00', '1.80', '0.00', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1042, '48', 416, '128', 0, 12, '6.24', '0.00', '0.00', '0.00', '0.00', '99.23', '0.00', '7', 'no hace falta mandar factura, ya está compensado el error de payout del mes pasado', 'Factura MOB 1062', '2017-08-01', NULL),
(1043, '400', 194, '39', 0, 36, '6.09', '0.00', '0.00', '0.00', '0.00', '6.27', '1.00', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1044, '393', 191, '138', 0, 8, '4.80', '0.00', '0.00', '0.00', '0.00', '4.80', '23.40', '2', 'basado en nuestros numeros', 'Notes...', '2017-08-01', NULL),
(1045, '411', 194, '39', 0, 13, '4.80', '0.00', '0.00', '0.00', '0.00', '32.55', '0.00', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1046, '364', 191, '138', 0, 3, '4.50', '4.80', '0.30', '0.00', '0.00', '4.80', '72.00', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1047, '160', 191, '138', 0, 1, '2.40', '1.60', '-0.80', '0.00', '0.00', '1.60', '42.10', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1048, '52', 191, '138', 0, 1, '1.85', '37.00', '35.15', '0.00', '0.00', '37.00', '0.00', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1049, '396', 191, '138', 0, 2, '1.70', '0.00', '0.00', '0.00', '0.00', '1.70', '16.80', '4', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1050, '412', 194, '39', 0, 1, '1.60', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1051, '406', 191, '138', 0, 1, '1.20', '0.00', '0.00', '0.00', '0.00', '1.20', '0.00', '4', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1052, '163', 191, '138', 0, 1, '0.85', '0.00', '0.00', '0.00', '0.00', '0.85', '0.00', '4', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1053, '405', 191, '138', 0, 1, '0.85', '0.00', '0.00', '0.00', '0.00', '0.85', '0.00', '4', 'Notes...', 'Notes...', '2017-08-01', NULL),
(1054, '413', 191, '138', 0, 1, '0.71', '0.00', '0.00', '0.00', '0.00', '0.90', '0.00', '2', 'Webpilots España, S.L.  Pellaires, 30-38, Nave B  08019 Barcelona, España  N.I.F.: ESB62268339 finanzas@webpilots.com ', 'Notes...', '2017-08-01', NULL),
(1056, '415', 416, '128', 0, 37, '13.80', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'agregado y facturado en la cuenta de collectcent', 'Notes...', '2017-08-01', NULL),
(1057, '159', 371, '304', 0, 0, '4424.00', '0.00', '0.00', '0.00', '0.00', '4370.59', '0.00', '7', ' 4.370,59€ + IVA', 'Factura MOB 1074', '2017-08-01', 85),
(1058, '134', 191, '134', 0, 0, '0.00', '8.00', '8.00', '0.00', '0.00', '8.00', NULL, '2', '', '', '2017-02-01', NULL),
(1059, '83', 541, '3', 0, 1272, '12052.80', '0.00', '0.00', '0.00', '0.00', '12310.00', '0.00', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1060, '318', 541, '3', 0, 11188, '8554.94', '0.00', '0.00', '0.00', '0.00', '4244.00', '41.70', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1061, '368', NULL, '3', 0, 2240, '5607.80', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1062, '4', 541, '3', 0, 437, '4553.00', '0.00', '0.00', '0.00', '0.00', '4324.00', '0.00', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1063, '403', 541, '3', 0, 1003, '3265.00', '0.00', '0.00', '0.00', '0.00', '5555.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', 98),
(1064, '530', 541, '3', 0, 691, '3264.05', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1065, '43', 541, '3', 0, 2693, '2980.49', '0.00', '0.00', '0.00', '0.00', '23.00', '0.00', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1066, '164', NULL, '3', 0, 1995, '2205.13', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1067, '359', 541, '3', 0, 640, '2016.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1068, '531', NULL, '0', 0, 99, '1164.39', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1069, '168', NULL, '3', 0, 313, '1107.40', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1070, '417', 541, '3', 0, 200, '1000.00', '0.00', '0.00', '0.00', '0.00', '999.00', '0.00', '1', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1071, '398', NULL, '3', 0, 822, '878.60', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1072, '345', NULL, '3', 0, 1046, '878.03', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1073, '223', NULL, '3', 0, 208, '832.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1074, '409', 541, '3', 0, 62, '570.39', '0.00', '0.00', '0.00', '0.00', '0.00', '22.29', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1075, '415', 541, '3', 0, 677, '516.03', '0.00', '0.00', '0.00', '0.00', '233.00', '0.00', '1', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1076, '532', 541, '3', 0, 250, '260.05', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '9', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1077, '60', 541, '3', 0, 25, '237.50', '0.00', '0.00', '0.00', '0.00', '1232.00', '0.00', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1078, '177', NULL, '3', 0, 14, '198.40', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1079, '383', NULL, '3', 0, 141, '141.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1080, '20', 541, '3', 0, 130, '131.30', '0.00', '0.00', '0.00', '0.00', '0.00', '1.80', '4', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1081, '361', NULL, '3', 0, 31, '78.22', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1082, '533', NULL, '0', 0, 7, '70.86', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1083, '411', NULL, '3', 0, 118, '60.90', '0.00', '0.00', '0.00', '0.00', '0.00', '32.55', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1084, '534', 541, '3', 0, 40, '57.50', '0.00', '0.00', '0.00', '0.00', '333.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', 95),
(1085, '319', 541, '3', 0, 8, '55.50', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1086, '388', 541, '3', 0, 125, '50.00', '232.00', '0.00', '22.00', '0.00', '2323.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', 96),
(1087, '31', NULL, '3', 0, 11, '36.20', '0.00', '0.00', '0.00', '0.00', '0.00', '642.85', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1088, '535', 541, '3', 0, 30, '34.93', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1089, '407', 541, '3', 0, 27, '32.40', '0.00', '0.00', '0.00', '0.00', '1232.00', '0.00', '1', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1090, '329', NULL, '3', 0, 7, '30.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1091, '536', 541, '3', 0, 54, '23.85', '0.00', '0.00', '0.00', '0.00', '23.00', '0.00', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1092, '401', 541, '3', 0, 2, '21.26', '0.00', '0.00', '0.00', '0.00', '22.00', '0.00', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1093, '367', 541, '3', 0, 16, '17.90', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1094, '393', NULL, '3', 0, 8, '14.40', '0.00', '0.00', '0.00', '0.00', '0.00', '28.20', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1095, '308', 541, '3', 0, 73, '11.82', '0.00', '0.00', '0.00', '0.00', '2323.00', '0.00', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1096, '166', NULL, '3', 0, 1, '9.45', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1097, '537', 541, '3', 0, 3, '9.00', '0.00', '0.00', '0.00', '0.00', '2300.00', '0.00', '9', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1098, '538', 541, '3', 0, 11, '9.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1099, '539', NULL, '0', 0, 26, '8.74', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1100, '394', NULL, '3', 0, 2, '8.62', '0.00', '0.00', '0.00', '0.00', '0.00', '65.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1101, '304', 541, '3', 0, 1, '8.40', '0.00', '0.00', '0.00', '0.00', '1232.00', '0.00', '7', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1102, '386', NULL, '3', 0, 1, '8.27', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1103, '540', NULL, '0', 0, 3, '8.27', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1104, '410', NULL, '3', 0, 14, '7.00', '0.00', '0.00', '0.00', '0.00', '0.00', '19.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1105, '139', NULL, '3', 0, 6, '6.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1106, '364', NULL, '3', 0, 2, '3.70', '0.00', '0.00', '0.00', '0.00', '0.00', '76.80', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1107, '541', NULL, '0', 0, 4, '2.70', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1108, '416', NULL, '3', 0, 4, '1.98', '0.00', '0.00', '0.00', '0.00', '0.00', '12.16', '0', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1109, '405', 541, '3', 0, 2, '1.70', '0.00', '0.00', '0.00', '0.00', '123.00', '0.85', '1', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1110, '149', 541, '3', 0, 1, '1.20', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1111, '312', 541, '3', 0, 1, '0.60', '0.00', '0.00', '0.00', '0.00', '3323.00', '0.00', '2', 'Notes...', 'Notes...', '2017-09-01', 97),
(1112, '397', 541, '3', 0, 1, '0.49', '0.00', '0.00', '0.00', '0.00', '0.00', '49.98', '10', 'Notes...', 'Notes...', '2017-09-01', NULL),
(1113, '387', NULL, '3', 0, 1, '0.12', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 'Notes...', 'Notes...', '2017-09-01', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission`
--

CREATE TABLE `permission` (
  `permission_id` int(11) NOT NULL,
  `permission_description` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permission`
--

INSERT INTO `permission` (`permission_id`, `permission_description`) VALUES
(1, 'create incomes'),
(2, 'edit incomes'),
(3, 'edit notes AM'),
(4, 'edit notes FN'),
(5, 'total access'),
(6, 'only read'),
(7, 'costes edit'),
(8, 'costes create');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reason_scrubs`
--

CREATE TABLE `reason_scrubs` (
  `id` int(2) NOT NULL,
  `value` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `reason_scrubs`
--

INSERT INTO `reason_scrubs` (`id`, `value`) VALUES
(1, 'KPI not reached'),
(2, 'RR'),
(3, 'Overcap'),
(4, 'Fraud'),
(5, 'On Hold'),
(6, 'Rebrokering');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(50) NOT NULL,
  `role_description` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `role`
--

INSERT INTO `role` (`role_id`, `role_name`, `role_description`) VALUES
(1, 'Contabilidad', 'Validar Fact+Prov,No edita Costes, notas Finanzas'),
(2, 'Account Manager', 'No edita Costes. Valida Facturas. Notas AM'),
(3, 'Prod', 'All in costes. rest only read'),
(4, 'read only', 'Solo lectura en todo ninguna edición.'),
(5, 'total access', 'full edition');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_permission`
--

CREATE TABLE `role_permission` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `role_permission`
--

INSERT INTO `role_permission` (`role_id`, `permission_id`) VALUES
(1, 4),
(2, 2),
(2, 3),
(2, 1),
(3, 7),
(3, 8),
(4, 6),
(5, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `scrubs`
--

CREATE TABLE `scrubs` (
  `id` int(10) NOT NULL,
  `date` date NOT NULL,
  `date_recived` date NOT NULL,
  `status_adv` int(2) NOT NULL,
  `advertiser` int(5) NOT NULL,
  `offer` varchar(100) NOT NULL,
  `affiliate` int(3) NOT NULL,
  `conversions` int(10) NOT NULL,
  `revenue_disc` decimal(10,2) NOT NULL,
  `cost_disc` decimal(10,2) DEFAULT NULL,
  `traffic_team` int(2) NOT NULL,
  `status_aff` int(2) NOT NULL,
  `reason` int(2) NOT NULL,
  `notas_am` varchar(500) NOT NULL DEFAULT 'Notes...',
  `notas_aff` varchar(500) NOT NULL DEFAULT 'Notes...'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `scrubs`
--

INSERT INTO `scrubs` (`id`, `date`, `date_recived`, `status_adv`, `advertiser`, `offer`, `affiliate`, `conversions`, `revenue_disc`, `cost_disc`, `traffic_team`, `status_aff`, `reason`, `notas_am`, `notas_aff`) VALUES
(1, '2017-03-01', '2017-03-13', 1, 68, 'CleanMaster_US_Android_41497', 1, 41, '40.30', NULL, 1, 1, 1, 'Not passed GAID', 'Comunicado. A la espera de reporte'),
(2, '2017-03-01', '2017-03-21', 1, 173, 'TOUTIAOSHIPIN_CN_iOS_89481', 11, 84, '147.60', NULL, 1, 2, 2, 'sent screenshot + 2 excels', ''),
(3, '2017-03-01', '2017-03-14', 1, 151, 'Verychic_FR_iOS', 25, 518, '518.00', NULL, 1, 2, 4, 'sent by email (fake address)', '  sent detailed report (05/04)'),
(4, '2017-03-01', '2017-03-21', 1, 151, 'Cheerz_FR_iOS', 29, 243, '306.18', NULL, 1, 1, 3, '', 'Comunicado. Screenshot'),
(5, '2017-03-01', '2017-03-16', 1, 378, 'LetGo_TR_iOS_51081', 29, 100, '70.00', NULL, 1, 1, 4, 'aparte rebrokering', 'Comunicado'),
(6, '2017-03-01', '2017-03-15', 1, 3, 'Conquest_KR_Android_43730', 29, 574, '1360.38', NULL, 1, 1, 2, '', ''),
(7, '2017-03-01', '2017-04-06', 1, 173, 'Jinritoutiao_CN_iOS_89576', 37, 192, '288.00', NULL, 1, 1, 2, 'aparte incent traffic', 'comunicado.'),
(8, '2017-03-01', '2017-04-07', 1, 2, 'CradleOfEmpires_US_iOS_222', 51, 2344, '5320.88', NULL, 1, 2, 1, '', ''),
(9, '2017-03-01', '2017-04-07', 1, 2, 'ZakaZaka_RU_and_126', 52, 0, '0.00', NULL, 1, 2, 1, 'Only part of the KPIs has been reached', ''),
(10, '2017-03-01', '2017-04-07', 1, 2, 'ZakaZaka_RU_and_126', 48, 704, '537.63', NULL, 2, 1, 1, 'Only part of the KPIs has been reached - no tenemos split por publisher por eso propongo pagar a Smartlead y descontar todo Instal', ''),
(11, '2017-03-01', '2017-04-07', 1, 2, 'ZakaZaka_RU_iOS_125', 46, 4, '3.80', NULL, 2, 1, 1, '', ''),
(12, '2017-03-01', '2017-04-07', 1, 2, 'ZakaZaka_RU_iOS_125', 27, 102, '96.90', NULL, 1, 2, 1, '', ''),
(13, '2017-03-01', '2017-04-07', 1, 2, 'ZakaZaka_RU_iOS_125', 52, 761, '722.95', NULL, 1, 2, 1, '', ''),
(14, '2017-03-01', '2017-04-11', 2, 162, 'Wadi_UAE and SA_iOS', 53, 132, '171.60', NULL, 1, 2, 1, '', 'Proof sent by email (THEY WILL PAY 135 CONVERSIONS FROM THE TOTAL)'),
(15, '2017-03-01', '2017-04-11', 2, 162, 'Wadi_UAE and SA_iOS', 5, 1579, '2052.70', NULL, 1, 1, 1, 'also incent traffic (see CRs per day)', 'Proof sent by email (THEY WILL PAY 135 CONVERSIONS FROM THE TOTAL)'),
(16, '2017-03-01', '2017-04-11', 2, 162, 'Wadi_UAE and SA_iOS', 6, 1686, '2191.80', NULL, 1, 1, 1, '', 'Proof sent by email (THEY WILL PAY 135 CONVERSIONS FROM THE TOTAL)'),
(17, '2017-03-01', '2017-04-11', 2, 162, 'Wadi_UAE and SA_Android', 5, 9009, '4504.05', NULL, 1, 1, 1, 'also incent traffic (see CRs per day)', 'Proof sent by email (THEY WILL PAY 135 CONVERSIONS FROM THE TOTAL)'),
(18, '2017-03-01', '2017-04-11', 2, 162, 'Wadi_UAE and SA_Android', 28, 333, '166.00', NULL, 1, 2, 1, 'also incent traffic (see CRs per day)', 'Proof sent by email (THEY WILL PAY 135 CONVERSIONS FROM THE TOTAL)'),
(19, '2017-03-01', '2017-04-11', 2, 162, 'Wadi_UAE and SA_Android', 30, 5306, '2643.00', NULL, 1, 2, 1, 'also incent traffic (see CRs per day)', 'Proof sent by email (THEY WILL PAY 135 CONVERSIONS FROM THE TOTAL)'),
(21, '2017-03-01', '2017-04-14', 1, 173, 'EmpireWar:AgeofHero_US_iOS_88945', 52, 26, '60.84', NULL, 1, 2, 1, 'sent by email (screenshot attached)', ''),
(24, '2017-03-01', '2017-04-19', 2, 353, 'HUOSHANSHIPIN2_CN_iOS_23128', 54, 119, '178.50', NULL, 1, 2, 4, 'esperando report. Mandada factura no OK', ''),
(25, '2017-03-01', '2017-04-19', 2, 353, 'HUOSHANSHIPIN2_CN_iOS_23128', 29, 181, '271.50', NULL, 1, 1, 4, 'esperando report. Mandada factura no OK', 'Validado hacia Aff'),
(26, '2017-03-01', '2017-04-19', 2, 353, 'HUOSHANSHIPIN2_CN_iOS_23128', 30, 340, '510.00', NULL, 1, 2, 4, 'esperando report. Mandada factura no OK', ''),
(27, '2017-03-01', '2017-04-19', 2, 353, 'HUOSHANSHIPIN2_CN_iOS_23128', 52, 10, '15.00', NULL, 1, 2, 4, 'esperando report. Mandada factura no OK', ''),
(28, '2017-03-01', '2017-04-19', 2, 24, 'Heydealer_KR_iOS_463', 6, 49, '129.85', NULL, 1, 2, 5, '', ''),
(30, '2017-03-01', '2017-04-19', 1, 24, 'Pokerstars_ES_iOS_510', 52, 103, '309.00', NULL, 1, 2, 1, '', ''),
(31, '2017-03-01', '2017-04-19', 2, 24, 'Namshi_SA_iOS_258', 6, 212, '254.40', NULL, 1, 2, 1, 'facturado en mayo por falta de pruebas', ''),
(32, '2017-03-01', '2017-04-20', 1, 17, 'UpLiveVideo_CN_iOS_75202753', 28, 1491, '1960.30', NULL, 1, 2, 1, ' (scrub por enviar tráfico de la versión mas viaja de la solicitada)', ''),
(33, '2017-03-01', '2017-04-20', 1, 17, 'GameOfWar_DE_and_75063581', 48, 41, '66.09', NULL, 2, 2, 1, '', ''),
(36, '2017-03-01', '2017-04-20', 2, 2, 'JollyChic_SA/KW/AE_iOS_124', 6, 486, '806.96', NULL, 1, 2, 1, '', ''),
(37, '2017-03-01', '2017-04-20', 2, 2, 'JollyChic_SA/AE/KW_and_123', 43, 503, '352.10', NULL, 2, 2, 1, '', ''),
(49, '2017-03-01', '2017-04-18', 1, 153, 'CGTN_US_iOS_8952', 54, 241, '296.60', NULL, 1, 2, 1, 'Report mandado por email', ''),
(50, '2017-03-01', '2017-04-24', 2, 153, 'Gardenscapes_US_iOS_8811', 28, 18, '35.10', NULL, 1, 2, 1, 'NO ACEPTADO- falta report', ''),
(51, '2017-03-01', '2017-04-18', 1, 153, 'CGTN_US_iOS_8952', 11, 10, '14.00', NULL, 1, 2, 1, 'Report sent by email. Aparte incent traffic', ''),
(52, '2017-04-01', '2017-05-16', 1, 24, 'Namshi_SA_iOS_258', 6, 218, '260.40', NULL, 1, 1, 5, 'se rehace la factura, pagan la mitad - no pagar a Digital Pow Wow', ''),
(53, '2017-04-01', '2017-05-15', 1, 153, 'Astromusme_JP_Android_8857', 52, 110, '348.80', NULL, 1, 1, 1, 'mandadas pruebas por email', ''),
(54, '2017-04-01', '2017-05-15', 1, 153, 'Astromusme_JP_Android_8857', 1, 14, '44.80', NULL, 1, 1, 1, 'mandadas pruebas por email', ''),
(55, '2017-04-01', '2017-05-15', 1, 153, 'Astromusme_JP_Android_8857', 37, 10, '28.80', NULL, 1, 1, 1, 'mandadas pruebas por email', ''),
(56, '2017-06-01', '2017-06-05', 1, 14, 'CPA - Perú -Astros- Entel', 60, 94, '98.20', NULL, 1, 1, 4, '', ''),
(57, '2017-07-01', '2017-07-19', 1, 83, 'France - SFR - Pacman', 49, 638, '9555.00', '7177.50', 1, 2, 4, '', ''),
(58, '2017-07-01', '2017-07-19', 1, 83, 'France - Gameasy - SFR', 60, 101, '1515.00', '1091.25', 1, 2, 4, '', ''),
(59, '2017-07-27', '2017-07-27', 1, 83, 'France - Gameasy - SFR', 78, 198, '2970.00', '2227.50', 1, 1, 4, '', ''),
(60, '2017-08-01', '2017-09-01', 1, 368, '', 132, 0, '84.26', '0.00', 2, 2, 3, '', ''),
(61, '2017-09-01', '2017-10-04', 1, 214, 'Offer_Simpsons_SP', 128, 123, '12300.00', '323.00', 2, 2, 1, '', ''),
(62, '2017-09-01', '2017-09-22', 1, 423, 'Offer_game_battle_123', 45, 32323, '7654.00', '233.00', 4, 1, 5, '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status`
--

CREATE TABLE `status` (
  `id` int(5) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `code` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `status`
--

INSERT INTO `status` (`id`, `name`, `code`) VALUES
(1, '', 'white'),
(2, 'Validado', 'blue'),
(4, 'Validado NOK', 'red'),
(7, 'Facturado', 'green'),
(9, 'Provisionado', 'yellow'),
(10, 'Facturado NOK', 'red');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_adv`
--

CREATE TABLE `status_adv` (
  `id` int(3) NOT NULL,
  `value` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `status_adv`
--

INSERT INTO `status_adv` (`id`, `value`) VALUES
(1, 'ok'),
(2, 'negociacion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_aff`
--

CREATE TABLE `status_aff` (
  `id` int(3) NOT NULL,
  `value` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `status_aff`
--

INSERT INTO `status_aff` (`id`, `value`) VALUES
(1, 'ok'),
(2, 'negociacion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `traffic_team`
--

CREATE TABLE `traffic_team` (
  `id` int(2) NOT NULL,
  `value` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `traffic_team`
--

INSERT INTO `traffic_team` (`id`, `value`) VALUES
(1, 'Affiliation'),
(2, 'Media Buying'),
(3, 'Traffic Team'),
(4, 'Diego Duarte'),
(5, 'Ruben Macías'),
(6, 'Juliana Borges'),
(7, 'Irene Camarena'),
(8, 'Claudia Plazaola');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `email` tinytext NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(30) NOT NULL,
  `created` datetime NOT NULL,
  `attempt` varchar(15) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `name`, `created`, `attempt`) VALUES
(2, 'billy', 'sergio.suarez@advancemobile.net', '$2y$10$Z6YpYRBai1N1E.HJFwijkuF.tSWnrwUwF0BBB9pan4a.q1LQ9EyC.', 'sergio suarez', '2017-03-06 18:50:48', '0'),
(3, 'Ramon', 'ramon.granero@advancemobile.net', '$2y$10$3CL4ODxb7zpriNQCfMHJG.LZw6vVBSr0WRNbEogceyZYMDv0hYNJ6', 'Ramón', '2017-03-07 13:23:00', '0'),
(4, 'Cris', 'cristina.amador@fasttracknet.es', '$2y$10$DhYAvvpOc1GFzBDZbW3KXO4sSbxcVVZl6RMbZ2uBBl86GSxUmppg.', 'CRISTINA AMADOR', '2017-03-07 16:05:41', '0'),
(6, 'camilla', 'camilla.rolando@advancemobile.net', '$2y$10$sINFR6IJv3Fhgc014oDstOM7HKQnbVWriQjfRkYRpamtapXLJNoeO', 'camilla', '2017-03-07 17:22:32', '0'),
(7, 'Elena', 'elena.rubio@fasttracknet.es', '$2y$10$XAeNLV07jX.reLWF7ovleOvXdaivMajh1GdcgY1hNvB4MshqROeqK', 'Elena Rubio', '2017-03-07 18:05:07', '0'),
(9, 'Kamila1989', 'kamila.krajnik@advancemobile.net', '$2y$10$WjKl0W9W4sBHmQEsARhujO9U1hx1.K9XByDYh/bwKAXj8MrcBwfwi', 'Kamila Krajnik', '2017-03-08 12:08:12', '0'),
(10, 'diegoduartec', 'diego.duarte@advancemobile.net', '$2y$10$llciFYsLqFUYmKVs45L9jebOujCECHKhfZBuBJTzjwlqC/eQDyso2', 'Diego Duarte', '2017-03-14 09:51:45', '0'),
(13, 'jurborges', 'juliana.borges@advancemobile.net', '$2y$10$SC.XApT7Fs4LEBBHKslqEOUG0V3yfuYE4zYGPs1Kgg/3EuYeiW/5W', 'Juliana Borges', '2017-04-12 17:07:11', '0'),
(15, 'javiulacia', 'javi.deulacia@fasttracknet.es', '$2y$10$K1CVvK5K5pqKrDvAXZCFiO0MYRgUT8VnmgBIGsrfcYcOc38H6qcxC', 'Javi de Ulacia', '2017-05-10 08:42:22', '0'),
(16, 'Adela', 'adela.pena@fasttracknet.es', '$2y$10$5X5jg/XjAnFfqZwm.edBO.sOxsX9GV5k4N/K5btepdvTXGKX2jjCm', 'Adela Peña', '2017-05-31 06:56:53', '0'),
(17, 'Claudia', 'claudia.plazaola@advancemobile.net', '$2y$10$men4UcUaty3ukTX/goG42OOjx3T9IYM7fdryIptxuFLfHD8fnAByG', 'Claudia', '2017-07-18 12:02:34', '0'),
(18, 'IreneCamarena', 'irene.camarena@advancemobile.net', '$2y$10$in9UAL4aXCIbYq5z2dHXVuPZAkiTyL3.0WwuCdz1pDG69WWJe7RTW', 'Irene Camarena', '2017-08-09 11:09:24', '0'),
(19, 'Ruben', 'ruben.macias@advancemobile.net', '$2y$10$UAQz14Lcos2aESCMMXX6u.BTSAIXheuDs6.JBmTwu4DZw7ex6Ff3y', 'Ruben Macias', '2017-08-16 13:09:23', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_devices`
--

CREATE TABLE `user_devices` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL COMMENT 'The user''s ID',
  `token` varchar(15) NOT NULL COMMENT 'A unique token for the user''s device',
  `last_access` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_role`
--

CREATE TABLE `user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user_role`
--

INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
(2, 5),
(3, 5),
(4, 1),
(6, 2),
(7, 1),
(9, 2),
(10, 3),
(13, 3),
(15, 5),
(16, 1),
(17, 3),
(18, 3),
(19, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_tokens`
--

CREATE TABLE `user_tokens` (
  `token` varchar(40) NOT NULL COMMENT 'The generated unique token',
  `uid` int(11) NOT NULL COMMENT 'The User ID',
  `requested` varchar(20) NOT NULL COMMENT 'The date when token was created'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accountmanager`
--
ALTER TABLE `accountmanager`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `advertisers`
--
ALTER TABLE `advertisers`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `affiliates`
--
ALTER TABLE `affiliates`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `costes`
--
ALTER TABLE `costes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `doc_number`
--
ALTER TABLE `doc_number`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `incomes`
--
ALTER TABLE `incomes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`permission_id`);

--
-- Indices de la tabla `reason_scrubs`
--
ALTER TABLE `reason_scrubs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indices de la tabla `role_permission`
--
ALTER TABLE `role_permission`
  ADD KEY `role_id` (`role_id`),
  ADD KEY `permission_id` (`permission_id`);

--
-- Indices de la tabla `scrubs`
--
ALTER TABLE `scrubs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status_adv`
--
ALTER TABLE `status_adv`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status_aff`
--
ALTER TABLE `status_aff`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `traffic_team`
--
ALTER TABLE `traffic_team`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_devices`
--
ALTER TABLE `user_devices`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_role`
--
ALTER TABLE `user_role`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `accountmanager`
--
ALTER TABLE `accountmanager`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `advertisers`
--
ALTER TABLE `advertisers`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=542;
--
-- AUTO_INCREMENT de la tabla `affiliates`
--
ALTER TABLE `affiliates`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;
--
-- AUTO_INCREMENT de la tabla `costes`
--
ALTER TABLE `costes`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=323;
--
-- AUTO_INCREMENT de la tabla `doc_number`
--
ALTER TABLE `doc_number`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `incomes`
--
ALTER TABLE `incomes`
  MODIFY `id` int(4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1114;
--
-- AUTO_INCREMENT de la tabla `permission`
--
ALTER TABLE `permission`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `reason_scrubs`
--
ALTER TABLE `reason_scrubs`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `scrubs`
--
ALTER TABLE `scrubs`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT de la tabla `status`
--
ALTER TABLE `status`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `status_adv`
--
ALTER TABLE `status_adv`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `status_aff`
--
ALTER TABLE `status_aff`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `traffic_team`
--
ALTER TABLE `traffic_team`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `user_devices`
--
ALTER TABLE `user_devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `role_permission`
--
ALTER TABLE `role_permission`
  ADD CONSTRAINT `role_permission_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`),
  ADD CONSTRAINT `role_permission_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`permission_id`);

--
-- Filtros para la tabla `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`);






  