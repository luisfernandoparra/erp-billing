<?php
/**
 * For Development Purposes
 */
ini_set("display_errors", "on");

require __DIR__ . "/src/LS.php";
$LS = new \Fr\LS(array(
  "db" => array(
    "host" => "localhost",
    "port" => 3306,
    "username" => "netsaleserp",  
    "password" => "kFgqHKXuTyUOdOst6rwX",
    // "username" => "root",  
    // "password" => "root",
    "name" => "netsaleserp",
    "table" => "users"
  ),
   "features" => array(
    "auto_init" => true 
  ),
  "pages" => array(
    "no_login" => array(
      "/netsales/erp/1.0.0/",
      "/netsales/erp/1.0.0/reset.php",
      "/netsales/erp/1.0.0/register.php"
    ),
    "everyone" => array(
      "/netsales/erp/1.0.0/status.php"
    ),
    "login_page" => "/netsales/erp/1.0.0/login.php",
    "home_page" => "/netsales/erp/1.0.0/index.php"
  )
));